"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedirectDeepLinkController = void 0;
const common_1 = require("@nestjs/common");
let RedirectDeepLinkController = class RedirectDeepLinkController {
    constructor() { }
    verifyEmail(id, res) {
        const link = `uget://auth/verification/email/${id}`;
        return res.redirect(link);
    }
    resetPassword(id, res) {
        const link = `uget://auth/change/password/${id}`;
        return res.redirect(link);
    }
};
__decorate([
    (0, common_1.Get)('resend/email/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", void 0)
], RedirectDeepLinkController.prototype, "verifyEmail", null);
__decorate([
    (0, common_1.Get)('reset-password/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", void 0)
], RedirectDeepLinkController.prototype, "resetPassword", null);
RedirectDeepLinkController = __decorate([
    (0, common_1.Controller)('redirect'),
    __metadata("design:paramtypes", [])
], RedirectDeepLinkController);
exports.RedirectDeepLinkController = RedirectDeepLinkController;
//# sourceMappingURL=redirect.controller.js.map