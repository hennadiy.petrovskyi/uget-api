export declare class RedirectDeepLinkController {
    constructor();
    verifyEmail(id: string, res: any): any;
    resetPassword(id: string, res: any): any;
}
