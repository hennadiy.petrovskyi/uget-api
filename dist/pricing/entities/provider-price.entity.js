"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProviderPrice = void 0;
const weight_enum_1 = require("../enums/weight.enum");
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../../common/entities");
const enums_1 = require("../enums");
const enums_2 = require("../../common/enums");
let ProviderPrice = class ProviderPrice extends entities_1.BaseEntity {
};
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.DeliveryProvider }),
    __metadata("design:type", String)
], ProviderPrice.prototype, "provider", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_2.CountryCode }),
    __metadata("design:type", String)
], ProviderPrice.prototype, "from", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_2.CountryCode, default: enums_2.CountryCode.UAE }),
    __metadata("design:type", String)
], ProviderPrice.prototype, "to", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.WeightUnit, default: enums_1.WeightUnit.KG }),
    __metadata("design:type", String)
], ProviderPrice.prototype, "weightUnit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.DimensionUnits, default: enums_1.DimensionUnits.CM }),
    __metadata("design:type", String)
], ProviderPrice.prototype, "dimensionUnit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.Currency, default: enums_1.Currency.USD }),
    __metadata("design:type", String)
], ProviderPrice.prototype, "currency", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: weight_enum_1.WeightOptions }),
    __metadata("design:type", String)
], ProviderPrice.prototype, "weight", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('int', { nullable: true }),
    __metadata("design:type", Number)
], ProviderPrice.prototype, "height", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('int', { nullable: true }),
    __metadata("design:type", Number)
], ProviderPrice.prototype, "length", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('int', { nullable: true }),
    __metadata("design:type", Number)
], ProviderPrice.prototype, "width", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('text', { nullable: true }),
    __metadata("design:type", String)
], ProviderPrice.prototype, "price", void 0);
ProviderPrice = __decorate([
    (0, typeorm_1.Entity)({
        name: 'provider_price',
    })
], ProviderPrice);
exports.ProviderPrice = ProviderPrice;
//# sourceMappingURL=provider-price.entity.js.map