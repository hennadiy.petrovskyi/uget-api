import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from 'src/auth/entities';
export declare class Discount extends BaseEntity {
    owner: AuthRecord;
    uuid: string;
    percentage: number;
    amount: number;
    isValid: boolean;
    expireIn: string;
}
