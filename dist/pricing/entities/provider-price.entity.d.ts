import { WeightOptions } from '../enums/weight.enum';
import { BaseEntity } from 'src/common/entities';
import { Currency, DeliveryProvider, DimensionUnits, WeightUnit } from '../enums';
import { CountryCode } from 'src/common/enums';
export declare class ProviderPrice extends BaseEntity {
    provider: DeliveryProvider;
    from: CountryCode;
    to: CountryCode;
    weightUnit: WeightUnit;
    dimensionUnit: DimensionUnits;
    currency: Currency;
    weight: WeightOptions;
    height: number;
    length: number;
    width: number;
    price: string;
}
