import { MiddlewareConsumer } from '@nestjs/common';
export declare class PriceModule {
    configure(consumer: MiddlewareConsumer): void;
}
