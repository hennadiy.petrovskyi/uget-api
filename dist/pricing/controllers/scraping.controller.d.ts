import { ScrapingService } from '../services';
export declare class ScrapingController {
    private readonly service;
    constructor(service: ScrapingService);
    getAll(): Promise<void>;
}
