"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PricingController = void 0;
const currency_enum_1 = require("./../enums/currency.enum");
const pricing_emitter_1 = require("./../emitters/pricing.emitter");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const services_1 = require("../services");
const enums_1 = require("../../common/enums");
let PricingController = class PricingController {
    constructor(service, pricingEmitter) {
        this.service = service;
        this.pricingEmitter = pricingEmitter;
    }
    async saveProviderDeliveryPrice(saveProviderPriceDto) {
        return await this.service.saveProviderPriceForTrip(saveProviderPriceDto);
    }
    async calculatePrice(saveProviderPriceDto) {
        const fromCountryCode = await this.pricingEmitter.getCountryCodeById(saveProviderPriceDto.from);
        const providerPrices = await this.service.findPricingByDestinationsAndWeight({
            from: fromCountryCode.toUpperCase(),
            to: enums_1.CountryCode.UAE,
            weight: saveProviderPriceDto.weight,
        });
        const averagePrice = providerPrices
            .map((el) => +el.price)
            .reduce((prev, current) => prev + current) / providerPrices.length;
        const routePriceModifier = 20;
        const total = averagePrice - (averagePrice / 100) * routePriceModifier;
        return { value: total.toFixed(2), currency: currency_enum_1.Currency.EUR };
    }
    async getAllPricing() {
        const data = await this.service.getAllPricing();
        return data.map((el) => {
            const { id, createdAt, updatedAt, deletedAt, weight } = el, rest = __rest(el, ["id", "createdAt", "updatedAt", "deletedAt", "weight"]);
            return Object.assign(Object.assign({}, rest), { weight: String(weight) });
        });
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)('')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PricingController.prototype, "saveProviderDeliveryPrice", null);
__decorate([
    (0, common_1.Post)('calculate'),
    __param(0, (0, common_1.Body)('')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PricingController.prototype, "calculatePrice", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PricingController.prototype, "getAllPricing", null);
PricingController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('Price'),
    (0, common_1.Controller)('pricing'),
    __metadata("design:paramtypes", [services_1.PricingService,
        pricing_emitter_1.PricingEmitter])
], PricingController);
exports.PricingController = PricingController;
//# sourceMappingURL=pricing.controller.js.map