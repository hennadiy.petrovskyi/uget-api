import { Currency } from './../enums/currency.enum';
import { PricingEmitter } from './../emitters/pricing.emitter';
import { PricingService } from '../services';
import { CountryCode } from 'src/common/enums';
export declare class PricingController {
    private readonly service;
    private readonly pricingEmitter;
    constructor(service: PricingService, pricingEmitter: PricingEmitter);
    saveProviderDeliveryPrice(saveProviderPriceDto: any): Promise<void>;
    calculatePrice(saveProviderPriceDto: any): Promise<{
        value: string;
        currency: Currency;
    }>;
    getAllPricing(): Promise<{
        weight: string;
        provider: import("../enums").DeliveryProvider;
        from: CountryCode;
        to: CountryCode;
        weightUnit: import("../enums").WeightUnit;
        dimensionUnit: import("../enums").DimensionUnits;
        currency: Currency;
        height: number;
        length: number;
        width: number;
        price: string;
    }[]>;
}
