"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PricingController = exports.ScrapingController = void 0;
var scraping_controller_1 = require("./scraping.controller");
Object.defineProperty(exports, "ScrapingController", { enumerable: true, get: function () { return scraping_controller_1.ScrapingController; } });
var pricing_controller_1 = require("./pricing.controller");
Object.defineProperty(exports, "PricingController", { enumerable: true, get: function () { return pricing_controller_1.PricingController; } });
//# sourceMappingURL=index.js.map