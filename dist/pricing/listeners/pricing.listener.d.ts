import { Currency } from './../enums/currency.enum';
import { PricingService } from '../services';
import { PricingEmitter } from '../emitters';
export declare class PricingListener {
    private readonly pricingService;
    private readonly pricingEmitter;
    constructor(pricingService: PricingService, pricingEmitter: PricingEmitter);
    calculateAveragePrice(calculatePriceDto: {
        from: string;
        to: string;
        size: any;
    }): Promise<{
        average: number;
        currency: Currency;
        routePriceModifier: number;
        total: string;
    }>;
}
