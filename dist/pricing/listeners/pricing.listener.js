"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PricingListener = void 0;
const currency_enum_1 = require("./../enums/currency.enum");
const events_constants_1 = require("./../../common/events/events-constants");
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const services_1 = require("../services");
const emitters_1 = require("../emitters");
let PricingListener = class PricingListener {
    constructor(pricingService, pricingEmitter) {
        this.pricingService = pricingService;
        this.pricingEmitter = pricingEmitter;
    }
    async calculateAveragePrice(calculatePriceDto) {
        const from = await this.pricingEmitter.getCountryCodeById(calculatePriceDto.from);
        const to = await this.pricingEmitter.getCountryCodeById(calculatePriceDto.to);
        const providerPrices = await this.pricingService.findPricingByDestinationsAndWeight({
            from,
            to,
            weight: calculatePriceDto.size,
        });
        const averagePrice = providerPrices
            .map((el) => +el.price)
            .reduce((prev, current) => prev + current) / providerPrices.length;
        const routePriceModifier = 20;
        const total = averagePrice - (averagePrice / 100) * routePriceModifier;
        return {
            average: averagePrice,
            currency: currency_enum_1.Currency.EUR,
            routePriceModifier,
            total: total.toFixed(2),
        };
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PRICING_EVENTS.CALCULATE_ORIGINAL_AVERAGE_PRICE, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PricingListener.prototype, "calculateAveragePrice", null);
PricingListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.PricingService,
        emitters_1.PricingEmitter])
], PricingListener);
exports.PricingListener = PricingListener;
//# sourceMappingURL=pricing.listener.js.map