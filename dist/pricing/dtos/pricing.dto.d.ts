import { ProviderPrice } from '../entities';
declare const FindPricingByDestinationsAmdWeightDto_base: import("@nestjs/mapped-types").MappedType<Pick<ProviderPrice, "to" | "from" | "weight">>;
export declare class FindPricingByDestinationsAmdWeightDto extends FindPricingByDestinationsAmdWeightDto_base {
    static buildDto(payload: ProviderPrice): FindPricingByDestinationsAmdWeightDto;
}
declare const SaveProviderPrice_base: import("@nestjs/mapped-types").MappedType<Omit<ProviderPrice, "id" | "createdAt" | "updatedAt" | "deletedAt">>;
export declare class SaveProviderPrice extends SaveProviderPrice_base {
}
export {};
