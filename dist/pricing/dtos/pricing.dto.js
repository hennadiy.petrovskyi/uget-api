"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveProviderPrice = exports.FindPricingByDestinationsAmdWeightDto = void 0;
const class_transformer_1 = require("class-transformer");
const mapped_types_1 = require("@nestjs/mapped-types");
const entities_1 = require("../entities");
class FindPricingByDestinationsAmdWeightDto extends (0, mapped_types_1.PickType)(entities_1.ProviderPrice, ['from', 'to', 'weight']) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(FindPricingByDestinationsAmdWeightDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.FindPricingByDestinationsAmdWeightDto = FindPricingByDestinationsAmdWeightDto;
class SaveProviderPrice extends (0, mapped_types_1.OmitType)(entities_1.ProviderPrice, [
    'createdAt',
    'deletedAt',
    'updatedAt',
    'id',
]) {
}
exports.SaveProviderPrice = SaveProviderPrice;
//# sourceMappingURL=pricing.dto.js.map