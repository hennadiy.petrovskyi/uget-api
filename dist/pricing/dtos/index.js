"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveProviderPrice = exports.FindPricingByDestinationsAmdWeightDto = void 0;
var pricing_dto_1 = require("./pricing.dto");
Object.defineProperty(exports, "FindPricingByDestinationsAmdWeightDto", { enumerable: true, get: function () { return pricing_dto_1.FindPricingByDestinationsAmdWeightDto; } });
Object.defineProperty(exports, "SaveProviderPrice", { enumerable: true, get: function () { return pricing_dto_1.SaveProviderPrice; } });
//# sourceMappingURL=index.js.map