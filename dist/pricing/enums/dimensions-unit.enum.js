"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DimensionUnits = void 0;
var DimensionUnits;
(function (DimensionUnits) {
    DimensionUnits["CM"] = "cm";
    DimensionUnits["IN"] = "in";
})(DimensionUnits = exports.DimensionUnits || (exports.DimensionUnits = {}));
//# sourceMappingURL=dimensions-unit.enum.js.map