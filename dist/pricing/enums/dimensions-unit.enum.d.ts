export declare enum DimensionUnits {
    CM = "cm",
    IN = "in"
}
