"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryProvider = void 0;
var DeliveryProvider;
(function (DeliveryProvider) {
    DeliveryProvider["ARAMEX"] = "ARX";
    DeliveryProvider["TIME_EXPRESS"] = "TES";
    DeliveryProvider["DHL"] = "DHL";
})(DeliveryProvider = exports.DeliveryProvider || (exports.DeliveryProvider = {}));
//# sourceMappingURL=providers.enum.js.map