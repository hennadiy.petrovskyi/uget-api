"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Currency = void 0;
var Currency;
(function (Currency) {
    Currency["USD"] = "USD";
    Currency["AED"] = "AED";
    Currency["EUR"] = "EUR";
})(Currency = exports.Currency || (exports.Currency = {}));
//# sourceMappingURL=currency.enum.js.map