"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WeightOptions = void 0;
var WeightOptions;
(function (WeightOptions) {
    WeightOptions["A"] = "0.5";
    WeightOptions["B"] = "1";
    WeightOptions["C"] = "1.5";
    WeightOptions["D"] = "2";
    WeightOptions["E"] = "2.5";
    WeightOptions["F"] = "3";
    WeightOptions["G"] = "3.5";
    WeightOptions["H"] = "4";
    WeightOptions["I"] = "4.5";
    WeightOptions["J"] = "5";
    WeightOptions["K"] = "5.5";
    WeightOptions["L"] = "6";
})(WeightOptions = exports.WeightOptions || (exports.WeightOptions = {}));
//# sourceMappingURL=weight.enum.js.map