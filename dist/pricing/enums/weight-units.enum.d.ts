export declare enum WeightUnit {
    KG = "kg",
    LB = "lb"
}
