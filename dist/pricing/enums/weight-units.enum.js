"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WeightUnit = void 0;
var WeightUnit;
(function (WeightUnit) {
    WeightUnit["KG"] = "kg";
    WeightUnit["LB"] = "lb";
})(WeightUnit = exports.WeightUnit || (exports.WeightUnit = {}));
//# sourceMappingURL=weight-units.enum.js.map