"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PricingService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
const logger_1 = require("../../utils/logger");
const entities_1 = require("../entities");
const enums_1 = require("../enums");
let PricingService = class PricingService {
    constructor(pricingRepository, logger) {
        this.pricingRepository = pricingRepository;
        this.logger = logger;
    }
    async findPricingByDestinationsAndWeight(findPricingByDestinationsDto) {
        const functionName = this.findPricingByDestinationsAndWeight.name;
        try {
            return this.pricingRepository.find({
                where: {
                    from: findPricingByDestinationsDto.from,
                    to: findPricingByDestinationsDto.to,
                    weight: findPricingByDestinationsDto.weight,
                },
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findPricingById(id) {
        const functionName = this.findPricingById.name;
        try {
            return this.pricingRepository.findOne({
                where: {
                    id,
                },
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async getAllPricing() {
        const functionName = this.findPricingById.name;
        try {
            return this.pricingRepository.find({
                where: { provider: enums_1.DeliveryProvider.TIME_EXPRESS },
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async saveProviderPriceForTrip(saveProviderPrice) {
        const functionName = this.saveProviderPriceForTrip.name;
        try {
            await this.pricingRepository.save(saveProviderPrice);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
};
PricingService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(entities_1.ProviderPrice)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        logger_1.LoggerService])
], PricingService);
exports.PricingService = PricingService;
//# sourceMappingURL=pricing.service.js.map