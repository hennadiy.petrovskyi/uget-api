import { Repository } from 'typeorm';
import { LoggerService } from 'src/utils/logger';
import { ProviderPrice } from '../entities';
import { FindPricingByDestinationsAmdWeightDto, SaveProviderPrice } from '../dtos';
export declare class PricingService {
    private pricingRepository;
    private logger;
    constructor(pricingRepository: Repository<ProviderPrice>, logger: LoggerService);
    findPricingByDestinationsAndWeight(findPricingByDestinationsDto: FindPricingByDestinationsAmdWeightDto): Promise<ProviderPrice[]>;
    findPricingById(id: number): Promise<ProviderPrice>;
    getAllPricing(): Promise<ProviderPrice[]>;
    saveProviderPriceForTrip(saveProviderPrice: SaveProviderPrice): Promise<void>;
}
