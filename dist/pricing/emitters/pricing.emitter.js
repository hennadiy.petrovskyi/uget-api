"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PricingEmitter = void 0;
const events_constants_1 = require("../../common/events/events-constants");
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const enums_1 = require("../../file/enums");
let PricingEmitter = class PricingEmitter {
    constructor(eventEmitter) {
        this.eventEmitter = eventEmitter;
    }
    async getLocationDetailsById(owner) {
        const file = await this.eventEmitter.emitAsync(events_constants_1.LOCATION_EVENT.GET_LOCATION_DETAILS_BY_ID, { owner, type: enums_1.FileType.PROFILE_IMAGE });
        if (file && file.length) {
            return file[0];
        }
        return null;
    }
    async getCountryCodeById(id) {
        const file = await this.eventEmitter.emitAsync(events_constants_1.LOCATION_EVENT.GET_COUNTRY_CODE_BY_LOCATION_ID, id);
        if (file && file.length) {
            return file[0];
        }
        return null;
    }
};
PricingEmitter = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [event_emitter_1.EventEmitter2])
], PricingEmitter);
exports.PricingEmitter = PricingEmitter;
//# sourceMappingURL=pricing.emitter.js.map