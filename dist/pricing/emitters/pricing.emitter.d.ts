import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class PricingEmitter {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    getLocationDetailsById(owner: string): Promise<any>;
    getCountryCodeById(id: string): Promise<any>;
}
