"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationModule = void 0;
const logger_1 = require("../utils/logger");
const common_1 = require("@nestjs/common");
const cache_module_1 = require("../common/modules/cache.module");
const services_1 = require("./services");
const listeners_1 = require("./listeners");
const controllers_1 = require("./controllers");
const typeorm_1 = require("@nestjs/typeorm");
const entities_1 = require("./entities");
const morgan_middleware_1 = require("../common/middlewares/morgan.middleware");
const emitters_1 = require("./emitters");
const notification_settings_service_1 = require("./services/notification-settings.service");
let NotificationModule = class NotificationModule {
    configure(consumer) {
        consumer.apply(morgan_middleware_1.MorganMiddleware).forRoutes(controllers_1.PushNotificationController);
    }
};
NotificationModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([entities_1.Token, entities_1.UserNotificationSettings]),
            cache_module_1.CacheRedisModule,
            logger_1.LoggerModule,
        ],
        controllers: [
            controllers_1.RecaptchaController,
            controllers_1.PushNotificationController,
            controllers_1.NotificationsSettingsController,
            controllers_1.EmailController,
        ],
        providers: [
            services_1.SmsService,
            listeners_1.SmsListener,
            notification_settings_service_1.NotificationsSettingsService,
            services_1.EmailService,
            emitters_1.PushNotificationsEmitter,
            listeners_1.PushNotificationListener,
            listeners_1.NotificationsSettingsListener,
            listeners_1.EmailListener,
        ],
        exports: [],
    })
], NotificationModule);
exports.NotificationModule = NotificationModule;
//# sourceMappingURL=notification.module.js.map