import { LoggerService } from 'src/utils/logger';
import { EmailType } from '../enums';
export declare class EmailService {
    private logger;
    constructor(logger: LoggerService);
    sendEmail(sendEmailDto: {
        email: string;
        link: string;
        type: EmailType;
        title: string;
    }): Promise<any>;
}
