"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmsService = void 0;
const common_1 = require("@nestjs/common");
const client_sns_1 = require("@aws-sdk/client-sns");
const crypto_1 = require("crypto");
const cache_service_1 = require("../../common/services/cache.service");
const logger_1 = require("../../utils/logger");
const config_1 = require("@nestjs/config");
let SmsService = class SmsService {
    constructor(cacheService, configService, logger) {
        this.cacheService = cacheService;
        this.configService = configService;
        this.logger = logger;
        this.sns = new client_sns_1.SNSClient({
            region: this.configService.get('REGION'),
            credentials: {
                accessKeyId: this.configService.get('ACCESS_KEY_ID'),
                secretAccessKey: this.configService.get('SECRET_ACCESS_KEY'),
            },
        });
    }
    generateConfirmationCode(isTest) {
        if (!isTest) {
            const chars = '0123456789';
            const sequenceLength = 6;
            let sequence = '';
            const rnd = (0, crypto_1.randomBytes)(sequenceLength);
            const d = 256 / chars.length;
            for (let i = 0; i < sequenceLength; i++) {
                sequence += chars[Math.floor(rnd[i] / d)];
            }
            return sequence;
        }
        return '111111';
    }
    async sendSMS(smsOptions) {
        return this.sns
            .publish(smsOptions)
            .promise()
            .then((info) => {
            this.logger.log(`[SMS] success: ${JSON.stringify(info)}`);
            return [
                {
                    statusCode: common_1.HttpStatus.OK,
                    message: 'Sms sent',
                    data: info,
                },
            ];
        })
            .catch((err) => {
            this.logger.error('[SMS] failed:', err);
            throw new common_1.HttpException({
                statusCode: common_1.HttpStatus.BAD_REQUEST,
                message: 'Failed to send',
                data: err,
            }, common_1.HttpStatus.BAD_REQUEST);
        });
    }
    async sendConfirmationCode(phoneNumber, code, onSuccess, isTesting) {
        const smsParams = {
            Message: `UGET code: ${code}`,
            PhoneNumber: phoneNumber,
        };
        if (!isTesting) {
            try {
                const result = await this.sendSMS(smsParams);
                if (result.length && result[0].statusCode === 200) {
                    onSuccess();
                }
            }
            catch (error) {
                console.log(error);
            }
            return;
        }
        onSuccess();
        console.log(smsParams);
    }
    async verifyCode(authVerifyCodeDto) {
        return await this.cacheService.get(authVerifyCodeDto.phoneNumber);
    }
};
SmsService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [cache_service_1.CacheService,
        config_1.ConfigService,
        logger_1.LoggerService])
], SmsService);
exports.SmsService = SmsService;
//# sourceMappingURL=sms.service.js.map