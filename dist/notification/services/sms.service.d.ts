import { CacheService } from 'src/common/services/cache.service';
import { AuthVerifyCodeDto } from 'src/auth/dtos';
import { LoggerService } from 'src/utils/logger';
import { ConfigService } from '@nestjs/config';
export declare class SmsService {
    private cacheService;
    private configService;
    private logger;
    private readonly sns;
    constructor(cacheService: CacheService, configService: ConfigService, logger: LoggerService);
    generateConfirmationCode(isTest?: boolean): string;
    sendSMS(smsOptions: any): Promise<any>;
    sendConfirmationCode(phoneNumber: string, code: string, onSuccess: () => void, isTesting?: boolean): Promise<void>;
    verifyCode(authVerifyCodeDto: AuthVerifyCodeDto): Promise<unknown>;
}
