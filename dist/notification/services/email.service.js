"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailService = void 0;
const nodemailer = require("nodemailer");
const common_1 = require("@nestjs/common");
const logger_1 = require("../../utils/logger");
const enums_1 = require("../enums");
const verify_email_1 = require("../templates/verify-email");
const reset_password_1 = require("../templates/reset-password");
let EmailService = class EmailService {
    constructor(logger) {
        this.logger = logger;
    }
    async sendEmail(sendEmailDto) {
        const credentials = {
            name: 'ses-smtp-user.20220304-151424',
            accessKeyId: 'AKIA22H73NCMNKGCA44L',
            secretAccessKey: 'BPeOHt9gIFvXnvf1J3vo6SEcX1k7U8DmgiM+aG0IyhBK',
        };
        const params = {
            from: 'no-reply@api.uget.co',
            port: 465,
            host: 'email-smtp.eu-central-1.amazonaws.com',
        };
        const mailerClient = nodemailer.createTransport({
            host: params.host,
            port: params.port,
            auth: {
                user: credentials.accessKeyId,
                pass: credentials.secretAccessKey,
            },
        });
        console.log('sendEmailDto.link', sendEmailDto.link);
        return mailerClient.sendMail({
            from: params.from,
            to: sendEmailDto.email,
            subject: sendEmailDto.title,
            text: sendEmailDto.title,
            html: sendEmailDto.type === enums_1.EmailType.VERIFY_EMAIL
                ? (0, verify_email_1.verifyEmailTemplate)(sendEmailDto.link)
                : (0, reset_password_1.resetPasswordTemplate)(sendEmailDto.link),
        });
    }
};
EmailService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [logger_1.LoggerService])
], EmailService);
exports.EmailService = EmailService;
//# sourceMappingURL=email.service.js.map