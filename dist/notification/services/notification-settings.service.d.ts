import { UserNotificationSettings } from '../entities';
import { FindOperator, Repository } from 'typeorm';
import { LoggerService } from 'src/utils/logger';
export declare class NotificationsSettingsService {
    private userNotificationRepository;
    private logger;
    constructor(userNotificationRepository: Repository<UserNotificationSettings>, logger: LoggerService);
    createUserSettings(createUserSettings: any): Promise<void>;
    getUserSettings(owner: FindOperator<any>): Promise<UserNotificationSettings>;
    updateUserSettings(id: any, update: any): Promise<import("typeorm").UpdateResult>;
}
