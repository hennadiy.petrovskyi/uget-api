import { MiddlewareConsumer } from '@nestjs/common';
export declare class NotificationModule {
    configure(consumer: MiddlewareConsumer): void;
}
