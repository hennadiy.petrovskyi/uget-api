"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTokenDto = void 0;
const entities_1 = require("../entities");
const swagger_1 = require("@nestjs/swagger");
class CreateTokenDto extends (0, swagger_1.PickType)(entities_1.Token, ['deviceToken', 'type']) {
}
exports.CreateTokenDto = CreateTokenDto;
//# sourceMappingURL=token.dto.js.map