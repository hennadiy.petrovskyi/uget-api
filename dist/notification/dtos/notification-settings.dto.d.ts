import { UserNotificationSettings } from '../entities';
declare const SettingsDto_base: import("@nestjs/common").Type<Pick<UserNotificationSettings, "email" | "push">>;
export declare class SettingsDto extends SettingsDto_base {
    static buildDto(payload: any): SettingsDto;
}
declare const UpdateUserSettingsDto_base: import("@nestjs/common").Type<Partial<SettingsDto>>;
export declare class UpdateUserSettingsDto extends UpdateUserSettingsDto_base {
}
export {};
