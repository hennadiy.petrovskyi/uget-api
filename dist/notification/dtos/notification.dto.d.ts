export declare class SendNotificationDto {
    userId: number;
    message: string;
}
export declare class RefreshDeviceTokenDto {
    oldToken: string;
    newToken: string;
}
export declare class PushNotificationMessageDto {
    title: string;
    body: string;
}
export declare class SendPushNotificationDto {
    userUUID: string;
    message: PushNotificationMessageDto;
    link?: string;
}
