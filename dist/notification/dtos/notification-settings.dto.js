"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateUserSettingsDto = exports.SettingsDto = void 0;
const entities_1 = require("../entities");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
class SettingsDto extends (0, swagger_1.PickType)(entities_1.UserNotificationSettings, [
    'email',
    'push',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(SettingsDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.SettingsDto = SettingsDto;
class UpdateUserSettingsDto extends (0, swagger_1.PartialType)(SettingsDto) {
}
exports.UpdateUserSettingsDto = UpdateUserSettingsDto;
//# sourceMappingURL=notification-settings.dto.js.map