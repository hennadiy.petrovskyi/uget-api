import { Token } from '../entities';
declare const CreateTokenDto_base: import("@nestjs/common").Type<Pick<Token, "type" | "deviceToken">>;
export declare class CreateTokenDto extends CreateTokenDto_base {
}
export {};
