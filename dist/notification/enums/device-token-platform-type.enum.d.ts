export declare enum DeviceTokenPlatformType {
    IOS = "APN",
    ANDROID = "GCM"
}
