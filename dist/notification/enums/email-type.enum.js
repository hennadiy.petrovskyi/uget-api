"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailType = void 0;
var EmailType;
(function (EmailType) {
    EmailType["RESET_PASSWORD"] = "password.reset";
    EmailType["VERIFY_EMAIL"] = "email.verify";
})(EmailType = exports.EmailType || (exports.EmailType = {}));
//# sourceMappingURL=email-type.enum.js.map