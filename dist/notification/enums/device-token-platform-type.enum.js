"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeviceTokenPlatformType = void 0;
var DeviceTokenPlatformType;
(function (DeviceTokenPlatformType) {
    DeviceTokenPlatformType["IOS"] = "APN";
    DeviceTokenPlatformType["ANDROID"] = "GCM";
})(DeviceTokenPlatformType = exports.DeviceTokenPlatformType || (exports.DeviceTokenPlatformType = {}));
//# sourceMappingURL=device-token-platform-type.enum.js.map