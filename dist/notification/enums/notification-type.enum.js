"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationType = void 0;
var NotificationType;
(function (NotificationType) {
    NotificationType["MY_ORDERS"] = "order.my";
    NotificationType["ORDER_RELATED_TRIPS"] = "order.related.trips";
    NotificationType["ORDERS_SEARCH_TRIPS"] = "order.search.trips";
    NotificationType["MY_TRIPS"] = "trip.my";
    NotificationType["TRIP_RELATED_ORDERS"] = "trip.related.orders";
    NotificationType["TRIP_SEARCH_ORDERS"] = "trip.search.orders";
})(NotificationType = exports.NotificationType || (exports.NotificationType = {}));
//# sourceMappingURL=notification-type.enum.js.map