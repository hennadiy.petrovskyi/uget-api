"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyEmailTemplate = void 0;
const verifyEmailTemplate = (link) => {
    return `<html lang="en">
    <head>
      <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
      <link
        href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@500;700&display=swap"
        rel="stylesheet"
      />
      <style>
        body {
          width: 100%;
          background-color: #e5e5e5;
        }
  
        p,
        h1 {
          margin: 0;
          padding: 0;
        }
  
        .logo {
          width: 120px;
          height: auto;
          margin: 0 auto;
          display: block;
          margin-bottom: 60px;
        }
  
        .container {
          max-width: 800px;
          margin: 0 auto;
          background-color: white;
          background-image: url("https://drive.google.com/file/d/1WQLh5fap3KwDZI_38KY2UvQRT4Awyhdg/view?usp=sharing");
          background-position: center;
          background-size: cover;
          padding: 50px 0;
        }
  
        .title {
          font-weight: bold;
          font-size: 28px;
          line-height: 41px;
          margin-bottom: 30px;
        }
  

  
        .copyright {
          font-weight: 500;
          font-size: 20px;
          font-family: Noto Sans KR;
          color: #0c1221;
        }

        a {
          -webkit-appearance: button;
            -moz-appearance: button;
            appearance: button; 
            background-color: #018080;
            padding: 15px 0;
            color: white;
            font-weight: bold;
            font-size: 16px;
            line-height: 20px;
            text-align: center;
            letter-spacing: -0.24px;
            border-radius: 62px;
            border: 0;
            cursor: pointer;
            font-family: Noto Sans KR;
            display: block;
            margin: 0 auto;
            max-width: 300px;
            margin-bottom: 100px;
            text-decoration: none;
        }
      </style>
      <title>Reset your password!</title>
    </head>
    <body
      style="
        width: 100%;
        background-color: #e5e5e5;
        font-family: Noto Sans KR;
        color: #0c1221;
        text-align: center;
      "
    >
      <div
        class="container"
        style="
          max-width: 800px;
          margin: 0 auto;
          background-color: white;
          padding: 50px 0;
        "
      >

  
        <h1
          class="title"
          style="
            font-family: Noto Sans KR;
            font-style: normal;
            font-weight: bold;
            font-size: 28px;
            line-height: 41px;
            color: #0c1221;
            margin-bottom: 30px;
          "
        >
          Email verification 
        </h1>
  
        <p
          style="
            font-weight: normal;
            font-size: 20px;
            line-height: 32px;
            text-align: center;
            max-width: 524px;
            margin: 0 auto;
            margin-bottom: 50px;
            font-family: Noto Sans KR;
            color: #0c1221;
          "
        >
         We need to verify your email address  ! To proceed click button bellow :)
        </p>
  
        <a
        href="${link}"
          style="
            -webkit-appearance: button;
            -moz-appearance: button;
            appearance: button; 
            background-color: #018080;
            padding: 15px 0;
            color: white;
            font-weight: bold;
            font-size: 16px;
            line-height: 20px;
            text-align: center;
            letter-spacing: -0.24px;
            border-radius: 62px;
            border: 0;
            cursor: pointer;
            font-family: Noto Sans KR;
            display: block;
            margin: 0 auto;
            max-width: 300px;
            margin-bottom: 100px;
            text-decoration: none;
          "
        >
         Verify
        </a>
  
        <p
          class="copyright"
          style="
            font-weight: 500;
            font-size: 20px;
            font-family: Noto Sans KR;
            color: #0c1221;
            text-align: center;
          "
        >
          Copyright ${new Date().getFullYear()}. All rights reserved
        </p>
      </div>
    </body>
  </html>
  `;
};
exports.verifyEmailTemplate = verifyEmailTemplate;
//# sourceMappingURL=verify-email.js.map