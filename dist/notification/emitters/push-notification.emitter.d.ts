import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class PushNotificationsEmitter {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    getAuthRecordByUUID(uuid: string): Promise<any>;
    getAuthRecordByID(id: number): Promise<any>;
    getUserDetailsById(owner: number): Promise<any>;
    getUserDetails(uuid: string): Promise<any>;
}
