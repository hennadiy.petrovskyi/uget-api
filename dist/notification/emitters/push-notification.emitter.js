"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PushNotificationsEmitter = void 0;
const events_1 = require("../../common/events");
const events_constants_1 = require("../../common/events/events-constants");
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
let PushNotificationsEmitter = class PushNotificationsEmitter {
    constructor(eventEmitter) {
        this.eventEmitter = eventEmitter;
    }
    async getAuthRecordByUUID(uuid) {
        const details = await this.eventEmitter.emitAsync(events_1.AUTH_EVENTS.GET_USER_BY_UUID, uuid);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async getAuthRecordByID(id) {
        const details = await this.eventEmitter.emitAsync(events_1.AUTH_EVENTS.GET_USER_BY_ID, id);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async getUserDetailsById(owner) {
        const details = await this.eventEmitter.emitAsync(events_constants_1.USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID, owner);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async getUserDetails(uuid) {
        const authRecord = await this.getAuthRecordByUUID(uuid);
        return this.getUserDetailsById(authRecord.id);
    }
};
PushNotificationsEmitter = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [event_emitter_1.EventEmitter2])
], PushNotificationsEmitter);
exports.PushNotificationsEmitter = PushNotificationsEmitter;
//# sourceMappingURL=push-notification.emitter.js.map