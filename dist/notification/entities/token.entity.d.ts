import { DeviceTokenPlatformType } from '../enums';
import { BaseEntity } from 'src/common/entities';
export declare class Token extends BaseEntity {
    userUUID: string;
    deviceToken: string;
    type: DeviceTokenPlatformType;
}
