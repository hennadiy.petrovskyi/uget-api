"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Token = void 0;
const typeorm_1 = require("typeorm");
const enums_1 = require("../enums");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../../common/entities");
let Token = class Token extends entities_1.BaseEntity {
};
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('text', { nullable: true }),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], Token.prototype, "userUUID", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('text'),
    (0, typeorm_1.Index)(),
    __metadata("design:type", String)
], Token.prototype, "deviceToken", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.DeviceTokenPlatformType }),
    __metadata("design:type", String)
], Token.prototype, "type", void 0);
Token = __decorate([
    (0, typeorm_1.Entity)({
        name: 'device_token',
    })
], Token);
exports.Token = Token;
//# sourceMappingURL=token.entity.js.map