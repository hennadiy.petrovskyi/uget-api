import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from 'src/auth/entities';
export declare class UserNotificationSettings extends BaseEntity {
    owner: AuthRecord;
    email: boolean;
    push: boolean;
}
