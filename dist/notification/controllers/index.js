"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailController = exports.NotificationsSettingsController = exports.PushNotificationController = exports.RecaptchaController = void 0;
var recapcha_controller_1 = require("./recapcha.controller");
Object.defineProperty(exports, "RecaptchaController", { enumerable: true, get: function () { return recapcha_controller_1.RecaptchaController; } });
var push_notification_controller_1 = require("./push-notification.controller");
Object.defineProperty(exports, "PushNotificationController", { enumerable: true, get: function () { return push_notification_controller_1.PushNotificationController; } });
var notification_settings_controller_1 = require("./notification-settings.controller");
Object.defineProperty(exports, "NotificationsSettingsController", { enumerable: true, get: function () { return notification_settings_controller_1.NotificationsSettingsController; } });
var email_controller_1 = require("./email.controller");
Object.defineProperty(exports, "EmailController", { enumerable: true, get: function () { return email_controller_1.EmailController; } });
//# sourceMappingURL=index.js.map