import { CreateTokenDto, RefreshDeviceTokenDto } from '../dtos';
export declare class PushNotificationController {
    constructor();
    registerDeviceToken(request: any, createTokenDto: CreateTokenDto): Promise<void>;
    refreshDeviceToken(refreshDeviceTokenDto: RefreshDeviceTokenDto): any;
}
