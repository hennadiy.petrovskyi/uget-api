import { EmailService } from '../services';
export declare class EmailController {
    private readonly service;
    constructor(service: EmailService);
    registerDeviceToken(): Promise<void>;
}
