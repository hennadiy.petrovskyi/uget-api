import { PushNotificationsEmitter } from '../emitters';
import { NotificationsSettingsService } from '../services';
export declare class NotificationsSettingsController {
    private readonly service;
    private readonly emitter;
    constructor(service: NotificationsSettingsService, emitter: PushNotificationsEmitter);
    registerDeviceToken(user: any, updateSettingsDto: any): Promise<any>;
}
