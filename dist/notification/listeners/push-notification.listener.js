"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PushNotificationListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_constants_1 = require("../../common/events/events-constants");
const emitters_1 = require("../emitters");
let PushNotificationListener = class PushNotificationListener {
    constructor(pushNotificationsEmitter) {
        this.pushNotificationsEmitter = pushNotificationsEmitter;
    }
    async sendChatNotification(sendNotificationDto) {
        const { to, from, message } = sendNotificationDto;
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetails(from);
    }
    async sendOrderRequest(sendNotificationDto) {
        const { to, from } = sendNotificationDto;
        console.log(sendNotificationDto);
        const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(to);
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetailsById(from);
    }
    async sendTripRequest(sendNotificationDto) {
        const { to, from } = sendNotificationDto;
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetailsById(from);
        const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(to);
    }
    async sendOrderRequestAccepted(sendNotificationDto) {
        const { to, from } = sendNotificationDto;
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetailsById(from);
        const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(to);
    }
    async sendTripRequestAccepted(sendNotificationDto) {
        const { to, from } = sendNotificationDto;
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetailsById(from);
        const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(to);
    }
    async sendOrderPickUpVerification(sendNotificationDto) {
        const { to, from, orderUUID } = sendNotificationDto;
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetailsById(from);
        const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(to);
    }
    async sendOrderPickedUp(sendNotificationDto) {
        const { to, by, orderUUID } = sendNotificationDto;
        console.log(sendNotificationDto);
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetailsById(by);
        const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(to);
    }
    async sendOrderDeliveryVerification(sendNotificationDto) {
        const { to, from, orderUUID } = sendNotificationDto;
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetailsById(from);
        const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(to);
    }
    async sendOrderDelivered(sendNotificationDto) {
        const { to, by, orderUUID } = sendNotificationDto;
        const fromUserDetails = await this.pushNotificationsEmitter.getUserDetailsById(by);
        const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(to);
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.SEND_CHAT_NOTIFICATION, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendChatNotification", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.ORDER_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendOrderRequest", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.TRIP_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendTripRequest", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.ORDER_REQUEST_ACCEPTED, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendOrderRequestAccepted", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.TRIP_REQUEST_ACCEPTED, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendTripRequestAccepted", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP_VERIFICATION, {
        promisify: true,
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendOrderPickUpVerification", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendOrderPickedUp", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.DELIVERY_VERIFICATION, {
        promisify: true,
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendOrderDeliveryVerification", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.PUSH_NOTIFICATION_EVENTS.ORDER_DELIVERED, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PushNotificationListener.prototype, "sendOrderDelivered", null);
PushNotificationListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [emitters_1.PushNotificationsEmitter])
], PushNotificationListener);
exports.PushNotificationListener = PushNotificationListener;
//# sourceMappingURL=push-notification.listener.js.map