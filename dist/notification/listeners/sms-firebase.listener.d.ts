import { SmsService } from '../services/sms.service';
import { CacheService } from 'src/common/services/cache.service';
export declare class SmsFirebaseListener {
    private readonly smsService;
    private cacheService;
    constructor(smsService: SmsService, cacheService: CacheService);
    sendConfirmationCode(sendConfirmationCodeDto: {
        recaptcha: string;
        phoneNumber: string;
    }): Promise<void>;
    verifyCode(verifyCodeDto: {
        phoneNumber: string;
        code: string;
    }): Promise<{
        sessionInfo: string;
    }>;
    pickupRequest(pickupVerificationDto: any): Promise<void>;
    pickupVerification(pickupVerificationDto: any): Promise<boolean>;
    deliveryVerification(verifyCodeDto: any): Promise<void>;
}
