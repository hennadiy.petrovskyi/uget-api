import { PushNotificationsEmitter } from '../emitters';
export declare class PushNotificationListener {
    private readonly pushNotificationsEmitter;
    constructor(pushNotificationsEmitter: PushNotificationsEmitter);
    sendChatNotification(sendNotificationDto: any): Promise<void>;
    sendOrderRequest(sendNotificationDto: any): Promise<void>;
    sendTripRequest(sendNotificationDto: any): Promise<void>;
    sendOrderRequestAccepted(sendNotificationDto: any): Promise<void>;
    sendTripRequestAccepted(sendNotificationDto: any): Promise<void>;
    sendOrderPickUpVerification(sendNotificationDto: any): Promise<void>;
    sendOrderPickedUp(sendNotificationDto: any): Promise<void>;
    sendOrderDeliveryVerification(sendNotificationDto: any): Promise<void>;
    sendOrderDelivered(sendNotificationDto: any): Promise<void>;
}
