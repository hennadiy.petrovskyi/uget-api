"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationsSettingsListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_constants_1 = require("../../common/events/events-constants");
const services_1 = require("../services");
const dtos_1 = require("../dtos");
let NotificationsSettingsListener = class NotificationsSettingsListener {
    constructor(notificationsSettingsService) {
        this.notificationsSettingsService = notificationsSettingsService;
    }
    async createSettings(dto) {
        return await this.notificationsSettingsService.createUserSettings({
            owner: dto.owner,
        });
    }
    async getOwnerSettings(id) {
        return dtos_1.SettingsDto.buildDto(await this.notificationsSettingsService.getUserSettings(id));
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.USER_EVENTS.CREATE_USER_DETAILS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], NotificationsSettingsListener.prototype, "createSettings", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.SETTINGS.GET_USER_NOTIFICATION_SETTINGS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], NotificationsSettingsListener.prototype, "getOwnerSettings", null);
NotificationsSettingsListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.NotificationsSettingsService])
], NotificationsSettingsListener);
exports.NotificationsSettingsListener = NotificationsSettingsListener;
//# sourceMappingURL=notificaitons-settings.listener.js.map