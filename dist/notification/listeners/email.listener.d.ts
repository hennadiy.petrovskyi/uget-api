import { EmailService } from '../services';
export declare class EmailListener {
    private readonly emailService;
    constructor(emailService: EmailService);
    resetPassword(resetPasswordDto: {
        email: string;
        link: string;
    }): Promise<void>;
    verifyEmail(verifyEmailDto: {
        email: string;
        link: string;
    }): Promise<void>;
}
