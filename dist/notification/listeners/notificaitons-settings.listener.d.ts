import { NotificationsSettingsService } from '../services';
import { SettingsDto } from '../dtos';
import { AuthRecord } from 'src/auth/entities';
export declare class NotificationsSettingsListener {
    private readonly notificationsSettingsService;
    constructor(notificationsSettingsService: NotificationsSettingsService);
    createSettings(dto: {
        owner: AuthRecord;
    }): Promise<void>;
    getOwnerSettings(id: number): Promise<SettingsDto>;
}
