"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketEvents = void 0;
var SocketEvents;
(function (SocketEvents) {
    SocketEvents["UPDATE_MY_TRIPS"] = "trips.update.my";
    SocketEvents["UPDATE_QUERY_TRIPS"] = "trips.update.query";
    SocketEvents["UPDATE_MY_ORDERS"] = "orders.update.my";
    SocketEvents["UPDATE_QUERY_ORDERS"] = "orders.update.query";
    SocketEvents["UPDATE_PROFILE"] = "profile.update";
})(SocketEvents = exports.SocketEvents || (exports.SocketEvents = {}));
//# sourceMappingURL=events.js.map