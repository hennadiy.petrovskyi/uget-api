import { SocketGateway } from '../gateway';
export declare class SocketListener {
    private readonly socketGateway;
    constructor(socketGateway: SocketGateway);
    sendMessage(message: {
        uuid: string;
        payload: any;
    }): Promise<void>;
}
