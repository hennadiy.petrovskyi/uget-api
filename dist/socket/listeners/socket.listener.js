"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const gateway_1 = require("../gateway");
const events_1 = require("../../common/events");
let SocketListener = class SocketListener {
    constructor(socketGateway) {
        this.socketGateway = socketGateway;
    }
    async sendMessage(message) {
        this.socketGateway.sendMessageByUUID(message.uuid, message.payload);
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.SOCKET_EVENTS.SEND_MESSAGE, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SocketListener.prototype, "sendMessage", null);
SocketListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [gateway_1.SocketGateway])
], SocketListener);
exports.SocketListener = SocketListener;
//# sourceMappingURL=socket.listener.js.map