import { OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
export declare class SocketGateway implements OnGatewayConnection, OnGatewayDisconnect {
    constructor();
    server: Server;
    get(): Server<import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, any>;
    handleConnection(client: Socket): void;
    handleDisconnect(client: Socket): void;
    sendMessageByUUID(uuid: string, payload: any): Promise<void>;
}
