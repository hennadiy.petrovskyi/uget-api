import { Trip } from '../entities';
import { LocationCreateDto } from '.';
import { AuthRecord } from 'src/auth/entities';
declare const TripCreateDto_base: import("@nestjs/mapped-types").MappedType<Pick<Trip, "uuid" | "description" | "departureDate" | "arrivalDate">>;
export declare class TripCreateDto extends TripCreateDto_base {
    owner: AuthRecord;
    static buildDto(payload: PublishTripDto): TripCreateDto;
}
declare const PublishTripDto_base: import("@nestjs/mapped-types").MappedType<Pick<Trip, "uuid" | "description" | "departureDate" | "arrivalDate">>;
export declare class PublishTripDto extends PublishTripDto_base {
    locations: {
        from: LocationCreateDto;
        to: LocationCreateDto;
    };
}
declare const GetTripByOrderDto_base: import("@nestjs/mapped-types").MappedType<Pick<Trip, "uuid" | "description" | "departureDate" | "arrivalDate">>;
export declare class GetTripByOrderDto extends GetTripByOrderDto_base {
    static buildDto(payload: Trip): GetTripByOrderDto;
}
export declare class OrderDeliveryActionsRequestDto {
    orderUUID: string;
    tripUUID: string;
    static buildDto(payload: any): OrderDeliveryActionsRequestDto;
}
export {};
