"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderDeliveryActionsRequestDto = exports.GetTripByOrderDto = exports.PublishTripDto = exports.TripCreateDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../entities");
const entities_2 = require("../../auth/entities");
class TripCreateDto extends (0, mapped_types_1.PickType)(entities_1.Trip, [
    'uuid',
    'departureDate',
    'arrivalDate',
    'description',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(TripCreateDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", entities_2.AuthRecord)
], TripCreateDto.prototype, "owner", void 0);
exports.TripCreateDto = TripCreateDto;
class PublishTripDto extends (0, mapped_types_1.PickType)(entities_1.Trip, [
    'uuid',
    'departureDate',
    'arrivalDate',
    'description',
]) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Object)
], PublishTripDto.prototype, "locations", void 0);
exports.PublishTripDto = PublishTripDto;
class GetTripByOrderDto extends (0, mapped_types_1.PickType)(entities_1.Trip, [
    'uuid',
    'departureDate',
    'arrivalDate',
    'description',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(GetTripByOrderDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.GetTripByOrderDto = GetTripByOrderDto;
class OrderDeliveryActionsRequestDto {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(OrderDeliveryActionsRequestDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], OrderDeliveryActionsRequestDto.prototype, "orderUUID", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], OrderDeliveryActionsRequestDto.prototype, "tripUUID", void 0);
exports.OrderDeliveryActionsRequestDto = OrderDeliveryActionsRequestDto;
//# sourceMappingURL=trip.dtos.js.map