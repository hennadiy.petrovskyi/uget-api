"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderDeliveryActionsRequestDto = exports.PublishTripDto = exports.GetTripByOrderDto = exports.TripCreateDto = exports.LocationCreateDto = void 0;
var location_dto_1 = require("./location.dto");
Object.defineProperty(exports, "LocationCreateDto", { enumerable: true, get: function () { return location_dto_1.LocationCreateDto; } });
var trip_dtos_1 = require("./trip.dtos");
Object.defineProperty(exports, "TripCreateDto", { enumerable: true, get: function () { return trip_dtos_1.TripCreateDto; } });
Object.defineProperty(exports, "GetTripByOrderDto", { enumerable: true, get: function () { return trip_dtos_1.GetTripByOrderDto; } });
Object.defineProperty(exports, "PublishTripDto", { enumerable: true, get: function () { return trip_dtos_1.PublishTripDto; } });
Object.defineProperty(exports, "OrderDeliveryActionsRequestDto", { enumerable: true, get: function () { return trip_dtos_1.OrderDeliveryActionsRequestDto; } });
//# sourceMappingURL=index.js.map