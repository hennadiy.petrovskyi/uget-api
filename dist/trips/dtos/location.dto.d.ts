import { TripLocation } from '../entities';
declare const LocationCreateDto_base: import("@nestjs/mapped-types").MappedType<Pick<TripLocation, "area">>;
export declare class LocationCreateDto extends LocationCreateDto_base {
    country: string;
    city: string;
}
export {};
