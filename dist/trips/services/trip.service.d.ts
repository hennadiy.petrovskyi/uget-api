import { Repository, FindOperator } from 'typeorm';
import { Trip } from '../entities';
import { LoggerService } from 'src/utils/logger';
export declare class TripService {
    private tripRepository;
    private logger;
    constructor(tripRepository: Repository<Trip>, logger: LoggerService);
    findTripByOwner(owner: FindOperator<any>, id: number): Promise<any>;
    findTripByID(id: number): Promise<Trip>;
    getAllUserTrips(owner: any): Promise<Trip[]>;
    updateTrip(id: number, update: any): Promise<Trip>;
    getAllByOrderParams(owner: number, deliveryDate: string): Promise<Trip[]>;
    findTripByUUID(uuid: string): Promise<Trip>;
    createTrip(orderCreateDto: any): Promise<Trip>;
    deleteTrip(id: number): Promise<void>;
}
