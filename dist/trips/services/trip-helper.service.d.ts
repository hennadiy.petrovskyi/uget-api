import { AuthRecord } from 'src/auth/entities';
import { FindOperator } from 'typeorm';
import { TripLocationService, TripService } from '.';
import { LoggerService } from '../../utils/logger';
import { PublishTripDto } from '../dtos';
import { TripEmitter } from '../emitters';
import { Trip } from '../entities';
import { TripLocationType, TripOrderStatus } from '../enums';
export declare class TripHelperService {
    private readonly tripService;
    private readonly locationService;
    private tripEmitter;
    private logger;
    constructor(tripService: TripService, locationService: TripLocationService, tripEmitter: TripEmitter, logger: LoggerService);
    handleFilterTripsByParams(userId: number, filterParams: any): Promise<Trip[]>;
    handlePublishTrip(publishTripDto: PublishTripDto, user: AuthRecord): Promise<void>;
    getLocationsDetails(tripId: FindOperator<any>, locationType: TripLocationType): Promise<any>;
    buildTripForOrderDto(trip: any, orderUUID: any): Promise<{
        from: any;
        to: any;
        status: TripOrderStatus;
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        uuid: string;
        description: string;
        departureDate: string;
        arrivalDate: string;
    }>;
    buildTripDto(trip: any): Promise<any>;
    handleDeleteTrip(id: FindOperator<any>): Promise<void>;
}
