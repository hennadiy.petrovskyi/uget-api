"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripHelperService = void 0;
const common_1 = require("@nestjs/common");
const _1 = require(".");
const logger_1 = require("../../utils/logger");
const dtos_1 = require("../dtos");
const emitters_1 = require("../emitters");
const enums_1 = require("../enums");
let TripHelperService = class TripHelperService {
    constructor(tripService, locationService, tripEmitter, logger) {
        this.tripService = tripService;
        this.locationService = locationService;
        this.tripEmitter = tripEmitter;
        this.logger = logger;
    }
    async handleFilterTripsByParams(userId, filterParams) {
        const trips = await this.tripService.getAllByOrderParams(userId, filterParams.deliveryDate);
        const filtered = await Promise.all(trips.map(async (trip) => {
            const from = await this.getLocationsDetails(trip.id, enums_1.TripLocationType.FROM);
            const to = await this.getLocationsDetails(trip.id, enums_1.TripLocationType.TO);
            if (from.city.id === filterParams.from &&
                to.city.id === filterParams.to) {
                return trip;
            }
        }));
        return Promise.all(filtered.filter((trip) => trip));
    }
    async handlePublishTrip(publishTripDto, user) {
        const trip = await this.tripService.createTrip(Object.assign(Object.assign({}, dtos_1.TripCreateDto.buildDto(publishTripDto)), { owner: user }));
        const pickupLocationDetailsId = await this.tripEmitter.saveLocationDetails({
            countryId: publishTripDto.locations.from.country,
            locationId: publishTripDto.locations.from.city,
        });
        const endpointLocationDetailsId = await this.tripEmitter.saveLocationDetails({
            countryId: publishTripDto.locations.to.country,
            locationId: publishTripDto.locations.to.city,
        });
        const pickupLocationDto = Object.assign(Object.assign({ owner: trip }, publishTripDto.locations.from), { detailsId: pickupLocationDetailsId, type: enums_1.TripLocationType.FROM });
        const endpointLocationDto = Object.assign(Object.assign({ owner: trip }, publishTripDto.locations.to), { detailsId: endpointLocationDetailsId, type: enums_1.TripLocationType.TO });
        await this.locationService.saveOrderLocation(pickupLocationDto);
        await this.locationService.saveOrderLocation(endpointLocationDto);
    }
    async getLocationsDetails(tripId, locationType) {
        const tripLocations = await this.locationService.getTripLocations(tripId, locationType);
        if (!tripLocations) {
            return;
        }
        const details = await this.tripEmitter.getLocationDetails(tripLocations.detailsId);
        return Object.assign(Object.assign({}, details), { area: tripLocations.area });
    }
    async buildTripForOrderDto(trip, orderUUID) {
        const from = await this.getLocationsDetails(trip.id, enums_1.TripLocationType.FROM);
        const to = await this.getLocationsDetails(trip.id, enums_1.TripLocationType.TO);
        const owner = await this.tripEmitter.getUserDetails(trip.owner);
        const profileImage = await this.tripEmitter.getUserProfileImage(trip.owner);
        function handleStatus() {
            if (trip.accepted.includes(orderUUID)) {
                return enums_1.TripOrderStatus.ACCEPTED;
            }
            if (trip.incomingRequests.includes(orderUUID)) {
                return enums_1.TripOrderStatus.REQUEST_OUTGOING;
            }
            if (trip.outgoingRequests.includes(orderUUID)) {
                return enums_1.TripOrderStatus.REQUEST_INCOMING;
            }
            return enums_1.TripOrderStatus.AVAILABLE;
        }
        const status = handleStatus();
        return Object.assign(Object.assign({}, dtos_1.GetTripByOrderDto.buildDto(trip)), { from,
            to,
            status, owner: Object.assign(Object.assign({}, owner), { profileImage }) });
    }
    async buildTripDto(trip) {
        const from = await this.getLocationsDetails(trip.id, enums_1.TripLocationType.FROM);
        const to = await this.getLocationsDetails(trip.id, enums_1.TripLocationType.TO);
        const { inProgress, accepted, delivered } = trip;
        const orders = inProgress.concat(accepted).concat(delivered);
        const earnings = await this.tripEmitter.getOrdersDeliveryEarnings(orders);
        const handleStatus = () => {
            const currentTime = new Date().toISOString().substring(0, 10);
            const departureDate = new Date(trip.departureDate)
                .toISOString()
                .substring(0, 10);
            if (currentTime >= departureDate && !trip.inProgress.length) {
                return 'past';
            }
            if (currentTime >= departureDate && trip.inProgress.length) {
                return 'active';
            }
            return 'upcoming';
        };
        return Object.assign(Object.assign({}, trip), { earnings, status: handleStatus(), locations: {
                from,
                to,
            } });
    }
    async handleDeleteTrip(id) {
        await this.locationService.deleteTripLocation(id);
        await this.tripService.deleteTrip(id);
    }
};
TripHelperService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)((0, common_1.forwardRef)(() => _1.TripService))),
    __param(1, (0, common_1.Inject)((0, common_1.forwardRef)(() => _1.TripLocationService))),
    __metadata("design:paramtypes", [_1.TripService,
        _1.TripLocationService,
        emitters_1.TripEmitter,
        logger_1.LoggerService])
], TripHelperService);
exports.TripHelperService = TripHelperService;
//# sourceMappingURL=trip-helper.service.js.map