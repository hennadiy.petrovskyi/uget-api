import { FindOperator, Repository } from 'typeorm';
import { TripLocation } from '../entities';
import { LoggerService } from 'src/utils/logger';
import { TripLocationType } from '../enums';
export declare class TripLocationService {
    private locationsRepository;
    private logger;
    constructor(locationsRepository: Repository<TripLocation>, logger: LoggerService);
    saveOrderLocation(orderLocationSaveDto: any): Promise<void>;
    getTripLocations(owner: FindOperator<any>, type: TripLocationType): Promise<TripLocation>;
    deleteOrderLocation(owner: FindOperator<any>): Promise<void>;
    deleteTripLocation(owner: FindOperator<any>): Promise<void>;
}
