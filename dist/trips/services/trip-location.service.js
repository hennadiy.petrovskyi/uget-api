"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripLocationService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entities_1 = require("../entities");
const logger_1 = require("../../utils/logger");
let TripLocationService = class TripLocationService {
    constructor(locationsRepository, logger) {
        this.locationsRepository = locationsRepository;
        this.logger = logger;
    }
    async saveOrderLocation(orderLocationSaveDto) {
        const functionName = this.saveOrderLocation.name;
        try {
            await this.locationsRepository.save(orderLocationSaveDto);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async getTripLocations(owner, type) {
        const functionName = this.getTripLocations.name;
        try {
            return await this.locationsRepository.findOne({
                where: {
                    owner,
                    type,
                },
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async deleteOrderLocation(owner) {
        const locations = await this.locationsRepository.find({
            where: {
                owner,
            },
        });
        const functionName = this.deleteOrderLocation.name;
        try {
            Promise.all(locations.map((location) => this.locationsRepository.delete(location.id)));
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async deleteTripLocation(owner) {
        const functionName = this.deleteTripLocation.name;
        try {
            const tripLocations = await this.locationsRepository.find({
                where: { owner },
            });
            Promise.all(tripLocations.map((el) => this.locationsRepository.delete(el.id)));
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
};
TripLocationService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.TripLocation)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        logger_1.LoggerService])
], TripLocationService);
exports.TripLocationService = TripLocationService;
//# sourceMappingURL=trip-location.service.js.map