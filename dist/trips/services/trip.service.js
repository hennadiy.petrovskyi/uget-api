"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entities_1 = require("../entities");
const logger_1 = require("../../utils/logger");
let TripService = class TripService {
    constructor(tripRepository, logger) {
        this.tripRepository = tripRepository;
        this.logger = logger;
    }
    async findTripByOwner(owner, id) {
        const functionName = this.findTripByOwner.name;
        try {
            const order = await this.tripRepository.findOne({
                where: { owner, id },
                loadRelationIds: true,
            });
            if (!order) {
                return;
            }
            return order;
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findTripByID(id) {
        const functionName = this.findTripByOwner.name;
        try {
            const order = await this.tripRepository.findOne({
                where: { id },
                loadRelationIds: true,
            });
            if (!order) {
                return;
            }
            return order;
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async getAllUserTrips(owner) {
        const functionName = this.getAllUserTrips.name;
        try {
            const orders = await this.tripRepository.find({
                where: { owner },
                loadRelationIds: true,
            });
            if (!orders) {
                return;
            }
            return orders.map((order) => order);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async updateTrip(id, update) {
        const functionName = this.updateTrip.name;
        try {
            await this.tripRepository.update(id, Object.assign({}, update));
            return this.findTripByID(id);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async getAllByOrderParams(owner, deliveryDate) {
        const functionName = this.getAllByOrderParams.name;
        try {
            const trips = await this.tripRepository.find({
                where: {
                    owner: (0, typeorm_2.Not)(owner),
                    departureDate: (0, typeorm_2.LessThanOrEqual)(new Date(deliveryDate).toISOString()),
                },
                loadRelationIds: true,
            });
            if (!trips) {
                return;
            }
            return trips.map((trip) => trip);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findTripByUUID(uuid) {
        const functionName = this.findTripByUUID.name;
        try {
            return await this.tripRepository.findOne({
                where: { uuid },
                loadRelationIds: true,
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async createTrip(orderCreateDto) {
        const functionName = this.createTrip.name;
        try {
            await this.tripRepository.save(orderCreateDto);
            return await this.findTripByUUID(orderCreateDto.uuid);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async deleteTrip(id) {
        const functionName = this.deleteTrip.name;
        try {
            await this.tripRepository.delete(id);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
};
TripService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.Trip)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        logger_1.LoggerService])
], TripService);
exports.TripService = TripService;
//# sourceMappingURL=trip.service.js.map