import { BaseEntity } from 'src/common/entities';
import { TripLocationType } from '../enums';
import { Trip } from '.';
export declare class TripLocation extends BaseEntity {
    owner: Trip;
    type: TripLocationType;
    detailsId: number;
    area: string;
}
