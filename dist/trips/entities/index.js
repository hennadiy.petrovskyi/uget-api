"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripLocation = exports.Trip = void 0;
var trip_entity_1 = require("./trip.entity");
Object.defineProperty(exports, "Trip", { enumerable: true, get: function () { return trip_entity_1.Trip; } });
var trips_locations_1 = require("./trips-locations");
Object.defineProperty(exports, "TripLocation", { enumerable: true, get: function () { return trips_locations_1.TripLocation; } });
//# sourceMappingURL=index.js.map