import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from 'src/auth/entities';
export declare class Trip extends BaseEntity {
    owner: AuthRecord;
    uuid: string;
    departureDate: string;
    arrivalDate: string;
    description: string;
    incomingRequests: string[];
    outgoingRequests: string[];
    accepted: string[];
    inProgress: string[];
    delivered: string[];
}
