"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripEmitter = void 0;
const events_constants_1 = require("./../../common/events/events-constants");
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_1 = require("../../common/events");
const enums_1 = require("../../file/enums");
let TripEmitter = class TripEmitter {
    constructor(eventEmitter) {
        this.eventEmitter = eventEmitter;
    }
    async getLocationDetails(id) {
        const locationDetails = await this.eventEmitter.emitAsync(events_1.LOCATION_EVENT.GET_LOCATION_DETAILS_BY_ID, id);
        if (locationDetails.length) {
            return locationDetails[0];
        }
    }
    async saveLocationDetails(saveLocationDetailsDto) {
        const locationDetails = await this.eventEmitter.emitAsync(events_1.LOCATION_EVENT.SAVE_LOCATION_IN_DB, saveLocationDetailsDto);
        if (locationDetails.length) {
            return locationDetails[0].id;
        }
    }
    async addOrderIncomingRequest(orderIncomingRequest) {
        await this.eventEmitter.emitAsync(events_constants_1.ORDER_EVENT.ADD_INCOMING_REQUEST, orderIncomingRequest);
    }
    async acceptOrderDelivery(acceptDeliveryRequestDto) {
        await this.eventEmitter.emitAsync(events_constants_1.ORDER_EVENT.ACCEPT_DELIVERY_BY_TRAVELER, acceptDeliveryRequestDto);
    }
    async cancelOrderDelivery(type, cancelOrderDeliveryDto) {
        const eventsTypes = {
            incoming: events_constants_1.ORDER_EVENT.CANCEL_INCOMING_REQUEST,
            outgoing: events_constants_1.ORDER_EVENT.CANCEL_OUTGOING_REQUEST,
            accepted: events_constants_1.ORDER_EVENT.CANCEL_ACCEPTED_REQUEST,
        };
        console.log('cancelOrderDelivery', cancelOrderDeliveryDto);
        await this.eventEmitter.emitAsync(eventsTypes[type], cancelOrderDeliveryDto);
    }
    async requestOwnerUUID(id) {
        const result = await this.eventEmitter.emitAsync(events_constants_1.AUTH_EVENTS.GET_USER_BY_ID, id);
        if (!result.length) {
            return;
        }
        return result[0];
    }
    async getUserDetails(id) {
        const userDetails = await this.eventEmitter.emitAsync(events_constants_1.USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID, id);
        if (!userDetails.length) {
            return;
        }
        const { firstName, lastName, rating, profileImage } = userDetails[0];
        return { fullName: firstName + ' ' + lastName, rating, profileImage };
    }
    async getUserProfileImage(ownerId) {
        const owner = (await this.getAuthRecordUUID(ownerId));
        if (!owner) {
            return;
        }
        const file = await this.eventEmitter.emitAsync(events_constants_1.FILE_EVENTS.GET_FILE_BY_TYPE, { owner: owner.uuid, type: enums_1.FileType.PROFILE_IMAGE });
        if (file && file.length) {
            return file[0];
        }
        return null;
    }
    async getAuthRecordUUID(id) {
        const result = await this.eventEmitter.emitAsync(events_constants_1.AUTH_EVENTS.GET_USER_BY_ID, id);
        if (result && result.length) {
            return result[0];
        }
    }
    async sendIncomingRequestNotification(dto) {
        await this.eventEmitter.emitAsync(events_constants_1.PUSH_NOTIFICATION_EVENTS.TRIP_REQUEST, dto);
    }
    async sendAcceptedRequestNotification(dto) {
        await this.eventEmitter.emitAsync(events_constants_1.PUSH_NOTIFICATION_EVENTS.TRIP_REQUEST_ACCEPTED, dto);
    }
    async sendOrderVerifyPickedUpNotification(dto) {
        await this.eventEmitter.emitAsync(events_constants_1.PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP_VERIFICATION, dto);
    }
    async sendOrderPickedUpNotification(dto) {
        await this.eventEmitter.emitAsync(events_constants_1.PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP, dto);
    }
    async sendVerifyOrderDeliveryNotification(dto) {
        await this.eventEmitter.emitAsync(events_constants_1.PUSH_NOTIFICATION_EVENTS.DELIVERY_VERIFICATION, dto);
    }
    async sendOrderDeliveredNotification(dto) {
        await this.eventEmitter.emitAsync(events_constants_1.PUSH_NOTIFICATION_EVENTS.ORDER_DELIVERED, dto);
    }
    async getOrdersDeliveryEarnings(orders) {
        const response = await this.eventEmitter.emitAsync(events_constants_1.ORDER_EVENT.GET_EARNINGS, orders);
        if (!response.length) {
            return 0;
        }
        return response[0];
    }
    async sendSocketMessage(dto) {
        await this.eventEmitter.emitAsync(events_constants_1.SOCKET_EVENTS.SEND_MESSAGE, dto);
    }
    async addUnratedUserRating(dto) {
        const result = await this.eventEmitter.emitAsync(events_constants_1.USER_EVENTS.ADD_NEW_UNRATED_RATE, dto);
        if (result && result.length) {
            return result[0];
        }
    }
};
TripEmitter = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [event_emitter_1.EventEmitter2])
], TripEmitter);
exports.TripEmitter = TripEmitter;
//# sourceMappingURL=trip.emitter.js.map