import { EventEmitter2 } from '@nestjs/event-emitter';
import { CancelEventType } from '../enums';
export declare class TripEmitter {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    getLocationDetails(id: number): Promise<any>;
    saveLocationDetails(saveLocationDetailsDto: any): Promise<any>;
    addOrderIncomingRequest(orderIncomingRequest: any): Promise<void>;
    acceptOrderDelivery(acceptDeliveryRequestDto: any): Promise<void>;
    cancelOrderDelivery(type: CancelEventType, cancelOrderDeliveryDto: any): Promise<void>;
    requestOwnerUUID(id: any): Promise<any>;
    getUserDetails(id: number): Promise<{
        fullName: string;
        rating: any;
        profileImage: any;
    }>;
    getUserProfileImage(ownerId: number): Promise<any>;
    getAuthRecordUUID(id: any): Promise<any>;
    sendIncomingRequestNotification(dto: {
        from: string;
        to: any;
    }): Promise<void>;
    sendAcceptedRequestNotification(dto: {
        from: string;
        to: any;
    }): Promise<void>;
    sendOrderVerifyPickedUpNotification(dto: {
        orderUUID: string;
        to: any;
        by: number;
    }): Promise<void>;
    sendOrderPickedUpNotification(dto: {
        orderUUID: string;
        to: any;
        by: number;
    }): Promise<void>;
    sendVerifyOrderDeliveryNotification(dto: {
        orderUUID: string;
        to: any;
        by: number;
    }): Promise<void>;
    sendOrderDeliveredNotification(dto: {
        orderUUID: string;
        to: any;
        by: number;
    }): Promise<void>;
    getOrdersDeliveryEarnings(orders: string[]): Promise<any>;
    sendSocketMessage(dto: {
        uuid: string;
        payload: any;
    }): Promise<void>;
    addUnratedUserRating(dto: any): Promise<any>;
}
