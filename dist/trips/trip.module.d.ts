import { MiddlewareConsumer } from '@nestjs/common';
export declare class TripModule {
    configure(consumer: MiddlewareConsumer): void;
}
