"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const axios_1 = require("@nestjs/axios");
const services_1 = require("./services");
const controllers_1 = require("./controllers");
const entities_1 = require("./entities");
const logger_1 = require("../utils/logger");
const emitters_1 = require("./emitters");
const morgan_middleware_1 = require("../common/middlewares/morgan.middleware");
const listeners_1 = require("./listeners");
let TripModule = class TripModule {
    configure(consumer) {
        consumer.apply(morgan_middleware_1.MorganMiddleware).forRoutes(controllers_1.TripController);
    }
};
TripModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([entities_1.Trip, entities_1.TripLocation]),
            axios_1.HttpModule,
            logger_1.LoggerModule,
        ],
        controllers: [controllers_1.TripController],
        providers: [
            services_1.TripService,
            services_1.TripLocationService,
            emitters_1.TripEmitter,
            listeners_1.TripListener,
            services_1.TripHelperService,
        ],
        exports: [],
    })
], TripModule);
exports.TripModule = TripModule;
//# sourceMappingURL=trip.module.js.map