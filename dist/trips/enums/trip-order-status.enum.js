"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripOrderStatus = void 0;
var TripOrderStatus;
(function (TripOrderStatus) {
    TripOrderStatus["REQUEST_INCOMING"] = "incoming";
    TripOrderStatus["REQUEST_OUTGOING"] = "outgoing";
    TripOrderStatus["ACCEPTED"] = "accepted";
    TripOrderStatus["AVAILABLE"] = "available";
    TripOrderStatus["DECLINED"] = "declined";
})(TripOrderStatus = exports.TripOrderStatus || (exports.TripOrderStatus = {}));
//# sourceMappingURL=trip-order-status.enum.js.map