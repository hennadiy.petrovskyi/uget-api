"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripLocationType = void 0;
var TripLocationType;
(function (TripLocationType) {
    TripLocationType["FROM"] = "from";
    TripLocationType["TO"] = "to";
})(TripLocationType = exports.TripLocationType || (exports.TripLocationType = {}));
//# sourceMappingURL=trip-type.enum.js.map