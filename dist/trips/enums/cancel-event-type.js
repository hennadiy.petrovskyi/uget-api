"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CancelEventType = void 0;
var CancelEventType;
(function (CancelEventType) {
    CancelEventType["OUTGOING"] = "outgoing";
    CancelEventType["INCOMING"] = "incoming";
    CancelEventType["ACCEPTED"] = "accepted";
})(CancelEventType = exports.CancelEventType || (exports.CancelEventType = {}));
//# sourceMappingURL=cancel-event-type.js.map