"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const services_1 = require("../services");
const trip_emitter_1 = require("../emitters/trip.emitter");
const jwt_auth_guard_1 = require("../../common/guards/jwt-auth.guard");
const decorators_1 = require("../../common/decorators");
const enums_1 = require("../enums");
const dtos_1 = require("../dtos");
let TripController = class TripController {
    constructor(service, helperService, tripEmitter) {
        this.service = service;
        this.helperService = helperService;
        this.tripEmitter = tripEmitter;
    }
    async getOrderRequests(getOrderRequestsDto, user) {
        const tripsForOrder = await Promise.all(getOrderRequestsDto.trips.map(async (id) => {
            const trip = await this.service.findTripByUUID(id);
            return await this.helperService.buildTripForOrderDto(trip, getOrderRequestsDto.orderUUID);
        }));
        return tripsForOrder;
    }
    async publishTrip(publishTripDto, user) {
        await this.helperService.handlePublishTrip(publishTripDto, user);
        const userTrips = await this.service.getAllUserTrips(user.id);
        return Promise.all(userTrips.map((trip) => this.helperService.buildTripDto(trip)));
    }
    async publishRoundTrip(trips, user) {
        await Promise.all(trips.map((trip) => this.helperService.handlePublishTrip(trip, user)));
        const userTrips = await this.service.getAllUserTrips(user.id);
        return Promise.all(userTrips.map((trip) => this.helperService.buildTripDto(trip)));
    }
    async getAllTrips(user) {
        const trips = await this.service.getAllUserTrips(user.id);
        return Promise.all(trips.map((order) => this.helperService.buildTripDto(order)));
    }
    async getManyFilteredByOrder(user, query) {
        const trips = await this.service.getAllByOrderParams(user.id, query.deliveryDate);
        const filteredTrips = await Promise.all(trips
            .map(async (trip) => {
            const from = await this.helperService.getLocationsDetails(trip.id, enums_1.TripLocationType.FROM);
            const to = await this.helperService.getLocationsDetails(trip.id, enums_1.TripLocationType.TO);
            if (from.city.id === query.from && to.city.id === query.to) {
                return trip;
            }
        })
            .filter((trip) => trip));
        return Promise.all(filteredTrips.map(async (trip) => this.helperService.buildTripForOrderDto(trip, query.uuid)));
    }
    async getOneFilteredByOrder(user, query) {
        const trip = await this.service.findTripByUUID(query.tripUUID);
        return this.helperService.buildTripForOrderDto(trip, query.orderUUID);
    }
    async requestOrderDelivery(requestOrderDeliveryDto, user) {
        const trip = await this.service.findTripByUUID(requestOrderDeliveryDto.tripUUID);
        if (!trip ||
            trip.outgoingRequests.includes(requestOrderDeliveryDto.orderUUID)) {
            throw new common_1.ForbiddenException();
        }
        trip.outgoingRequests.push(requestOrderDeliveryDto.orderUUID);
        const updatedTrip = await this.service.updateTrip(trip.id, {
            outgoingRequests: trip.outgoingRequests,
        });
        const socketMessageData = await this.helperService.buildTripForOrderDto(updatedTrip, requestOrderDeliveryDto.orderUUID);
        await this.tripEmitter.addOrderIncomingRequest(Object.assign(Object.assign({}, dtos_1.OrderDeliveryActionsRequestDto.buildDto(requestOrderDeliveryDto)), { trip: {
                uuid: trip.uuid,
                update: socketMessageData,
            }, from: user.id }));
        return this.helperService.buildTripDto(updatedTrip);
    }
    async cancelDeliveryOrderRequest(requestOrderDeliveryDto) {
        const trip = await this.service.findTripByUUID(requestOrderDeliveryDto.tripUUID);
        if (!trip) {
            throw new common_1.ForbiddenException();
        }
        function handleUpdateBody() {
            let update = {
                body: null,
                orderCancelAction: null,
            };
            const isInAccepted = trip.accepted.find((id) => id === requestOrderDeliveryDto.orderUUID);
            const isInIncomings = trip.incomingRequests.find((id) => id === requestOrderDeliveryDto.orderUUID);
            const isInOutgoings = trip.outgoingRequests.find((id) => id === requestOrderDeliveryDto.orderUUID);
            if (isInIncomings) {
                update.body = {
                    incomingRequests: trip.incomingRequests.filter((id) => id !== requestOrderDeliveryDto.orderUUID),
                };
                update.orderCancelAction = enums_1.CancelEventType.OUTGOING;
            }
            if (isInOutgoings) {
                update.body = {
                    outgoingRequests: trip.outgoingRequests.filter((id) => id !== requestOrderDeliveryDto.orderUUID),
                };
                update.orderCancelAction = enums_1.CancelEventType.INCOMING;
            }
            if (isInAccepted) {
                update.body = {
                    accepted: trip.accepted.filter((id) => id !== requestOrderDeliveryDto.orderUUID),
                };
                update.orderCancelAction = enums_1.CancelEventType.ACCEPTED;
            }
            return update;
        }
        const update = handleUpdateBody();
        const updatedTrip = await this.service.updateTrip(trip.id, update.body);
        const socketMessageData = await this.helperService.buildTripForOrderDto(updatedTrip, requestOrderDeliveryDto.orderUUID);
        await this.tripEmitter.cancelOrderDelivery(update.orderCancelAction, Object.assign(Object.assign({}, dtos_1.OrderDeliveryActionsRequestDto.buildDto(requestOrderDeliveryDto)), { trip: {
                uuid: dtos_1.OrderDeliveryActionsRequestDto.buildDto(requestOrderDeliveryDto)
                    .tripUUID,
                data: socketMessageData,
            } }));
        return this.helperService.buildTripDto(updatedTrip);
    }
    async acceptDeliveryOrderRequest(requestOrderDeliveryDto, user) {
        const trip = await this.service.findTripByUUID(requestOrderDeliveryDto.tripUUID);
        if (!trip || trip.accepted.includes(requestOrderDeliveryDto.orderUUID)) {
            throw new common_1.ForbiddenException();
        }
        const updatedTrip = await this.service.updateTrip(trip.id, {
            incomingRequests: trip.incomingRequests.filter((id) => id !== requestOrderDeliveryDto.orderUUID),
            accepted: trip.accepted.concat([requestOrderDeliveryDto.orderUUID]),
        });
        const socketMessageData = await this.helperService.buildTripForOrderDto(updatedTrip, requestOrderDeliveryDto.orderUUID);
        await this.tripEmitter.acceptOrderDelivery(Object.assign(Object.assign({}, requestOrderDeliveryDto), { trip: { uuid: requestOrderDeliveryDto.tripUUID, data: socketMessageData }, from: user.id }));
        return this.helperService.buildTripDto(updatedTrip);
    }
    async requestOwnerDetails(uuid, user) {
        const trip = await this.service.findTripByUUID(uuid);
        const tripOwner = await this.tripEmitter.requestOwnerUUID(trip.owner);
        if (!tripOwner) {
            throw new common_1.NotFoundException();
        }
        return tripOwner.uuid;
    }
    async deleteTrip(orderUUID, user) {
        const trip = await this.service.findTripByUUID(orderUUID);
        if (trip.accepted.length) {
            throw new common_1.ForbiddenException();
        }
        await this.helperService.handleDeleteTrip(trip.id);
        const trips = await this.service.getAllUserTrips(user.id);
        return Promise.all(trips.map((order) => this.helperService.buildTripDto(order)));
    }
};
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('order'),
    __param(0, (0, common_1.Body)('')),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "getOrderRequests", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('oneway'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.PublishTripDto, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "publishTrip", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('roundtrip'),
    __param(0, (0, common_1.Body)('trips')),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "publishRoundTrip", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Get)(),
    __param(0, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "getAllTrips", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Get)('/filtered/many'),
    __param(0, (0, decorators_1.User)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "getManyFilteredByOrder", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Get)('/filtered/one'),
    __param(0, (0, decorators_1.User)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "getOneFilteredByOrder", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Put)('delivery/request'),
    __param(0, (0, common_1.Body)('')),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "requestOrderDelivery", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Put)('delivery/cancel'),
    __param(0, (0, common_1.Body)('')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "cancelDeliveryOrderRequest", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Put)('delivery/accept'),
    __param(0, (0, common_1.Body)('')),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "acceptDeliveryOrderRequest", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('owner/details'),
    __param(0, (0, common_1.Body)('uuid')),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "requestOwnerDetails", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Delete)(),
    __param(0, (0, common_1.Body)('uuid')),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], TripController.prototype, "deleteTrip", null);
TripController = __decorate([
    (0, swagger_1.ApiTags)('Trips'),
    (0, common_1.Controller)('trips'),
    __param(1, (0, common_1.Inject)((0, common_1.forwardRef)(() => services_1.TripHelperService))),
    __metadata("design:paramtypes", [services_1.TripService,
        services_1.TripHelperService,
        trip_emitter_1.TripEmitter])
], TripController);
exports.TripController = TripController;
//# sourceMappingURL=trip.controller.js.map