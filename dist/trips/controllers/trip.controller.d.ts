import { TripService, TripHelperService } from '../services';
import { TripEmitter } from '../emitters/trip.emitter';
import { TripOrderStatus } from '../enums';
import { PublishTripDto } from '../dtos';
export declare class TripController {
    private readonly service;
    private helperService;
    private tripEmitter;
    constructor(service: TripService, helperService: TripHelperService, tripEmitter: TripEmitter);
    getOrderRequests(getOrderRequestsDto: {
        trips: string[];
        orderUUID: string;
    }, user: any): Promise<{
        from: any;
        to: any;
        status: TripOrderStatus;
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        uuid: string;
        description: string;
        departureDate: string;
        arrivalDate: string;
    }[]>;
    publishTrip(publishTripDto: PublishTripDto, user: any): Promise<any[]>;
    publishRoundTrip(trips: any, user: any): Promise<any[]>;
    getAllTrips(user: any): Promise<any[]>;
    getManyFilteredByOrder(user: any, query: any): Promise<{
        from: any;
        to: any;
        status: TripOrderStatus;
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        uuid: string;
        description: string;
        departureDate: string;
        arrivalDate: string;
    }[]>;
    getOneFilteredByOrder(user: any, query: any): Promise<{
        from: any;
        to: any;
        status: TripOrderStatus;
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        uuid: string;
        description: string;
        departureDate: string;
        arrivalDate: string;
    }>;
    requestOrderDelivery(requestOrderDeliveryDto: any, user: any): Promise<any>;
    cancelDeliveryOrderRequest(requestOrderDeliveryDto: any): Promise<any>;
    acceptDeliveryOrderRequest(requestOrderDeliveryDto: any, user: any): Promise<any>;
    requestOwnerDetails(uuid: any, user: any): Promise<any>;
    deleteTrip(orderUUID: string, user: any): Promise<any[]>;
}
