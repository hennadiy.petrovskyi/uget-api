"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TripListener = void 0;
const events_1 = require("../../common/events");
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_constants_1 = require("./../../common/events/events-constants");
const services_1 = require("../services");
const emitters_1 = require("../emitters");
const events_2 = require("../../socket/enums/events");
const enums_1 = require("../../user/enums");
let TripListener = class TripListener {
    constructor(tripService, tripEmitter, helperService) {
        this.tripService = tripService;
        this.tripEmitter = tripEmitter;
        this.helperService = helperService;
    }
    async tripAddOutgoingRequest(dto) {
        console.log('TRIP_EVENT.ADD_INCOMING_REQUEST');
        const trip = await this.tripService.findTripByUUID(dto.tripUUID);
        if (trip.incomingRequests.includes(dto.orderUUID)) {
            return;
        }
        trip.incomingRequests.push(dto.orderUUID);
        await this.tripService.updateTrip(trip.id, {
            incomingRequests: trip.incomingRequests,
        });
        const owner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
        await this.tripEmitter.sendSocketMessage({
            uuid: owner.uuid,
            payload: { action: events_2.SocketEvents.UPDATE_MY_TRIPS },
        });
        await this.tripEmitter.sendSocketMessage({
            uuid: 'order_' + dto.order.uuid,
            payload: { data: dto.order.update },
        });
    }
    async tripCancelIncomingRequest(dto) {
        const trip = await this.tripService.findTripByUUID(dto.tripUUID);
        if (!trip.incomingRequests.includes(dto.orderUUID)) {
            return;
        }
        await this.tripService.updateTrip(trip.id, {
            incomingRequests: trip.incomingRequests.filter((orderUUID) => orderUUID !== dto.orderUUID),
        });
        console.log('TRIP_EVENT.CANCEL_INCOMING_REQUEST');
        const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
        await this.tripEmitter.sendSocketMessage({
            uuid: tripOwner.uuid,
            payload: { action: events_2.SocketEvents.UPDATE_MY_TRIPS },
        });
        await this.tripEmitter.sendSocketMessage({
            uuid: 'order_' + dto.order.uuid,
            payload: { data: dto.order.update },
        });
    }
    async tripCancelOutgoingRequest(dto) {
        const trip = await this.tripService.findTripByUUID(dto.tripUUID);
        if (!trip.outgoingRequests.includes(dto.orderUUID)) {
            return;
        }
        await this.tripService.updateTrip(trip.id, {
            outgoingRequests: trip.outgoingRequests.filter((orderUUID) => orderUUID !== dto.orderUUID),
        });
        console.log('TRIP_EVENT.CANCEL_OUTGOING_REQUEST');
        const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
        await this.tripEmitter.sendSocketMessage({
            uuid: tripOwner.uuid,
            payload: { action: events_2.SocketEvents.UPDATE_MY_TRIPS },
        });
        await this.tripEmitter.sendSocketMessage({
            uuid: 'order_' + dto.order.uuid,
            payload: { data: dto.order.update },
        });
    }
    async setOrderAcceptedByTraveler(dto) {
        const trip = await this.tripService.findTripByUUID(dto.tripUUID);
        if (!trip.outgoingRequests.includes(dto.orderUUID)) {
            return;
        }
        await this.tripService.updateTrip(trip.id, {
            outgoingRequests: trip.outgoingRequests.filter((orderUUID) => orderUUID !== dto.orderUUID),
            accepted: trip.accepted.concat([dto.orderUUID]),
        });
        console.log('TRIP_EVENT.ACCEPT_DELIVERY_BY_ORDER_OWNER');
        const owner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
        await this.tripEmitter.sendSocketMessage({
            uuid: owner.uuid,
            payload: { action: events_2.SocketEvents.UPDATE_MY_TRIPS },
        });
        await this.tripEmitter.sendSocketMessage({
            uuid: 'order_' + dto.order.uuid,
            payload: { data: dto.order.update },
        });
    }
    async tripCancelAcceptedRequest(dto) {
        const trip = await this.tripService.findTripByUUID(dto.tripUUID);
        if (!trip.accepted.includes(dto.orderUUID)) {
            return;
        }
        await this.tripService.updateTrip(trip.id, {
            accepted: trip.accepted.filter((orderUUID) => orderUUID !== dto.orderUUID),
        });
        console.log('TRIP_EVENT.CANCEL_ACCEPTED_REQUEST');
        const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
        await this.tripEmitter.sendSocketMessage({
            uuid: tripOwner.uuid,
            payload: { action: events_2.SocketEvents.UPDATE_MY_TRIPS },
        });
        await this.tripEmitter.sendSocketMessage({
            uuid: 'order_' + dto.order.uuid,
            payload: { data: dto.order.update },
        });
    }
    async getAllUserTrips(owner) {
        const trips = await this.tripService.getAllUserTrips(owner);
        if (!trips) {
            return [];
        }
        return Promise.all(trips.map((el) => this.helperService.buildTripDto(el)));
    }
    async startDelivery(dto) {
        const trip = await this.tripService.findTripByUUID(dto.tripUUID);
        if (!trip.accepted.includes(dto.orderUUID)) {
            return;
        }
        await this.tripService.updateTrip(trip.id, {
            accepted: trip.outgoingRequests.filter((orderUUID) => orderUUID !== dto.orderUUID),
            inProgress: trip.inProgress.concat([dto.orderUUID]),
        });
        const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
        const orderOwner = await this.tripEmitter.getAuthRecordUUID(dto.to);
        await this.tripEmitter.sendSocketMessage({
            uuid: tripOwner.uuid,
            payload: { action: events_2.SocketEvents.UPDATE_MY_TRIPS },
        });
        console.log(orderOwner);
        await this.tripEmitter.sendSocketMessage({
            uuid: orderOwner.uuid,
            payload: { action: events_2.SocketEvents.UPDATE_MY_ORDERS },
        });
    }
    async finishDelivery(dto) {
        const trip = await this.tripService.findTripByUUID(dto.tripUUID);
        if (!trip.inProgress.includes(dto.orderUUID)) {
            return;
        }
        await this.tripService.updateTrip(trip.id, {
            inProgress: trip.inProgress.filter((orderUUID) => orderUUID !== dto.orderUUID),
            delivered: trip.delivered.concat([dto.orderUUID]),
        });
        console.log('ORDER_EVENT.FINISH_DELIVERY');
        const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
        const orderOwner = await this.tripEmitter.getAuthRecordUUID(dto.to);
        await this.tripEmitter.sendSocketMessage({
            uuid: tripOwner.uuid,
            payload: {
                action: events_2.SocketEvents.UPDATE_MY_TRIPS,
            },
        });
        await this.tripEmitter.sendSocketMessage({
            uuid: orderOwner.uuid,
            payload: {
                action: events_2.SocketEvents.UPDATE_MY_ORDERS,
            },
        });
        await this.tripEmitter.addUnratedUserRating({
            owner: tripOwner,
            orderId: dto.orderUUID,
            role: enums_1.UserDeliveryRole.TRAVELLER,
            ratedBy: orderOwner.id,
        });
        await this.tripEmitter.addUnratedUserRating({
            owner: orderOwner,
            orderId: dto.orderUUID,
            role: enums_1.UserDeliveryRole.SENDER,
            ratedBy: tripOwner.id,
        });
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.TRIP_EVENT.ADD_INCOMING_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripListener.prototype, "tripAddOutgoingRequest", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.TRIP_EVENT.CANCEL_INCOMING_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripListener.prototype, "tripCancelIncomingRequest", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.TRIP_EVENT.CANCEL_OUTGOING_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripListener.prototype, "tripCancelOutgoingRequest", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.TRIP_EVENT.ACCEPT_DELIVERY_BY_ORDER_OWNER, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripListener.prototype, "setOrderAcceptedByTraveler", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.TRIP_EVENT.CANCEL_ACCEPTED_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripListener.prototype, "tripCancelAcceptedRequest", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.TRIP_EVENT.GET_USER_TRIPS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], TripListener.prototype, "getAllUserTrips", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.ORDER_EVENT.START_DELIVERY, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripListener.prototype, "startDelivery", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.ORDER_EVENT.FINISH_DELIVERY, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TripListener.prototype, "finishDelivery", null);
TripListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.TripService,
        emitters_1.TripEmitter,
        services_1.TripHelperService])
], TripListener);
exports.TripListener = TripListener;
//# sourceMappingURL=trip.listener.js.map