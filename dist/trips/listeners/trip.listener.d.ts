import { TripService, TripHelperService } from '../services';
import { TripEmitter } from '../emitters';
export declare class TripListener {
    private readonly tripService;
    private readonly tripEmitter;
    private readonly helperService;
    constructor(tripService: TripService, tripEmitter: TripEmitter, helperService: TripHelperService);
    tripAddOutgoingRequest(dto: any): Promise<void>;
    tripCancelIncomingRequest(dto: any): Promise<void>;
    tripCancelOutgoingRequest(dto: any): Promise<void>;
    setOrderAcceptedByTraveler(dto: any): Promise<void>;
    tripCancelAcceptedRequest(dto: any): Promise<void>;
    getAllUserTrips(owner: number): Promise<any[]>;
    startDelivery(dto: any): Promise<void>;
    finishDelivery(dto: any): Promise<void>;
}
