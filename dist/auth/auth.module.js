"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const logger_1 = require("../utils/logger");
const config_1 = require("@nestjs/config");
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const axios_1 = require("@nestjs/axios");
const services_1 = require("./services");
const controllers_1 = require("./controllers");
const cache_module_1 = require("../common/modules/cache.module");
const entities_1 = require("./entities");
const emitters_1 = require("./emitters");
const listeners_1 = require("./listeners");
const morgan_middleware_1 = require("../common/middlewares/morgan.middleware");
const strategies_1 = require("./strategies");
let AuthModule = class AuthModule {
    configure(consumer) {
        consumer.apply(morgan_middleware_1.MorganMiddleware).forRoutes(controllers_1.AuthController);
    }
};
AuthModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([entities_1.AuthRecord, entities_1.Credential, entities_1.PhoneNumber]),
            jwt_1.JwtModule.registerAsync({
                imports: [config_1.ConfigModule],
                inject: [config_1.ConfigService],
                useFactory: async (configService) => ({
                    secret: configService.get('JWT_SECRET'),
                }),
            }),
            passport_1.PassportModule,
            cache_module_1.CacheRedisModule,
            logger_1.LoggerModule,
            axios_1.HttpModule,
        ],
        controllers: [controllers_1.AuthController],
        providers: [
            strategies_1.JwtStrategy,
            strategies_1.GuestStrategy,
            services_1.AuthService,
            services_1.CredentialService,
            services_1.CryptoService,
            emitters_1.AuthEmitter,
            listeners_1.AuthListener,
            services_1.PhoneNumberService,
            listeners_1.PhoneNumberListener,
        ],
        exports: [services_1.AuthService, jwt_1.JwtModule],
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map