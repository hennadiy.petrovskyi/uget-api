"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhoneDetailsDto = exports.SavePhoneDetailsDto = void 0;
const entities_1 = require("../entities");
const mapped_types_1 = require("@nestjs/mapped-types");
const class_transformer_1 = require("class-transformer");
class SavePhoneDetailsDto extends (0, mapped_types_1.PickType)(entities_1.PhoneNumber, [
    'code',
    'countryCode',
    'formatted',
    'number',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(SavePhoneDetailsDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.SavePhoneDetailsDto = SavePhoneDetailsDto;
class PhoneDetailsDto extends (0, mapped_types_1.PickType)(entities_1.PhoneNumber, [
    'code',
    'countryCode',
    'formatted',
    'number',
    'isVerified',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(PhoneDetailsDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.PhoneDetailsDto = PhoneDetailsDto;
//# sourceMappingURL=phone.dto.js.map