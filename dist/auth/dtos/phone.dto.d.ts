import { PhoneNumber } from '../entities';
declare const SavePhoneDetailsDto_base: import("@nestjs/mapped-types").MappedType<Pick<PhoneNumber, "number" | "formatted" | "countryCode" | "code">>;
export declare class SavePhoneDetailsDto extends SavePhoneDetailsDto_base {
    static buildDto(payload: any): SavePhoneDetailsDto;
}
declare const PhoneDetailsDto_base: import("@nestjs/mapped-types").MappedType<Pick<PhoneNumber, "number" | "formatted" | "countryCode" | "code" | "isVerified">>;
export declare class PhoneDetailsDto extends PhoneDetailsDto_base {
    static buildDto(payload: any): PhoneDetailsDto;
}
export {};
