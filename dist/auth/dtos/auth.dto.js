"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthTokensDto = exports.AuthVerifyCodeDto = exports.AuthGetEmailWithPhoneNumberDto = exports.AuthCreateUserDetailsDto = exports.AuthCreateCredentialsDto = exports.AuthCreateNewAuthRecordDto = exports.AuthRegisterUserDto = exports.AuthLoginByPhoneNumberDto = exports.AuthLoginByEmailDto = exports.AuthCheckPhoneNumberDto = exports.AuthCheckEmailDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const entities_1 = require("../entities");
class AuthCheckEmailDto extends (0, mapped_types_1.PickType)(entities_1.AuthRecord, ['email']) {
}
exports.AuthCheckEmailDto = AuthCheckEmailDto;
class AuthCheckPhoneNumberDto extends (0, mapped_types_1.PickType)(entities_1.AuthRecord, [
    'phoneNumber',
]) {
}
exports.AuthCheckPhoneNumberDto = AuthCheckPhoneNumberDto;
class AuthLoginByEmailDto extends (0, mapped_types_1.PickType)(entities_1.AuthRecord, ['email']) {
}
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthLoginByEmailDto.prototype, "password", void 0);
exports.AuthLoginByEmailDto = AuthLoginByEmailDto;
class AuthLoginByPhoneNumberDto extends (0, mapped_types_1.PickType)(entities_1.AuthRecord, [
    'phoneNumber',
]) {
}
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthLoginByPhoneNumberDto.prototype, "code", void 0);
exports.AuthLoginByPhoneNumberDto = AuthLoginByPhoneNumberDto;
class AuthRegisterUserDto extends (0, mapped_types_1.PickType)(entities_1.AuthRecord, ['email']) {
}
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthRegisterUserDto.prototype, "password", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthRegisterUserDto.prototype, "firstName", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthRegisterUserDto.prototype, "lastName", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthRegisterUserDto.prototype, "recaptcha", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Object)
], AuthRegisterUserDto.prototype, "phoneNumber", void 0);
exports.AuthRegisterUserDto = AuthRegisterUserDto;
class AuthCreateNewAuthRecordDto extends (0, mapped_types_1.PickType)(entities_1.AuthRecord, [
    'email',
    'phoneNumber',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(AuthCreateNewAuthRecordDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.AuthCreateNewAuthRecordDto = AuthCreateNewAuthRecordDto;
class AuthCreateCredentialsDto extends (0, mapped_types_1.PickType)(entities_1.Credential, [
    'owner',
    'password',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(AuthCreateCredentialsDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.AuthCreateCredentialsDto = AuthCreateCredentialsDto;
class AuthCreateUserDetailsDto {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(AuthCreateUserDetailsDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthCreateUserDetailsDto.prototype, "firstName", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthCreateUserDetailsDto.prototype, "lastName", void 0);
exports.AuthCreateUserDetailsDto = AuthCreateUserDetailsDto;
class AuthGetEmailWithPhoneNumberDto extends (0, mapped_types_1.PickType)(entities_1.AuthRecord, [
    'email',
    'phoneNumber',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(AuthGetEmailWithPhoneNumberDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.AuthGetEmailWithPhoneNumberDto = AuthGetEmailWithPhoneNumberDto;
class AuthVerifyCodeDto {
}
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], AuthVerifyCodeDto.prototype, "code", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AuthVerifyCodeDto.prototype, "phoneNumber", void 0);
exports.AuthVerifyCodeDto = AuthVerifyCodeDto;
class AuthTokensDto {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(AuthTokensDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AuthTokensDto.prototype, "accessToken", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AuthTokensDto.prototype, "refreshToken", void 0);
exports.AuthTokensDto = AuthTokensDto;
//# sourceMappingURL=auth.dto.js.map