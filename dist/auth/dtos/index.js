"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhoneDetailsDto = exports.SavePhoneDetailsDto = exports.AuthGetEmailWithPhoneNumberDto = exports.AuthCreateUserDetailsDto = exports.AuthLoginByEmailDto = exports.AuthCheckPhoneNumberDto = exports.AuthCheckEmailDto = exports.AuthTokensDto = exports.AuthVerifyCodeDto = exports.AuthCreateNewAuthRecordDto = exports.AuthRegisterUserDto = void 0;
var auth_dto_1 = require("./auth.dto");
Object.defineProperty(exports, "AuthRegisterUserDto", { enumerable: true, get: function () { return auth_dto_1.AuthRegisterUserDto; } });
Object.defineProperty(exports, "AuthCreateNewAuthRecordDto", { enumerable: true, get: function () { return auth_dto_1.AuthCreateNewAuthRecordDto; } });
Object.defineProperty(exports, "AuthVerifyCodeDto", { enumerable: true, get: function () { return auth_dto_1.AuthVerifyCodeDto; } });
Object.defineProperty(exports, "AuthTokensDto", { enumerable: true, get: function () { return auth_dto_1.AuthTokensDto; } });
Object.defineProperty(exports, "AuthCheckEmailDto", { enumerable: true, get: function () { return auth_dto_1.AuthCheckEmailDto; } });
Object.defineProperty(exports, "AuthCheckPhoneNumberDto", { enumerable: true, get: function () { return auth_dto_1.AuthCheckPhoneNumberDto; } });
Object.defineProperty(exports, "AuthLoginByEmailDto", { enumerable: true, get: function () { return auth_dto_1.AuthLoginByEmailDto; } });
Object.defineProperty(exports, "AuthCreateUserDetailsDto", { enumerable: true, get: function () { return auth_dto_1.AuthCreateUserDetailsDto; } });
Object.defineProperty(exports, "AuthGetEmailWithPhoneNumberDto", { enumerable: true, get: function () { return auth_dto_1.AuthGetEmailWithPhoneNumberDto; } });
var phone_dto_1 = require("./phone.dto");
Object.defineProperty(exports, "SavePhoneDetailsDto", { enumerable: true, get: function () { return phone_dto_1.SavePhoneDetailsDto; } });
Object.defineProperty(exports, "PhoneDetailsDto", { enumerable: true, get: function () { return phone_dto_1.PhoneDetailsDto; } });
//# sourceMappingURL=index.js.map