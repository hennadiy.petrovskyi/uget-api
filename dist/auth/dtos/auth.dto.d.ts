import { AuthRecord, Credential } from '../entities';
declare const AuthCheckEmailDto_base: import("@nestjs/mapped-types").MappedType<Pick<AuthRecord, "email">>;
export declare class AuthCheckEmailDto extends AuthCheckEmailDto_base {
}
declare const AuthCheckPhoneNumberDto_base: import("@nestjs/mapped-types").MappedType<Pick<AuthRecord, "phoneNumber">>;
export declare class AuthCheckPhoneNumberDto extends AuthCheckPhoneNumberDto_base {
}
declare const AuthLoginByEmailDto_base: import("@nestjs/mapped-types").MappedType<Pick<AuthRecord, "email">>;
export declare class AuthLoginByEmailDto extends AuthLoginByEmailDto_base {
    password: string;
}
declare const AuthLoginByPhoneNumberDto_base: import("@nestjs/mapped-types").MappedType<Pick<AuthRecord, "phoneNumber">>;
export declare class AuthLoginByPhoneNumberDto extends AuthLoginByPhoneNumberDto_base {
    code: string;
}
declare const AuthRegisterUserDto_base: import("@nestjs/mapped-types").MappedType<Pick<AuthRecord, "email">>;
export declare class AuthRegisterUserDto extends AuthRegisterUserDto_base {
    password: string;
    firstName: string;
    lastName: string;
    recaptcha: string;
    phoneNumber: {
        code: string;
        countryCode: string;
        number: string;
        formatted: string;
        isValid: boolean;
    };
}
declare const AuthCreateNewAuthRecordDto_base: import("@nestjs/mapped-types").MappedType<Pick<AuthRecord, "phoneNumber" | "email">>;
export declare class AuthCreateNewAuthRecordDto extends AuthCreateNewAuthRecordDto_base {
    static buildDto(payload: AuthRegisterUserDto): AuthCreateNewAuthRecordDto;
}
declare const AuthCreateCredentialsDto_base: import("@nestjs/mapped-types").MappedType<Pick<Credential, "owner" | "password">>;
export declare class AuthCreateCredentialsDto extends AuthCreateCredentialsDto_base {
    static buildDto(payload: AuthRegisterUserDto): AuthCreateCredentialsDto;
}
export declare class AuthCreateUserDetailsDto {
    firstName: string;
    lastName: string;
    static buildDto(payload: AuthRegisterUserDto): AuthCreateUserDetailsDto;
}
declare const AuthGetEmailWithPhoneNumberDto_base: import("@nestjs/mapped-types").MappedType<Pick<AuthRecord, "phoneNumber" | "email">>;
export declare class AuthGetEmailWithPhoneNumberDto extends AuthGetEmailWithPhoneNumberDto_base {
    static buildDto(payload: AuthRecord): AuthGetEmailWithPhoneNumberDto;
}
export declare class AuthVerifyCodeDto {
    code: number;
    phoneNumber: string;
}
export declare class AuthTokensDto {
    accessToken: string;
    refreshToken: string;
    static buildDto(payload: any): AuthTokensDto;
}
export {};
