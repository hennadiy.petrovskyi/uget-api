import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from '.';
export declare class Email extends BaseEntity {
    owner: AuthRecord;
    value: string;
    isVerified: boolean;
}
