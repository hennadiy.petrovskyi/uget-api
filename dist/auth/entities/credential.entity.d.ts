import { AuthRecord } from '.';
import { BaseEntity } from 'src/common/entities';
export declare class Credential extends BaseEntity {
    owner: AuthRecord;
    password: string;
}
