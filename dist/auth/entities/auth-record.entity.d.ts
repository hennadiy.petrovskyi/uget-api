import { BaseEntity } from 'src/common/entities';
import { Role } from '../enums/index';
export declare class AuthRecord extends BaseEntity {
    role: Role[];
    uuid: string;
    phoneNumber: string;
    email: string;
    isPhoneNumberVerified: boolean;
    isEmailVerified: boolean;
}
