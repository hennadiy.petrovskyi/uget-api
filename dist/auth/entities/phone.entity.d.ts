import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from '.';
export declare class PhoneNumber extends BaseEntity {
    owner: AuthRecord;
    formatted: string;
    countryCode: string;
    code: string;
    number: string;
    isVerified: boolean;
}
