"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const auth_record_entity_1 = require("./../entities/auth-record.entity");
const auth_emitter_1 = require("./../emitters/auth.emitter");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const services_1 = require("../services");
const dtos_1 = require("../dtos");
const role_enum_1 = require("../enums/role.enum");
const jwt_auth_guard_1 = require("../../common/guards/jwt-auth.guard");
const decorators_1 = require("../../common/decorators");
const axios_1 = require("@nestjs/axios");
const rxjs_1 = require("rxjs");
const ms = require('ms');
let AuthController = class AuthController {
    constructor(service, phoneNumberService, credentialService, cryptoService, authEmitter, httpService) {
        this.service = service;
        this.phoneNumberService = phoneNumberService;
        this.credentialService = credentialService;
        this.cryptoService = cryptoService;
        this.authEmitter = authEmitter;
        this.httpService = httpService;
    }
    async loginWithCredentials(authLoginByEmailDto) {
        const user = await this.service.findAuthRecordByEmail(authLoginByEmailDto.email);
        if (!user) {
            throw new common_1.HttpException({
                type: 404,
                error: `User with this email not found`,
                field: 'email',
            }, common_1.HttpStatus.NOT_FOUND);
        }
        const isPasswordMatch = await this.credentialService.matchCredential(user.id, authLoginByEmailDto.password);
        if (!isPasswordMatch) {
            throw new common_1.HttpException({
                type: 'wrongPassword',
                error: `Wrong password`,
                field: 'password',
            }, common_1.HttpStatus.FORBIDDEN);
        }
        const tokens = await this.cryptoService.createAccessToken(user.uuid, role_enum_1.Role.USER);
        const profile = await this.buildUserProfileDto(user);
        return { profile, session: tokens };
    }
    async registerNewUser(authRegisterUserDto, response) {
        const isEmailInUse = await this.service.findAuthRecordByEmail(authRegisterUserDto.email);
        const isPhoneNumberInUse = await this.phoneNumberService.findPhoneNumberByNumber(authRegisterUserDto.phoneNumber.formatted);
        const errors = [];
        if (isEmailInUse) {
            errors.push({
                field: 'email',
                type: 'exist',
                message: 'This email already in use',
            });
        }
        if (isPhoneNumberInUse) {
            errors.push({
                field: 'phoneNumber',
                type: 'exist',
                message: 'This phone number already in use',
            });
        }
        if (errors.length) {
            throw new common_1.HttpException({
                errors,
            }, common_1.HttpStatus.FORBIDDEN);
        }
        const { phoneNumber } = authRegisterUserDto, createAuthRecordDTO = __rest(authRegisterUserDto, ["phoneNumber"]);
        const user = await this.service.createNewUserAuthRecord(Object.assign(Object.assign({}, createAuthRecordDTO), { phoneNumber: phoneNumber.formatted }));
        await this.phoneNumberService.savePhoneNumber(Object.assign({ owner: user }, dtos_1.SavePhoneDetailsDto.buildDto(phoneNumber)));
        await this.credentialService.createNewCredential({
            owner: user,
            password: authRegisterUserDto.password,
        });
        const userDetails = dtos_1.AuthCreateUserDetailsDto.buildDto(authRegisterUserDto);
        await this.authEmitter.createUserDetails({
            owner: user,
            authCreateUserDetailsDto: userDetails,
        });
        await this.authEmitter.sendVerificationCode({
            phoneNumber: phoneNumber.formatted,
        });
        const verifyEmailToken = await this.cryptoService.generatePasswordResetToken(user.uuid);
        const link = `https://api.uget.co/redirect/resend/email/${verifyEmailToken}`;
        await this.authEmitter.sendVerifyEmail({ email: user.email, link });
        const tokens = await this.cryptoService.createAccessToken(user.uuid, role_enum_1.Role.USER);
        const profile = await this.buildUserProfileDto(user);
        return response.status(201).json({ profile, session: tokens });
    }
    async logout(refreshToken) {
        if (refreshToken) {
            return await this.cryptoService.deleteSession(refreshToken);
        }
        return;
    }
    async verifyEmail(verifyEmailDto) {
        const payload = (await this.cryptoService.decodeToken(verifyEmailDto.token));
        const user = await this.service.findAuthRecordByUUID(payload.uuid);
        if (!user) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.NOT_FOUND,
                error: `User with this email not found`,
                field: 'email',
            }, common_1.HttpStatus.NOT_FOUND);
        }
        await this.service.verifyUserEmail(user.id);
        const userUpdated = await this.service.findAuthRecordByUUID(user.uuid);
        const profile = await this.buildUserProfileDto(userUpdated);
        const response = { profile };
        return response;
    }
    async verifyIsUserWithPhoneNumberExist(authCheckPhoneNumberDto) {
        const user = await this.service.findAuthRecordByPhoneNumber(authCheckPhoneNumberDto.phoneNumber);
        if (!user) {
            throw new common_1.HttpException({
                error: `User with this phone number not found`,
                type: 404,
                field: 'phoneNumber',
            }, common_1.HttpStatus.NOT_FOUND);
        }
        const smsCodeResponse = await this.authEmitter.sendVerificationCode(authCheckPhoneNumberDto);
        if (!smsCodeResponse) {
            throw new common_1.HttpException({
                message: 'Failed to send sms code',
            }, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    async verifyUserSMSCode(authVerifyCodeDto) {
        console.log(authVerifyCodeDto);
        const user = await this.service.findAuthRecordByPhoneNumber(authVerifyCodeDto.phoneNumber);
        const dbVerificationData = await this.authEmitter.verifyCode(authVerifyCodeDto);
        if (dbVerificationData.code !== authVerifyCodeDto.code) {
            throw new common_1.HttpException({
                field: 'sms',
                type: '404',
                message: 'Sms code is not found or expired',
            }, common_1.HttpStatus.NOT_FOUND);
        }
        if (!user.isPhoneNumberVerified) {
            await this.service.verifyUserPhoneNumber(user.id);
        }
        const tokens = await this.cryptoService.createAccessToken(user.uuid, role_enum_1.Role.USER);
        const userUpdated = await this.service.findAuthRecordByUUID(user.uuid);
        const profile = await this.buildUserProfileDto(userUpdated);
        const response = { profile, session: tokens };
        return response;
    }
    async verifyRecaptcha(verifyRecaptchaDto) {
        const verificationUrl = 'https://www.google.com/recaptcha/api/siteverify';
        const googleResponse = await (0, rxjs_1.lastValueFrom)(this.httpService.post(verificationUrl, {}, {
            params: {
                secret: '6Lfd8cIdAAAAADw36RC9t8TtflxIJ-_Hah9S-fEv',
                response: verifyRecaptchaDto.token,
            },
        }));
        return googleResponse.data;
    }
    async updateDetails(user, updateDetailsDto) {
        const { phoneNumber, email } = updateDetailsDto, userDetailsUpdate = __rest(updateDetailsDto, ["phoneNumber", "email"]);
        if (phoneNumber) {
            const isPhoneNumberInUse = await this.phoneNumberService.findPhoneNumberByNumber(phoneNumber.formatted);
            if (isPhoneNumberInUse) {
                throw new common_1.HttpException({
                    field: 'phoneNumber',
                    type: 'exist',
                    message: 'This phone number already in use',
                }, common_1.HttpStatus.FORBIDDEN);
            }
            const phoneNumberDetails = await this.phoneNumberService.findPhoneNumberByOwner(user.id);
            await this.phoneNumberService.updatePhoneNumberDetails(phoneNumberDetails.id, dtos_1.SavePhoneDetailsDto.buildDto(phoneNumber));
            await this.service.updateUserPhoneNumber(user.id, phoneNumber.formatted);
        }
        if (email) {
            const isEmailInUse = await this.service.findAuthRecordByEmail(email);
            if (isEmailInUse) {
                throw new common_1.HttpException({
                    field: 'email',
                    type: 'exist',
                    message: 'This email already in use',
                }, common_1.HttpStatus.FORBIDDEN);
            }
            await this.service.updateUserEmail(user.id, email);
        }
        if (Object.values(userDetailsUpdate)) {
            await this.authEmitter.updateUserDetails(Object.assign({ owner: user.id }, userDetailsUpdate));
        }
        const userUpdated = await this.service.findAuthRecordByUUID(user.uuid);
        const profile = await this.buildUserProfileDto(userUpdated);
        const response = { profile };
        return response;
    }
    async refreshSession(refreshToken) {
        if (!refreshToken) {
            throw new common_1.UnauthorizedException();
        }
        return await this.cryptoService.refreshSession(refreshToken);
    }
    async resetPassword(resetPasswordDto) {
        const authRecord = await this.service.findAuthRecordByEmail(resetPasswordDto);
        if (!authRecord) {
            throw new common_1.HttpException({
                type: common_1.HttpStatus.NOT_FOUND,
                error: `User with this email not found`,
                field: 'email',
            }, common_1.HttpStatus.NOT_FOUND);
        }
        const resetToken = await this.cryptoService.generateVerifyEmailToken(authRecord.uuid, authRecord.email);
        const link = `https://api.uget.co/redirect/reset-password/${resetToken}`;
        await this.authEmitter.sendResetPasswordEmail({
            email: authRecord.email,
            link,
        });
    }
    async resendSmsCode(phoneNumber) {
        const smsCodeResponse = await this.authEmitter.sendVerificationCode({
            phoneNumber,
        });
        if (!smsCodeResponse) {
            throw new common_1.HttpException({
                message: 'Failed to send sms code',
            }, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    async resendEmail(email) {
        const authRecord = await this.service.findAuthRecordByEmail(email);
        if (!authRecord) {
            throw new common_1.HttpException({
                message: 'User with this email was not found',
            }, common_1.HttpStatus.FORBIDDEN);
        }
        const verifyEmailToken = await this.cryptoService.generatePasswordResetToken(authRecord.uuid);
        const link = `https://api.uget.co/redirect/resend/email/${verifyEmailToken}`;
        await this.authEmitter.sendVerifyEmail({ email: authRecord.email, link });
    }
    async changePhoneNumber(user, phoneNumber) {
        const authRecord = await this.service.findAuthRecordByPhoneNumber(phoneNumber.formatted);
        if (authRecord && authRecord.id === user.id) {
            throw new common_1.HttpException({
                field: 'phoneNumber',
                type: 'same',
                message: 'Nothing to change , number are same',
            }, common_1.HttpStatus.BAD_REQUEST);
        }
        if (authRecord && authRecord.id !== user.id) {
            throw new common_1.HttpException({
                field: 'phoneNumber',
                type: 'exist',
                message: 'This phone number already in use',
            }, common_1.HttpStatus.BAD_REQUEST);
        }
        const updatedUser = await this.service.updateUserPhoneNumber(user.id, phoneNumber.formatted);
        const userPhoneNumberDetails = await this.phoneNumberService.findPhoneNumberByOwner(user.id);
        await this.phoneNumberService.updatePhoneNumberDetails(userPhoneNumberDetails.id, phoneNumber);
        await this.authEmitter.sendVerificationCode({
            phoneNumber: phoneNumber.formatted,
        });
        return { phoneNumber: updatedUser.phoneNumber };
    }
    async changeEmail(user, email) {
        const authRecord = await this.service.findAuthRecordByEmail(email);
        if (authRecord && authRecord.id === user.id) {
            throw new common_1.HttpException({
                field: 'email',
                type: 'same',
                message: 'Nothing to change , email are same',
            }, common_1.HttpStatus.BAD_REQUEST);
        }
        if (authRecord && authRecord.id !== user.id) {
            throw new common_1.HttpException({
                field: 'email',
                type: 'exist',
                message: 'This email already in use',
            }, common_1.HttpStatus.BAD_REQUEST);
        }
        const updatedUser = await this.service.updateUserEmail(user.id, email);
        const verifyEmailToken = await this.cryptoService.generatePasswordResetToken(authRecord.uuid);
        const link = `https://api.uget.co/redirect/resend/email/${verifyEmailToken}`;
        await this.authEmitter.sendVerifyEmail({ email, link });
        return { email: updatedUser.email };
    }
    async changePassword(changePasswordDto) {
        const tokenPayload = (await this.cryptoService.decodeToken(changePasswordDto.token));
        const isPasswordRequestTokenRequestExist = await this.cryptoService.findPasswordResetToken(tokenPayload.uuid);
        if (!isPasswordRequestTokenRequestExist &&
            isPasswordRequestTokenRequestExist !== changePasswordDto.token) {
            throw new common_1.ForbiddenException();
        }
        const user = await this.service.findAuthRecordByUUID(tokenPayload.uuid);
        const hashedPassword = await this.cryptoService.hashPassword(changePasswordDto.password);
        await this.credentialService.updatePassword(user.id, {
            password: hashedPassword,
        });
    }
    async getUserProfile(user) {
        return await this.buildUserProfileDto(user);
    }
    async buildUserProfileDto(user) {
        const userDetails = await this.authEmitter.getUserDetails(user.id);
        const profileImage = await this.authEmitter.getUserProfileImage(user.uuid);
        const phoneNumberDetails = await this.phoneNumberService.findPhoneNumberByOwner(user.id);
        const orders = await this.authEmitter.getUserOrders(user.id);
        const trips = await this.authEmitter.getUserTrips(user.id);
        const notificationSettings = await this.authEmitter.getNotificationSettings(user.id);
        const conversations = await this.authEmitter.getUserConversations(user.uuid);
        const response = Object.assign(Object.assign({}, userDetails), { createdAt: user.createdAt, uuid: user.uuid, profileImage, phoneNumber: Object.assign(Object.assign({}, dtos_1.PhoneDetailsDto.buildDto(phoneNumberDetails)), { isVerified: user.isPhoneNumberVerified }), email: {
                value: user.email,
                isVerified: user.isEmailVerified,
            }, orders,
            trips,
            notificationSettings,
            conversations });
        return response;
    }
};
__decorate([
    (0, common_1.Post)('login'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.AuthLoginByEmailDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "loginWithCredentials", null);
__decorate([
    (0, common_1.Post)('register'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.AuthRegisterUserDto, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "registerNewUser", null);
__decorate([
    (0, common_1.Get)('logout'),
    __param(0, (0, common_1.Headers)('RefreshToken')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logout", null);
__decorate([
    (0, common_1.Post)('verify/email'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "verifyEmail", null);
__decorate([
    (0, common_1.Post)('verify/phone'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.AuthCheckPhoneNumberDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "verifyIsUserWithPhoneNumberExist", null);
__decorate([
    (0, common_1.Post)('verify/code'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.AuthVerifyCodeDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "verifyUserSMSCode", null);
__decorate([
    (0, common_1.Post)('verify/recaptcha'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "verifyRecaptcha", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Put)('profile'),
    __param(0, (0, decorators_1.User)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [auth_record_entity_1.AuthRecord, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "updateDetails", null);
__decorate([
    (0, common_1.Get)('refresh-session'),
    __param(0, (0, common_1.Headers)('RefreshToken')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "refreshSession", null);
__decorate([
    (0, common_1.Post)('reset-password'),
    __param(0, (0, common_1.Body)('email')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "resetPassword", null);
__decorate([
    (0, common_1.Post)('resend/sms-code'),
    __param(0, (0, common_1.Body)('phoneNumber')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "resendSmsCode", null);
__decorate([
    (0, common_1.Post)('resend/email'),
    __param(0, (0, common_1.Body)('email')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "resendEmail", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('change/phone'),
    __param(0, (0, decorators_1.User)()),
    __param(1, (0, common_1.Body)('phoneNumber')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [auth_record_entity_1.AuthRecord, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "changePhoneNumber", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('change/email'),
    __param(0, (0, decorators_1.User)()),
    __param(1, (0, common_1.Body)('email')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [auth_record_entity_1.AuthRecord, String]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "changeEmail", null);
__decorate([
    (0, common_1.Post)('change/password'),
    __param(0, (0, common_1.Body)('')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "changePassword", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Get)('/profile'),
    __param(0, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [auth_record_entity_1.AuthRecord]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "getUserProfile", null);
AuthController = __decorate([
    (0, swagger_1.ApiTags)('Authentication'),
    (0, common_1.Controller)('auth'),
    __metadata("design:paramtypes", [services_1.AuthService,
        services_1.PhoneNumberService,
        services_1.CredentialService,
        services_1.CryptoService,
        auth_emitter_1.AuthEmitter,
        axios_1.HttpService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map