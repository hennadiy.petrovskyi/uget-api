import { AuthRecord } from './../entities/auth-record.entity';
import { AuthEmitter } from './../emitters/auth.emitter';
import { AuthService, CryptoService, CredentialService, PhoneNumberService } from '../services';
import { AuthCheckPhoneNumberDto, AuthRegisterUserDto, AuthVerifyCodeDto, AuthLoginByEmailDto } from '../dtos';
import e, { Response } from 'express';
import { HttpService } from '@nestjs/axios';
export declare class AuthController {
    private service;
    private phoneNumberService;
    private credentialService;
    private cryptoService;
    private authEmitter;
    private httpService;
    constructor(service: AuthService, phoneNumberService: PhoneNumberService, credentialService: CredentialService, cryptoService: CryptoService, authEmitter: AuthEmitter, httpService: HttpService);
    loginWithCredentials(authLoginByEmailDto: AuthLoginByEmailDto): Promise<{
        profile: any;
        session: import("../dtos").AuthTokensDto;
    }>;
    registerNewUser(authRegisterUserDto: AuthRegisterUserDto, response: Response): Promise<e.Response<any, Record<string, any>>>;
    logout(refreshToken: string): Promise<void>;
    verifyEmail(verifyEmailDto: {
        token: string;
    }): Promise<{
        profile: any;
    }>;
    verifyIsUserWithPhoneNumberExist(authCheckPhoneNumberDto: AuthCheckPhoneNumberDto): Promise<void>;
    verifyUserSMSCode(authVerifyCodeDto: AuthVerifyCodeDto): Promise<{
        profile: any;
        session: import("../dtos").AuthTokensDto;
    }>;
    verifyRecaptcha(verifyRecaptchaDto: any): Promise<any>;
    updateDetails(user: AuthRecord, updateDetailsDto: any): Promise<{
        profile: any;
    }>;
    refreshSession(refreshToken: string): Promise<{
        accessToken: string;
        refreshToken: string;
    }>;
    resetPassword(resetPasswordDto: any): Promise<void>;
    resendSmsCode(phoneNumber: any): Promise<void>;
    resendEmail(email: any): Promise<void>;
    changePhoneNumber(user: AuthRecord, phoneNumber: any): Promise<{
        phoneNumber: any;
    }>;
    changeEmail(user: AuthRecord, email: string): Promise<{
        email: any;
    }>;
    changePassword(changePasswordDto: any): Promise<void>;
    getUserProfile(user: AuthRecord): Promise<any>;
    buildUserProfileDto(user: AuthRecord): Promise<any>;
}
