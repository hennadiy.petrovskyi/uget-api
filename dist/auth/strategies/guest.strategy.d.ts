import { Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { AuthEmitter } from '../emitters';
declare const GuestStrategy_base: new (...args: any[]) => Strategy;
export declare class GuestStrategy extends GuestStrategy_base {
    private readonly configService;
    private readonly authEmitter;
    constructor(configService: ConfigService, authEmitter: AuthEmitter);
    validate(payload: any): Promise<{
        id: number;
    }>;
}
export {};
