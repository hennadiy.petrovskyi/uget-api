"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GuestStrategy = void 0;
const passport_jwt_1 = require("passport-jwt");
const passport_1 = require("@nestjs/passport");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const emitters_1 = require("../emitters");
let GuestStrategy = class GuestStrategy extends (0, passport_1.PassportStrategy)(passport_jwt_1.Strategy, 'JWT_GUEST') {
    constructor(configService, authEmitter) {
        super({
            ignoreExpiration: false,
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromHeader('authorization'),
            secretOrKey: process.env.JWT_SECRET,
        });
        this.configService = configService;
        this.authEmitter = authEmitter;
    }
    async validate(payload) {
        try {
            const { sub } = payload;
            return sub;
        }
        catch (e) {
            console.log(e);
        }
    }
};
GuestStrategy = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService,
        emitters_1.AuthEmitter])
], GuestStrategy);
exports.GuestStrategy = GuestStrategy;
//# sourceMappingURL=guest.strategy.js.map