import { Role } from '../enums';
export declare const ROLES_KEYS = "roles";
export declare const Roles: (...args: Role[]) => import("@nestjs/common").CustomDecorator<string>;
