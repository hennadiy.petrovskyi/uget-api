"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Permissions = exports.ROLES_KEYS = exports.Roles = void 0;
var roles_decorator_1 = require("./roles.decorator");
Object.defineProperty(exports, "Roles", { enumerable: true, get: function () { return roles_decorator_1.Roles; } });
Object.defineProperty(exports, "ROLES_KEYS", { enumerable: true, get: function () { return roles_decorator_1.ROLES_KEYS; } });
var permissions_decorator_1 = require("./permissions.decorator");
Object.defineProperty(exports, "Permissions", { enumerable: true, get: function () { return permissions_decorator_1.Permissions; } });
//# sourceMappingURL=index.js.map