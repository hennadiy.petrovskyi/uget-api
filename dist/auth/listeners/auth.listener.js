"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_1 = require("../../common/events");
const services_1 = require("../services");
const typeorm_1 = require("typeorm");
let AuthListener = class AuthListener {
    constructor(authService) {
        this.authService = authService;
    }
    async getUserCredentials(owner) {
        return await this.authService.getEmailWithPhoneNumber(owner);
    }
    async getUserByUUID(uuid) {
        return await this.authService.findAuthRecordByUUID(uuid);
    }
    async getUserByID(id) {
        const authRecord = await this.authService.findAuthRecordById(id);
        if (!authRecord) {
            return;
        }
        return authRecord;
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.USER_EVENTS.GET_AUTH_CREDENTIALS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], AuthListener.prototype, "getUserCredentials", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.AUTH_EVENTS.GET_USER_BY_UUID, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AuthListener.prototype, "getUserByUUID", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.AUTH_EVENTS.GET_USER_BY_ID, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeorm_1.FindOperator]),
    __metadata("design:returntype", Promise)
], AuthListener.prototype, "getUserByID", null);
AuthListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.AuthService])
], AuthListener);
exports.AuthListener = AuthListener;
//# sourceMappingURL=auth.listener.js.map