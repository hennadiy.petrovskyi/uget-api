import { AuthService } from '../services';
import { FindOperator } from 'typeorm';
export declare class AuthListener {
    private readonly authService;
    constructor(authService: AuthService);
    getUserCredentials(owner: number): Promise<import("../dtos").AuthGetEmailWithPhoneNumberDto>;
    getUserByUUID(uuid: string): Promise<import("../entities").AuthRecord>;
    getUserByID(id: FindOperator<any>): Promise<any>;
}
