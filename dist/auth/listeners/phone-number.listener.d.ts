import { PhoneNumberService } from '../services';
export declare class PhoneNumberListener {
    private readonly phoneNumberService;
    constructor(phoneNumberService: PhoneNumberService);
    savePhoneNumberDetails(phoneNumberToSaveDto: any): Promise<any>;
    getPhoneNumberDetails(phoneNumberToGetDto: any): Promise<import("../entities").PhoneNumber>;
    deletePhoneNumberDetails(phoneNumberToDeleteDto: any): Promise<void>;
}
