import { EventEmitter2 } from '@nestjs/event-emitter';
import { AuthCreateUserDetailsDto, AuthVerifyCodeDto } from '../dtos';
import { AuthRecord } from '../../auth/entities/auth-record.entity';
export declare class AuthEmitter {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    sendVerificationCode(sendVerificationCodeDto: any): Promise<any[]>;
    verifyCode(authVerifyCodeDto: AuthVerifyCodeDto): Promise<any>;
    createUserDetails({ owner, authCreateUserDetailsDto, }: {
        owner: AuthRecord;
        authCreateUserDetailsDto: AuthCreateUserDetailsDto;
    }): Promise<any[]>;
    getUserDetails(owner: any): Promise<any>;
    updateUserDetails(updateDto: any): Promise<any>;
    getUserProfileImage(owner: string): Promise<any>;
    getUserOrders(owner: number): Promise<any>;
    getUserTrips(owner: number): Promise<any>;
    getNotificationSettings(owner: number): Promise<any>;
    getUserConversations(owner: string): Promise<any>;
    sendResetPasswordEmail(sendResetPasswordEmailDto: {
        email: string;
        link: string;
    }): Promise<void>;
    sendVerifyEmail(sendResetPasswordEmailDto: {
        email: string;
        link: string;
    }): Promise<void>;
}
