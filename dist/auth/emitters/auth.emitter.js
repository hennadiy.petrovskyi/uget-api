"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthEmitter = void 0;
const events_constants_1 = require("./../../common/events/events-constants");
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_1 = require("../../common/events");
const enums_1 = require("../../file/enums");
let AuthEmitter = class AuthEmitter {
    constructor(eventEmitter) {
        this.eventEmitter = eventEmitter;
    }
    async sendVerificationCode(sendVerificationCodeDto) {
        return await this.eventEmitter.emitAsync(events_1.AUTH_EVENTS.SEND_CONFIRMATION_CODE, sendVerificationCodeDto);
    }
    async verifyCode(authVerifyCodeDto) {
        const response = await this.eventEmitter.emitAsync(events_1.AUTH_EVENTS.VERIFY_CONFIRMATION_CODE, authVerifyCodeDto);
        if (!response.length) {
            return;
        }
        return response[0];
    }
    async createUserDetails({ owner, authCreateUserDetailsDto, }) {
        return await this.eventEmitter.emitAsync(events_constants_1.USER_EVENTS.CREATE_USER_DETAILS, Object.assign({ owner }, authCreateUserDetailsDto));
    }
    async getUserDetails(owner) {
        const details = await this.eventEmitter.emitAsync(events_constants_1.USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID, owner);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async updateUserDetails(updateDto) {
        const details = await this.eventEmitter.emitAsync(events_constants_1.USER_EVENTS.UPDATE_USER_DETAILS, updateDto);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async getUserProfileImage(owner) {
        const file = await this.eventEmitter.emitAsync(events_constants_1.FILE_EVENTS.GET_FILE_BY_TYPE, { owner, type: enums_1.FileType.PROFILE_IMAGE });
        if (file && file.length) {
            return file[0];
        }
        return null;
    }
    async getUserOrders(owner) {
        const details = await this.eventEmitter.emitAsync(events_1.ORDER_EVENT.GET_USER_ORDERS, owner);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async getUserTrips(owner) {
        const details = await this.eventEmitter.emitAsync(events_constants_1.TRIP_EVENT.GET_USER_TRIPS, owner);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async getNotificationSettings(owner) {
        const details = await this.eventEmitter.emitAsync(events_constants_1.SETTINGS.GET_USER_NOTIFICATION_SETTINGS, owner);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async getUserConversations(owner) {
        const details = await this.eventEmitter.emitAsync(events_constants_1.CHAT_EVENTS.GET_USER_CONVERSATIONS, owner);
        if (details && details.length) {
            return details[0];
        }
        return details;
    }
    async sendResetPasswordEmail(sendResetPasswordEmailDto) {
        await this.eventEmitter.emitAsync(events_constants_1.EMAIL_EVENTS.SEND_RESET_PASSWORD, sendResetPasswordEmailDto);
    }
    async sendVerifyEmail(sendResetPasswordEmailDto) {
        await this.eventEmitter.emitAsync(events_constants_1.EMAIL_EVENTS.SEND_VERIFICATION, sendResetPasswordEmailDto);
    }
};
AuthEmitter = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [event_emitter_1.EventEmitter2])
], AuthEmitter);
exports.AuthEmitter = AuthEmitter;
//# sourceMappingURL=auth.emitter.js.map