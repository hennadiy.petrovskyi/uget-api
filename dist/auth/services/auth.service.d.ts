import { FindOperator, Repository } from 'typeorm';
import { LoggerService } from '../../utils/logger/app.logger';
import { AuthRecord } from '../entities';
import { AuthGetEmailWithPhoneNumberDto } from '../dtos';
export declare class AuthService {
    private authRepository;
    private logger;
    constructor(authRepository: Repository<AuthRecord>, logger: LoggerService);
    findAuthRecordById(id: FindOperator<any>): Promise<any>;
    findAuthRecordByEmail(email: string): Promise<any>;
    findAuthRecordByPhoneNumber(phoneNumber: string): Promise<AuthRecord>;
    findAuthRecordByUUID(uuid: string): Promise<AuthRecord>;
    createNewUserAuthRecord(authRegisterUserDto: any): Promise<{
        uuid: any;
        phoneNumber: string;
        email: string;
    } & AuthRecord>;
    verifyUserPhoneNumber(userId: any): Promise<any>;
    verifyUserEmail(userId: any): Promise<any>;
    getEmailWithPhoneNumber(userId: any): Promise<AuthGetEmailWithPhoneNumberDto>;
    updateUserPhoneNumber(userId: any, phoneNumber: any): Promise<any>;
    updateUserEmail(userId: any, email: any): Promise<any>;
}
