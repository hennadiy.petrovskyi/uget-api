import { JwtService } from '@nestjs/jwt';
import { AuthTokensDto } from 'src/auth/dtos';
import { Role } from 'src/auth/enums';
import { CacheService } from 'src/common/services/cache.service';
interface ISession {
    uuid: string;
    userUUID: string;
    accessToken: string;
    refreshToken: string;
    expireIn: number;
    updatedAt: number;
    role: Role;
}
export declare class CryptoService {
    private jwt;
    private cacheService;
    constructor(jwt: JwtService, cacheService: CacheService);
    private get ttl();
    decodeToken(token: string): string | {
        [key: string]: any;
    };
    verifyToken(token: string): Promise<any>;
    hashPassword(password: string): Promise<string>;
    comparePasswords(newPassword: string, passwordHash: string): Promise<boolean>;
    createAccessToken(sub: string, role: Role): Promise<AuthTokensDto>;
    findUserSessions(sub: string): Promise<ISession[] | undefined>;
    findUserSessionsOrCreate(sub: string): Promise<ISession[]>;
    generatePasswordResetToken(uuid: string): Promise<string>;
    generateVerifyEmailToken(uuid: string, email: any): Promise<string>;
    findPasswordResetToken(uuid: string): Promise<unknown>;
    generateSession(sub: string, role: Role): Promise<ISession>;
    addNewSession(sub: string, newSession: ISession): Promise<ISession>;
    checkTokenBySession(sub: string, accessToken: string): Promise<ISession>;
    deleteSession(token: string): Promise<void>;
    refreshSession(refreshToken: string): Promise<{
        accessToken: string;
        refreshToken: string;
    }>;
}
export {};
