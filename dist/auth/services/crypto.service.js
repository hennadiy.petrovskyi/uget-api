"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CryptoService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const bcrypt = require("bcryptjs");
const dtos_1 = require("../dtos");
const cache_service_1 = require("../../common/services/cache.service");
const uuid_1 = require("uuid");
const ms = require('ms');
let CryptoService = class CryptoService {
    constructor(jwt, cacheService) {
        this.jwt = jwt;
        this.cacheService = cacheService;
    }
    get ttl() {
        const timestamp = Date.now();
        return {
            accessToken: { expiresIn: process.env.JWT_ACCESS_TOKEN || '10m' },
            refreshToken: { expiresIn: process.env.JWT_REFRESH_TOKEN || '28d' },
            passwordResetToken: { expiresIn: '5m' },
            resetPasswordCache: ms('5m'),
            session: ms(process.env.JWT_REFRESH_TOKEN) + timestamp,
            cache: ms(process.env.JWT_REFRESH_TOKEN),
        };
    }
    decodeToken(token) {
        return this.jwt.decode(token, { json: true });
    }
    async verifyToken(token) {
        const isValid = this.jwt.verify(token);
        return isValid;
    }
    async hashPassword(password) {
        const saltOrRounds = 10;
        return bcrypt.hash(password, saltOrRounds);
    }
    async comparePasswords(newPassword, passwordHash) {
        return bcrypt.compare(newPassword, passwordHash);
    }
    async createAccessToken(sub, role) {
        await this.cacheService.delete(sub);
        const session = await this.generateSession(sub, role);
        const currentSession = await this.addNewSession(sub, session);
        return dtos_1.AuthTokensDto.buildDto({
            accessToken: currentSession.accessToken,
            refreshToken: currentSession.refreshToken,
        });
    }
    async findUserSessions(sub) {
        const functionName = this.findUserSessions.name;
        try {
            const result = (await this.cacheService.get(sub));
            return result ? result.sessions : undefined;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: 'Failed to find user session',
            });
        }
    }
    async findUserSessionsOrCreate(sub) {
        const functionName = this.findUserSessionsOrCreate.name;
        try {
            const userSessions = (await this.cacheService.get(sub));
            if (!userSessions) {
                await this.cacheService.set(sub, { sessions: [] }, this.ttl.cache);
                const createdUserSessions = (await this.cacheService.get(sub));
                return createdUserSessions.sessions;
            }
            return userSessions.sessions;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: 'Failed to find or create users sessions',
            });
        }
    }
    async generatePasswordResetToken(uuid) {
        const functionName = this.generatePasswordResetToken.name;
        try {
            const accessToken = await this.jwt.signAsync({
                uuid,
            }, { expiresIn: '5m' });
            await this.cacheService.set(`${uuid}_RESET_PASSWORD`, accessToken, this.ttl.resetPasswordCache);
            return accessToken;
        }
        catch (error) { }
    }
    async generateVerifyEmailToken(uuid, email) {
        const functionName = this.generateVerifyEmailToken.name;
        try {
            const token = await this.jwt.signAsync({
                uuid,
                email,
            }, { expiresIn: '5m' });
            return token;
        }
        catch (error) { }
    }
    async findPasswordResetToken(uuid) {
        const functionName = this.generatePasswordResetToken.name;
        try {
            return await this.cacheService.get(`${uuid}_RESET_PASSWORD`);
        }
        catch (error) { }
    }
    async generateSession(sub, role) {
        const functionName = this.generateSession.name;
        const sessionID = (0, uuid_1.v4)();
        try {
            const accessToken = await this.jwt.signAsync({
                sub,
                role,
            }, this.ttl.accessToken);
            const refreshToken = await this.jwt.signAsync({
                sub,
                uuid: sessionID,
            }, this.ttl.refreshToken);
            return {
                uuid: sessionID,
                userUUID: sub,
                expireIn: this.ttl.session,
                accessToken,
                refreshToken,
                updatedAt: null,
                role,
            };
        }
        catch (error) {
            console.log(error);
            throw new common_1.InternalServerErrorException({
                message: 'Failed to generate session',
            });
        }
    }
    async addNewSession(sub, newSession) {
        const functionName = this.addNewSession.name;
        const maxSessionsAllowed = 3;
        try {
            const sessions = await this.findUserSessionsOrCreate(sub);
            if (sessions.length === maxSessionsAllowed) {
                const update = (await this.cacheService.set(sub, { sessions: [newSession] }, this.ttl.cache));
                return update.sessions.find((session) => session.uuid === newSession.uuid);
            }
            sessions.push(newSession);
            const update = (await this.cacheService.set(sub, { sessions }, this.ttl.cache));
            return update.sessions.find((session) => session.uuid === newSession.uuid);
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: 'Failed to update update user sessions',
            });
        }
    }
    async checkTokenBySession(sub, accessToken) {
        const functionName = this.checkTokenBySession.name;
        try {
            const userSessions = await this.findUserSessions(sub);
            const session = userSessions.find((session) => session.accessToken === accessToken);
            if (!userSessions || !session) {
                throw new common_1.UnauthorizedException();
            }
            return session;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: 'Failed to check token by session',
            });
        }
    }
    async deleteSession(token) {
        const functionName = this.deleteSession.name;
        const { sub } = this.jwt.decode(token);
        try {
            const userSessions = (await this.cacheService.get(sub));
            const update = userSessions.sessions.filter((session) => session.accessToken !== token);
            await this.cacheService.set(sub, { sessions: update }, this.ttl.cache);
        }
        catch (error) {
            throw new common_1.InternalServerErrorException({
                message: 'Failed to delete session',
            });
        }
    }
    async refreshSession(refreshToken) {
        const isTokenVerified = await this.jwt.verify(refreshToken);
        if (!isTokenVerified) {
            throw new common_1.UnauthorizedException();
        }
        const { sub, uuid } = this.jwt.decode(refreshToken);
        const sessions = await this.findUserSessions(sub);
        const currentSession = await (sessions === null || sessions === void 0 ? void 0 : sessions.find((session) => session.uuid === uuid && session.refreshToken === refreshToken));
        if (!sessions || !currentSession) {
            throw new common_1.UnauthorizedException();
        }
        const newAccessToken = this.jwt.sign({ sub: currentSession.userUUID, role: currentSession.role }, this.ttl.accessToken);
        const newRefreshToken = this.jwt.sign({ sub: currentSession.userUUID, uuid: currentSession.uuid }, this.ttl.refreshToken);
        const updatedSession = Object.assign(Object.assign({}, currentSession), { accessToken: newAccessToken, refreshToken: newRefreshToken, updatedAt: Date.now(), expiresIn: this.ttl.session });
        const update = sessions.filter((session) => session.uuid !== updatedSession.uuid);
        update.push(updatedSession);
        await this.cacheService.set(sub, { sessions: update }, this.ttl.cache);
        return dtos_1.AuthTokensDto.buildDto(updatedSession);
    }
};
CryptoService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [jwt_1.JwtService, cache_service_1.CacheService])
], CryptoService);
exports.CryptoService = CryptoService;
//# sourceMappingURL=crypto.service.js.map