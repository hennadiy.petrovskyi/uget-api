"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CredentialService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entities_1 = require("../entities");
const event_emitter_1 = require("@nestjs/event-emitter");
const crypto_service_1 = require("./crypto.service");
const logger_1 = require("../../utils/logger");
let CredentialService = class CredentialService {
    constructor(credentialsRepository, cryptoService, eventEmitter, logger) {
        this.credentialsRepository = credentialsRepository;
        this.cryptoService = cryptoService;
        this.eventEmitter = eventEmitter;
        this.logger = logger;
    }
    async findAuthRecordCredentials(owner) {
        const functionName = this.findAuthRecordCredentials.name;
        try {
            return this.credentialsRepository.findOne({ where: { owner } });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async createNewCredential(createCredentialDTO) {
        const functionName = this.createNewCredential.name;
        try {
            const hashedPassword = await this.cryptoService.hashPassword(createCredentialDTO.password);
            await this.credentialsRepository.save(Object.assign(Object.assign({}, createCredentialDTO), { password: hashedPassword }));
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async matchCredential(owner, loginCredential) {
        const credentials = await this.findAuthRecordCredentials(owner);
        return await this.cryptoService.comparePasswords(loginCredential, credentials.password);
    }
    async updatePassword(owner, dto) {
        const functionName = this.updatePassword.name;
        try {
            const userCredentials = await this.findAuthRecordCredentials(owner);
            await this.credentialsRepository.update(userCredentials.id, dto);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
};
CredentialService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.Credential)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        crypto_service_1.CryptoService,
        event_emitter_1.EventEmitter2,
        logger_1.LoggerService])
], CredentialService);
exports.CredentialService = CredentialService;
//# sourceMappingURL=credential.service.js.map