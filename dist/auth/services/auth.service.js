"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const uuid_1 = require("uuid");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const app_logger_1 = require("../../utils/logger/app.logger");
const entities_1 = require("../entities");
const dtos_1 = require("../dtos");
let AuthService = class AuthService {
    constructor(authRepository, logger) {
        this.authRepository = authRepository;
        this.logger = logger;
    }
    async findAuthRecordById(id) {
        const functionName = this.findAuthRecordById.name;
        try {
            return this.authRepository.findOne({ where: { id } });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findAuthRecordByEmail(email) {
        const functionName = this.findAuthRecordByEmail.name;
        try {
            return this.authRepository.findOne({
                where: {
                    email,
                },
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findAuthRecordByPhoneNumber(phoneNumber) {
        const functionName = this.findAuthRecordByPhoneNumber.name;
        try {
            return this.authRepository.findOne({
                where: {
                    phoneNumber,
                },
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findAuthRecordByUUID(uuid) {
        const functionName = this.findAuthRecordByUUID.name;
        try {
            return this.authRepository.findOne({
                where: {
                    uuid,
                },
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async createNewUserAuthRecord(authRegisterUserDto) {
        const functionName = this.createNewUserAuthRecord.name;
        try {
            return await this.authRepository.save(Object.assign(Object.assign({}, dtos_1.AuthCreateNewAuthRecordDto.buildDto(authRegisterUserDto)), { uuid: (0, uuid_1.v4)() }));
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async verifyUserPhoneNumber(userId) {
        const functionName = this.verifyUserPhoneNumber.name;
        try {
            await this.authRepository.save({
                id: userId,
                isPhoneNumberVerified: true,
            });
            return await this.findAuthRecordById(userId);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async verifyUserEmail(userId) {
        const functionName = this.verifyUserEmail.name;
        try {
            await this.authRepository.update(userId, {
                isEmailVerified: true,
            });
            return await this.findAuthRecordById(userId);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async getEmailWithPhoneNumber(userId) {
        const functionName = this.getEmailWithPhoneNumber.name;
        try {
            const user = await this.findAuthRecordById(userId);
            return dtos_1.AuthGetEmailWithPhoneNumberDto.buildDto(user);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async updateUserPhoneNumber(userId, phoneNumber) {
        const functionName = this.updateUserPhoneNumber.name;
        try {
            await this.authRepository.save({
                id: userId,
                phoneNumber,
                isPhoneNumberVerified: false,
            });
            return await this.findAuthRecordById(userId);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async updateUserEmail(userId, email) {
        const functionName = this.updateUserEmail.name;
        try {
            await this.authRepository.save({
                id: userId,
                email,
                isEmailVerified: false,
            });
            return await this.findAuthRecordById(userId);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.AuthRecord)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        app_logger_1.LoggerService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map