import { FindOperator, Repository } from 'typeorm';
import { PhoneNumber } from '../entities';
import { LoggerService } from '../../utils/logger';
export declare class PhoneNumberService {
    private phoneNumberRepository;
    private logger;
    constructor(phoneNumberRepository: Repository<PhoneNumber>, logger: LoggerService);
    findPhoneNumberByOwner(owner: FindOperator<any>): Promise<PhoneNumber>;
    findPhoneNumberByNumber(formatted: string): Promise<PhoneNumber>;
    savePhoneNumber(savePhoneNumber: any): Promise<any>;
    updatePhoneNumberDetails(id: any, update: any): Promise<any>;
}
