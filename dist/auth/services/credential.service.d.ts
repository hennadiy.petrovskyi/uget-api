import { FindOperator, Repository } from 'typeorm';
import { Credential } from '../entities';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CryptoService } from './crypto.service';
import { LoggerService } from '../../utils/logger';
export declare class CredentialService {
    private credentialsRepository;
    private cryptoService;
    private eventEmitter;
    private logger;
    constructor(credentialsRepository: Repository<Credential>, cryptoService: CryptoService, eventEmitter: EventEmitter2, logger: LoggerService);
    findAuthRecordCredentials(owner: FindOperator<any>): Promise<Credential>;
    createNewCredential(createCredentialDTO: any): Promise<void>;
    matchCredential(owner: FindOperator<any>, loginCredential: string): Promise<boolean>;
    updatePassword(owner: any, dto: any): Promise<void>;
}
