"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhoneNumberType = void 0;
var PhoneNumberType;
(function (PhoneNumberType) {
    PhoneNumberType["ACCOUNT_OWNER"] = "ACCOUNT_OWNER";
    PhoneNumberType["CONTACT_PERSON"] = "CONTACT_PERSON";
})(PhoneNumberType = exports.PhoneNumberType || (exports.PhoneNumberType = {}));
//# sourceMappingURL=phone-number-type.enum.js.map