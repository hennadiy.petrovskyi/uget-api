export declare enum PhoneNumberType {
    ACCOUNT_OWNER = "ACCOUNT_OWNER",
    CONTACT_PERSON = "CONTACT_PERSON"
}
