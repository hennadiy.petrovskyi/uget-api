"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileModule = void 0;
const common_1 = require("@nestjs/common");
const services_1 = require("./services");
const typeorm_1 = require("@nestjs/typeorm");
const axios_1 = require("@nestjs/axios");
const logger_1 = require("../utils/logger");
const controllers_1 = require("./controllers");
const entities_1 = require("./entities");
const listeners_1 = require("./listeners");
let FileModule = class FileModule {
    constructor() { }
};
FileModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([entities_1.File]), axios_1.HttpModule, logger_1.LoggerModule],
        providers: [services_1.FileService, listeners_1.FileListener],
        controllers: [controllers_1.FileController],
        exports: [services_1.FileService],
    }),
    __metadata("design:paramtypes", [])
], FileModule);
exports.FileModule = FileModule;
//# sourceMappingURL=file.module.js.map