/// <reference types="multer" />
import { File } from '../entities';
declare const GetFileByOwnerAndTypeDto_base: import("@nestjs/common").Type<Pick<File, "owner" | "type">>;
export declare class GetFileByOwnerAndTypeDto extends GetFileByOwnerAndTypeDto_base {
}
declare const GetFileDto_base: import("@nestjs/common").Type<Pick<File, "owner" | "type" | "fileName">>;
export declare class GetFileDto extends GetFileDto_base {
    static buildDto(payload: File): GetFileDto;
}
export declare class CreateFileDto {
    file: Express.Multer.File;
    type: any;
    uuid: any;
}
export declare class GetLinkDto {
    id: string;
    url: string;
}
export {};
