"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetFileDto = exports.GetFileByOwnerAndTypeDto = exports.GetLinkDto = exports.CreateFileDto = void 0;
var file_dto_1 = require("./file.dto");
Object.defineProperty(exports, "CreateFileDto", { enumerable: true, get: function () { return file_dto_1.CreateFileDto; } });
Object.defineProperty(exports, "GetLinkDto", { enumerable: true, get: function () { return file_dto_1.GetLinkDto; } });
Object.defineProperty(exports, "GetFileByOwnerAndTypeDto", { enumerable: true, get: function () { return file_dto_1.GetFileByOwnerAndTypeDto; } });
Object.defineProperty(exports, "GetFileDto", { enumerable: true, get: function () { return file_dto_1.GetFileDto; } });
//# sourceMappingURL=index.js.map