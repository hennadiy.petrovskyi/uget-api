"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetLinkDto = exports.CreateFileDto = exports.GetFileDto = exports.GetFileByOwnerAndTypeDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../entities");
class GetFileByOwnerAndTypeDto extends (0, swagger_1.PickType)(entities_1.File, [
    'owner',
    'type',
]) {
}
exports.GetFileByOwnerAndTypeDto = GetFileByOwnerAndTypeDto;
class GetFileDto extends (0, swagger_1.PickType)(entities_1.File, ['fileName', 'type', 'owner']) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(GetFileDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.GetFileDto = GetFileDto;
class CreateFileDto {
}
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Object)
], CreateFileDto.prototype, "file", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Object)
], CreateFileDto.prototype, "type", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Object)
], CreateFileDto.prototype, "uuid", void 0);
exports.CreateFileDto = CreateFileDto;
class GetLinkDto {
}
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetLinkDto.prototype, "id", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetLinkDto.prototype, "url", void 0);
exports.GetLinkDto = GetLinkDto;
//# sourceMappingURL=file.dto.js.map