"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileType = void 0;
var FileType;
(function (FileType) {
    FileType["PROFILE_IMAGE"] = "PROFILE_IMAGE";
    FileType["ORDER_IMAGE"] = "ORDER_IMAGE";
})(FileType = exports.FileType || (exports.FileType = {}));
//# sourceMappingURL=type.enum.js.map