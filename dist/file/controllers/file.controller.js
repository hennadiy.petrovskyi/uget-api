"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const services_1 = require("../services");
const platform_express_1 = require("@nestjs/platform-express");
const decorators_1 = require("../../common/decorators");
const jwt_auth_guard_1 = require("../../common/guards/jwt-auth.guard");
const entities_1 = require("../../auth/entities");
const enums_1 = require("../enums");
let FileController = class FileController {
    constructor(service) {
        this.service = service;
    }
    async createFile(user, file, createFileDto) {
        const ownerDto = { owner: user.uuid, type: createFileDto.type };
        if (createFileDto.type === enums_1.FileType.PROFILE_IMAGE) {
            const existingUserProfileImage = await this.service.findFileByOwnerAndType(ownerDto);
            if (existingUserProfileImage) {
                await this.service.deleteFileByName(existingUserProfileImage.fileName);
            }
            await this.service.upload(ownerDto, file);
        }
        return { status: 'ok' };
    }
    async saveMultipleFiles(files, body) {
        console.log(body, files);
        const filesDetailsInDb = await Promise.all(files.map(async (file) => {
            return await this.service.upload({ owner: body.owner, type: body.type }, file);
        }));
        const urls = await Promise.all(filesDetailsInDb.map(async ({ type, owner, fileName }) => {
            const url = await this.service.getFileLink({ type, owner, fileName });
            return { id: fileName, url };
        }));
        console.log(urls);
        return { status: 'ok', photos: urls };
    }
    async deleteFile(res, fileName) {
        await this.service.deleteFileByName(fileName);
        return res.json({
            fileName,
            text: `${fileName} was deleted from s3 andå db`,
        });
    }
};
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('single'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file'), decorators_1.FileToBodyInterceptor),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    __param(0, (0, decorators_1.User)()),
    __param(1, (0, common_1.UploadedFile)()),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [entities_1.AuthRecord, Object, Object]),
    __metadata("design:returntype", Promise)
], FileController.prototype, "createFile", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('multiple'),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FilesInterceptor)('files')),
    __param(0, (0, common_1.UploadedFiles)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array, Object]),
    __metadata("design:returntype", Promise)
], FileController.prototype, "saveMultipleFiles", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Delete)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)('fileName')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], FileController.prototype, "deleteFile", null);
FileController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('file'),
    (0, common_1.Controller)('file'),
    __metadata("design:paramtypes", [services_1.FileService])
], FileController);
exports.FileController = FileController;
//# sourceMappingURL=file.controller.js.map