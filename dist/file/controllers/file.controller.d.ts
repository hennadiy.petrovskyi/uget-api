/// <reference types="multer" />
import { FileService } from '../services';
import { AuthRecord } from 'src/auth/entities';
export declare class FileController {
    private readonly service;
    constructor(service: FileService);
    createFile(user: AuthRecord, file: Express.Multer.File, createFileDto: any): Promise<any>;
    saveMultipleFiles(files: Array<Express.Multer.File>, body: any): Promise<any>;
    deleteFile(res: any, fileName: string): Promise<any>;
}
