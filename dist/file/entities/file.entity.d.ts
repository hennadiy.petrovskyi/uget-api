import { FileType, Provider } from '../enums';
import { BaseEntity } from 'src/common/entities';
export declare class File extends BaseEntity {
    fileName: string;
    type: FileType;
    owner: string;
    provider: Provider;
}
