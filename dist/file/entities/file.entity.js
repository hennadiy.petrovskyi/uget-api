"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.File = void 0;
const typeorm_1 = require("typeorm");
const enums_1 = require("../enums");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../../common/entities");
let File = class File extends entities_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.Index)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('text', { unique: true, nullable: false }),
    __metadata("design:type", String)
], File.prototype, "fileName", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.FileType }),
    __metadata("design:type", String)
], File.prototype, "type", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('text', { nullable: false }),
    __metadata("design:type", String)
], File.prototype, "owner", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.Provider, default: enums_1.Provider.AWS }),
    __metadata("design:type", String)
], File.prototype, "provider", void 0);
File = __decorate([
    (0, typeorm_1.Entity)({
        name: 'file',
    }),
    (0, typeorm_1.Unique)(['fileName'])
], File);
exports.File = File;
//# sourceMappingURL=file.entity.js.map