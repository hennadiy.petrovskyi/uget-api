"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_1 = require("../../common/events");
const services_1 = require("../services");
const dtos_1 = require("../dtos");
let FileListener = class FileListener {
    constructor(fileService) {
        this.fileService = fileService;
    }
    async getAllOwnerFiles(owner) {
        const files = await this.fileService.findOwnerAllFiles(owner);
        if (files && files.length) {
            return Promise.all(files.map((file) => this.fileService.buildFileDto(file)));
        }
    }
    async getSingleFileByOwnerAndType(getFileByOwnerAndTypeDto) {
        const file = await this.fileService.findFileByOwnerAndType(getFileByOwnerAndTypeDto);
        if (file) {
            return await this.fileService.buildFileDto(file);
        }
    }
    async deleteAllOwnerPhotos(owner) {
        await this.fileService.deleteAllOwnerFiles(owner);
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.FILE_EVENTS.GET_FILES, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], FileListener.prototype, "getAllOwnerFiles", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.FILE_EVENTS.GET_FILE_BY_TYPE, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.GetFileByOwnerAndTypeDto]),
    __metadata("design:returntype", Promise)
], FileListener.prototype, "getSingleFileByOwnerAndType", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.FILE_EVENTS.DELETE_OWNER_FILES, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], FileListener.prototype, "deleteAllOwnerPhotos", null);
FileListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.FileService])
], FileListener);
exports.FileListener = FileListener;
//# sourceMappingURL=file.listener.js.map