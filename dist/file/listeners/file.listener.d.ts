import { FileService } from '../services';
import { GetFileByOwnerAndTypeDto } from '../dtos';
export declare class FileListener {
    private readonly fileService;
    constructor(fileService: FileService);
    getAllOwnerFiles(owner: string): Promise<{
        id: string;
        url: any;
    }[]>;
    getSingleFileByOwnerAndType(getFileByOwnerAndTypeDto: GetFileByOwnerAndTypeDto): Promise<{
        id: string;
        url: any;
    }>;
    deleteAllOwnerPhotos(owner: string): Promise<void>;
}
