/// <reference types="node" />
/// <reference types="multer" />
import { Repository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';
import { LoggerService } from 'src/utils/logger';
import { File } from '../entities';
import { GetFileByOwnerAndTypeDto } from '../dtos';
import { FileType } from '../enums';
export declare class FileService {
    private configService;
    private fileRepository;
    private httpService;
    private logger;
    private readonly s3;
    constructor(configService: ConfigService, fileRepository: Repository<File>, httpService: HttpService, logger: LoggerService);
    findOwnerAllFiles(owner: string): Promise<File[]>;
    private findFileDetailsInDb;
    buildFileDto(file: File): Promise<{
        id: string;
        url: any;
    }>;
    findFileByOwnerAndType(getFileByOwnerAndTypeDto: GetFileByOwnerAndTypeDto): Promise<any>;
    saveFileDetailsInDb(fileDetailsDto: any): Promise<any>;
    uploadFileToS3({ buffer, contentType, size, }: {
        buffer: Buffer;
        contentType: string;
        size: number;
    }, urlKey: string): Promise<string>;
    getFileLink({ type, owner, fileName }: {
        type: any;
        owner: any;
        fileName: any;
    }): Promise<any>;
    upload({ owner, type }: {
        owner: string;
        type: FileType;
    }, file: Express.Multer.File): Promise<any>;
    deleteFileByName(fileName: string): Promise<boolean>;
    deleteAllOwnerFiles(owner: string): Promise<void>;
}
