"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileService = void 0;
const common_1 = require("@nestjs/common");
const client_s3_1 = require("@aws-sdk/client-s3");
const uuid_1 = require("uuid");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const config_1 = require("@nestjs/config");
const axios_1 = require("@nestjs/axios");
const logger_1 = require("../../utils/logger");
const entities_1 = require("../entities");
const dtos_1 = require("../dtos");
let FileService = class FileService {
    constructor(configService, fileRepository, httpService, logger) {
        this.configService = configService;
        this.fileRepository = fileRepository;
        this.httpService = httpService;
        this.logger = logger;
        this.s3 = new client_s3_1.S3Client({
            credentials: {
                accessKeyId: configService.get('AWS_ACCESS_KEY_ID'),
                secretAccessKey: configService.get('AWS_SECRET_ACCESS_KEY'),
            },
        });
    }
    async findOwnerAllFiles(owner) {
        const functionName = this.findOwnerAllFiles.name;
        try {
            return this.fileRepository.find({ where: { owner } });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findFileDetailsInDb(fileName) {
        const functionName = this.findFileDetailsInDb.name;
        try {
            return await this.fileRepository.findOne({ where: { fileName } });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async buildFileDto(file) {
        const functionName = this.buildFileDto.name;
        try {
            const url = await this.getFileLink(dtos_1.GetFileDto.buildDto(file));
            if (!url) {
                return;
            }
            return { id: file.fileName, url };
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findFileByOwnerAndType(getFileByOwnerAndTypeDto) {
        const functionName = this.findFileDetailsInDb.name;
        try {
            return this.fileRepository.findOne({
                where: {
                    owner: getFileByOwnerAndTypeDto.owner,
                    type: getFileByOwnerAndTypeDto.type,
                },
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async saveFileDetailsInDb(fileDetailsDto) {
        const functionName = this.saveFileDetailsInDb.name;
        const { fileName, type, owner } = fileDetailsDto;
        const file = new entities_1.File();
        file.fileName = fileName;
        file.type = type;
        file.owner = owner;
        try {
            await this.fileRepository.save(file);
            return await this.findFileDetailsInDb(fileDetailsDto.fileName);
        }
        catch (error) {
            this.logger.logError(functionName, error, fileDetailsDto);
        }
    }
    async uploadFileToS3({ buffer, contentType, size, }, urlKey) {
        const fileName = (0, uuid_1.v4)();
        const params = {
            Body: buffer,
            Bucket: this.configService.get('AWS_S3_BUCKET_NAME'),
            Key: `${urlKey}/${fileName}`,
            ContentType: contentType,
            ContentLength: size,
        };
        try {
            await this.s3.putObject(params).promise();
        }
        catch (err) {
            throw new common_1.InternalServerErrorException();
        }
        return fileName;
    }
    async getFileLink({ type, owner, fileName }) {
        const params = {
            Bucket: this.configService.get('AWS_S3_BUCKET_NAME'),
            Key: `${type}/${owner}/${fileName}`,
            Expires: 3600,
        };
        try {
            return await this.s3.getSignedUrl('getObject', params);
        }
        catch (err) {
            throw new common_1.InternalServerErrorException();
        }
    }
    async upload({ owner, type }, file) {
        const { buffer, mimetype, size } = file;
        const urlKey = `${type}/${owner}`;
        const fileName = await this.uploadFileToS3({ buffer, contentType: mimetype, size }, urlKey);
        const params = {
            fileName,
            type,
            owner,
        };
        const fileDetails = await this.saveFileDetailsInDb(params);
        return fileDetails;
    }
    async deleteFileByName(fileName) {
        const file = await this.findFileDetailsInDb(fileName);
        if (!file) {
            throw new common_1.NotFoundException();
        }
        const params = {
            Bucket: this.configService.get('AWS_S3_BUCKET_NAME'),
            Key: `${file.type}/${file.owner}/${file.fileName}`,
        };
        try {
            await this.s3.deleteObject(params, async (err, data) => {
                if (err === null) {
                    await this.fileRepository.delete({ fileName });
                }
            });
            return true;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
    async deleteAllOwnerFiles(owner) {
        const files = await this.findOwnerAllFiles(owner);
        Promise.all(files.map((file) => this.deleteFileByName(file.fileName)));
    }
};
FileService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(entities_1.File)),
    __metadata("design:paramtypes", [config_1.ConfigService,
        typeorm_2.Repository,
        axios_1.HttpService,
        logger_1.LoggerService])
], FileService);
exports.FileService = FileService;
//# sourceMappingURL=file.service.js.map