import { ConsoleLogger } from '@nestjs/common';
export declare class LoggerService extends ConsoleLogger {
    logError(functionName: string, error: any, props?: any, context?: string): void;
    logWarn(functionName: string, warn?: any, props?: any, context?: string): void;
}
