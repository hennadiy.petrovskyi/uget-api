"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerModule = exports.LoggerService = void 0;
var app_logger_1 = require("./app.logger");
Object.defineProperty(exports, "LoggerService", { enumerable: true, get: function () { return app_logger_1.LoggerService; } });
var logger_module_1 = require("./logger.module");
Object.defineProperty(exports, "LoggerModule", { enumerable: true, get: function () { return logger_module_1.LoggerModule; } });
//# sourceMappingURL=index.js.map