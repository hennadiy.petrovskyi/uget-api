"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Receiver = void 0;
var Receiver;
(function (Receiver) {
    Receiver["MYSELF"] = "self";
    Receiver["SOMEONE_ELSE"] = "other";
})(Receiver = exports.Receiver || (exports.Receiver = {}));
//# sourceMappingURL=receiver-type.enum.js.map