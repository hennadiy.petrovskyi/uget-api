"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ItemSize = void 0;
var ItemSize;
(function (ItemSize) {
    ItemSize[ItemSize["A"] = 0.5] = "A";
    ItemSize[ItemSize["B"] = 1] = "B";
    ItemSize[ItemSize["C"] = 1.5] = "C";
    ItemSize[ItemSize["D"] = 2] = "D";
    ItemSize[ItemSize["E"] = 2.5] = "E";
    ItemSize[ItemSize["F"] = 3] = "F";
    ItemSize[ItemSize["G"] = 3.5] = "G";
    ItemSize[ItemSize["H"] = 4] = "H";
    ItemSize[ItemSize["I"] = 4.5] = "I";
    ItemSize[ItemSize["J"] = 5] = "J";
    ItemSize[ItemSize["K"] = 5.5] = "K";
    ItemSize[ItemSize["L"] = 6] = "L";
})(ItemSize = exports.ItemSize || (exports.ItemSize = {}));
//# sourceMappingURL=item-size.enum.js.map