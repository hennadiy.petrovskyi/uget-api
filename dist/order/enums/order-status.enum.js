"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderStatus = void 0;
var OrderStatus;
(function (OrderStatus) {
    OrderStatus["PUBLISHED"] = "published";
    OrderStatus["INACTIVE"] = "inactive";
    OrderStatus["ACCEPTED"] = "accepted";
    OrderStatus["DRAFT"] = "draft";
    OrderStatus["EXPIRED"] = "expired";
    OrderStatus["REQUEST_INCOMING"] = "request.incoming";
    OrderStatus["PICKED_UP"] = "picked-up";
    OrderStatus["DELIVERED"] = "delivered";
})(OrderStatus = exports.OrderStatus || (exports.OrderStatus = {}));
//# sourceMappingURL=order-status.enum.js.map