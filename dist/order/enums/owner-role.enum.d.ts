export declare enum OwnerRole {
    SENDER = "SENDER",
    RECEIVER = "RECEIVER"
}
