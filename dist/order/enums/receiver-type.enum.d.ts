export declare enum Receiver {
    MYSELF = "self",
    SOMEONE_ELSE = "other"
}
