"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OwnerRole = void 0;
var OwnerRole;
(function (OwnerRole) {
    OwnerRole["SENDER"] = "SENDER";
    OwnerRole["RECEIVER"] = "RECEIVER";
})(OwnerRole = exports.OwnerRole || (exports.OwnerRole = {}));
//# sourceMappingURL=owner-role.enum.js.map