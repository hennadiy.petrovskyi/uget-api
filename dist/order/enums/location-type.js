"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderLocationType = void 0;
var OrderLocationType;
(function (OrderLocationType) {
    OrderLocationType["PICKUP"] = "PICKUP";
    OrderLocationType["ENDPOINT"] = "ENDPOINT";
})(OrderLocationType = exports.OrderLocationType || (exports.OrderLocationType = {}));
//# sourceMappingURL=location-type.js.map