"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderType = void 0;
var OrderType;
(function (OrderType) {
    OrderType["DOCUMENTS"] = "document";
    OrderType["PARCEL"] = "parcel";
})(OrderType = exports.OrderType || (exports.OrderType = {}));
//# sourceMappingURL=order-type.enum.js.map