"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderListener = void 0;
const currency_enum_1 = require("./../../pricing/enums/currency.enum");
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_constants_1 = require("./../../common/events/events-constants");
const services_1 = require("../services");
const emitters_1 = require("../emitters");
const enums_1 = require("../enums");
const events_1 = require("../../socket/enums/events");
const typeorm_1 = require("typeorm");
let OrderListener = class OrderListener {
    constructor(orderService, statusService, orderEmitter, helperService, deliveryPriceService) {
        this.orderService = orderService;
        this.statusService = statusService;
        this.orderEmitter = orderEmitter;
        this.helperService = helperService;
        this.deliveryPriceService = deliveryPriceService;
    }
    async orderAddIncomingRequest(dto) {
        const order = await this.orderService.findOrderByUUID(dto.orderUUID);
        if (order.incomingRequests.includes(dto.tripUUID)) {
            return;
        }
        order.incomingRequests.push(dto.tripUUID);
        await this.orderService.updateOrderRequests(order.id, {
            incomingRequests: order.incomingRequests,
        });
        console.log('ORDER_EVENT.ADD_INCOMING_REQUEST');
        const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);
        await this.orderEmitter.sendSocketMessage({
            uuid: orderOwner.uuid,
            payload: { action: events_1.SocketEvents.UPDATE_MY_ORDERS },
        });
        await this.orderEmitter.sendSocketMessage({
            uuid: 'trip_' + dto.trip.uuid,
            payload: { data: dto.trip.update },
        });
    }
    async orderCancelIncomingRequestDelivery(dto) {
        const order = await this.orderService.findOrderByUUID(dto.orderUUID);
        if (!order.incomingRequests.includes(dto.tripUUID)) {
            return;
        }
        await this.orderService.updateOrderRequests(order.id, {
            incomingRequests: order.incomingRequests.filter((tripUUID) => tripUUID !== dto.tripUUID),
        });
        console.log('ORDER_EVENT.CANCEL_INCOMING_REQUEST');
        const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);
        await this.orderEmitter.sendSocketMessage({
            uuid: orderOwner.uuid,
            payload: { action: events_1.SocketEvents.UPDATE_MY_ORDERS },
        });
        await this.orderEmitter.sendSocketMessage({
            uuid: 'trip_' + dto.trip.uuid,
            payload: { data: dto.trip.data },
        });
    }
    async orderCancelOutgoingRequestDelivery(dto) {
        const order = await this.orderService.findOrderByUUID(dto.orderUUID);
        if (!order.outgoingRequests.includes(dto.tripUUID)) {
            return;
        }
        await this.orderService.updateOrderRequests(order.id, {
            outgoingRequests: order.outgoingRequests.filter((tripUUID) => tripUUID !== dto.tripUUID),
        });
        console.log('ORDER_EVENT.CANCEL_OUTGOING_REQUEST');
        const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);
        await this.orderEmitter.sendSocketMessage({
            uuid: orderOwner.uuid,
            payload: { action: events_1.SocketEvents.UPDATE_MY_ORDERS },
        });
        await this.orderEmitter.sendSocketMessage({
            uuid: 'trip_' + dto.trip.uuid,
            payload: { data: dto.trip.data },
        });
    }
    async acceptDeliveryRequest(dto) {
        const order = await this.orderService.findOrderByUUID(dto.orderUUID);
        if (order.tripId === dto.tripUUID) {
            return;
        }
        await this.orderService.updateOrderRequests(order.id, {
            outgoingRequests: order.outgoingRequests.filter((tripUUID) => tripUUID !== dto.tripUUID),
            tripId: dto.tripUUID,
        });
        console.log('ORDER_EVENT.ACCEPT_DELIVERY_BY_TRAVELER');
        await this.statusService.addOrderStatus({
            owner: order,
            status: enums_1.OrderStatus.ACCEPTED,
        });
        const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);
        await this.orderEmitter.sendSocketMessage({
            uuid: orderOwner.uuid,
            payload: { action: events_1.SocketEvents.UPDATE_MY_ORDERS },
        });
        await this.orderEmitter.sendSocketMessage({
            uuid: 'trip_' + dto.trip.uuid,
            payload: { data: dto.trip.data },
        });
    }
    async orderCancelAcceptedRequestDelivery(dto) {
        const order = await this.orderService.findOrderByUUID(dto.orderUUID);
        if (order.tripId !== dto.tripUUID) {
            return;
        }
        await this.orderService.updateOrderRequests(order.id, {
            tripId: null,
        });
        await this.statusService.deleteAcceptedStatus(order.id);
        console.log('ORDER_EVENT.CANCEL_ACCEPTED_REQUEST');
        const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);
        await this.orderEmitter.sendSocketMessage({
            uuid: orderOwner.uuid,
            payload: { action: events_1.SocketEvents.UPDATE_MY_ORDERS },
        });
        await this.orderEmitter.sendSocketMessage({
            uuid: 'trip_' + dto.trip.uuid,
            payload: { data: dto.trip.data },
        });
    }
    async getAllUserOrders(owner) {
        const orders = await this.orderService.getAllUserOrders(owner);
        if (!orders) {
            return [];
        }
        return Promise.all(orders.map((el) => this.helperService.buildOrderDto(el)));
    }
    async getEarnings(ordersUUIDs) {
        const orders = await Promise.all(ordersUUIDs.map((uuid) => this.orderService.findOrderByUUID(uuid)));
        const deliveryPricesDetails = await Promise.all(orders
            .map((order) => order.id)
            .map((orderId) => this.deliveryPriceService.getOrderDeliveryPrice(orderId)));
        const deliveryPrices = deliveryPricesDetails.map((price) => price.total);
        const temporaryEarningModifier = 2;
        const earnings = deliveryPrices.length
            ? +deliveryPrices.reduce((prev, current) => prev + current) /
                temporaryEarningModifier
            : 0;
        return {
            value: !deliveryPrices.length ? 0 : earnings.toFixed(2),
            currency: currency_enum_1.Currency.EUR,
        };
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.ORDER_EVENT.ADD_INCOMING_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], OrderListener.prototype, "orderAddIncomingRequest", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.ORDER_EVENT.CANCEL_INCOMING_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], OrderListener.prototype, "orderCancelIncomingRequestDelivery", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.ORDER_EVENT.CANCEL_OUTGOING_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], OrderListener.prototype, "orderCancelOutgoingRequestDelivery", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.ORDER_EVENT.ACCEPT_DELIVERY_BY_TRAVELER, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], OrderListener.prototype, "acceptDeliveryRequest", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.ORDER_EVENT.CANCEL_ACCEPTED_REQUEST, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], OrderListener.prototype, "orderCancelAcceptedRequestDelivery", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.ORDER_EVENT.GET_USER_ORDERS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeorm_1.FindOperator]),
    __metadata("design:returntype", Promise)
], OrderListener.prototype, "getAllUserOrders", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_constants_1.ORDER_EVENT.GET_EARNINGS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], OrderListener.prototype, "getEarnings", null);
OrderListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.OrderService,
        services_1.StatusService,
        emitters_1.OrderEmitter,
        services_1.OrderHelperService,
        services_1.DeliveryPriceService])
], OrderListener);
exports.OrderListener = OrderListener;
//# sourceMappingURL=order.listener.js.map