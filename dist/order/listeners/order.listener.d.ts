import { Currency } from './../../pricing/enums/currency.enum';
import { OrderService, StatusService, OrderHelperService, DeliveryPriceService } from '../services';
import { OrderEmitter } from '../emitters';
import { FindOperator } from 'typeorm';
export declare class OrderListener {
    private readonly orderService;
    private readonly statusService;
    private readonly orderEmitter;
    private readonly helperService;
    private readonly deliveryPriceService;
    constructor(orderService: OrderService, statusService: StatusService, orderEmitter: OrderEmitter, helperService: OrderHelperService, deliveryPriceService: DeliveryPriceService);
    orderAddIncomingRequest(dto: any): Promise<void>;
    orderCancelIncomingRequestDelivery(dto: any): Promise<void>;
    orderCancelOutgoingRequestDelivery(dto: any): Promise<void>;
    acceptDeliveryRequest(dto: any): Promise<void>;
    orderCancelAcceptedRequestDelivery(dto: any): Promise<void>;
    getAllUserOrders(owner: FindOperator<any>): Promise<any[]>;
    getEarnings(ordersUUIDs: string[]): Promise<{
        value: string | number;
        currency: Currency;
    }>;
}
