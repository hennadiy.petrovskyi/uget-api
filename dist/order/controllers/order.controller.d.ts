import { OrderEmitter } from './../emitters/order.emitter';
import { PublishOrderDto } from '../dtos';
import { OrderService, StatusService, LocationService, DetailsService, ThirdPartyNumberService, OrderHelperService, DeliveryPriceService } from '../services';
import { CacheService } from 'src/common/services/cache.service';
export declare class OrderController {
    private readonly service;
    private readonly helper;
    private readonly statusService;
    private readonly detailsService;
    private readonly locationService;
    private readonly thirdPartyNumberService;
    private readonly priceService;
    private cacheService;
    private orderEmitter;
    constructor(service: OrderService, helper: OrderHelperService, statusService: StatusService, detailsService: DetailsService, locationService: LocationService, thirdPartyNumberService: ThirdPartyNumberService, priceService: DeliveryPriceService, cacheService: CacheService, orderEmitter: OrderEmitter);
    publishOrder(publishOrderDto: PublishOrderDto, user: any): Promise<{
        owner: string;
        orders: any[];
    }>;
    getAllOrders(user: any): Promise<any[]>;
    getTravellerActiveOrders(user: any, tripUUID: string): Promise<{
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        status: string;
        deliveryPrice: {
            value: string;
            currency: import("../../pricing/enums").Currency;
        };
        details: import("../dtos").DetailsCreateDto;
        locations: {
            pickup: import("../dtos").LocationCreateDto;
            endpoint: import("../dtos").LocationCreateDto;
        };
        photos: any[];
        uuid: string;
        deliveryDate: string;
    }[]>;
    getTravellerTripDeliveredOrders(user: any, tripUUID: string): Promise<{
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        status: string;
        deliveryPrice: {
            value: string;
            currency: import("../../pricing/enums").Currency;
        };
        details: import("../dtos").DetailsCreateDto;
        locations: {
            pickup: import("../dtos").LocationCreateDto;
            endpoint: import("../dtos").LocationCreateDto;
        };
        photos: any[];
        uuid: string;
        deliveryDate: string;
    }[]>;
    getOne(orderUUID: string, user: any): Promise<any>;
    unpublishOrder(orderUUID: string, user: any): Promise<any[]>;
    getOrderRequests(orderUUID: string, user: any): Promise<void>;
    publishInactiveOrder(orderUUID: string, user: any): Promise<any[]>;
    deleteOrder(orderUUID: string, user: any): Promise<any[]>;
    getAllByOrderParams(user: any, query: any): Promise<{
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        status: string;
        deliveryPrice: {
            value: string;
            currency: import("../../pricing/enums").Currency;
        };
        details: import("../dtos").DetailsCreateDto;
        locations: {
            pickup: import("../dtos").LocationCreateDto;
            endpoint: import("../dtos").LocationCreateDto;
        };
        photos: any[];
        uuid: string;
        deliveryDate: string;
    }[]>;
    getOrderUpdatedOrderForTraveler(query: any): Promise<{
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        status: string;
        deliveryPrice: {
            value: string;
            currency: import("../../pricing/enums").Currency;
        };
        details: import("../dtos").DetailsCreateDto;
        locations: {
            pickup: import("../dtos").LocationCreateDto;
            endpoint: import("../dtos").LocationCreateDto;
        };
        photos: any[];
        uuid: string;
        deliveryDate: string;
    }>;
    requestOrderDelivery(requestOrderDeliveryDto: any, user: any): Promise<any>;
    cancelDeliveryOrderRequest(requestOrderDeliveryDto: any, user: any): Promise<any>;
    acceptDeliveryOrderRequest(requestOrderDeliveryDto: any, user: any): Promise<any>;
    requestPickupVerification(requestOrderDeliveryDto: any, user: any): Promise<void>;
    verifyPickup(requestOrderDeliveryDto: any, user: any): Promise<{
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        status: string;
        deliveryPrice: {
            value: string;
            currency: import("../../pricing/enums").Currency;
        };
        details: import("../dtos").DetailsCreateDto;
        locations: {
            pickup: import("../dtos").LocationCreateDto;
            endpoint: import("../dtos").LocationCreateDto;
        };
        photos: any[];
        uuid: string;
        deliveryDate: string;
    }>;
    requestPFinishDeliveryVerification(requestOrderDeliveryDto: any, user: any): Promise<void>;
    verifyDelivery(requestOrderDeliveryDto: any, user: any): Promise<{
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        status: string;
        deliveryPrice: {
            value: string;
            currency: import("../../pricing/enums").Currency;
        };
        details: import("../dtos").DetailsCreateDto;
        locations: {
            pickup: import("../dtos").LocationCreateDto;
            endpoint: import("../dtos").LocationCreateDto;
        };
        photos: any[];
        uuid: string;
        deliveryDate: string;
    }>;
    requestOwnerDetails(uuid: any, user: any): Promise<any>;
}
