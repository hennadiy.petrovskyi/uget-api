"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const axios_1 = require("@nestjs/axios");
const services_1 = require("./services");
const controllers_1 = require("./controllers");
const entities_1 = require("./entities");
const logger_1 = require("../utils/logger");
const emitters_1 = require("./emitters");
const morgan_middleware_1 = require("../common/middlewares/morgan.middleware");
const listeners_1 = require("./listeners");
const cache_module_1 = require("../common/modules/cache.module");
const sse_service_1 = require("../common/services/sse-service");
let OrderModule = class OrderModule {
    configure(consumer) {
        consumer.apply(morgan_middleware_1.MorganMiddleware).forRoutes(controllers_1.OrderController);
    }
};
OrderModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                entities_1.Order,
                entities_1.Details,
                entities_1.Status,
                entities_1.OrderLocation,
                entities_1.ThirdPartyPhoneNumber,
                entities_1.DeliveryPrice,
            ]),
            axios_1.HttpModule,
            logger_1.LoggerModule,
            cache_module_1.CacheRedisModule,
        ],
        providers: [
            services_1.OrderService,
            services_1.DeliveryPriceService,
            services_1.StatusService,
            services_1.LocationService,
            services_1.DetailsService,
            emitters_1.OrderEmitter,
            listeners_1.OrderListener,
            sse_service_1.SseService,
            services_1.ThirdPartyNumberService,
            services_1.OrderHelperService,
        ],
        controllers: [controllers_1.OrderController],
        exports: [],
    })
], OrderModule);
exports.OrderModule = OrderModule;
//# sourceMappingURL=order.module.js.map