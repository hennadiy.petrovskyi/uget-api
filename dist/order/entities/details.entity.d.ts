import { ItemSize, OrderType } from '../enums';
import { BaseEntity } from 'src/common/entities';
import { Order } from '.';
export declare class Details extends BaseEntity {
    owner: Order;
    type: OrderType;
    name: string;
    size: ItemSize;
    quantity: number;
    description: string;
}
