import { Currency } from './../../pricing/enums/currency.enum';
import { BaseEntity } from 'src/common/entities';
import { Order } from '.';
export declare class DeliveryPrice extends BaseEntity {
    owner: Order;
    currency: Currency;
    average: string;
    routePriceModifier: string;
    total: string;
    discountId: string;
}
