import { BaseEntity } from 'src/common/entities';
import { OrderStatus } from '../enums';
import { Order } from '.';
export declare class Status extends BaseEntity {
    owner: Order;
    status: OrderStatus;
}
