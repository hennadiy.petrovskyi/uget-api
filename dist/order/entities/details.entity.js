"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Details = void 0;
const swagger_1 = require("@nestjs/swagger");
const typeorm_1 = require("typeorm");
const class_transformer_1 = require("class-transformer");
const enums_1 = require("../enums");
const entities_1 = require("../../common/entities");
const _1 = require(".");
let Details = class Details extends entities_1.BaseEntity {
};
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, swagger_1.ApiProperty)(),
    (0, typeorm_1.Index)(),
    (0, typeorm_1.ManyToOne)(() => _1.Order),
    (0, typeorm_1.JoinColumn)({ name: 'orderId', referencedColumnName: 'id' }),
    __metadata("design:type", _1.Order)
], Details.prototype, "owner", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.OrderType }),
    __metadata("design:type", String)
], Details.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('text'),
    __metadata("design:type", String)
], Details.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('enum', { enum: enums_1.ItemSize }),
    __metadata("design:type", Number)
], Details.prototype, "size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('int'),
    __metadata("design:type", Number)
], Details.prototype, "quantity", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    (0, typeorm_1.Column)('text'),
    __metadata("design:type", String)
], Details.prototype, "description", void 0);
Details = __decorate([
    (0, typeorm_1.Entity)({
        name: 'order_details',
    })
], Details);
exports.Details = Details;
//# sourceMappingURL=details.entity.js.map