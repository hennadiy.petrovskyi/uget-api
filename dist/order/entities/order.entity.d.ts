import { BaseEntity } from 'src/common/entities';
import { OwnerRole } from '../enums';
import { AuthRecord } from 'src/auth/entities';
export declare class Order extends BaseEntity {
    owner: AuthRecord;
    ownerRole: OwnerRole;
    uuid: string;
    deliveryDate: string;
    isDelivered: boolean;
    tripId: string;
    incomingRequests: string[];
    outgoingRequests: string[];
}
