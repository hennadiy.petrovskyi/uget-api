import { BaseEntity } from 'src/common/entities';
import { Order } from '.';
import { OrderLocationType } from '../enums';
export declare class OrderLocation extends BaseEntity {
    owner: Order;
    type: OrderLocationType;
    detailsId: number;
    area: string;
}
