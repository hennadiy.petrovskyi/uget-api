"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryPrice = exports.ThirdPartyPhoneNumber = exports.Status = exports.OrderLocation = exports.Details = exports.Order = void 0;
var order_entity_1 = require("./order.entity");
Object.defineProperty(exports, "Order", { enumerable: true, get: function () { return order_entity_1.Order; } });
var details_entity_1 = require("./details.entity");
Object.defineProperty(exports, "Details", { enumerable: true, get: function () { return details_entity_1.Details; } });
var location_entity_1 = require("./location.entity");
Object.defineProperty(exports, "OrderLocation", { enumerable: true, get: function () { return location_entity_1.OrderLocation; } });
var status_entity_1 = require("./status.entity");
Object.defineProperty(exports, "Status", { enumerable: true, get: function () { return status_entity_1.Status; } });
var third_party_number_entity_1 = require("./third-party-number.entity");
Object.defineProperty(exports, "ThirdPartyPhoneNumber", { enumerable: true, get: function () { return third_party_number_entity_1.ThirdPartyPhoneNumber; } });
var price_entity_1 = require("./price.entity");
Object.defineProperty(exports, "DeliveryPrice", { enumerable: true, get: function () { return price_entity_1.DeliveryPrice; } });
//# sourceMappingURL=index.js.map