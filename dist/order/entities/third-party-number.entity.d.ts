import { BaseEntity } from 'src/common/entities';
import { Order } from '.';
export declare class ThirdPartyPhoneNumber extends BaseEntity {
    owner: Order;
    formatted: string;
    countryCode: string;
    code: string;
    number: string;
    isVerified: boolean;
}
