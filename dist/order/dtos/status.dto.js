"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetStatusDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../entities");
class GetStatusDto extends (0, mapped_types_1.PickType)(entities_1.Status, ['createdAt', 'status']) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(GetStatusDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.GetStatusDto = GetStatusDto;
//# sourceMappingURL=status.dto.js.map