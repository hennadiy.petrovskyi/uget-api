"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThirdPartPhoneNumberDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../entities");
class ThirdPartPhoneNumberDto extends (0, mapped_types_1.PickType)(entities_1.ThirdPartyPhoneNumber, [
    'code',
    'countryCode',
    'number',
    'formatted',
    'isVerified',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(ThirdPartPhoneNumberDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.ThirdPartPhoneNumberDto = ThirdPartPhoneNumberDto;
//# sourceMappingURL=third-party-phone.dto.js.map