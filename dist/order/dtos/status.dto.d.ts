import { Status } from '../entities';
declare const GetStatusDto_base: import("@nestjs/mapped-types").MappedType<Pick<Status, "createdAt" | "status">>;
export declare class GetStatusDto extends GetStatusDto_base {
    static buildDto(payload: Status): GetStatusDto;
}
export {};
