import { ThirdPartyPhoneNumber } from '../entities';
declare const ThirdPartPhoneNumberDto_base: import("@nestjs/mapped-types").MappedType<Pick<ThirdPartyPhoneNumber, "number" | "formatted" | "countryCode" | "code" | "isVerified">>;
export declare class ThirdPartPhoneNumberDto extends ThirdPartPhoneNumberDto_base {
    static buildDto(payload: any): ThirdPartPhoneNumberDto;
}
export {};
