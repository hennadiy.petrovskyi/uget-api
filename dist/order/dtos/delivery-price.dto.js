"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOrderDeliveryPriceDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../entities");
class GetOrderDeliveryPriceDto extends (0, mapped_types_1.PickType)(entities_1.DeliveryPrice, [
    'total',
    'currency',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(GetOrderDeliveryPriceDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.GetOrderDeliveryPriceDto = GetOrderDeliveryPriceDto;
//# sourceMappingURL=delivery-price.dto.js.map