"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DetailsCreateDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../entities");
class DetailsCreateDto extends (0, mapped_types_1.PickType)(entities_1.Details, [
    'name',
    'quantity',
    'size',
    'quantity',
    'description',
    'type',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(DetailsCreateDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.DetailsCreateDto = DetailsCreateDto;
//# sourceMappingURL=details.dto.js.map