import { OrderLocation } from '../entities';
declare const LocationCreateDto_base: import("@nestjs/mapped-types").MappedType<Pick<OrderLocation, "detailsId" | "area">>;
export declare class LocationCreateDto extends LocationCreateDto_base {
    country: string;
    city: string;
}
declare const LocationResponseDto_base: import("@nestjs/mapped-types").MappedType<Pick<OrderLocation, "type" | "area">>;
export declare class LocationResponseDto extends LocationResponseDto_base {
    static buildDto(payload: any): LocationResponseDto;
}
export {};
