import { Currency } from './../../pricing/enums/currency.enum';
import { DetailsCreateDto } from './details.dto';
import { Order } from '../entities';
import { LocationCreateDto } from '.';
import { AuthRecord } from 'src/auth/entities';
declare const OrderCreateDto_base: import("@nestjs/mapped-types").MappedType<Pick<Order, "uuid" | "ownerRole" | "deliveryDate">>;
export declare class OrderCreateDto extends OrderCreateDto_base {
    owner: AuthRecord;
    static buildDto(payload: PublishOrderDto): OrderCreateDto;
}
export declare class PublishOrderDto extends OrderCreateDto {
    ownerRole: any;
    details: DetailsCreateDto;
    locations: {
        pickup: LocationCreateDto;
        endpoint: LocationCreateDto;
    };
    deliveryPrice: {
        value: string;
        currency: Currency;
    };
    thirdPartyPhoneNumber: any;
}
declare const GetOrderDto_base: import("@nestjs/mapped-types").MappedType<Pick<Order, "id" | "uuid" | "createdAt" | "ownerRole" | "deliveryDate" | "tripId" | "incomingRequests" | "outgoingRequests">>;
export declare class GetOrderDto extends GetOrderDto_base {
    static buildDto(payload: Order): GetOrderDto;
}
declare const GetOrderForTipDto_base: import("@nestjs/mapped-types").MappedType<Pick<Order, "uuid" | "deliveryDate">>;
export declare class GetOrderForTipDto extends GetOrderForTipDto_base {
    details: DetailsCreateDto;
    locations: {
        pickup: LocationCreateDto;
        endpoint: LocationCreateDto;
    };
    photos: any[];
    static buildDto(payload: any): GetOrderForTipDto;
}
export {};
