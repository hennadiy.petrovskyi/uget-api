"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOrderForTipDto = exports.GetOrderDto = exports.PublishOrderDto = exports.OrderCreateDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const details_dto_1 = require("./details.dto");
const entities_1 = require("../entities");
const entities_2 = require("../../auth/entities");
class OrderCreateDto extends (0, mapped_types_1.PickType)(entities_1.Order, [
    'deliveryDate',
    'uuid',
    'ownerRole',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(OrderCreateDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", entities_2.AuthRecord)
], OrderCreateDto.prototype, "owner", void 0);
exports.OrderCreateDto = OrderCreateDto;
class PublishOrderDto extends OrderCreateDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Object)
], PublishOrderDto.prototype, "ownerRole", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", details_dto_1.DetailsCreateDto)
], PublishOrderDto.prototype, "details", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Object)
], PublishOrderDto.prototype, "locations", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Object)
], PublishOrderDto.prototype, "deliveryPrice", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Object)
], PublishOrderDto.prototype, "thirdPartyPhoneNumber", void 0);
exports.PublishOrderDto = PublishOrderDto;
class GetOrderDto extends (0, mapped_types_1.PickType)(entities_1.Order, [
    'id',
    'createdAt',
    'deliveryDate',
    'uuid',
    'tripId',
    'ownerRole',
    'incomingRequests',
    'outgoingRequests',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(GetOrderDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.GetOrderDto = GetOrderDto;
class GetOrderForTipDto extends (0, mapped_types_1.PickType)(entities_1.Order, [
    'deliveryDate',
    'uuid',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(GetOrderForTipDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", details_dto_1.DetailsCreateDto)
], GetOrderForTipDto.prototype, "details", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Object)
], GetOrderForTipDto.prototype, "locations", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Array)
], GetOrderForTipDto.prototype, "photos", void 0);
exports.GetOrderForTipDto = GetOrderForTipDto;
//# sourceMappingURL=order.dto.js.map