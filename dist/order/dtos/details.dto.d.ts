import { Details } from '../entities';
declare const DetailsCreateDto_base: import("@nestjs/mapped-types").MappedType<Pick<Details, "name" | "type" | "description" | "size" | "quantity">>;
export declare class DetailsCreateDto extends DetailsCreateDto_base {
    static buildDto(payload: Details): DetailsCreateDto;
}
export {};
