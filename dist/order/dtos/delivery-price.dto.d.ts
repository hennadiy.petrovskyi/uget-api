import { DeliveryPrice } from '../entities';
declare const GetOrderDeliveryPriceDto_base: import("@nestjs/mapped-types").MappedType<Pick<DeliveryPrice, "currency" | "total">>;
export declare class GetOrderDeliveryPriceDto extends GetOrderDeliveryPriceDto_base {
    static buildDto(payload: DeliveryPrice): GetOrderDeliveryPriceDto;
}
export {};
