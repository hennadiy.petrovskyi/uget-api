"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryPriceService = exports.OrderHelperService = exports.ThirdPartyNumberService = exports.DetailsService = exports.LocationService = exports.StatusService = exports.OrderService = void 0;
var order_service_1 = require("./order.service");
Object.defineProperty(exports, "OrderService", { enumerable: true, get: function () { return order_service_1.OrderService; } });
var status_service_1 = require("./status.service");
Object.defineProperty(exports, "StatusService", { enumerable: true, get: function () { return status_service_1.StatusService; } });
var location_service_1 = require("./location.service");
Object.defineProperty(exports, "LocationService", { enumerable: true, get: function () { return location_service_1.LocationService; } });
var details_service_1 = require("./details.service");
Object.defineProperty(exports, "DetailsService", { enumerable: true, get: function () { return details_service_1.DetailsService; } });
var third_party_number_service_1 = require("./third-party-number.service");
Object.defineProperty(exports, "ThirdPartyNumberService", { enumerable: true, get: function () { return third_party_number_service_1.ThirdPartyNumberService; } });
var order_helper_service_1 = require("./order-helper.service");
Object.defineProperty(exports, "OrderHelperService", { enumerable: true, get: function () { return order_helper_service_1.OrderHelperService; } });
var price_service_1 = require("./price.service");
Object.defineProperty(exports, "DeliveryPriceService", { enumerable: true, get: function () { return price_service_1.DeliveryPriceService; } });
//# sourceMappingURL=index.js.map