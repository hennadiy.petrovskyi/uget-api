"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entities_1 = require("../entities");
const logger_1 = require("../../utils/logger");
const dtos_1 = require("../dtos");
const event_emitter_1 = require("@nestjs/event-emitter");
let OrderService = class OrderService {
    constructor(orderRepository, logger, eventEmitter) {
        this.orderRepository = orderRepository;
        this.logger = logger;
        this.eventEmitter = eventEmitter;
    }
    async findOrderByOwner(owner, id) {
        const functionName = this.findOrderByUUID.name;
        try {
            const order = await this.orderRepository.findOne({
                where: { owner, id },
            });
            if (!order) {
                return;
            }
            return dtos_1.GetOrderDto.buildDto(order);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async getAllUserOrders(owner) {
        const functionName = this.findOrderByUUID.name;
        try {
            const orders = await this.orderRepository.find({
                where: { owner },
            });
            if (!orders) {
                return;
            }
            return orders.map((order) => dtos_1.GetOrderDto.buildDto(order));
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findTravellerAcceptedOrders(findTravellerAcceptedOrdersDto) {
        const functionName = this.findTravellerAcceptedOrders.name;
        try {
            const orders = await this.orderRepository.find({
                where: findTravellerAcceptedOrdersDto,
                loadRelationIds: true,
            });
            return orders;
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findOrderByUUID(uuid) {
        const functionName = this.findOrderByUUID.name;
        try {
            return await this.orderRepository.findOne({
                where: { uuid },
                loadRelationIds: true,
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findOrderById(id) {
        const functionName = this.findOrderById.name;
        try {
            return await this.orderRepository.findOne({
                where: { id },
                loadRelationIds: true,
            });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async updateOrder(updateOrderDto) {
        const functionName = this.updateOrder.name;
        try {
            return await this.orderRepository.save(updateOrderDto);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async createOrder(orderCreateDto) {
        const functionName = this.createOrder.name;
        try {
            await this.orderRepository.save(orderCreateDto);
            return await this.findOrderByUUID(orderCreateDto.uuid);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async getAllByParams(owner, arrivalDate) {
        const functionName = this.getAllByParams.name;
        const modifiedArrivalDate = new Date(arrivalDate);
        try {
            const orders = await this.orderRepository.find({
                where: {
                    owner: (0, typeorm_2.Not)(owner),
                    deliveryDate: (0, typeorm_2.Between)(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString(), new Date(modifiedArrivalDate.getFullYear(), modifiedArrivalDate.getMonth(), modifiedArrivalDate.getDate() + 1).toISOString()),
                },
                loadRelationIds: true,
            });
            if (!orders) {
                return;
            }
            return orders;
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async updateOrderRequests(orderId, update) {
        const functionName = this.updateOrderRequests.name;
        try {
            await this.orderRepository.update(orderId, update);
            return await this.findOrderById(orderId);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async deleteOrder(id) {
        const functionName = this.deleteOrder.name;
        try {
            await this.orderRepository.delete(id);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
};
OrderService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.Order)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        logger_1.LoggerService,
        event_emitter_1.EventEmitter2])
], OrderService);
exports.OrderService = OrderService;
//# sourceMappingURL=order.service.js.map