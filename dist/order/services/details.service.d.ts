import { FindOperator, Repository } from 'typeorm';
import { Details } from '../entities';
import { LoggerService } from 'src/utils/logger';
import { DetailsCreateDto } from '../dtos';
export declare class DetailsService {
    private detailsRepository;
    private logger;
    constructor(detailsRepository: Repository<Details>, logger: LoggerService);
    saveOrderDetails(orderDetailsDto: any): Promise<void>;
    getOrderDetails(owner: FindOperator<any>): Promise<DetailsCreateDto>;
    deleteOrderDetails(owner: FindOperator<any>): Promise<void>;
}
