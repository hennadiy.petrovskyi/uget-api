import { FindOperator, Repository } from 'typeorm';
import { Order } from '../entities';
import { LoggerService } from 'src/utils/logger';
import { GetOrderDto, OrderCreateDto } from '../dtos';
import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class OrderService {
    private orderRepository;
    private logger;
    private eventEmitter;
    constructor(orderRepository: Repository<Order>, logger: LoggerService, eventEmitter: EventEmitter2);
    findOrderByOwner(owner: FindOperator<any>, id: number): Promise<GetOrderDto>;
    getAllUserOrders(owner: FindOperator<any>): Promise<GetOrderDto[]>;
    findTravellerAcceptedOrders(findTravellerAcceptedOrdersDto: {
        tripId: string;
        isDelivered: boolean;
    }): Promise<Order[]>;
    findOrderByUUID(uuid: string): Promise<Order>;
    findOrderById(id: FindOperator<any>): Promise<Order>;
    updateOrder(updateOrderDto: any): Promise<any>;
    createOrder(orderCreateDto: OrderCreateDto): Promise<Order>;
    getAllByParams(owner: number, arrivalDate: string): Promise<Order[]>;
    updateOrderRequests(orderId: any, update: any): Promise<Order>;
    deleteOrder(id: number): Promise<void>;
}
