import { FindOperator, Repository } from 'typeorm';
import { Status } from '../entities';
import { LoggerService } from 'src/utils/logger';
import { GetStatusDto } from '../dtos';
export declare class StatusService {
    private statusRepository;
    private logger;
    constructor(statusRepository: Repository<Status>, logger: LoggerService);
    findStatusById(owner: FindOperator<any>): Promise<Status>;
    findAllOrderStatusesHistory(owner: FindOperator<any>): Promise<GetStatusDto[]>;
    addOrderStatus(statusAddNewDto: any): Promise<void>;
    deleteAcceptedStatus(owner: FindOperator<any>): Promise<void>;
    deleteAllOrderStatuses(owner: FindOperator<any>): Promise<void>;
}
