import { FindOperator, Repository } from 'typeorm';
import { DeliveryPrice } from '../entities';
import { LoggerService } from 'src/utils/logger';
export declare class DeliveryPriceService {
    private priceRepository;
    private logger;
    constructor(priceRepository: Repository<DeliveryPrice>, logger: LoggerService);
    saveOrderDeliveryPrice(orderDeliveryPriceDto: any): Promise<void>;
    getOrderDeliveryPrice(owner: FindOperator<any>): Promise<DeliveryPrice>;
    deleteOrderDeliveryPrice(owner: FindOperator<any>): Promise<void>;
}
