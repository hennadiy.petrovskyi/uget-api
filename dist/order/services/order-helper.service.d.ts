import { FindOperator } from 'typeorm';
import { DetailsService, LocationService, DeliveryPriceService, StatusService, ThirdPartyNumberService } from '.';
import { LoggerService } from '../../utils/logger';
import { OrderEmitter } from '../emitters';
import { OrderLocationType } from '../enums';
export declare class OrderHelperService {
    private readonly statusService;
    private readonly detailsService;
    private readonly locationService;
    private readonly thirdPartyNumberService;
    private readonly deliveryPriceService;
    private logger;
    private orderEmitter;
    constructor(statusService: StatusService, detailsService: DetailsService, locationService: LocationService, thirdPartyNumberService: ThirdPartyNumberService, deliveryPriceService: DeliveryPriceService, logger: LoggerService, orderEmitter: OrderEmitter);
    getLocationsDetails(tripId: FindOperator<any>, locationType: OrderLocationType): Promise<any>;
    buildOrderForTripDTO(order: any, tripUUID: any): Promise<{
        owner: {
            profileImage: any;
            fullName: string;
            rating: any;
        };
        status: string;
        deliveryPrice: {
            value: string;
            currency: import("../../pricing/enums").Currency;
        };
        details: import("../dtos").DetailsCreateDto;
        locations: {
            pickup: import("../dtos").LocationCreateDto;
            endpoint: import("../dtos").LocationCreateDto;
        };
        photos: any[];
        uuid: string;
        deliveryDate: string;
    }>;
    buildOrderDto(order: any): Promise<any>;
}
