import { FindOperator, Repository } from 'typeorm';
import { ThirdPartyPhoneNumber } from '../entities';
import { LoggerService } from '../../utils/logger';
export declare class ThirdPartyNumberService {
    private phoneNumberRepository;
    private logger;
    constructor(phoneNumberRepository: Repository<ThirdPartyPhoneNumber>, logger: LoggerService);
    findPhoneNumberByOwner(owner: FindOperator<any>): Promise<ThirdPartyPhoneNumber>;
    savePhoneNumber(savePhoneNumber: any): Promise<any>;
    updatePhoneNumberDetails(id: any, update: any): Promise<any>;
    deletePhoneNumber(owner: FindOperator<any>): Promise<void>;
}
