"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entities_1 = require("../entities");
const logger_1 = require("../../utils/logger");
const dtos_1 = require("../dtos");
const enums_1 = require("../enums");
let StatusService = class StatusService {
    constructor(statusRepository, logger) {
        this.statusRepository = statusRepository;
        this.logger = logger;
    }
    async findStatusById(owner) {
        const functionName = this.findStatusById.name;
        try {
            return await this.statusRepository.findOne({ where: { owner } });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async findAllOrderStatusesHistory(owner) {
        const functionName = this.findAllOrderStatusesHistory.name;
        try {
            const statuses = await this.statusRepository.find({ where: { owner } });
            return statuses.map((status) => dtos_1.GetStatusDto.buildDto(status));
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async addOrderStatus(statusAddNewDto) {
        const functionName = this.addOrderStatus.name;
        try {
            await this.statusRepository.save(statusAddNewDto);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async deleteAcceptedStatus(owner) {
        const functionName = this.deleteAllOrderStatuses.name;
        const statusAccepted = await this.statusRepository.findOne({
            where: { owner, status: enums_1.OrderStatus.ACCEPTED },
        });
        try {
            this.statusRepository.delete(statusAccepted.id);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async deleteAllOrderStatuses(owner) {
        const functionName = this.deleteAllOrderStatuses.name;
        const statuses = await this.statusRepository.find({ where: { owner } });
        try {
            Promise.all(statuses.map((status) => this.statusRepository.delete(status.id)));
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
};
StatusService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.Status)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        logger_1.LoggerService])
], StatusService);
exports.StatusService = StatusService;
//# sourceMappingURL=status.service.js.map