import { FindOperator, Repository } from 'typeorm';
import { OrderLocation } from '../entities';
import { LoggerService } from 'src/utils/logger';
export declare class LocationService {
    private locationsRepository;
    private logger;
    constructor(locationsRepository: Repository<OrderLocation>, logger: LoggerService);
    saveOrderLocation(orderLocationSaveDto: any): Promise<void>;
    getOrderLocations(owner: FindOperator<any>, type: any): Promise<OrderLocation>;
    deleteOrderLocation(owner: FindOperator<any>): Promise<void>;
}
