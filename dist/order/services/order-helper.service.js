"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderHelperService = void 0;
const common_1 = require("@nestjs/common");
const _1 = require(".");
const logger_1 = require("../../utils/logger");
const dtos_1 = require("../dtos");
const emitters_1 = require("../emitters");
const enums_1 = require("../enums");
let OrderHelperService = class OrderHelperService {
    constructor(statusService, detailsService, locationService, thirdPartyNumberService, deliveryPriceService, logger, orderEmitter) {
        this.statusService = statusService;
        this.detailsService = detailsService;
        this.locationService = locationService;
        this.thirdPartyNumberService = thirdPartyNumberService;
        this.deliveryPriceService = deliveryPriceService;
        this.logger = logger;
        this.orderEmitter = orderEmitter;
    }
    async getLocationsDetails(tripId, locationType) {
        const tripLocations = await this.locationService.getOrderLocations(tripId, locationType);
        const details = await this.orderEmitter.getLocationDetails(tripLocations.detailsId);
        return Object.assign(Object.assign({}, details), { area: tripLocations.area });
    }
    async buildOrderForTripDTO(order, tripUUID) {
        const dto = await this.buildOrderDto(order);
        const statusesHistory = await this.statusService.findAllOrderStatusesHistory(order.id);
        const statusesParsed = statusesHistory.map((status) => status.status);
        const owner = await this.orderEmitter.getUserDetails(order.owner);
        const profileImage = await this.orderEmitter.getUserProfileImage(order.owner);
        const priceDetails = dtos_1.GetOrderDeliveryPriceDto.buildDto(await this.deliveryPriceService.getOrderDeliveryPrice(order.id));
        const priceForTraveler = +priceDetails.total / 2;
        function handleStatus() {
            if (statusesParsed.includes(enums_1.OrderStatus.DELIVERED) &&
                order.tripId === tripUUID) {
                return 'delivered';
            }
            if (statusesParsed.includes(enums_1.OrderStatus.PICKED_UP) &&
                order.tripId === tripUUID) {
                return 'progress';
            }
            if (order.tripId === tripUUID) {
                return 'accepted';
            }
            if (order.outgoingRequests.includes(tripUUID)) {
                return 'request.incoming';
            }
            if (order.incomingRequests.includes(tripUUID)) {
                return 'request.outgoing';
            }
            return 'available';
        }
        return Object.assign(Object.assign({}, dtos_1.GetOrderForTipDto.buildDto(dto)), { owner: Object.assign(Object.assign({}, owner), { profileImage }), status: handleStatus(), deliveryPrice: {
                value: priceForTraveler.toFixed(2),
                currency: priceDetails.currency,
            } });
    }
    async buildOrderDto(order) {
        const statusHistory = await this.statusService.findAllOrderStatusesHistory(order.id);
        const details = await this.detailsService.getOrderDetails(order.id);
        const photos = (await this.orderEmitter.getOrderPhotos(order.uuid)) || [];
        const thirdPartyPhoneNumber = dtos_1.ThirdPartPhoneNumberDto.buildDto(await this.thirdPartyNumberService.findPhoneNumberByOwner(order.id));
        const priceDetails = dtos_1.GetOrderDeliveryPriceDto.buildDto(await this.deliveryPriceService.getOrderDeliveryPrice(order.id));
        const pickup = await this.locationService.getOrderLocations(order.id, enums_1.OrderLocationType.PICKUP);
        const endpoint = await this.locationService.getOrderLocations(order.id, enums_1.OrderLocationType.ENDPOINT);
        const pickupDetails = await this.orderEmitter.getLocationDetails(pickup.detailsId);
        const endpointDetails = await this.orderEmitter.getLocationDetails(endpoint.detailsId);
        return Object.assign(Object.assign({}, order), { status: statusHistory[statusHistory.length - 1].status, statusHistory,
            details,
            thirdPartyPhoneNumber, locations: {
                pickup: Object.assign(Object.assign({}, pickupDetails), dtos_1.LocationResponseDto.buildDto(pickup)),
                endpoint: Object.assign(Object.assign({}, endpointDetails), dtos_1.LocationResponseDto.buildDto(endpoint)),
            }, photos, deliveryPrice: {
                value: priceDetails.total,
                currency: priceDetails.currency,
            } });
    }
};
OrderHelperService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)((0, common_1.forwardRef)(() => _1.StatusService))),
    __param(1, (0, common_1.Inject)((0, common_1.forwardRef)(() => _1.DetailsService))),
    __param(2, (0, common_1.Inject)((0, common_1.forwardRef)(() => _1.LocationService))),
    __param(3, (0, common_1.Inject)((0, common_1.forwardRef)(() => _1.ThirdPartyNumberService))),
    __param(4, (0, common_1.Inject)((0, common_1.forwardRef)(() => _1.DeliveryPriceService))),
    __metadata("design:paramtypes", [_1.StatusService,
        _1.DetailsService,
        _1.LocationService,
        _1.ThirdPartyNumberService,
        _1.DeliveryPriceService,
        logger_1.LoggerService,
        emitters_1.OrderEmitter])
], OrderHelperService);
exports.OrderHelperService = OrderHelperService;
//# sourceMappingURL=order-helper.service.js.map