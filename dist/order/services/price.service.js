"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryPriceService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entities_1 = require("../entities");
const logger_1 = require("../../utils/logger");
let DeliveryPriceService = class DeliveryPriceService {
    constructor(priceRepository, logger) {
        this.priceRepository = priceRepository;
        this.logger = logger;
    }
    async saveOrderDeliveryPrice(orderDeliveryPriceDto) {
        const functionName = this.saveOrderDeliveryPrice.name;
        try {
            await this.priceRepository.save(orderDeliveryPriceDto);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async getOrderDeliveryPrice(owner) {
        const functionName = this.getOrderDeliveryPrice.name;
        try {
            return await this.priceRepository.findOne({ where: { owner } });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async deleteOrderDeliveryPrice(owner) {
        const functionName = this.deleteOrderDeliveryPrice.name;
        try {
            const details = await this.priceRepository.findOne({
                where: { owner },
            });
            await this.priceRepository.delete(details.id);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
};
DeliveryPriceService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.DeliveryPrice)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        logger_1.LoggerService])
], DeliveryPriceService);
exports.DeliveryPriceService = DeliveryPriceService;
//# sourceMappingURL=price.service.js.map