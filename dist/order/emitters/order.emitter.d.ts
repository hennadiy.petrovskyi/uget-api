import { EventEmitter2 } from '@nestjs/event-emitter';
import { CancelEventType } from 'src/trips/enums';
export declare class OrderEmitter {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    getOrderPhotos(owner: string): Promise<any>;
    getLocationDetails(id: number): Promise<any>;
    saveLocationDetails(saveLocationDetailsDto: any): Promise<any>;
    deleteAllPhotos(id: string): Promise<any[]>;
    addTripOutgoingRequest(orderOutgoingRequestDto: any): Promise<void>;
    cancelTripDelivery(type: CancelEventType, cancelOrderOutgoingRequestDto: any): Promise<void>;
    orderOwnerAcceptDeliveryRequest(acceptDeliveryRequestDto: any): Promise<void>;
    requestPickup(verifyPickup: any): Promise<void>;
    verifyPickupRequest(verifyPickupDto: any): Promise<any>;
    startDelivery(startDeliveryDto: any): Promise<void>;
    finishOrderDelivery(startDeliveryDto: any): Promise<void>;
    requestOwnerUUID(id: any): Promise<any>;
    savePhoneNumberDetails(phoneNumberDto: any): Promise<void>;
    getPhoneNumberDetails(phoneNumberDto: any): Promise<any>;
    getUserDetails(id: number): Promise<{
        fullName: string;
        rating: any;
        profileImage: any;
    }>;
    getUserProfileImage(ownerId: number): Promise<any>;
    getAuthRecordUUID(id: number): Promise<any>;
    addUnratedUserRating(dto: any): Promise<any>;
    sendIncomingRequestNotification(dto: {
        from: string;
        to: any;
    }): Promise<void>;
    sendAcceptedRequestNotification(dto: {
        from: string;
        to: any;
    }): Promise<void>;
    sendOrderVerifyPickedUpNotification(dto: {
        orderUUID: string;
        to: any;
        from: number;
    }): Promise<void>;
    sendVerifyOrderDeliveryNotification(dto: {
        orderUUID: string;
        to: any;
        from: number;
    }): Promise<void>;
    calculateDeliveryPrice(calculatePriceDto: {
        from: string;
        to: string;
        size: any;
    }): Promise<any>;
    sendSocketMessage(dto: {
        uuid: string;
        payload: any;
    }): Promise<void>;
}
