"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const event_emitter_1 = require("@nestjs/event-emitter");
const config_validator_1 = require("./utils/config/config.validator");
const auth_module_1 = require("./auth/auth.module");
const notification_module_1 = require("./notification/notification.module");
const user_module_1 = require("./user/user.module");
const location_module_1 = require("./location/location.module");
const file_module_1 = require("./file/file.module");
const order_module_1 = require("./order/order.module");
const terminus_1 = require("@nestjs/terminus");
const health_controller_1 = require("./health.controller");
const trip_module_1 = require("./trips/trip.module");
const pricing_module_1 = require("./pricing/pricing.module");
const chat_module_1 = require("./chat/chat.module");
const socket_module_1 = require("./socket/socket.module");
const verify_controller_1 = require("./verify.controller");
const redirect_controller_1 = require("./redirect.controller");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        controllers: [
            health_controller_1.HealthController,
            verify_controller_1.AndroidLinksVerificationController,
            redirect_controller_1.RedirectDeepLinkController,
        ],
        imports: [
            config_1.ConfigModule.forRoot({
                envFilePath: [`.env.${process.env.NODE_ENV}`],
                isGlobal: true,
                cache: true,
                validationSchema: config_validator_1.JoiConfigValidator,
                validationOptions: {
                    allowUnknown: true,
                    abortEarly: true,
                },
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                inject: [config_1.ConfigService],
                useFactory: (configService) => {
                    return {
                        type: 'postgres',
                        autoLoadEntities: true,
                        host: configService.get('DB_HOST'),
                        port: configService.get('DB_PORT'),
                        database: configService.get('DB_NAME'),
                        username: configService.get('DB_USER'),
                        password: configService.get('DB_PASS'),
                        synchronize: configService.get('DB_SYNC'),
                        logging: configService.get('DB_LOGGING'),
                    };
                },
            }),
            event_emitter_1.EventEmitterModule.forRoot({
                wildcard: false,
                delimiter: '.',
                newListener: false,
                removeListener: false,
                verboseMemoryLeak: false,
                ignoreErrors: false,
            }),
            terminus_1.TerminusModule,
            socket_module_1.SocketModule,
            auth_module_1.AuthModule,
            user_module_1.UserModule,
            notification_module_1.NotificationModule,
            location_module_1.LocationModule,
            file_module_1.FileModule,
            order_module_1.OrderModule,
            trip_module_1.TripModule,
            pricing_module_1.PriceModule,
            chat_module_1.ChatModule,
        ],
        providers: [],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map