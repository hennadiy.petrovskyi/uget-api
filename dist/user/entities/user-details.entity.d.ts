import { AuthRecord } from '../../auth/entities';
import { BaseEntity } from 'src/common/entities';
export declare class UserDetails extends BaseEntity {
    owner: AuthRecord;
    firstName: string;
    lastName: string;
    toRate: string[];
}
