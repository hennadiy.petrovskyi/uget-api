"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Rating = exports.UserDetails = void 0;
var user_details_entity_1 = require("./user-details.entity");
Object.defineProperty(exports, "UserDetails", { enumerable: true, get: function () { return user_details_entity_1.UserDetails; } });
var rating_entity_1 = require("./rating.entity");
Object.defineProperty(exports, "Rating", { enumerable: true, get: function () { return rating_entity_1.Rating; } });
//# sourceMappingURL=index.js.map