import { BaseEntity } from 'src/common/entities';
import { UserDeliveryRole } from '../enums';
import { AuthRecord } from 'src/auth/entities';
export declare class Rating extends BaseEntity {
    owner: AuthRecord;
    role: UserDeliveryRole;
    orderId: string;
    isRated: boolean;
    ratedBy: number;
    stars: number;
    comment: string;
}
