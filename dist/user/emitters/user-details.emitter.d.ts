import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class UserDetailsEmitter {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    getUserAuthCredentials(owner: number): Promise<any[]>;
    geAuthRecord(owner: string): Promise<any>;
    getUserProfileImage(owner: string): Promise<any>;
}
