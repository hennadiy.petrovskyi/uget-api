"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserGetUserDetailsDto = exports.UserCreateUserDetailsDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const entities_1 = require("../entities");
class UserCreateUserDetailsDto extends (0, swagger_1.PickType)(entities_1.UserDetails, [
    'owner',
    'firstName',
    'lastName',
]) {
}
exports.UserCreateUserDetailsDto = UserCreateUserDetailsDto;
class UserGetUserDetailsDto extends (0, swagger_1.PickType)(entities_1.UserDetails, [
    'firstName',
    'lastName',
]) {
    static buildDto(payload) {
        return (0, class_transformer_1.plainToClass)(UserGetUserDetailsDto, payload, {
            excludeExtraneousValues: true,
        });
    }
}
exports.UserGetUserDetailsDto = UserGetUserDetailsDto;
//# sourceMappingURL=user-details.dto.js.map