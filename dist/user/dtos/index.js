"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserGetUserDetailsDto = exports.UserCreateUserDetailsDto = void 0;
var user_details_dto_1 = require("./user-details.dto");
Object.defineProperty(exports, "UserCreateUserDetailsDto", { enumerable: true, get: function () { return user_details_dto_1.UserCreateUserDetailsDto; } });
Object.defineProperty(exports, "UserGetUserDetailsDto", { enumerable: true, get: function () { return user_details_dto_1.UserGetUserDetailsDto; } });
//# sourceMappingURL=index.js.map