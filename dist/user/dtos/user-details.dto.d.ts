import { UserDetails } from '../entities';
declare const UserCreateUserDetailsDto_base: import("@nestjs/common").Type<Pick<UserDetails, "owner" | "firstName" | "lastName">>;
export declare class UserCreateUserDetailsDto extends UserCreateUserDetailsDto_base {
}
declare const UserGetUserDetailsDto_base: import("@nestjs/common").Type<Pick<UserDetails, "firstName" | "lastName">>;
export declare class UserGetUserDetailsDto extends UserGetUserDetailsDto_base {
    static buildDto(payload: UserDetails): UserGetUserDetailsDto;
}
export {};
