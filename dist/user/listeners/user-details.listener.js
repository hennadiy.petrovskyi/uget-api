"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDetailsListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_1 = require("../../common/events");
const dtos_1 = require("../dtos");
const services_1 = require("../services");
const enums_1 = require("../enums");
const typeorm_1 = require("typeorm");
let UserDetailsListener = class UserDetailsListener {
    constructor(userDetailsService, ratingService) {
        this.userDetailsService = userDetailsService;
        this.ratingService = ratingService;
    }
    async createUserDetails(userCreateUserDetailsDto) {
        return await this.userDetailsService.createNewUserDetails(userCreateUserDetailsDto);
    }
    async getUserDetails(owner) {
        const details = await this.userDetailsService.findUserDetails(owner);
        const ratedRatings = await this.ratingService.findAllRatingsByOwner(owner);
        const countAverage = (arr) => {
            if (!arr.length) {
                return 0;
            }
            return (arr.map((el) => el.stars).reduce((prev, current) => prev + current) /
                arr.length);
        };
        const notRated = await this.ratingService.findNotRatedRatingsByOwner(owner);
        if (!details) {
            return null;
        }
        const asTraveler = ratedRatings.filter((el) => el.role === enums_1.UserDeliveryRole.TRAVELLER);
        const asSender = ratedRatings.filter((el) => el.role === enums_1.UserDeliveryRole.SENDER);
        return Object.assign(Object.assign({}, dtos_1.UserGetUserDetailsDto.buildDto(details)), { rating: {
                average: countAverage(ratedRatings),
                sender: {
                    value: countAverage(asSender),
                    amount: asSender.length,
                },
                traveler: {
                    value: countAverage(asTraveler),
                    amount: asTraveler.length,
                },
                toRate: notRated.map(({ id, role }) => ({ id, role })),
            } });
    }
    async updateUserDetails(updateDto) {
        const { owner } = updateDto, update = __rest(updateDto, ["owner"]);
        const details = await this.userDetailsService.findUserDetails(owner);
        await this.userDetailsService.updateUserDetails(Object.assign({ id: details.id }, update));
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.USER_EVENTS.CREATE_USER_DETAILS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.UserCreateUserDetailsDto]),
    __metadata("design:returntype", Promise)
], UserDetailsListener.prototype, "createUserDetails", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeorm_1.FindOperator]),
    __metadata("design:returntype", Promise)
], UserDetailsListener.prototype, "getUserDetails", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.USER_EVENTS.UPDATE_USER_DETAILS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserDetailsListener.prototype, "updateUserDetails", null);
UserDetailsListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.UserDetailsService,
        services_1.RatingService])
], UserDetailsListener);
exports.UserDetailsListener = UserDetailsListener;
//# sourceMappingURL=user-details.listener.js.map