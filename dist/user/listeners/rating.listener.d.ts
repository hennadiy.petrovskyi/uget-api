import { EventEmitter2 } from '@nestjs/event-emitter';
import { RatingService } from '../services';
export declare class RatingListener {
    private readonly ratingService;
    private eventEmitter;
    constructor(ratingService: RatingService, eventEmitter: EventEmitter2);
    createUserDetails(newRateDto: any): Promise<void>;
    getUserDetails(updateRateDto: any): Promise<void>;
}
