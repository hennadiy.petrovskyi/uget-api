import { UserCreateUserDetailsDto } from '../dtos';
import { UserDetailsService, RatingService } from '../services';
import { UserDeliveryRole } from '../enums';
import { FindOperator } from 'typeorm';
export declare class UserDetailsListener {
    private readonly userDetailsService;
    private readonly ratingService;
    constructor(userDetailsService: UserDetailsService, ratingService: RatingService);
    createUserDetails(userCreateUserDetailsDto: UserCreateUserDetailsDto): Promise<void>;
    getUserDetails(owner: FindOperator<any>): Promise<{
        rating: {
            average: number;
            sender: {
                value: number;
                amount: number;
            };
            traveler: {
                value: number;
                amount: number;
            };
            toRate: {
                id: number;
                role: UserDeliveryRole;
            }[];
        };
        firstName: string;
        lastName: string;
    }>;
    updateUserDetails(updateDto: any): Promise<void>;
}
