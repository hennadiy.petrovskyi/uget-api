"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RatingListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_1 = require("../../common/events");
const services_1 = require("../services");
const events_2 = require("../../socket/enums/events");
let RatingListener = class RatingListener {
    constructor(ratingService, eventEmitter) {
        this.ratingService = ratingService;
        this.eventEmitter = eventEmitter;
    }
    async createUserDetails(newRateDto) {
        await this.ratingService.createNewRatingRecord(newRateDto);
        await this.eventEmitter.emitAsync(events_1.SOCKET_EVENTS.SEND_MESSAGE, {
            uuid: newRateDto.owner.uuid,
            payload: {
                action: events_2.SocketEvents.UPDATE_PROFILE,
            },
        });
    }
    async getUserDetails(updateRateDto) {
        const rating = await this.ratingService.findRatingByOwner(updateRateDto.owner);
        console.log(updateRateDto);
        await this.ratingService.updateRatingRecord(updateRateDto);
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.USER_EVENTS.ADD_NEW_UNRATED_RATE, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RatingListener.prototype, "createUserDetails", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.USER_EVENTS.SET_RATING_IS_RATED, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RatingListener.prototype, "getUserDetails", null);
RatingListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.RatingService,
        event_emitter_1.EventEmitter2])
], RatingListener);
exports.RatingListener = RatingListener;
//# sourceMappingURL=rating.listener.js.map