"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const logger_1 = require("../utils/logger");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const services_1 = require("./services");
const entities_1 = require("./entities");
const controllers_1 = require("./controllers");
const listeners_1 = require("./listeners");
const emitters_1 = require("./emitters");
const morgan_middleware_1 = require("../common/middlewares/morgan.middleware");
let UserModule = class UserModule {
    configure(consumer) {
        consumer.apply(morgan_middleware_1.MorganMiddleware).forRoutes(controllers_1.UserController);
    }
};
UserModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([entities_1.UserDetails, entities_1.Rating]), logger_1.LoggerModule],
        controllers: [controllers_1.UserController, controllers_1.RatingController],
        providers: [
            services_1.UserDetailsService,
            services_1.RatingService,
            listeners_1.UserDetailsListener,
            listeners_1.RatingListener,
            emitters_1.UserDetailsEmitter,
        ],
        exports: [services_1.UserDetailsService],
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map