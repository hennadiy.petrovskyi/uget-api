export declare enum UserDeliveryRole {
    SENDER = "SENDER",
    TRAVELLER = "TRAVELLER"
}
