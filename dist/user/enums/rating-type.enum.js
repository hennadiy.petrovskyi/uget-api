"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDeliveryRole = void 0;
var UserDeliveryRole;
(function (UserDeliveryRole) {
    UserDeliveryRole["SENDER"] = "SENDER";
    UserDeliveryRole["TRAVELLER"] = "TRAVELLER";
})(UserDeliveryRole = exports.UserDeliveryRole || (exports.UserDeliveryRole = {}));
//# sourceMappingURL=rating-type.enum.js.map