"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RatingService = exports.UserDetailsService = void 0;
var user_details_service_1 = require("./user-details.service");
Object.defineProperty(exports, "UserDetailsService", { enumerable: true, get: function () { return user_details_service_1.UserDetailsService; } });
var rating_service_1 = require("./rating.service");
Object.defineProperty(exports, "RatingService", { enumerable: true, get: function () { return rating_service_1.RatingService; } });
//# sourceMappingURL=index.js.map