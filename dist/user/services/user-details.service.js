"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDetailsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entities_1 = require("../entities");
const emitters_1 = require("../emitters");
const logger_1 = require("../../utils/logger");
const dtos_1 = require("../dtos");
let UserDetailsService = class UserDetailsService {
    constructor(userDetailsRepository, userDetailsEmitter, logger) {
        this.userDetailsRepository = userDetailsRepository;
        this.userDetailsEmitter = userDetailsEmitter;
        this.logger = logger;
    }
    async findUserDetails(owner) {
        const functionName = this.findUserDetails.name;
        try {
            return this.userDetailsRepository.findOne({ where: { owner } });
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async createNewUserDetails(userCreateUserDetailsDto) {
        const functionName = this.createNewUserDetails.name;
        try {
            await this.userDetailsRepository.save(userCreateUserDetailsDto);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async updateUserDetails(update) {
        const functionName = this.updateUserDetails.name;
        try {
            return await this.userDetailsRepository.save(update);
        }
        catch (error) {
            this.logger.logError(functionName, error);
        }
    }
    async buildUserDto() {
        try {
            const owner = 1;
            const userDetails = await this.findUserDetails(owner);
            const credentials = await this.userDetailsEmitter.getUserAuthCredentials(owner);
            const modifiedUserDetails = dtos_1.UserGetUserDetailsDto.buildDto(userDetails);
            console.log('user', Object.assign(Object.assign({}, modifiedUserDetails), credentials[0]));
        }
        catch (error) { }
    }
};
UserDetailsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.UserDetails)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        emitters_1.UserDetailsEmitter,
        logger_1.LoggerService])
], UserDetailsService);
exports.UserDetailsService = UserDetailsService;
//# sourceMappingURL=user-details.service.js.map