import { FindOperator, Repository } from 'typeorm';
import { UserDetails } from '../entities';
import { UserDetailsEmitter } from '../emitters';
import { LoggerService } from '../../utils/logger';
import { UserCreateUserDetailsDto } from '../dtos';
export declare class UserDetailsService {
    private userDetailsRepository;
    private userDetailsEmitter;
    private logger;
    constructor(userDetailsRepository: Repository<UserDetails>, userDetailsEmitter: UserDetailsEmitter, logger: LoggerService);
    findUserDetails(owner: FindOperator<number>): Promise<UserDetails>;
    createNewUserDetails(userCreateUserDetailsDto: UserCreateUserDetailsDto): Promise<void>;
    updateUserDetails(update: any): Promise<any>;
    buildUserDto(): Promise<void>;
}
