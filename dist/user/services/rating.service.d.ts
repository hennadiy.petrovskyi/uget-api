import { FindOperator, Repository } from 'typeorm';
import { Rating } from '../entities';
import { LoggerService } from '../../utils/logger';
export declare class RatingService {
    private ratingRepository;
    private logger;
    constructor(ratingRepository: Repository<Rating>, logger: LoggerService);
    findRatingByOwner(owner: FindOperator<any>): Promise<Rating>;
    findRatingById(id: number): Promise<Rating>;
    findNotRatedRatingsByOwner(ratedBy: number): Promise<Rating[]>;
    findAllRatingsByOwner(owner: FindOperator<any>): Promise<Rating[]>;
    createNewRatingRecord(createRate: any): Promise<void>;
    updateRatingRecord(update: any): Promise<any>;
}
