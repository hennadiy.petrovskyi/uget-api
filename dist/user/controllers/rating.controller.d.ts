import { RatingService } from '../services';
import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class RatingController {
    private service;
    private eventEmitter;
    constructor(service: RatingService, eventEmitter: EventEmitter2);
    rateUser(rateUserDto: any, user: any): Promise<void>;
}
