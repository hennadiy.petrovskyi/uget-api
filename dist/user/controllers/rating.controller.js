"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RatingController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const services_1 = require("../services");
const jwt_auth_guard_1 = require("../../common/guards/jwt-auth.guard");
const decorators_1 = require("../../common/decorators");
const event_emitter_1 = require("@nestjs/event-emitter");
const events_1 = require("../../socket/enums/events");
const events_2 = require("../../common/events");
let RatingController = class RatingController {
    constructor(service, eventEmitter) {
        this.service = service;
        this.eventEmitter = eventEmitter;
    }
    async rateUser(rateUserDto, user) {
        const ratingToUpdate = await this.service.findRatingById(rateUserDto.id);
        if (!ratingToUpdate) {
            throw new common_1.NotFoundException();
        }
        if (ratingToUpdate.ratedBy !== user.id || ratingToUpdate.isRated) {
            throw new common_1.ForbiddenException();
        }
        await this.service.updateRatingRecord(Object.assign(Object.assign({}, rateUserDto), { isRated: true }));
        const result = await this.eventEmitter.emitAsync(events_2.AUTH_EVENTS.GET_USER_BY_ID, ratingToUpdate.owner);
        if (result && result.length) {
            await this.eventEmitter.emitAsync(events_2.SOCKET_EVENTS.SEND_MESSAGE, {
                uuid: result[0].uuid,
                payload: {
                    action: events_1.SocketEvents.UPDATE_PROFILE,
                },
            });
        }
        console.log(ratingToUpdate);
    }
};
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Put)(),
    __param(0, (0, common_1.Body)('')),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], RatingController.prototype, "rateUser", null);
RatingController = __decorate([
    (0, swagger_1.ApiTags)('Rating'),
    (0, common_1.Controller)('rating'),
    __metadata("design:paramtypes", [services_1.RatingService,
        event_emitter_1.EventEmitter2])
], RatingController);
exports.RatingController = RatingController;
//# sourceMappingURL=rating.controller.js.map