"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RatingController = exports.UserController = void 0;
var user_controller_1 = require("./user.controller");
Object.defineProperty(exports, "UserController", { enumerable: true, get: function () { return user_controller_1.UserController; } });
var rating_controller_1 = require("./rating.controller");
Object.defineProperty(exports, "RatingController", { enumerable: true, get: function () { return rating_controller_1.RatingController; } });
//# sourceMappingURL=index.js.map