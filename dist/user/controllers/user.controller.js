"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const user_details_emitter_1 = require("../emitters/user-details.emitter");
const services_1 = require("../services");
let UserController = class UserController {
    constructor(service, ratingService, userDetailsEmitter) {
        this.service = service;
        this.ratingService = ratingService;
        this.userDetailsEmitter = userDetailsEmitter;
    }
    async getUserDetails(userUUID) {
        const authRecord = await this.userDetailsEmitter.geAuthRecord(userUUID);
        if (!authRecord) {
            throw new common_1.NotFoundException();
        }
        const profileImage = await this.userDetailsEmitter.getUserProfileImage(userUUID);
        const userDetails = await this.service.findUserDetails(authRecord.id);
        const ratedRatings = await this.ratingService.findAllRatingsByOwner(authRecord.id);
        const countAverage = (arr) => {
            if (!arr.length) {
                return 0;
            }
            return (arr.map((el) => el.stars).reduce((prev, current) => prev + current) /
                arr.length);
        };
        return {
            fullName: userDetails.firstName + ' ' + userDetails.lastName,
            profileImage,
            rating: { average: countAverage(ratedRatings) },
        };
    }
};
__decorate([
    (0, common_1.Post)(''),
    __param(0, (0, common_1.Body)('userUUID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUserDetails", null);
UserController = __decorate([
    (0, swagger_1.ApiTags)('User Details'),
    (0, common_1.Controller)('user-details'),
    __metadata("design:paramtypes", [services_1.UserDetailsService,
        services_1.RatingService,
        user_details_emitter_1.UserDetailsEmitter])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map