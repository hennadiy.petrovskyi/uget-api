import { UserDetailsEmitter } from '../emitters/user-details.emitter';
import { UserDetailsService, RatingService } from '../services';
export declare class UserController {
    private service;
    private ratingService;
    private userDetailsEmitter;
    constructor(service: UserDetailsService, ratingService: RatingService, userDetailsEmitter: UserDetailsEmitter);
    getUserDetails(userUUID: any): Promise<{
        fullName: string;
        profileImage: any;
        rating: {
            average: number;
        };
    }>;
}
