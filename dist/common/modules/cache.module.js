"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CacheRedisModule = void 0;
const redisStore = require("cache-manager-redis-store");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const cache_service_1 = require("../services/cache.service");
const logger_1 = require("../../utils/logger");
let CacheRedisModule = class CacheRedisModule {
};
CacheRedisModule = __decorate([
    (0, common_1.Module)({
        imports: [
            common_1.CacheModule.registerAsync({
                imports: [config_1.ConfigModule, logger_1.LoggerModule],
                inject: [config_1.ConfigService],
                useFactory: (configService) => ({
                    store: redisStore,
                    host: configService.get('REDIS_HOST'),
                    port: configService.get('REDIS_PORT'),
                    ttl: 300,
                }),
            }),
        ],
        providers: [cache_service_1.CacheService, logger_1.LoggerService],
        exports: [cache_service_1.CacheService],
    })
], CacheRedisModule);
exports.CacheRedisModule = CacheRedisModule;
//# sourceMappingURL=cache.module.js.map