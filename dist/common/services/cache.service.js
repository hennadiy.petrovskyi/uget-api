"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CacheService = void 0;
const common_1 = require("@nestjs/common");
const logger_1 = require("../../utils/logger");
let CacheService = class CacheService {
    constructor(cacheManager, logger) {
        this.cacheManager = cacheManager;
        this.logger = logger;
    }
    async set(key, payload, ttl) {
        try {
            await this.cacheManager.set(key, payload, { ttl });
            return await this.cacheManager.get(key);
        }
        catch (error) {
            this.logger.error(`Failed to set key ==> ${key}`);
            this.logger.error(error);
        }
    }
    async get(key) {
        try {
            return await this.cacheManager.get(key);
        }
        catch (error) {
            this.logger.error(`Failed to get key ==> ${key}`);
            this.logger.error(error);
        }
    }
    async delete(key) {
        try {
            return await this.cacheManager.del(key);
        }
        catch (error) {
            this.logger.error(`Failed to delete key ==> ${key}`);
            this.logger.error(error);
        }
    }
};
CacheService = __decorate([
    __param(0, (0, common_1.Inject)(common_1.CACHE_MANAGER)),
    __metadata("design:paramtypes", [Object, logger_1.LoggerService])
], CacheService);
exports.CacheService = CacheService;
//# sourceMappingURL=cache.service.js.map