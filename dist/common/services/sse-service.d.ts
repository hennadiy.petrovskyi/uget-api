export declare class SseService {
    private readonly emitter;
    subscribe(eventId: string | number): import("rxjs").Observable<unknown>;
    emit(data: any): Promise<void>;
}
