import { Cache } from 'cache-manager';
import { LoggerService } from '../../utils/logger';
export declare class CacheService {
    private cacheManager;
    private logger;
    constructor(cacheManager: Cache, logger: LoggerService);
    set(key: string, payload: any, ttl: number): Promise<unknown>;
    get(key: string): Promise<unknown>;
    delete(key: string): Promise<any>;
}
