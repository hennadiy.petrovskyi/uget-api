export declare enum Role {
    USER = "user",
    DEVELOPER = "developer",
    ADMIN = "admin"
}
