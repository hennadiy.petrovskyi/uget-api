"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MultipleFilesToBodyInterceptor = exports.FileToBodyInterceptor = exports.User = void 0;
var user_decorator_1 = require("./user.decorator");
Object.defineProperty(exports, "User", { enumerable: true, get: function () { return user_decorator_1.User; } });
var file_decorator_1 = require("./file.decorator");
Object.defineProperty(exports, "FileToBodyInterceptor", { enumerable: true, get: function () { return file_decorator_1.FileToBodyInterceptor; } });
Object.defineProperty(exports, "MultipleFilesToBodyInterceptor", { enumerable: true, get: function () { return file_decorator_1.MultipleFilesToBodyInterceptor; } });
//# sourceMappingURL=index.js.map