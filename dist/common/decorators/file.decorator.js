"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileToBodyInterceptor = exports.MultipleFilesToBodyInterceptor = exports.ApiFile = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const ApiFile = (options) => (target, propertyKey) => {
    if (options === null || options === void 0 ? void 0 : options.isArray) {
        (0, swagger_1.ApiProperty)({
            type: 'array',
            items: {
                type: 'file',
                properties: {
                    [propertyKey]: {
                        type: 'string',
                        format: 'binary',
                    },
                },
            },
        })(target, propertyKey);
    }
    else {
        (0, swagger_1.ApiProperty)({
            type: 'file',
            properties: {
                [propertyKey]: {
                    type: 'string',
                    format: 'binary',
                },
            },
        })(target, propertyKey);
    }
};
exports.ApiFile = ApiFile;
let MultipleFilesToBodyInterceptor = class MultipleFilesToBodyInterceptor {
    intercept(context, next) {
        const ctx = context.switchToHttp();
        const req = ctx.getRequest();
        console.log(req.body, req.file);
        return next.handle();
    }
};
MultipleFilesToBodyInterceptor = __decorate([
    (0, common_1.Injectable)()
], MultipleFilesToBodyInterceptor);
exports.MultipleFilesToBodyInterceptor = MultipleFilesToBodyInterceptor;
let FileToBodyInterceptor = class FileToBodyInterceptor {
    intercept(context, next) {
        var _a;
        const ctx = context.switchToHttp();
        const req = ctx.getRequest();
        if (req.body && ((_a = req.file) === null || _a === void 0 ? void 0 : _a.fieldname)) {
            const { fieldname } = req.file;
            if (!req.body[fieldname]) {
                req.body[fieldname] = req.file;
            }
        }
        return next.handle();
    }
};
FileToBodyInterceptor = __decorate([
    (0, common_1.Injectable)()
], FileToBodyInterceptor);
exports.FileToBodyInterceptor = FileToBodyInterceptor;
//# sourceMappingURL=file.decorator.js.map