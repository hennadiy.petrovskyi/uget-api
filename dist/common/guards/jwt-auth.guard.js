"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtAuthGuard = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const passport_1 = require("@nestjs/passport");
const services_1 = require("../../auth/services");
let JwtAuthGuard = class JwtAuthGuard extends (0, passport_1.AuthGuard)('jwt') {
    constructor(moduleRef) {
        super();
        this.moduleRef = moduleRef;
    }
    canActivate(context) {
        const authService = this.moduleRef.get(services_1.AuthService, { strict: false });
        const cryptoService = this.moduleRef.get(services_1.CryptoService, { strict: false });
        const request = context.switchToHttp().getRequest();
        const token = request.headers['authorization'];
        if (!token) {
            return false;
        }
        const payload = cryptoService.decodeToken(token);
        return authService.findAuthRecordByUUID(payload.sub).then((authRecord) => {
            if (!authRecord) {
                return false;
            }
            return cryptoService.findUserSessions(payload.sub).then((sessions) => {
                if (!sessions.length) {
                    return false;
                }
                const session = sessions.find((session) => session.accessToken === token);
                if (session) {
                    return super.canActivate(context);
                }
            });
        });
    }
    handleRequest(err, user, info) {
        if (err || !user) {
            throw err || new common_1.UnauthorizedException();
        }
        return user;
    }
};
JwtAuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [core_1.ModuleRef])
], JwtAuthGuard);
exports.JwtAuthGuard = JwtAuthGuard;
//# sourceMappingURL=jwt-auth.guard.js.map