"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var MorganMiddleware_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.MorganMiddleware = void 0;
const common_1 = require("@nestjs/common");
const morgan = require("morgan");
function stringify(val, depth, replacer, space) {
    depth = isNaN(+depth) ? 1 : depth;
    function _build(key, val, depth, o) {
        let a;
        return !val || typeof val != 'object'
            ? val
            : ((a = Array.isArray(val)),
                JSON.stringify(val, function (k, v) {
                    if (a || depth > 0) {
                        if (replacer)
                            v = replacer(k, v);
                        if (!k)
                            return (a = Array.isArray(v)), (val = v);
                        !o && (o = a ? [] : {});
                        o[k] = _build(k, v, a ? depth : depth - 1);
                    }
                }),
                o || (a ? [] : {}));
    }
    return JSON.stringify(_build('', val, depth), null, space);
}
let MorganMiddleware = MorganMiddleware_1 = class MorganMiddleware {
    static token(name, callback) {
        return morgan.token(name, callback);
    }
    use(req, res, next) {
        if (MorganMiddleware_1.IS_EXTENDED) {
            morgan.token('body', (req) => '\n' +
                '\n' +
                'Request Body :\n' +
                stringify(req.body, 1, null, 2) +
                '\n-------------------------\n');
            morgan(MorganMiddleware_1.extendedFormat)(req, res, next);
        }
        else {
            morgan(MorganMiddleware_1.format)(req, res, next);
        }
    }
};
MorganMiddleware.IS_EXTENDED = false;
MorganMiddleware.format = 'tiny';
MorganMiddleware.extendedFormat = ':method :url :status :response-time ms - :res[content-length] :body';
MorganMiddleware = MorganMiddleware_1 = __decorate([
    (0, common_1.Injectable)()
], MorganMiddleware);
exports.MorganMiddleware = MorganMiddleware;
//# sourceMappingURL=morgan.middleware.js.map