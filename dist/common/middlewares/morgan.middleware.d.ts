import { NestMiddleware } from '@nestjs/common';
import * as morgan from 'morgan';
export declare class MorganMiddleware implements NestMiddleware {
    static IS_EXTENDED: boolean;
    private static format;
    private static extendedFormat;
    static token(name: string, callback: any): morgan.Morgan;
    use(req: any, res: any, next: any): void;
}
