export declare const AUTH_EVENTS: {
    SEND_CONFIRMATION_CODE: string;
    VERIFY_CONFIRMATION_CODE: string;
    GET_USER_AUTH_SESSIONS: string;
    GET_USER_BY_UUID: string;
    GET_USER_BY_ID: string;
};
export declare const FILE_EVENTS: {
    UPLOAD_FILE: string;
    GET_FILE: string;
    GET_FILE_BY_TYPE: string;
    DELETE_FILE: string;
    GET_FILES: string;
    DELETE_OWNER_FILES: string;
};
export declare const USER_EVENTS: {
    GET_USER_DETAILS_BY_OWNER_ID: string;
    GET_USER_DETAILS_BY_OWNER_UUID: string;
    UPDATE_USER_DETAILS: string;
    CREATE_USER_DETAILS: string;
    GET_AUTH_CREDENTIALS: string;
    ADD_NEW_UNRATED_RATE: string;
    SET_RATING_IS_RATED: string;
};
export declare const LOCATION_EVENT: {
    GET_LOCATION_DETAILS_BY_ID: string;
    GET_COUNTRY_CODE_BY_LOCATION_ID: string;
    SAVE_LOCATION_IN_DB: string;
};
export declare const PHONE_NUMBER_EVENT: {
    GET_PHONE_NUMBER: string;
    SAVE_PHONE_NUMBER: string;
    DELETE_PHONE_NUMBER: string;
    UPDATE_PHONE_NUMBER: string;
};
export declare const ORDER_EVENT: {
    GET_USER_ORDERS: string;
    ADD_OUTGOING_REQUEST: string;
    ADD_INCOMING_REQUEST: string;
    ACCEPT_DELIVERY_BY_TRAVELER: string;
    CANCEL_OUTGOING_REQUEST: string;
    CANCEL_INCOMING_REQUEST: string;
    CANCEL_ACCEPTED_REQUEST: string;
    PICKUP_REQUEST: string;
    PICKUP_VERIFICATION: string;
    DELIVERY_REQUEST: string;
    DELIVERY_VERIFICATION: string;
    START_DELIVERY: string;
    FINISH_DELIVERY: string;
    GET_DELIVERY_PRICE: string;
    GET_EARNINGS: string;
};
export declare const TRIP_EVENT: {
    GET_USER_TRIPS: string;
    ADD_INCOMING_REQUEST: string;
    CANCEL_OUTGOING_REQUEST: string;
    CANCEL_INCOMING_REQUEST: string;
    CANCEL_ACCEPTED_REQUEST: string;
    ACCEPT_DELIVERY_BY_ORDER_OWNER: string;
};
export declare const PUSH_NOTIFICATION_EVENTS: {
    SEND_CHAT_NOTIFICATION: string;
    TRIP_REQUEST: string;
    ORDER_REQUEST: string;
    ORDER_REQUEST_ACCEPTED: string;
    TRIP_REQUEST_ACCEPTED: string;
    ORDER_PICK_UP_VERIFICATION: string;
    ORDER_PICK_UP: string;
    DELIVERY_VERIFICATION: string;
    ORDER_DELIVERED: string;
};
export declare const SETTINGS: {
    GET_USER_NOTIFICATION_SETTINGS: string;
};
export declare const PRICING_EVENTS: {
    CALCULATE_ORIGINAL_AVERAGE_PRICE: string;
};
export declare const CHAT_EVENTS: {
    GET_USER_CONVERSATIONS: string;
};
export declare const EMAIL_EVENTS: {
    SEND_VERIFICATION: string;
    SEND_RESET_PASSWORD: string;
};
export declare const SOCKET_EVENTS: {
    SEND_MESSAGE: string;
};
