"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SOCKET_EVENTS = exports.EMAIL_EVENTS = exports.CHAT_EVENTS = exports.SETTINGS = exports.PRICING_EVENTS = exports.PUSH_NOTIFICATION_EVENTS = exports.PHONE_NUMBER_EVENT = exports.ORDER_EVENT = exports.LOCATION_EVENT = exports.USER_EVENTS = exports.FILE_EVENTS = exports.AUTH_EVENTS = void 0;
var events_constants_1 = require("./events-constants");
Object.defineProperty(exports, "AUTH_EVENTS", { enumerable: true, get: function () { return events_constants_1.AUTH_EVENTS; } });
Object.defineProperty(exports, "FILE_EVENTS", { enumerable: true, get: function () { return events_constants_1.FILE_EVENTS; } });
Object.defineProperty(exports, "USER_EVENTS", { enumerable: true, get: function () { return events_constants_1.USER_EVENTS; } });
Object.defineProperty(exports, "LOCATION_EVENT", { enumerable: true, get: function () { return events_constants_1.LOCATION_EVENT; } });
Object.defineProperty(exports, "ORDER_EVENT", { enumerable: true, get: function () { return events_constants_1.ORDER_EVENT; } });
Object.defineProperty(exports, "PHONE_NUMBER_EVENT", { enumerable: true, get: function () { return events_constants_1.PHONE_NUMBER_EVENT; } });
Object.defineProperty(exports, "PUSH_NOTIFICATION_EVENTS", { enumerable: true, get: function () { return events_constants_1.PUSH_NOTIFICATION_EVENTS; } });
Object.defineProperty(exports, "PRICING_EVENTS", { enumerable: true, get: function () { return events_constants_1.PRICING_EVENTS; } });
Object.defineProperty(exports, "SETTINGS", { enumerable: true, get: function () { return events_constants_1.SETTINGS; } });
Object.defineProperty(exports, "CHAT_EVENTS", { enumerable: true, get: function () { return events_constants_1.CHAT_EVENTS; } });
Object.defineProperty(exports, "EMAIL_EVENTS", { enumerable: true, get: function () { return events_constants_1.EMAIL_EVENTS; } });
Object.defineProperty(exports, "SOCKET_EVENTS", { enumerable: true, get: function () { return events_constants_1.SOCKET_EVENTS; } });
//# sourceMappingURL=index.js.map