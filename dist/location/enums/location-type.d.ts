export declare enum LocationType {
    COUNTRY = "country",
    CITY = "city",
    CITY_AREA = "subarea"
}
