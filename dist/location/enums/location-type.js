"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationType = void 0;
var LocationType;
(function (LocationType) {
    LocationType["COUNTRY"] = "country";
    LocationType["CITY"] = "city";
    LocationType["CITY_AREA"] = "subarea";
})(LocationType = exports.LocationType || (exports.LocationType = {}));
//# sourceMappingURL=location-type.js.map