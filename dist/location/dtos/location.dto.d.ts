import { Location } from '../entities';
import { LocationType } from '../enums';
declare const SaveLocationDto_base: import("@nestjs/mapped-types").MappedType<Pick<Location, "countryCode" | "locationId">>;
export declare class SaveLocationDto extends SaveLocationDto_base {
}
export declare class FindCountryDto {
    language: string;
    input: string;
    type: LocationType;
}
export declare class FindCityDto {
    language: string;
    input: string;
    countryId: string;
    type: LocationType;
}
export {};
