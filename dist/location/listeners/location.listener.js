"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_1 = require("../../common/events");
const services_1 = require("../services");
let LocationListener = class LocationListener {
    constructor(locationService) {
        this.locationService = locationService;
    }
    async getLocationById(id) {
        return await this.locationService.parseLocationId(id);
    }
    async getCountryCode(id) {
        const googlePlace = await this.locationService.googlePlacesFindByPlaceId(id);
        console.log(googlePlace[0].short_name);
        if (!googlePlace.length) {
            return;
        }
        return googlePlace[0].short_name;
    }
    async saveLocationInDb(saveLocationDetailsDto) {
        const location = await this.locationService.findLocationByLocationId(saveLocationDetailsDto.locationId);
        if (location) {
            return location;
        }
        const countryCode = await this.locationService.getCountryCode(saveLocationDetailsDto.countryId);
        return await this.locationService.saveLocationInDb({
            countryCode,
            locationId: saveLocationDetailsDto.locationId,
        });
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.LOCATION_EVENT.GET_LOCATION_DETAILS_BY_ID, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], LocationListener.prototype, "getLocationById", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.LOCATION_EVENT.GET_COUNTRY_CODE_BY_LOCATION_ID, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], LocationListener.prototype, "getCountryCode", null);
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.LOCATION_EVENT.SAVE_LOCATION_IN_DB, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], LocationListener.prototype, "saveLocationInDb", null);
LocationListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.LocationService])
], LocationListener);
exports.LocationListener = LocationListener;
//# sourceMappingURL=location.listener.js.map