import { LocationService } from '../services';
export declare class LocationListener {
    private locationService;
    constructor(locationService: LocationService);
    getLocationById(id: number): Promise<{
        country: {
            id: string;
            label: any;
        };
        city: {
            id: string;
            label: any;
        };
    }>;
    getCountryCode(id: string): Promise<any>;
    saveLocationInDb(saveLocationDetailsDto: any): Promise<import("../entities").Location>;
}
