import { MiddlewareConsumer } from '@nestjs/common';
export declare class LocationModule {
    configure(consumer: MiddlewareConsumer): void;
}
