"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationService = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const google_maps_services_js_1 = require("@googlemaps/google-maps-services-js");
const app_logger_1 = require("../../utils/logger/app.logger");
const entities_1 = require("../entities");
const enums_1 = require("../enums");
const rxjs_1 = require("rxjs");
let LocationService = class LocationService {
    constructor(locationRepository, httpService, logger) {
        this.locationRepository = locationRepository;
        this.httpService = httpService;
        this.logger = logger;
    }
    async googlePlacesFindByPlaceId(id) {
        const requestUrl = `https://maps.googleapis.com/maps/api/place/details/json?place_id=${id}&fields=address_components&key=${process.env.GOOGLE_MAPS_API_KEY}`;
        try {
            const response = await (0, rxjs_1.firstValueFrom)(this.httpService.get(requestUrl));
            if (response.status === 200 && response.data.status === 'OK') {
                return response.data.result.address_components;
            }
        }
        catch (error) {
            this.logger.log(error);
        }
    }
    async getCountryCode(id) {
        const location = await this.googlePlacesFindByPlaceId(id);
        if (location.length) {
            return location[0].short_name.toLowerCase();
        }
    }
    async googlePlacesFindByInput(findLocationDto, type) {
        const googleMaps = new google_maps_services_js_1.Client({});
        const defaultParams = Object.assign({ key: process.env.GOOGLE_MAPS_API_KEY }, findLocationDto);
        if (findLocationDto.type === enums_1.LocationType.COUNTRY) {
            defaultParams.types = google_maps_services_js_1.PlaceAutocompleteType.regions;
        }
        console.log(defaultParams, process.env.GOOGLE_MAPS_API_KEY);
        try {
            const response = await googleMaps.placeAutocomplete({
                params: defaultParams,
                timeout: 1000,
            });
            console.log('googlePlacesFindByInput', defaultParams, response);
            const predictions = response.data.predictions;
            if (!predictions.length) {
                return [];
            }
            return this.filterResults(type, predictions);
        }
        catch (error) {
            this.logger.log('googlePlacesFindByInput' + error);
        }
        return [];
    }
    async filterResults(type, suggestions) {
        let results;
        results = [];
        if (type === enums_1.LocationType.COUNTRY) {
            results = suggestions.filter((location) => location.types.includes(enums_1.LocationType.COUNTRY));
        }
        if (type === enums_1.LocationType.CITY) {
            results = suggestions.filter((location) => location.types.includes('locality'));
        }
        if (!results.length) {
            return [];
        }
        return results.map((location) => ({
            id: location.place_id,
            label: location.structured_formatting.main_text,
        }));
    }
    async saveLocationInDb(saveLocationDto) {
        try {
            await this.locationRepository.save(saveLocationDto);
            return await this.findLocationByLocationId(saveLocationDto.locationId);
        }
        catch (error) {
            this.logger.log(error);
        }
    }
    async findLocationByLocationId(locationId) {
        try {
            return this.locationRepository.findOne({ where: { locationId } });
        }
        catch (error) {
            this.logger.log(error);
        }
    }
    async findLocationById(id) {
        try {
            return this.locationRepository.findOne({ where: { id } });
        }
        catch (error) {
            this.logger.log(error);
        }
    }
    async parseLocationId(id) {
        const location = await this.findLocationById(id);
        const locationData = await this.googlePlacesFindByPlaceId(location.locationId);
        const country = locationData.find((details) => details.types.includes('country'));
        const city = locationData.find((details) => details.types.includes('locality'));
        if (country && city) {
            return {
                country: { id: '', label: country.long_name },
                city: {
                    id: location.locationId,
                    label: city.short_name,
                },
            };
        }
    }
    async test(id) {
        const locationData = await this.googlePlacesFindByPlaceId(id);
        const country = locationData.find((details) => details.types.includes('country'));
        const city = locationData.find((details) => details.types.includes('locality'));
        const searchParams = {
            language: 'en',
            input: country.long_name,
            types: google_maps_services_js_1.PlaceAutocompleteType.regions,
        };
        return await this.googlePlacesFindByInput(searchParams, enums_1.LocationType.COUNTRY);
        if (country && city) {
            return {
                country: { label: country.long_name },
                city: {
                    id: '',
                    label: city.short_name,
                },
            };
        }
    }
};
LocationService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.Location)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        axios_1.HttpService,
        app_logger_1.LoggerService])
], LocationService);
exports.LocationService = LocationService;
//# sourceMappingURL=location.service.js.map