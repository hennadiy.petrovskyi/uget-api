import { HttpService } from '@nestjs/axios';
import { Repository } from 'typeorm';
import { LoggerService } from '../../utils/logger/app.logger';
import { Location } from '../entities';
import { SaveLocationDto } from '../dtos';
import { LocationType } from '../enums';
export declare class LocationService {
    private locationRepository;
    private httpService;
    private logger;
    constructor(locationRepository: Repository<Location>, httpService: HttpService, logger: LoggerService);
    googlePlacesFindByPlaceId(id: string): Promise<any>;
    getCountryCode(id: string): Promise<any>;
    googlePlacesFindByInput(findLocationDto: any, type: LocationType): Promise<any>;
    filterResults(type: any, suggestions: any): Promise<any>;
    saveLocationInDb(saveLocationDto: SaveLocationDto): Promise<Location>;
    findLocationByLocationId(locationId: string): Promise<Location>;
    findLocationById(id: number): Promise<Location>;
    parseLocationId(id: number): Promise<{
        country: {
            id: string;
            label: any;
        };
        city: {
            id: string;
            label: any;
        };
    }>;
    test(id: string): Promise<any>;
}
