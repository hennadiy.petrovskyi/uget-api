import { Response } from 'express';
import { LocationService } from '../services';
import { FindCityDto, FindCountryDto } from '../dtos';
export declare class LocationController {
    private service;
    constructor(service: LocationService);
    findCountry(findCountryDto: FindCountryDto, response: Response): Promise<Response<any, Record<string, any>>>;
    findCity(findCityDto: FindCityDto, response: Response): Promise<Response<any, Record<string, any>>>;
    getById(): Promise<void>;
}
