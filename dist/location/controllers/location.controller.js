"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const services_1 = require("../services");
const dtos_1 = require("../dtos");
const google_maps_services_js_1 = require("@googlemaps/google-maps-services-js");
let LocationController = class LocationController {
    constructor(service) {
        this.service = service;
    }
    async findCountry(findCountryDto, response) {
        const searchParams = {
            language: findCountryDto.language,
            input: findCountryDto.input,
            types: google_maps_services_js_1.PlaceAutocompleteType.regions,
        };
        const countriesList = await this.service.googlePlacesFindByInput(searchParams, findCountryDto.type);
        return response.status(200).json(countriesList);
    }
    async findCity(findCityDto, response) {
        const countryCode = await this.service.getCountryCode(findCityDto.countryId);
        const searchParams = {
            language: findCityDto.language,
            input: findCityDto.input,
            types: google_maps_services_js_1.PlaceAutocompleteType.cities,
            components: [`country:${countryCode}`],
        };
        if (!countryCode) {
            delete searchParams.components;
        }
        const citiesList = await this.service.googlePlacesFindByInput(searchParams, findCityDto.type);
        return response.status(200).json(citiesList);
    }
    async getById() {
    }
};
__decorate([
    (0, common_1.Post)('find/country'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.FindCountryDto, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "findCountry", null);
__decorate([
    (0, common_1.Post)('find/city'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dtos_1.FindCityDto, Object]),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "findCity", null);
__decorate([
    (0, common_1.Get)('test'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], LocationController.prototype, "getById", null);
LocationController = __decorate([
    (0, swagger_1.ApiTags)('Location'),
    (0, common_1.Controller)('location'),
    __metadata("design:paramtypes", [services_1.LocationService])
], LocationController);
exports.LocationController = LocationController;
//# sourceMappingURL=location.controller.js.map