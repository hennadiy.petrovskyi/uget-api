import { BaseEntity } from 'src/common/entities';
export declare class Location extends BaseEntity {
    countryCode: string;
    locationId: string;
}
