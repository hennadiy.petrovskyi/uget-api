import { ChatEmitter } from './../emitters/chat.emitter';
import { OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ChatService } from '../services';
export declare class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
    private chatService;
    private chatEmitter;
    constructor(chatService: ChatService, chatEmitter: ChatEmitter);
    server: Server;
    get(): Server<import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, import("socket.io/dist/typed-events").DefaultEventsMap, any>;
    handleDisconnect(client: Socket): void;
    handleConnection(client: Socket): void;
    getAllConversations(messagePayload: any): Promise<void>;
    listenForMessages(messagePayload: any): Promise<void>;
    requestAllMessages(message: any, socket: Socket): Promise<void>;
}
