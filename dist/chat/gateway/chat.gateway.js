"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatGateway = void 0;
const chat_emitter_1 = require("./../emitters/chat.emitter");
const websockets_1 = require("@nestjs/websockets");
const socket_io_1 = require("socket.io");
const services_1 = require("../services");
let ChatGateway = class ChatGateway {
    constructor(chatService, chatEmitter) {
        this.chatService = chatService;
        this.chatEmitter = chatEmitter;
    }
    get() {
        return this.server;
    }
    handleDisconnect(client) {
        console.log('disconnected', client.id);
    }
    handleConnection(client) {
        console.log('connected', client.id);
    }
    async getAllConversations(messagePayload) {
        const conversations = await this.chatService.findAllUserConversation(messagePayload.uuid);
        const conversationsWithMessages = await Promise.all(conversations.map(async (el) => {
            const contactPerson = el.users.find((user) => user !== messagePayload.uuid);
            const messages = await this.chatService.getAllConversationMessages(el);
            return {
                id: el.uuid,
                orderUUID: el.orderUUID,
                contactPerson,
                messages,
            };
        }));
        this.server.emit(messagePayload.uuid, conversationsWithMessages.map((el) => el));
    }
    async listenForMessages(messagePayload) {
        const conversation = (await this.chatService.findConversationByUUID(messagePayload.conversationId));
        const messages = await this.chatService.getAllConversationMessages(conversation);
        if (!messages.length) {
            conversation.users.map((user) => {
                this.server.emit(user, { type: 'new.conversation' });
            });
        }
        const message = await this.chatService.publishMessage(Object.assign({ owner: conversation }, messagePayload));
        this.server.emit(`get_message_${messagePayload.conversationId}`, message);
    }
    async requestAllMessages(message, socket) {
        console.log('request_all_messages', message);
        const conversation = (await this.chatService.findConversationByUUID(message.conversationId));
        const messages = await this.chatService.getAllConversationMessages(conversation);
        this.server.emit(`receive_all_messages_${message.conversationId}`, {
            messages,
        });
    }
};
__decorate([
    (0, websockets_1.WebSocketServer)(),
    __metadata("design:type", socket_io_1.Server)
], ChatGateway.prototype, "server", void 0);
__decorate([
    (0, websockets_1.SubscribeMessage)('get_all_conversations'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "getAllConversations", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('send_message'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "listenForMessages", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('request_all_messages'),
    __param(0, (0, websockets_1.MessageBody)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, socket_io_1.Socket]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "requestAllMessages", null);
ChatGateway = __decorate([
    (0, websockets_1.WebSocketGateway)(),
    __metadata("design:paramtypes", [services_1.ChatService,
        chat_emitter_1.ChatEmitter])
], ChatGateway);
exports.ChatGateway = ChatGateway;
//# sourceMappingURL=chat.gateway.js.map