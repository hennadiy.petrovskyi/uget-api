import { ChatService } from '../services';
export declare class ChatListener {
    private readonly chatService;
    constructor(chatService: ChatService);
    getUserConversations(userUUID: any): Promise<{
        id: string;
        orderUUID: string;
        contactPerson: string;
        messages: import("../entities").Message[];
    }[]>;
}
