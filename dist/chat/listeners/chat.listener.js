"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatListener = void 0;
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
const events_1 = require("../../common/events");
const services_1 = require("../services");
let ChatListener = class ChatListener {
    constructor(chatService) {
        this.chatService = chatService;
    }
    async getUserConversations(userUUID) {
        const conversations = await this.chatService.findAllUserConversation(userUUID);
        const conversationsWithMessages = await Promise.all(conversations.map(async (el) => {
            const contactPerson = el.users.find((person) => person !== userUUID);
            const messages = await this.chatService.getAllConversationMessages(el);
            return {
                id: el.uuid,
                orderUUID: el.orderUUID,
                contactPerson,
                messages,
            };
        }));
        return conversationsWithMessages;
    }
};
__decorate([
    (0, event_emitter_1.OnEvent)(events_1.CHAT_EVENTS.GET_USER_CONVERSATIONS, { promisify: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChatListener.prototype, "getUserConversations", null);
ChatListener = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [services_1.ChatService])
], ChatListener);
exports.ChatListener = ChatListener;
//# sourceMappingURL=chat.listener.js.map