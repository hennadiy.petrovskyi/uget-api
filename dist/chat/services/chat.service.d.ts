import { FindOperator, Repository } from 'typeorm';
import { Conversation, Message } from '../entities';
import { LoggerService } from '../../utils/logger';
import { PublishMessageDto } from '../dtos';
export declare class ChatService {
    private messageRepository;
    private conversationRepository;
    private logger;
    constructor(messageRepository: Repository<Message>, conversationRepository: Repository<Conversation>, logger: LoggerService);
    createConversation(users: string[], orderUUID: string): Promise<{
        users: string[];
        orderUUID: string;
    } & Conversation>;
    findConversationByUsers(users: string[]): Promise<Conversation>;
    findConversationByOrderUUID(users: string[], orderUUID: string): Promise<Conversation>;
    findAllUserConversation(uuid: string): Promise<Conversation[]>;
    findConversationByUUID(uuid: string): Promise<Conversation>;
    getAllConversationMessages(conversation: FindOperator<any>): Promise<Message[]>;
    publishMessage(publishMessageDto: PublishMessageDto): Promise<PublishMessageDto & Message>;
}
