"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const entities_1 = require("../entities");
const logger_1 = require("../../utils/logger");
let ChatService = class ChatService {
    constructor(messageRepository, conversationRepository, logger) {
        this.messageRepository = messageRepository;
        this.conversationRepository = conversationRepository;
        this.logger = logger;
    }
    async createConversation(users, orderUUID) {
        try {
            const newConversation = await this.conversationRepository.save({
                users,
                orderUUID,
            });
            return newConversation;
        }
        catch (error) { }
    }
    async findConversationByUsers(users) {
        try {
            const conversation = await this.conversationRepository
                .createQueryBuilder('conversation')
                .where('conversation.users @> ARRAY[:...users]', {
                users,
            })
                .getOne();
            return conversation;
        }
        catch (error) { }
    }
    async findConversationByOrderUUID(users, orderUUID) {
        try {
            const conversation = await this.conversationRepository
                .createQueryBuilder('conversation')
                .where('conversation.orderUUID = :orderUUID', { orderUUID })
                .andWhere('conversation.users @> ARRAY[:...users]', {
                users,
            })
                .getOne();
            return conversation;
        }
        catch (error) { }
    }
    async findAllUserConversation(uuid) {
        try {
            const conversations = await this.conversationRepository
                .createQueryBuilder('conversation')
                .where('conversation.users @> ARRAY[:users]', {
                users: uuid,
            })
                .getMany();
            return conversations;
        }
        catch (error) { }
    }
    async findConversationByUUID(uuid) {
        try {
            return this.conversationRepository.findOne({ where: { uuid } });
        }
        catch (error) { }
    }
    async getAllConversationMessages(conversation) {
        try {
            const messages = await this.messageRepository.find({
                where: { owner: conversation },
            });
            return messages || [];
        }
        catch (error) { }
    }
    async publishMessage(publishMessageDto) {
        try {
            const message = await this.messageRepository.save(publishMessageDto);
            return message;
        }
        catch (error) { }
    }
};
ChatService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(entities_1.Message)),
    __param(1, (0, typeorm_1.InjectRepository)(entities_1.Conversation)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        logger_1.LoggerService])
], ChatService);
exports.ChatService = ChatService;
//# sourceMappingURL=chat.service.js.map