"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublishMessageDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const entities_1 = require("../entities");
class PublishMessageDto extends (0, mapped_types_1.PickType)(entities_1.Message, [
    'owner',
    'message',
    'userId',
]) {
}
exports.PublishMessageDto = PublishMessageDto;
//# sourceMappingURL=message.dto.js.map