import { Message } from '../entities';
declare const PublishMessageDto_base: import("@nestjs/mapped-types").MappedType<Pick<Message, "owner" | "userId" | "message">>;
export declare class PublishMessageDto extends PublishMessageDto_base {
}
export {};
