import { BaseEntity } from 'src/common/entities';
import { Conversation } from './conversation.entity';
export declare class Message extends BaseEntity {
    owner: Conversation;
    userId: string;
    message: string;
    createdAt: Date;
}
