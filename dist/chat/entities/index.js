"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Message = exports.Conversation = void 0;
var conversation_entity_1 = require("./conversation.entity");
Object.defineProperty(exports, "Conversation", { enumerable: true, get: function () { return conversation_entity_1.Conversation; } });
var message_entity_1 = require("./message.entity");
Object.defineProperty(exports, "Message", { enumerable: true, get: function () { return message_entity_1.Message; } });
//# sourceMappingURL=index.js.map