import { BaseEntity } from 'src/common/entities';
export declare class Conversation extends BaseEntity {
    uuid: string;
    users: string[];
    orderUUID: string;
}
