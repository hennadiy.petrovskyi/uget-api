import { EventEmitter2 } from '@nestjs/event-emitter';
export declare class ChatEmitter {
    private eventEmitter;
    constructor(eventEmitter: EventEmitter2);
    getUserDetails(uuid: string): Promise<any>;
    getUserAuthRecordByUUID(uuid: string): Promise<any[]>;
    sendPushNotification(sendChatMessageDto: {
        to: string;
        from: string;
        message: string;
    }): Promise<any[]>;
}
