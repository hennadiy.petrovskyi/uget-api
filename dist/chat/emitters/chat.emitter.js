"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatEmitter = void 0;
const events_constants_1 = require("../../common/events/events-constants");
const event_emitter_1 = require("@nestjs/event-emitter");
const common_1 = require("@nestjs/common");
let ChatEmitter = class ChatEmitter {
    constructor(eventEmitter) {
        this.eventEmitter = eventEmitter;
    }
    async getUserDetails(uuid) {
        const authRecord = await this.getUserAuthRecordByUUID(uuid);
        if (!authRecord.length) {
            return;
        }
        const record = authRecord[0];
        if (!record) {
            return;
        }
        const result = await this.eventEmitter.emitAsync(events_constants_1.USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID, record.id);
        if (!result.length) {
            return;
        }
        const userDetails = result[0];
        return Object.assign(Object.assign({}, userDetails), { fullName: `${userDetails.firstName} ${userDetails.lastName}` });
    }
    async getUserAuthRecordByUUID(uuid) {
        return await this.eventEmitter.emitAsync(events_constants_1.AUTH_EVENTS.GET_USER_BY_UUID, uuid);
    }
    async sendPushNotification(sendChatMessageDto) {
        return await this.eventEmitter.emitAsync(events_constants_1.PUSH_NOTIFICATION_EVENTS.SEND_CHAT_NOTIFICATION, sendChatMessageDto);
    }
};
ChatEmitter = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [event_emitter_1.EventEmitter2])
], ChatEmitter);
exports.ChatEmitter = ChatEmitter;
//# sourceMappingURL=chat.emitter.js.map