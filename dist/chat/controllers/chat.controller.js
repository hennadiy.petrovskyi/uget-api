"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const services_1 = require("../services");
const gateway_1 = require("../gateway");
const jwt_auth_guard_1 = require("../../common/guards/jwt-auth.guard");
const decorators_1 = require("../../common/decorators");
const entities_1 = require("../../auth/entities");
const emitters_1 = require("../emitters");
let ChatController = class ChatController {
    constructor(service, chatService, chatEmitter, chatGateway) {
        this.service = service;
        this.chatService = chatService;
        this.chatEmitter = chatEmitter;
        this.chatGateway = chatGateway;
    }
    async startConversation(dto, user) {
        const users = [dto.contactPersonUUID, user.uuid];
        const existingConversation = (await this.service.findConversationByOrderUUID(users, dto.orderUUID));
        if (!existingConversation) {
            const newConversation = await this.service.createConversation(users, dto.orderUUID);
            return {
                conversationId: newConversation.uuid,
                conversations: [
                    {
                        id: newConversation.uuid,
                        orderUUID: newConversation.orderUUID,
                        contactPerson: user.uuid,
                        messages: [],
                    },
                ],
            };
        }
        const messages = await this.chatService.getAllConversationMessages(existingConversation);
        return {
            conversationId: existingConversation.uuid,
            conversations: [
                {
                    id: existingConversation.uuid,
                    orderUUID: existingConversation.orderUUID,
                    contactPerson: user.uuid,
                    messages,
                },
            ],
        };
    }
    async getAllUserConversations(user) {
        const conversations = await this.chatService.findAllUserConversation(user.uuid);
        const conversationsWithMessages = await Promise.all(conversations.map(async (el) => {
            const contactPerson = el.users.find((person) => person !== user.uuid);
            const messages = await this.chatService.getAllConversationMessages(el);
            return {
                id: el.uuid,
                orderUUID: el.orderUUID,
                contactPerson,
                messages,
            };
        }));
        return conversationsWithMessages;
    }
};
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('/join'),
    __param(0, (0, common_1.Body)('')),
    __param(1, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, entities_1.AuthRecord]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "startConversation", null);
__decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Get)('/conversations'),
    __param(0, (0, decorators_1.User)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [entities_1.AuthRecord]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "getAllUserConversations", null);
ChatController = __decorate([
    (0, swagger_1.ApiTags)('Chat'),
    (0, common_1.Controller)('chat'),
    __metadata("design:paramtypes", [services_1.ChatService,
        services_1.ChatService,
        emitters_1.ChatEmitter,
        gateway_1.ChatGateway])
], ChatController);
exports.ChatController = ChatController;
//# sourceMappingURL=chat.controller.js.map