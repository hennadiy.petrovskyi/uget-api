import { ChatService } from '../services';
import { ChatGateway } from '../gateway';
import { AuthRecord } from 'src/auth/entities';
import { ChatEmitter } from '../emitters';
export declare class ChatController {
    private service;
    private chatService;
    private chatEmitter;
    private chatGateway;
    constructor(service: ChatService, chatService: ChatService, chatEmitter: ChatEmitter, chatGateway: ChatGateway);
    startConversation(dto: any, user: AuthRecord): Promise<{
        conversationId: any;
        conversations: {
            id: any;
            orderUUID: any;
            contactPerson: string;
            messages: import("../entities").Message[];
        }[];
    }>;
    getAllUserConversations(user: AuthRecord): Promise<{
        id: string;
        orderUUID: string;
        contactPerson: string;
        messages: import("../entities").Message[];
    }[]>;
}
