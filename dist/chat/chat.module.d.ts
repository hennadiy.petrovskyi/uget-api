import { MiddlewareConsumer } from '@nestjs/common';
export declare class ChatModule {
    configure(consumer: MiddlewareConsumer): void;
}
