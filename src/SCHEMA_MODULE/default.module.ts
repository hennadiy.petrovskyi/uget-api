import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import {} from './services';
import {} from './controllers';
import {} from './entities';
import { LoggerModule } from 'src/utils/logger';

@Module({
  imports: [TypeOrmModule.forFeature([]), HttpModule, LoggerModule],
  providers: [],
  controllers: [],
  exports: [],
})
export class OrderModule {
  constructor() {}
}
