import { Module, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import { ChatService } from './services';
import { ChatController } from './controllers';
import { Conversation, Message } from './entities';
import { LoggerModule } from 'src/utils/logger';
import { ChatGateway } from './gateway';
import { MorganMiddleware } from 'src/common/middlewares/morgan.middleware';
import { ChatEmitter } from './emitters';
import { ChatListener } from './listeners';

@Module({
  imports: [
    TypeOrmModule.forFeature([Conversation, Message]),
    HttpModule,
    LoggerModule,
  ],
  controllers: [ChatController],
  providers: [ChatGateway, ChatService, ChatEmitter, ChatListener],
  exports: [],
})
export class ChatModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes(ChatController);
  }
}
