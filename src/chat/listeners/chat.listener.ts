import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { CHAT_EVENTS, USER_EVENTS } from 'src/common/events';

import { ChatService } from '../services';

@Injectable()
export class ChatListener {
  constructor(private readonly chatService: ChatService) {}

  @OnEvent(CHAT_EVENTS.GET_USER_CONVERSATIONS, { promisify: true })
  public async getUserConversations(userUUID: any) {
    const conversations = await this.chatService.findAllUserConversation(
      userUUID,
    );
    const conversationsWithMessages = await Promise.all(
      conversations.map(async (el) => {
        const contactPerson = el.users.find((person) => person !== userUUID);
        const messages = await this.chatService.getAllConversationMessages(
          el as any,
        );
        return {
          id: el.uuid,
          orderUUID: el.orderUUID,
          contactPerson,
          messages,
        };
      }),
    );

    return conversationsWithMessages;
  }
}
