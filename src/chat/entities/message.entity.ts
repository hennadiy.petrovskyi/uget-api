import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import {} from '.';
import { BaseEntity } from 'src/common/entities';
import { Conversation } from './conversation.entity';

@Entity({
  name: 'conversation_message',
})
export class Message extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => Conversation)
  @JoinColumn({ name: 'conversationId', referencedColumnName: 'id' })
  owner: Conversation;

  @Expose()
  @ApiProperty()
  @Column('text')
  userId: string;

  @Expose()
  @ApiProperty()
  @Column('text')
  message: string;

  @Expose()
  @ApiProperty()
  @Column('timestamp with time zone', {
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;
}
