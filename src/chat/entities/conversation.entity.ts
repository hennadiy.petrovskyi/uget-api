import {
  Column,
  Entity,
  Generated,
  Index,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import {} from '.';
import { BaseEntity } from 'src/common/entities';
import { Order } from 'src/order/entities';

@Entity({
  name: 'conversation',
})
export class Conversation extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Column()
  @Generated('uuid')
  uuid: string;

  @Expose()
  @ApiProperty()
  @Column('text', { default: [], array: true })
  users: string[];

  @Expose()
  @ApiProperty()
  @Column('text', { nullable: true })
  orderUUID: string;
}
