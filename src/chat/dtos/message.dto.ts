import { PickType, IntersectionType, OmitType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Expose, plainToClass } from 'class-transformer';
import { isNumber, IsString } from 'class-validator';
import { Message } from '../entities';

export class PublishMessageDto extends PickType(Message, [
  'owner',
  'message',
  'userId',
]) {
  //   static buildDto(payload: AuthRegisterUserDto): AuthCreateNewAuthRecordDto {
  //     return plainToClass(AuthCreateNewAuthRecordDto, payload, {
  //       excludeExtraneousValues: true,
  //     });
  //   }
}
