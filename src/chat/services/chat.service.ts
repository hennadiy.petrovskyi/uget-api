import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { Conversation, Message } from '../entities';

import { LoggerService } from '../../utils/logger';
import { PublishMessageDto } from '../dtos';

@Injectable()
export class ChatService {
  constructor(
    @InjectRepository(Message)
    private messageRepository: Repository<Message>,
    @InjectRepository(Conversation)
    private conversationRepository: Repository<Conversation>,
    private logger: LoggerService,
  ) {}

  async createConversation(users: string[], orderUUID: string) {
    try {
      const newConversation = await this.conversationRepository.save({
        users,
        orderUUID,
      });

      return newConversation;
    } catch (error) {}
  }

  async findConversationByUsers(users: string[]) {
    try {
      const conversation = await this.conversationRepository
        .createQueryBuilder('conversation')
        .where('conversation.users @> ARRAY[:...users]', {
          users,
        })
        .getOne();

      return conversation;
    } catch (error) {}
  }

  async findConversationByOrderUUID(users: string[], orderUUID: string) {
    try {
      const conversation = await this.conversationRepository
        .createQueryBuilder('conversation')
        .where('conversation.orderUUID = :orderUUID', { orderUUID })
        .andWhere('conversation.users @> ARRAY[:...users]', {
          users,
        })
        .getOne();

      return conversation;
    } catch (error) {}
  }

  async findAllUserConversation(uuid: string) {
    try {
      const conversations = await this.conversationRepository
        .createQueryBuilder('conversation')
        .where('conversation.users @> ARRAY[:users]', {
          users: uuid,
        })
        .getMany();

      return conversations;
    } catch (error) {}
  }

  async findConversationByUUID(uuid: string) {
    try {
      return this.conversationRepository.findOne({ where: { uuid } });
    } catch (error) {}
  }

  async getAllConversationMessages(conversation: FindOperator<any>) {
    try {
      const messages = await this.messageRepository.find({
        where: { owner: conversation },
      });

      return messages || [];
    } catch (error) {}
  }

  async publishMessage(publishMessageDto: PublishMessageDto) {
    try {
      const message = await this.messageRepository.save(publishMessageDto);

      return message;
    } catch (error) {}
  }
}
