import { EventEmitter } from './../../common/events/eventEmitter';
import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  Res,
  Headers,
  UnauthorizedException,
  ForbiddenException,
  Get,
  UseGuards,
  NotFoundException,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { ChatService } from '../services';
import { ChatGateway } from '../gateway';

import {} from '../dtos';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { User } from 'src/common/decorators';

import { AuthRecord } from 'src/auth/entities';
import { ChatEmitter } from '../emitters';
import { FindOperator } from 'typeorm';

@ApiTags('Chat')
@Controller('chat')
export class ChatController {
  constructor(
    private service: ChatService,
    private chatService: ChatService,
    private chatEmitter: ChatEmitter,
    private chatGateway: ChatGateway,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post('/join')
  public async startConversation(@Body('') dto: any, @User() user: AuthRecord) {
    const users = [dto.contactPersonUUID, user.uuid];

    const existingConversation =
      (await this.service.findConversationByOrderUUID(
        users,
        dto.orderUUID,
      )) as any;

    if (!existingConversation) {
      const newConversation = await this.service.createConversation(
        users,
        dto.orderUUID,
      );

      return {
        conversationId: newConversation.uuid,
        conversations: [
          {
            id: newConversation.uuid,
            orderUUID: newConversation.orderUUID,
            contactPerson: user.uuid,
            messages: [],
          },
        ],
      };
    }

    const messages = await this.chatService.getAllConversationMessages(
      existingConversation,
    );

    return {
      conversationId: existingConversation.uuid,
      conversations: [
        {
          id: existingConversation.uuid,
          orderUUID: existingConversation.orderUUID,
          contactPerson: user.uuid,
          messages,
        },
      ],
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get('/conversations')
  public async getAllUserConversations(@User() user: AuthRecord) {
    const conversations = await this.chatService.findAllUserConversation(
      user.uuid,
    );
    const conversationsWithMessages = await Promise.all(
      conversations.map(async (el) => {
        const contactPerson = el.users.find((person) => person !== user.uuid);
        const messages = await this.chatService.getAllConversationMessages(
          el as any,
        );
        return {
          id: el.uuid,
          orderUUID: el.orderUUID,
          contactPerson,
          messages,
        };
      }),
    );

    return conversationsWithMessages;
  }
}
