import { ChatEmitter } from './../emitters/chat.emitter';
import { Logger } from '@nestjs/common/services';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ChatService } from '../services';

@WebSocketGateway()
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private chatService: ChatService,
    private chatEmitter: ChatEmitter,
  ) {}

  @WebSocketServer()
  server: Server;

  get() {
    return this.server;
  }

  handleDisconnect(client: Socket) {
    console.log('disconnected', client.id);
  }

  handleConnection(client: Socket) {
    console.log('connected', client.id);
  }

  @SubscribeMessage('get_all_conversations')
  async getAllConversations(@MessageBody() messagePayload: any) {
    const conversations = await this.chatService.findAllUserConversation(
      messagePayload.uuid,
    );

    const conversationsWithMessages = await Promise.all(
      conversations.map(async (el) => {
        const contactPerson = el.users.find(
          (user) => user !== messagePayload.uuid,
        );
        const messages = await this.chatService.getAllConversationMessages(
          el as any,
        );
        return {
          id: el.uuid,
          orderUUID: el.orderUUID,
          contactPerson,
          messages,
        };
      }),
    );

    this.server.emit(
      messagePayload.uuid,
      conversationsWithMessages.map((el) => el),
    );
  }

  @SubscribeMessage('send_message')
  async listenForMessages(@MessageBody() messagePayload: any) {
    const conversation = (await this.chatService.findConversationByUUID(
      messagePayload.conversationId,
    )) as any;

    const messages = await this.chatService.getAllConversationMessages(
      conversation,
    );

    if (!messages.length) {
      conversation.users.map((user) => {
        this.server.emit(user, { type: 'new.conversation' });
      });
    }

    const message = await this.chatService.publishMessage({
      owner: conversation,
      ...messagePayload,
    });

    this.server.emit(`get_message_${messagePayload.conversationId}`, message);

    // await this.chatEmitter.sendPushNotification({
    //   to: conversation.users.find((user) => user !== messagePayload.userId),
    //   from: messagePayload.userId,
    //   message: messagePayload.message,
    // });
  }

  @SubscribeMessage('request_all_messages')
  async requestAllMessages(
    @MessageBody() message: any,
    @ConnectedSocket() socket: Socket,
  ) {
    console.log('request_all_messages', message);

    const conversation = (await this.chatService.findConversationByUUID(
      message.conversationId,
    )) as any;

    const messages = await this.chatService.getAllConversationMessages(
      conversation,
    );

    this.server.emit(`receive_all_messages_${message.conversationId}`, {
      messages,
    });
  }
}
