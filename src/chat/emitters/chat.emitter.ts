import {
  USER_EVENTS,
  AUTH_EVENTS,
  PUSH_NOTIFICATION_EVENTS,
} from '../../common/events/events-constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ChatEmitter {
  constructor(private eventEmitter: EventEmitter2) {}

  public async getUserDetails(uuid: string) {
    const authRecord = await this.getUserAuthRecordByUUID(uuid);

    if (!authRecord.length) {
      return;
    }

    const record = authRecord[0];

    if (!record) {
      return;
    }

    const result = await this.eventEmitter.emitAsync(
      USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID,
      record.id,
    );

    if (!result.length) {
      return;
    }

    const userDetails = result[0];

    return {
      ...userDetails,
      fullName: `${userDetails.firstName} ${userDetails.lastName}`,
    };
  }

  public async getUserAuthRecordByUUID(uuid: string) {
    return await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_UUID,
      uuid,
    );
  }

  public async sendPushNotification(sendChatMessageDto: {
    to: string;
    from: string;
    message: string;
  }) {
    return await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.SEND_CHAT_NOTIFICATION,
      sendChatMessageDto,
    );
  }
}
