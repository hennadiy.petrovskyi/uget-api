import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';

import { LoggerService } from './app.logger';

@Module({
  imports: [HttpModule],
  providers: [LoggerService],
  exports: [LoggerService],
})
export class LoggerModule {
  constructor() {}
}
