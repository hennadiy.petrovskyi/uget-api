import { ConsoleLogger, Injectable, Scope } from '@nestjs/common';

@Injectable({ scope: Scope.TRANSIENT })
export class LoggerService extends ConsoleLogger {
  logError(functionName: string, error: any, props?: any, context?: string) {
    const message = `${functionName} error: ${JSON.stringify(
      error,
    )}, ${JSON.stringify(props)}`;
    this.error(message, context);
  }

  logWarn(functionName: string, warn?: any, props?: any, context?: string) {
    const message = `${functionName} warn: ${JSON.stringify(
      warn,
    )}, ${JSON.stringify(props)}`;
    this.warn(message, context);
  }
}
