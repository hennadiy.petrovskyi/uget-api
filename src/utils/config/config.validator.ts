import * as Joi from 'joi';

const configValidator = () => ({
  PORT: Joi.number().default(3000),
  SECRET_KEY: Joi.string(),

  JWT_ACCESS_TOKEN: Joi.string().default('10m'),
  JWT_REFRESH_TOKEN: Joi.string().default('28d'),
  MAX_SESSIONS_ALLOWED: Joi.number().default(1),

  DB_HOST: Joi.string().default('localhost'),
  DB_PORT: Joi.number().default(5432),
  DB_NAME: Joi.string(),
  DB_USER: Joi.string(),
  DB_PASS: Joi.string(),
  DB_LOGGING: Joi.boolean().default(false),
  DB_SYNC: Joi.boolean().default(true),

  AWS_ACCESS_KEY_ID: Joi.string(),
  AWS_SECRET_ACCESS_KEY: Joi.string(),
  AWS_REGION: Joi.string(),
  AWS_S3_BUCKET_NAME: Joi.string(),
});

export const JoiConfigValidator = Joi.object(configValidator());
