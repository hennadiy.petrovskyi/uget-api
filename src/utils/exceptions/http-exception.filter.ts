import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  ConsoleLogger,
} from '@nestjs/common';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private readonly logger = new ConsoleLogger(AllExceptionsFilter.name);

  catch(exception: HttpException | Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    this.logger.error(
      request.url,
      'getStatus' in exception ? exception.getResponse() : exception.message,
      {
        ip: request.ip,
        url: request.url,
        method: request.method,
        params: request.params,
        query: request.query,
      },
    );

    response.status(status).json({
      statusCode: status,
      path: request.url,
      // message: exception,
      // timestamp: new Date().toISOString(),
    });
  }
}
