import {
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import * as bcrypt from 'bcryptjs';

import { AuthTokensDto } from 'src/auth/dtos';
import { Role } from 'src/auth/enums';
import { CacheService } from 'src/common/services/cache.service';

import { v4 as uuidv4 } from 'uuid';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ms = require('ms');

interface ISession {
  uuid: string;
  userUUID: string;
  accessToken: string;
  refreshToken: string;
  expireIn: number;
  updatedAt: number;
  role: Role;
}
type TSessions = { sessions: ISession[] };

@Injectable()
export class CryptoService {
  constructor(private jwt: JwtService, private cacheService: CacheService) {}

  private get ttl() {
    const timestamp = Date.now();

    return {
      // accessToken: { expiresIn: '1m' },
      accessToken: { expiresIn: process.env.JWT_ACCESS_TOKEN || '10m' },
      refreshToken: { expiresIn: process.env.JWT_REFRESH_TOKEN || '28d' },
      passwordResetToken: { expiresIn: '5m' },
      resetPasswordCache: ms('5m'),
      session: ms(process.env.JWT_REFRESH_TOKEN) + timestamp,
      cache: ms(process.env.JWT_REFRESH_TOKEN),
    };
  }

  decodeToken(token: string) {
    return this.jwt.decode(token, { json: true });
  }

  async verifyToken(token: string) {
    const isValid = this.jwt.verify(token);

    return isValid;
  }

  async hashPassword(password: string): Promise<string> {
    const saltOrRounds = 10;
    return bcrypt.hash(password, saltOrRounds);
  }

  async comparePasswords(
    newPassword: string,
    passwordHash: string,
  ): Promise<boolean> {
    return bcrypt.compare(newPassword, passwordHash);
  }

  public async createAccessToken(sub: string, role: Role) {
    await this.cacheService.delete(sub);

    const session = await this.generateSession(sub, role);
    const currentSession = await this.addNewSession(sub, session);

    return AuthTokensDto.buildDto({
      accessToken: currentSession.accessToken,
      refreshToken: currentSession.refreshToken,
    });
  }

  async findUserSessions(sub: string): Promise<ISession[] | undefined> {
    const functionName = this.findUserSessions.name;

    try {
      const result: TSessions = (await this.cacheService.get(sub)) as TSessions;
      return result ? result.sessions : undefined;
    } catch (error) {
      throw new InternalServerErrorException({
        message: 'Failed to find user session',
      });
    }
  }

  async findUserSessionsOrCreate(sub: string): Promise<ISession[]> {
    const functionName = this.findUserSessionsOrCreate.name;

    try {
      const userSessions: TSessions | undefined = (await this.cacheService.get(
        sub,
      )) as TSessions;

      if (!userSessions) {
        await this.cacheService.set(sub, { sessions: [] }, this.ttl.cache);

        const createdUserSessions: TSessions = (await this.cacheService.get(
          sub,
        )) as TSessions;

        return createdUserSessions.sessions;
      }

      return userSessions.sessions;
    } catch (error) {
      throw new InternalServerErrorException({
        message: 'Failed to find or create users sessions',
      });
    }
  }

  async generatePasswordResetToken(uuid: string) {
    const functionName = this.generatePasswordResetToken.name;

    try {
      const accessToken = await this.jwt.signAsync(
        {
          uuid,
        },
        { expiresIn: '5m' },
      );

      await this.cacheService.set(
        `${uuid}_RESET_PASSWORD`,
        accessToken,
        this.ttl.resetPasswordCache,
      );

      return accessToken;
    } catch (error) {}
  }

  async generateVerifyEmailToken(uuid: string, email) {
    const functionName = this.generateVerifyEmailToken.name;

    try {
      const token = await this.jwt.signAsync(
        {
          uuid,
          email,
        },
        { expiresIn: '5m' },
      );

      return token;
    } catch (error) {}
  }

  async findPasswordResetToken(uuid: string) {
    const functionName = this.generatePasswordResetToken.name;

    try {
      return await this.cacheService.get(`${uuid}_RESET_PASSWORD`);
    } catch (error) {}
  }

  async generateSession(sub: string, role: Role): Promise<ISession> {
    const functionName = this.generateSession.name;

    const sessionID = uuidv4();

    try {
      const accessToken = await this.jwt.signAsync(
        {
          sub,
          role,
        },
        this.ttl.accessToken,
      );

      const refreshToken = await this.jwt.signAsync(
        {
          sub,
          uuid: sessionID,
        },
        this.ttl.refreshToken,
      );

      return {
        uuid: sessionID,
        userUUID: sub,
        expireIn: this.ttl.session,
        accessToken,
        refreshToken,
        updatedAt: null,
        role,
      };
    } catch (error) {
      console.log(error);

      throw new InternalServerErrorException({
        message: 'Failed to generate session',
      });
    }
  }

  async addNewSession(sub: string, newSession: ISession) {
    const functionName = this.addNewSession.name;
    const maxSessionsAllowed = 3;

    try {
      const sessions = await this.findUserSessionsOrCreate(sub);

      if (sessions.length === maxSessionsAllowed) {
        const update: TSessions = (await this.cacheService.set(
          sub,
          { sessions: [newSession] },
          this.ttl.cache,
        )) as TSessions;

        return update.sessions.find(
          (session) => session.uuid === newSession.uuid,
        );
      }

      sessions.push(newSession);

      const update: TSessions = (await this.cacheService.set(
        sub,
        { sessions },
        this.ttl.cache,
      )) as TSessions;

      return update.sessions.find(
        (session) => session.uuid === newSession.uuid,
      );
    } catch (error) {
      throw new InternalServerErrorException({
        message: 'Failed to update update user sessions',
      });
    }
  }

  async checkTokenBySession(sub: string, accessToken: string) {
    const functionName = this.checkTokenBySession.name;

    try {
      const userSessions = await this.findUserSessions(sub);
      const session = userSessions.find(
        (session) => session.accessToken === accessToken,
      );

      if (!userSessions || !session) {
        throw new UnauthorizedException();
      }

      return session;
    } catch (error) {
      throw new InternalServerErrorException({
        message: 'Failed to check token by session',
      });
    }
  }

  async deleteSession(token: string) {
    const functionName = this.deleteSession.name;
    const { sub } = this.jwt.decode(token);

    try {
      const userSessions: TSessions = (await this.cacheService.get(
        sub,
      )) as TSessions;

      const update = userSessions.sessions.filter(
        (session) => session.accessToken !== token,
      );

      await this.cacheService.set(sub, { sessions: update }, this.ttl.cache);
    } catch (error) {
      throw new InternalServerErrorException({
        message: 'Failed to delete session',
      });
    }
  }

  async refreshSession(
    refreshToken: string,
  ): Promise<{ accessToken: string; refreshToken: string }> {
    const isTokenVerified = await this.jwt.verify(refreshToken);

    if (!isTokenVerified) {
      throw new UnauthorizedException();
    }

    const { sub, uuid } = this.jwt.decode(refreshToken) as {
      sub: string;
      uuid: string;
    };

    const sessions = await this.findUserSessions(sub);

    const currentSession = await sessions?.find(
      (session) =>
        session.uuid === uuid && session.refreshToken === refreshToken,
    );

    if (!sessions || !currentSession) {
      throw new UnauthorizedException();
    }

    const newAccessToken = this.jwt.sign(
      { sub: currentSession.userUUID, role: currentSession.role },
      this.ttl.accessToken,
    );

    const newRefreshToken = this.jwt.sign(
      { sub: currentSession.userUUID, uuid: currentSession.uuid },
      this.ttl.refreshToken,
    );

    const updatedSession = {
      ...currentSession,
      accessToken: newAccessToken,
      refreshToken: newRefreshToken,
      updatedAt: Date.now(),
      expiresIn: this.ttl.session,
    };

    const update = sessions.filter(
      (session) => session.uuid !== updatedSession.uuid,
    );

    update.push(updatedSession);
    await this.cacheService.set(sub, { sessions: update }, this.ttl.cache);

    return AuthTokensDto.buildDto(updatedSession);
  }
}
