import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { LoggerService } from '../../utils/logger/app.logger';
import { AuthRecord } from '../entities';
import {
  AuthRegisterUserDto,
  AuthCreateNewAuthRecordDto,
  AuthGetEmailWithPhoneNumberDto,
} from '../dtos';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(AuthRecord)
    private authRepository: Repository<AuthRecord>,
    private logger: LoggerService,
  ) {}

  async findAuthRecordById(id: FindOperator<any>): Promise<any> {
    const functionName = this.findAuthRecordById.name;

    try {
      return this.authRepository.findOne({ where: { id } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async findAuthRecordByEmail(email: string): Promise<any> {
    const functionName = this.findAuthRecordByEmail.name;
    try {
      return this.authRepository.findOne({
        where: {
          email,
        },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async findAuthRecordByPhoneNumber(phoneNumber: string) {
    const functionName = this.findAuthRecordByPhoneNumber.name;
    try {
      return this.authRepository.findOne({
        where: {
          phoneNumber,
        },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async findAuthRecordByUUID(uuid: string) {
    const functionName = this.findAuthRecordByUUID.name;

    try {
      return this.authRepository.findOne({
        where: {
          uuid,
        },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async createNewUserAuthRecord(authRegisterUserDto: any) {
    const functionName = this.createNewUserAuthRecord.name;

    try {
      return await this.authRepository.save({
        ...AuthCreateNewAuthRecordDto.buildDto(authRegisterUserDto),
        uuid: uuidv4(),
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async verifyUserPhoneNumber(userId) {
    const functionName = this.verifyUserPhoneNumber.name;

    try {
      await this.authRepository.save({
        id: userId,
        isPhoneNumberVerified: true,
      });

      return await this.findAuthRecordById(userId);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async verifyUserEmail(userId) {
    const functionName = this.verifyUserEmail.name;

    try {
      await this.authRepository.update(userId, {
        isEmailVerified: true,
      });

      return await this.findAuthRecordById(userId);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async getEmailWithPhoneNumber(userId) {
    const functionName = this.getEmailWithPhoneNumber.name;

    try {
      const user = await this.findAuthRecordById(userId);

      return AuthGetEmailWithPhoneNumberDto.buildDto(user);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async updateUserPhoneNumber(userId, phoneNumber) {
    const functionName = this.updateUserPhoneNumber.name;

    try {
      await this.authRepository.save({
        id: userId,
        phoneNumber,
        isPhoneNumberVerified: false,
      });

      return await this.findAuthRecordById(userId);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async updateUserEmail(userId, email) {
    const functionName = this.updateUserEmail.name;

    try {
      await this.authRepository.save({
        id: userId,
        email,
        isEmailVerified: false,
      });

      return await this.findAuthRecordById(userId);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
