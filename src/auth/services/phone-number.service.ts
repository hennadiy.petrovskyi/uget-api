import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { PhoneNumber } from '../entities';

import { LoggerService } from '../../utils/logger';

@Injectable()
export class PhoneNumberService {
  constructor(
    @InjectRepository(PhoneNumber)
    private phoneNumberRepository: Repository<PhoneNumber>,
    private logger: LoggerService,
  ) {}

  async findPhoneNumberByOwner(owner: FindOperator<any>): Promise<PhoneNumber> {
    const functionName = this.findPhoneNumberByOwner.name;

    try {
      return this.phoneNumberRepository.findOne({ where: { owner } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async findPhoneNumberByNumber(formatted: string): Promise<PhoneNumber> {
    const functionName = this.findPhoneNumberByOwner.name;

    try {
      return this.phoneNumberRepository.findOne({ where: { formatted } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async savePhoneNumber(savePhoneNumber: any) {
    const functionName = this.savePhoneNumber.name;

    try {
      return await this.phoneNumberRepository.save(savePhoneNumber);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async updatePhoneNumberDetails(id, update: any) {
    const functionName = this.updatePhoneNumberDetails.name;

    try {
      return await this.phoneNumberRepository.save({ id, ...update });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
