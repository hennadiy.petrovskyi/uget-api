import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { Credential } from '../entities';

import { EventEmitter2 } from '@nestjs/event-emitter';
import { CryptoService } from './crypto.service';
import { LoggerService } from '../../utils/logger';

@Injectable()
export class CredentialService {
  constructor(
    @InjectRepository(Credential)
    private credentialsRepository: Repository<Credential>,
    private cryptoService: CryptoService,
    private eventEmitter: EventEmitter2,
    private logger: LoggerService,
  ) {}

  async findAuthRecordCredentials(
    owner: FindOperator<any>,
  ): Promise<Credential> {
    const functionName = this.findAuthRecordCredentials.name;

    try {
      return this.credentialsRepository.findOne({ where: { owner } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async createNewCredential(createCredentialDTO: any) {
    const functionName = this.createNewCredential.name;

    try {
      const hashedPassword = await this.cryptoService.hashPassword(
        createCredentialDTO.password,
      );
      await this.credentialsRepository.save({
        ...createCredentialDTO,
        password: hashedPassword,
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async matchCredential(
    owner: FindOperator<any>,
    loginCredential: string,
  ): Promise<boolean> {
    const credentials = await this.findAuthRecordCredentials(owner);

    return await this.cryptoService.comparePasswords(
      loginCredential,
      credentials.password,
    );
  }

  async updatePassword(owner: any, dto: any) {
    const functionName = this.updatePassword.name;
    try {
      const userCredentials = await this.findAuthRecordCredentials(owner);

      await this.credentialsRepository.update(userCredentials.id, dto);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
