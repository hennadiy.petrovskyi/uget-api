import { PhoneNumber } from '../entities/phone.entity';
import { PickType, IntersectionType, OmitType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Expose, plainToClass } from 'class-transformer';
import { isNumber, IsString } from 'class-validator';

import { AuthRecord, Credential } from '../entities';

export class AuthCheckEmailDto extends PickType(AuthRecord, ['email']) {}

export class AuthCheckPhoneNumberDto extends PickType(AuthRecord, [
  'phoneNumber',
]) {}

export class AuthLoginByEmailDto extends PickType(AuthRecord, ['email']) {
  @Expose()
  @ApiProperty()
  password: string;
}

export class AuthLoginByPhoneNumberDto extends PickType(AuthRecord, [
  'phoneNumber',
]) {
  @Expose()
  @ApiProperty()
  code: string;
}

export class AuthRegisterUserDto extends PickType(AuthRecord, ['email']) {
  @Expose()
  @ApiProperty()
  password: string;

  @Expose()
  @ApiProperty()
  firstName: string;

  @Expose()
  @ApiProperty()
  lastName: string;

  @Expose()
  @ApiProperty()
  recaptcha: string;

  @Expose()
  @ApiProperty()
  phoneNumber: {
    code: string;
    countryCode: string;
    number: string;
    formatted: string;
    isValid: boolean;
  };
}

export class AuthCreateNewAuthRecordDto extends PickType(AuthRecord, [
  'email',
  'phoneNumber',
]) {
  static buildDto(payload: AuthRegisterUserDto): AuthCreateNewAuthRecordDto {
    return plainToClass(AuthCreateNewAuthRecordDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class AuthCreateCredentialsDto extends PickType(Credential, [
  'owner',
  'password',
]) {
  static buildDto(payload: AuthRegisterUserDto): AuthCreateCredentialsDto {
    return plainToClass(AuthCreateCredentialsDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class AuthCreateUserDetailsDto {
  @Expose()
  @ApiProperty()
  firstName: string;

  @Expose()
  @ApiProperty()
  lastName: string;

  static buildDto(payload: AuthRegisterUserDto): AuthCreateUserDetailsDto {
    return plainToClass(AuthCreateUserDetailsDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class AuthGetEmailWithPhoneNumberDto extends PickType(AuthRecord, [
  'email',
  'phoneNumber',
]) {
  static buildDto(payload: AuthRecord): AuthGetEmailWithPhoneNumberDto {
    return plainToClass(AuthGetEmailWithPhoneNumberDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class AuthVerifyCodeDto {
  @Expose()
  @ApiProperty()
  code: number;

  @Expose()
  @ApiProperty()
  phoneNumber: string;
}

export class AuthTokensDto {
  @Expose()
  @IsString()
  accessToken: string;

  @Expose()
  @IsString()
  refreshToken: string;

  static buildDto(payload: any): AuthTokensDto {
    return plainToClass(AuthTokensDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
