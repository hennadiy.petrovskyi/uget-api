import { PhoneNumber } from '../entities';
import { PickType } from '@nestjs/mapped-types';
import { plainToClass } from 'class-transformer';

export class SavePhoneDetailsDto extends PickType(PhoneNumber, [
  'code',
  'countryCode',
  'formatted',
  'number',
]) {
  static buildDto(payload: any): SavePhoneDetailsDto {
    return plainToClass(SavePhoneDetailsDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class PhoneDetailsDto extends PickType(PhoneNumber, [
  'code',
  'countryCode',
  'formatted',
  'number',
  'isVerified',
]) {
  static buildDto(payload: any): PhoneDetailsDto {
    return plainToClass(PhoneDetailsDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
