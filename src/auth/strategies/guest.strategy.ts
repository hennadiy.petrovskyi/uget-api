import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

import { ConfigService } from '@nestjs/config';
import { AuthEmitter } from '../emitters';

@Injectable()
export class GuestStrategy extends PassportStrategy(Strategy, 'JWT_GUEST') {
  constructor(
    private readonly configService: ConfigService,
    private readonly authEmitter: AuthEmitter,
  ) {
    super({
      ignoreExpiration: false,
      jwtFromRequest: ExtractJwt.fromHeader('authorization'),
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  public async validate(payload: any): Promise<{ id: number }> {
    try {
      const { sub } = payload;

      return sub;
    } catch (e) {
      console.log(e);
    }
  }
}
