import {
  CHAT_EVENTS,
  EMAIL_EVENTS,
  FILE_EVENTS,
  SETTINGS,
  TRIP_EVENT,
  USER_EVENTS,
} from './../../common/events/events-constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { AUTH_EVENTS, ORDER_EVENT } from 'src/common/events';
import { AuthCreateUserDetailsDto, AuthVerifyCodeDto } from '../dtos';
import { AuthRecord } from '../../auth/entities/auth-record.entity';
import { FileType } from 'src/file/enums';

@Injectable()
export class AuthEmitter {
  constructor(private eventEmitter: EventEmitter2) {}

  public async sendVerificationCode(sendVerificationCodeDto: any) {
    return await this.eventEmitter.emitAsync(
      AUTH_EVENTS.SEND_CONFIRMATION_CODE,
      sendVerificationCodeDto,
    );
  }

  public async verifyCode(authVerifyCodeDto: AuthVerifyCodeDto) {
    const response = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.VERIFY_CONFIRMATION_CODE,
      authVerifyCodeDto,
    );

    if (!response.length) {
      return;
    }

    return response[0];
  }

  public async createUserDetails({
    owner,
    authCreateUserDetailsDto,
  }: {
    owner: AuthRecord;
    authCreateUserDetailsDto: AuthCreateUserDetailsDto;
  }) {
    return await this.eventEmitter.emitAsync(USER_EVENTS.CREATE_USER_DETAILS, {
      owner,
      ...authCreateUserDetailsDto,
    });
  }

  public async getUserDetails(owner: any) {
    const details = await this.eventEmitter.emitAsync(
      USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID,
      owner,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async updateUserDetails(updateDto: any) {
    const details = await this.eventEmitter.emitAsync(
      USER_EVENTS.UPDATE_USER_DETAILS,
      updateDto,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async getUserProfileImage(owner: string) {
    const file = await this.eventEmitter.emitAsync(
      FILE_EVENTS.GET_FILE_BY_TYPE,
      { owner, type: FileType.PROFILE_IMAGE },
    );

    if (file && file.length) {
      return file[0];
    }

    return null;
  }

  public async getUserOrders(owner: number) {
    const details = await this.eventEmitter.emitAsync(
      ORDER_EVENT.GET_USER_ORDERS,
      owner,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async getUserTrips(owner: number) {
    const details = await this.eventEmitter.emitAsync(
      TRIP_EVENT.GET_USER_TRIPS,
      owner,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async getNotificationSettings(owner: number) {
    const details = await this.eventEmitter.emitAsync(
      SETTINGS.GET_USER_NOTIFICATION_SETTINGS,
      owner,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async getUserConversations(owner: string) {
    const details = await this.eventEmitter.emitAsync(
      CHAT_EVENTS.GET_USER_CONVERSATIONS,
      owner,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async sendResetPasswordEmail(sendResetPasswordEmailDto: {
    email: string;
    link: string;
  }) {
    await this.eventEmitter.emitAsync(
      EMAIL_EVENTS.SEND_RESET_PASSWORD,
      sendResetPasswordEmailDto,
    );
  }

  public async sendVerifyEmail(sendResetPasswordEmailDto: {
    email: string;
    link: string;
  }) {
    await this.eventEmitter.emitAsync(
      EMAIL_EVENTS.SEND_VERIFICATION,
      sendResetPasswordEmailDto,
    );
  }
}
