import { LoggerModule } from '../utils/logger';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import {
  AuthService,
  CryptoService,
  CredentialService,
  PhoneNumberService,
} from './services';
import { AuthController } from './controllers';
import { CacheRedisModule } from '../common/modules/cache.module';
import { AuthRecord, Credential, PhoneNumber } from './entities';
import { AuthEmitter } from './emitters';
import { AuthListener, PhoneNumberListener } from './listeners';
import { MorganMiddleware } from 'src/common/middlewares/morgan.middleware';
import { JwtStrategy, GuestStrategy } from './strategies';

@Module({
  imports: [
    TypeOrmModule.forFeature([AuthRecord, Credential, PhoneNumber]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
      }),
    }),
    PassportModule,
    CacheRedisModule,
    LoggerModule,
    HttpModule,
  ],

  controllers: [AuthController],
  providers: [
    JwtStrategy,
    GuestStrategy,
    AuthService,
    CredentialService,
    CryptoService,
    AuthEmitter,
    AuthListener,
    PhoneNumberService,
    PhoneNumberListener,
  ],
  exports: [AuthService, JwtModule],
})
export class AuthModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes(AuthController);
  }
}
