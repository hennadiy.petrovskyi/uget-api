import { AuthRecord } from './../entities/auth-record.entity';
import { AuthEmitter } from './../emitters/auth.emitter';
import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  Res,
  Headers,
  UnauthorizedException,
  Get,
  UseGuards,
  Put,
  ForbiddenException,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import {
  AuthService,
  CryptoService,
  CredentialService,
  PhoneNumberService,
} from '../services';

import {
  AuthCheckPhoneNumberDto,
  AuthRegisterUserDto,
  AuthVerifyCodeDto,
  AuthLoginByEmailDto,
  AuthCreateUserDetailsDto,
  SavePhoneDetailsDto,
  PhoneDetailsDto,
} from '../dtos';
import e, { Response } from 'express';
import { Role } from '../enums/role.enum';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { User } from 'src/common/decorators';

import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';
import { FindOperator } from 'typeorm';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ms = require('ms');

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(
    private service: AuthService,
    private phoneNumberService: PhoneNumberService,
    private credentialService: CredentialService,
    private cryptoService: CryptoService,
    private authEmitter: AuthEmitter,
    private httpService: HttpService,
  ) {}

  @Post('login')
  async loginWithCredentials(@Body() authLoginByEmailDto: AuthLoginByEmailDto) {
    const user = await this.service.findAuthRecordByEmail(
      authLoginByEmailDto.email,
    );

    if (!user) {
      throw new HttpException(
        {
          type: 404,
          error: `User with this email not found`,
          field: 'email',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const isPasswordMatch = await this.credentialService.matchCredential(
      user.id,
      authLoginByEmailDto.password,
    );

    if (!isPasswordMatch) {
      throw new HttpException(
        {
          type: 'wrongPassword',
          error: `Wrong password`,
          field: 'password',
        },
        HttpStatus.FORBIDDEN,
      );
    }

    const tokens = await this.cryptoService.createAccessToken(
      user.uuid,
      Role.USER,
    );

    const profile = await this.buildUserProfileDto(user);

    return { profile, session: tokens };
  }

  @Post('register')
  async registerNewUser(
    @Body() authRegisterUserDto: AuthRegisterUserDto,
    @Res() response: Response,
  ) {
    const isEmailInUse = await this.service.findAuthRecordByEmail(
      authRegisterUserDto.email,
    );

    const isPhoneNumberInUse =
      await this.phoneNumberService.findPhoneNumberByNumber(
        authRegisterUserDto.phoneNumber.formatted,
      );

    const errors = [];

    if (isEmailInUse) {
      errors.push({
        field: 'email',
        type: 'exist',
        message: 'This email already in use',
      });
    }

    if (isPhoneNumberInUse) {
      errors.push({
        field: 'phoneNumber',
        type: 'exist',
        message: 'This phone number already in use',
      });
    }

    if (errors.length) {
      throw new HttpException(
        {
          errors,
        },
        HttpStatus.FORBIDDEN,
      );
    }

    const { phoneNumber, ...createAuthRecordDTO } = authRegisterUserDto;

    const user = await this.service.createNewUserAuthRecord({
      ...createAuthRecordDTO,
      phoneNumber: phoneNumber.formatted,
    });

    await this.phoneNumberService.savePhoneNumber({
      owner: user,
      ...SavePhoneDetailsDto.buildDto(phoneNumber),
    });

    await this.credentialService.createNewCredential({
      owner: user,
      password: authRegisterUserDto.password,
    });

    const userDetails = AuthCreateUserDetailsDto.buildDto(authRegisterUserDto);

    await this.authEmitter.createUserDetails({
      owner: user,
      authCreateUserDetailsDto: userDetails,
    });

    await this.authEmitter.sendVerificationCode({
      phoneNumber: phoneNumber.formatted,
    });

    const verifyEmailToken =
      await this.cryptoService.generatePasswordResetToken(user.uuid);

    // const link = `uget://auth/verification/email/${verifyEmailToken}`;
    // const link = `https://api.uget.co/auth/verification/email`;
    const link = `https://api.uget.co/redirect/resend/email/${verifyEmailToken}`;

    await this.authEmitter.sendVerifyEmail({ email: user.email, link });

    const tokens = await this.cryptoService.createAccessToken(
      user.uuid,
      Role.USER,
    );

    const profile = await this.buildUserProfileDto(user);

    return response.status(201).json({ profile, session: tokens });
  }

  @Get('logout')
  public async logout(@Headers('RefreshToken') refreshToken: string) {
    if (refreshToken) {
      return await this.cryptoService.deleteSession(refreshToken);
    }
    return;
  }

  @Post('verify/email')
  async verifyEmail(@Body() verifyEmailDto: { token: string }) {
    const payload = (await this.cryptoService.decodeToken(
      verifyEmailDto.token,
    )) as { uuid: string; email: string };

    const user = await this.service.findAuthRecordByUUID(payload.uuid);

    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: `User with this email not found`,
          field: 'email',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    await this.service.verifyUserEmail(user.id);

    const userUpdated = await this.service.findAuthRecordByUUID(user.uuid);
    const profile = await this.buildUserProfileDto(userUpdated);

    const response = { profile };
    return response;
  }

  @Post('verify/phone')
  async verifyIsUserWithPhoneNumberExist(
    @Body() authCheckPhoneNumberDto: AuthCheckPhoneNumberDto,
  ) {
    const user = await this.service.findAuthRecordByPhoneNumber(
      authCheckPhoneNumberDto.phoneNumber,
    );

    if (!user) {
      throw new HttpException(
        {
          error: `User with this phone number not found`,
          type: 404,
          field: 'phoneNumber',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const smsCodeResponse = await this.authEmitter.sendVerificationCode(
      authCheckPhoneNumberDto,
    );

    if (!smsCodeResponse) {
      throw new HttpException(
        {
          message: 'Failed to send sms code',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  @Post('verify/code')
  async verifyUserSMSCode(@Body() authVerifyCodeDto: AuthVerifyCodeDto) {
    console.log(authVerifyCodeDto);

    const user = await this.service.findAuthRecordByPhoneNumber(
      authVerifyCodeDto.phoneNumber,
    );

    const dbVerificationData = await this.authEmitter.verifyCode(
      authVerifyCodeDto,
    );

    if (dbVerificationData.code !== authVerifyCodeDto.code) {
      throw new HttpException(
        {
          field: 'sms',
          type: '404',
          message: 'Sms code is not found or expired',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    if (!user.isPhoneNumberVerified) {
      await this.service.verifyUserPhoneNumber(user.id);
    }

    const tokens = await this.cryptoService.createAccessToken(
      user.uuid,
      Role.USER,
    );

    const userUpdated = await this.service.findAuthRecordByUUID(user.uuid);

    const profile = await this.buildUserProfileDto(userUpdated);
    const response = { profile, session: tokens };
    return response;
  }

  @Post('verify/recaptcha')
  async verifyRecaptcha(@Body() verifyRecaptchaDto: any) {
    const verificationUrl = 'https://www.google.com/recaptcha/api/siteverify';

    const googleResponse = await lastValueFrom(
      this.httpService.post(
        verificationUrl,
        {},
        {
          params: {
            secret: '6Lfd8cIdAAAAADw36RC9t8TtflxIJ-_Hah9S-fEv',
            response: verifyRecaptchaDto.token,
          },
        },
      ),
    );

    return googleResponse.data;
  }

  @UseGuards(JwtAuthGuard)
  @Put('profile')
  async updateDetails(@User() user: AuthRecord, @Body() updateDetailsDto: any) {
    const { phoneNumber, email, ...userDetailsUpdate } = updateDetailsDto;

    if (phoneNumber) {
      const isPhoneNumberInUse =
        await this.phoneNumberService.findPhoneNumberByNumber(
          phoneNumber.formatted,
        );

      if (isPhoneNumberInUse) {
        throw new HttpException(
          {
            field: 'phoneNumber',
            type: 'exist',
            message: 'This phone number already in use',
          },
          HttpStatus.FORBIDDEN,
        );
      }

      const phoneNumberDetails =
        await this.phoneNumberService.findPhoneNumberByOwner(
          user.id as unknown as FindOperator<any>,
        );

      await this.phoneNumberService.updatePhoneNumberDetails(
        phoneNumberDetails.id,
        SavePhoneDetailsDto.buildDto(phoneNumber),
      );

      await this.service.updateUserPhoneNumber(user.id, phoneNumber.formatted);
    }

    if (email) {
      const isEmailInUse = await this.service.findAuthRecordByEmail(email);

      if (isEmailInUse) {
        throw new HttpException(
          {
            field: 'email',
            type: 'exist',
            message: 'This email already in use',
          },
          HttpStatus.FORBIDDEN,
        );
      }

      await this.service.updateUserEmail(user.id, email);
    }

    if (Object.values(userDetailsUpdate)) {
      await this.authEmitter.updateUserDetails({
        owner: user.id,
        ...userDetailsUpdate,
      });
    }

    const userUpdated = await this.service.findAuthRecordByUUID(user.uuid);
    const profile = await this.buildUserProfileDto(userUpdated);

    const response = { profile };
    return response;
  }

  @Get('refresh-session')
  public async refreshSession(@Headers('RefreshToken') refreshToken: string) {
    if (!refreshToken) {
      throw new UnauthorizedException();
    }

    return await this.cryptoService.refreshSession(refreshToken);
  }

  @Post('reset-password')
  public async resetPassword(@Body('email') resetPasswordDto: any) {
    const authRecord = await this.service.findAuthRecordByEmail(
      resetPasswordDto,
    );

    if (!authRecord) {
      throw new HttpException(
        {
          type: HttpStatus.NOT_FOUND,
          error: `User with this email not found`,
          field: 'email',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    const resetToken = await this.cryptoService.generateVerifyEmailToken(
      authRecord.uuid,
      authRecord.email,
    );

    // const link = `uget://auth/change/password/${resetToken}`;
    const link = `https://api.uget.co/redirect/reset-password/${resetToken}`;

    await this.authEmitter.sendResetPasswordEmail({
      email: authRecord.email,
      link,
    });
  }

  @Post('resend/sms-code')
  public async resendSmsCode(@Body('phoneNumber') phoneNumber: any) {
    const smsCodeResponse = await this.authEmitter.sendVerificationCode({
      phoneNumber,
    });

    if (!smsCodeResponse) {
      throw new HttpException(
        {
          message: 'Failed to send sms code',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  @Post('resend/email')
  public async resendEmail(@Body('email') email: any) {
    const authRecord = await this.service.findAuthRecordByEmail(email);

    if (!authRecord) {
      throw new HttpException(
        {
          message: 'User with this email was not found',
        },
        HttpStatus.FORBIDDEN,
      );
    }

    const verifyEmailToken =
      await this.cryptoService.generatePasswordResetToken(authRecord.uuid);

    // const link = `uget://auth/verification/email/${verifyEmailToken}`;

    const link = `https://api.uget.co/redirect/resend/email/${verifyEmailToken}`;

    await this.authEmitter.sendVerifyEmail({ email: authRecord.email, link });
  }

  @UseGuards(JwtAuthGuard)
  @Post('change/phone')
  public async changePhoneNumber(
    @User() user: AuthRecord,
    @Body('phoneNumber') phoneNumber: any,
  ) {
    const authRecord = await this.service.findAuthRecordByPhoneNumber(
      phoneNumber.formatted,
    );

    if (authRecord && authRecord.id === user.id) {
      throw new HttpException(
        {
          field: 'phoneNumber',
          type: 'same',
          message: 'Nothing to change , number are same',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    if (authRecord && authRecord.id !== user.id) {
      throw new HttpException(
        {
          field: 'phoneNumber',
          type: 'exist',
          message: 'This phone number already in use',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const updatedUser = await this.service.updateUserPhoneNumber(
      user.id,
      phoneNumber.formatted,
    );

    const userPhoneNumberDetails =
      await this.phoneNumberService.findPhoneNumberByOwner(
        user.id as unknown as FindOperator<any>,
      );

    await this.phoneNumberService.updatePhoneNumberDetails(
      userPhoneNumberDetails.id,
      phoneNumber,
    );

    await this.authEmitter.sendVerificationCode({
      phoneNumber: phoneNumber.formatted,
    });

    return { phoneNumber: updatedUser.phoneNumber };
  }

  @UseGuards(JwtAuthGuard)
  @Post('change/email')
  public async changeEmail(
    @User() user: AuthRecord,
    @Body('email') email: string,
  ) {
    const authRecord = await this.service.findAuthRecordByEmail(email);

    if (authRecord && authRecord.id === user.id) {
      throw new HttpException(
        {
          field: 'email',
          type: 'same',
          message: 'Nothing to change , email are same',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    if (authRecord && authRecord.id !== user.id) {
      throw new HttpException(
        {
          field: 'email',
          type: 'exist',
          message: 'This email already in use',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const updatedUser = await this.service.updateUserEmail(user.id, email);

    const verifyEmailToken =
      await this.cryptoService.generatePasswordResetToken(authRecord.uuid);

    const link = `https://api.uget.co/redirect/resend/email/${verifyEmailToken}`;

    await this.authEmitter.sendVerifyEmail({ email, link });

    return { email: updatedUser.email };
  }

  @Post('change/password')
  public async changePassword(@Body('') changePasswordDto: any) {
    const tokenPayload = (await this.cryptoService.decodeToken(
      changePasswordDto.token,
    )) as { uuid: string };

    const isPasswordRequestTokenRequestExist =
      await this.cryptoService.findPasswordResetToken(tokenPayload.uuid);

    if (
      !isPasswordRequestTokenRequestExist &&
      isPasswordRequestTokenRequestExist !== changePasswordDto.token
    ) {
      throw new ForbiddenException();
    }

    const user = await this.service.findAuthRecordByUUID(tokenPayload.uuid);

    const hashedPassword = await this.cryptoService.hashPassword(
      changePasswordDto.password,
    );

    await this.credentialService.updatePassword(user.id, {
      password: hashedPassword,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get('/profile')
  public async getUserProfile(@User() user: AuthRecord) {
    return await this.buildUserProfileDto(user);
  }

  async buildUserProfileDto(user: AuthRecord) {
    const userDetails = await this.authEmitter.getUserDetails(user.id);
    const profileImage = await this.authEmitter.getUserProfileImage(user.uuid);
    const phoneNumberDetails =
      await this.phoneNumberService.findPhoneNumberByOwner(
        user.id as unknown as FindOperator<any>,
      );

    const orders = await this.authEmitter.getUserOrders(user.id);
    const trips = await this.authEmitter.getUserTrips(user.id);
    const notificationSettings = await this.authEmitter.getNotificationSettings(
      user.id,
    );
    const conversations = await this.authEmitter.getUserConversations(
      user.uuid,
    );

    const response = {
      ...userDetails,
      createdAt: user.createdAt,
      uuid: user.uuid,
      profileImage,
      phoneNumber: {
        ...PhoneDetailsDto.buildDto(phoneNumberDetails),
        isVerified: user.isPhoneNumberVerified,
      },
      email: {
        value: user.email,
        isVerified: user.isEmailVerified,
      },
      orders,
      trips,
      notificationSettings,
      conversations,
    };

    return response;
  }
}
