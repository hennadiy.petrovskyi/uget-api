import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from '.';
import { PhoneNumberType } from '../enums';

@Entity({
  name: 'phone_number',
})
export class PhoneNumber extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  formatted: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  countryCode: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  code: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  number: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('boolean', { default: false })
  isVerified: boolean;
}
