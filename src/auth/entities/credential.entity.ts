import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { AuthRecord } from '.';
import { BaseEntity } from 'src/common/entities';

@Entity({
  name: 'credential',
})
export class Credential extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @Expose()
  @ApiProperty()
  @Column('text')
  password: string;
}
