import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity } from 'typeorm';
import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

import { BaseEntity } from 'src/common/entities';
import { Role } from '../enums/index';

@Entity({
  name: 'auth_record',
})
export class AuthRecord extends BaseEntity {
  @ApiProperty()
  @Column('enum', { array: true, default: [Role.USER], enum: Role })
  role: Role[];

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  uuid: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  phoneNumber: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  email: string;

  @Expose()
  @ApiProperty()
  @Column('boolean', { default: false })
  isPhoneNumberVerified: boolean;

  @Expose()
  @ApiProperty()
  @Column('boolean', { default: false })
  isEmailVerified: boolean;
}
