import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from '.';

@Entity({
  name: 'email',
})
export class Email extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  value: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('boolean')
  isVerified: boolean;
}
