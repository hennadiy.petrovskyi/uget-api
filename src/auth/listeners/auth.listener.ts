import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { AUTH_EVENTS, USER_EVENTS } from 'src/common/events';

import { AuthService } from '../services';
import { FindOperator } from 'typeorm';

@Injectable()
export class AuthListener {
  constructor(private readonly authService: AuthService) {}

  @OnEvent(USER_EVENTS.GET_AUTH_CREDENTIALS, { promisify: true })
  public async getUserCredentials(owner: number) {
    return await this.authService.getEmailWithPhoneNumber(owner);
  }

  @OnEvent(AUTH_EVENTS.GET_USER_BY_UUID, { promisify: true })
  public async getUserByUUID(uuid: string) {
    // console.log('OnEvent(AUTH_EVENTS.GET_USER_BY_UUID', uuid);

    return await this.authService.findAuthRecordByUUID(uuid);
  }

  @OnEvent(AUTH_EVENTS.GET_USER_BY_ID, { promisify: true })
  public async getUserByID(id: FindOperator<any>) {
    const authRecord = await this.authService.findAuthRecordById(id);

    if (!authRecord) {
      return;
    }

    return authRecord;
  }
}
