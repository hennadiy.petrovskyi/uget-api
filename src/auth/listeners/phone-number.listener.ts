import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import {
  AUTH_EVENTS,
  PHONE_NUMBER_EVENT,
  USER_EVENTS,
} from 'src/common/events';

import { PhoneNumberService } from '../services';

@Injectable()
export class PhoneNumberListener {
  constructor(private readonly phoneNumberService: PhoneNumberService) {}

  @OnEvent(PHONE_NUMBER_EVENT.SAVE_PHONE_NUMBER, { promisify: true })
  public async savePhoneNumberDetails(phoneNumberToSaveDto: any) {
    const existingRecord =
      await this.phoneNumberService.findPhoneNumberByNumber(
        phoneNumberToSaveDto.formatted,
      );

    if (existingRecord) {
      return existingRecord;
    }

    return await this.phoneNumberService.savePhoneNumber(phoneNumberToSaveDto);
  }

  @OnEvent(PHONE_NUMBER_EVENT.GET_PHONE_NUMBER, { promisify: true })
  public async getPhoneNumberDetails(phoneNumberToGetDto: any) {
    return await this.phoneNumberService.findPhoneNumberByOwner(
      phoneNumberToGetDto,
    );
  }

  @OnEvent(PHONE_NUMBER_EVENT.DELETE_PHONE_NUMBER, { promisify: true })
  public async deletePhoneNumberDetails(phoneNumberToDeleteDto: any) {
    // return await this.phoneNumberService.getEmailWithPhoneNumber(owner);
  }
}
