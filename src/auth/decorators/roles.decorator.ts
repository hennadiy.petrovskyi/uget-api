import { SetMetadata } from '@nestjs/common';
import { Role } from '../enums';

export const ROLES_KEYS = 'roles';
export const Roles = (...args: Role[]) => SetMetadata(ROLES_KEYS, args);
