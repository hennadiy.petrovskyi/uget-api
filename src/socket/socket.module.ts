import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import { SocketGateway } from './gateway';
import { SocketListener } from './listeners';
import {} from './controllers';
import { LoggerModule } from 'src/utils/logger';

@Module({
  imports: [TypeOrmModule.forFeature([]), HttpModule, LoggerModule],
  controllers: [],
  providers: [SocketGateway, SocketListener],
  exports: [],
})
export class SocketModule {}
