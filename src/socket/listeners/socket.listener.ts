import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { SocketGateway } from '../gateway';
import { SOCKET_EVENTS } from 'src/common/events';

@Injectable()
export class SocketListener {
  constructor(private readonly socketGateway: SocketGateway) {}

  @OnEvent(SOCKET_EVENTS.SEND_MESSAGE, { promisify: true })
  public async sendMessage(message: { uuid: string; payload: any }) {
    this.socketGateway.sendMessageByUUID(message.uuid, message.payload);
  }
}
