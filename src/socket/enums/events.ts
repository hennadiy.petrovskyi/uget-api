export enum SocketEvents {
  UPDATE_MY_TRIPS = 'trips.update.my',
  UPDATE_QUERY_TRIPS = 'trips.update.query',
  UPDATE_MY_ORDERS = 'orders.update.my',
  UPDATE_QUERY_ORDERS = 'orders.update.query',
  UPDATE_PROFILE = 'profile.update',
}
