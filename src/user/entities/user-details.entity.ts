import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { AuthRecord } from '../../auth/entities';
import { BaseEntity } from 'src/common/entities';

@Entity({
  name: 'user_details',
})
export class UserDetails extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @Expose()
  @ApiProperty()
  @Column('text')
  firstName: string;

  @Expose()
  @ApiProperty()
  @Column('text')
  lastName: string;

  @Expose()
  @ApiProperty()
  @Column('text', { array: true, default: [], nullable: true })
  toRate: string[];
}
