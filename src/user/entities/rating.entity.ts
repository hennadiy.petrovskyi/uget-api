import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

import { BaseEntity } from 'src/common/entities';
import { UserDeliveryRole } from '../enums';
import { AuthRecord } from 'src/auth/entities';

@Entity({
  name: 'user_rating',
})
export class Rating extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @ApiProperty()
  @Column('enum', { enum: UserDeliveryRole })
  role: UserDeliveryRole;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  orderId: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('boolean', { default: false })
  isRated: boolean;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('int', { nullable: true })
  ratedBy: number;

  @Expose()
  @ApiProperty()
  @Column('int', { nullable: true })
  stars: number;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text', { nullable: true })
  comment: string;
}
