import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { UserDetails } from '../entities';

import { UserDetailsEmitter } from '../emitters';
import { LoggerService } from '../../utils/logger';

import { UserCreateUserDetailsDto, UserGetUserDetailsDto } from '../dtos';

@Injectable()
export class UserDetailsService {
  constructor(
    @InjectRepository(UserDetails)
    private userDetailsRepository: Repository<UserDetails>,
    private userDetailsEmitter: UserDetailsEmitter,
    private logger: LoggerService,
  ) {}

  async findUserDetails(owner: FindOperator<number>): Promise<UserDetails> {
    const functionName = this.findUserDetails.name;

    try {
      return this.userDetailsRepository.findOne({ where: { owner } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async createNewUserDetails(
    userCreateUserDetailsDto: UserCreateUserDetailsDto,
  ) {
    const functionName = this.createNewUserDetails.name;

    try {
      await this.userDetailsRepository.save(userCreateUserDetailsDto);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async updateUserDetails(update: any) {
    const functionName = this.updateUserDetails.name;

    try {
      return await this.userDetailsRepository.save(update);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async buildUserDto() {
    try {
      const owner = 1;
      const userDetails = await this.findUserDetails(owner as any);
      const credentials = await this.userDetailsEmitter.getUserAuthCredentials(
        owner,
      );
      const modifiedUserDetails = UserGetUserDetailsDto.buildDto(userDetails);

      console.log('user', { ...modifiedUserDetails, ...credentials[0] });
    } catch (error) {}
  }
}
