import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { Rating } from '../entities';

import { LoggerService } from '../../utils/logger';

@Injectable()
export class RatingService {
  constructor(
    @InjectRepository(Rating)
    private ratingRepository: Repository<Rating>,
    private logger: LoggerService,
  ) {}

  async findRatingByOwner(owner: FindOperator<any>): Promise<Rating> {
    const functionName = this.findRatingByOwner.name;

    try {
      return this.ratingRepository.findOne({ where: { owner, isRated: true } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async findRatingById(id: number): Promise<Rating> {
    const functionName = this.findRatingByOwner.name;

    try {
      return this.ratingRepository.findOne({
        where: { id },
        loadRelationIds: true,
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async findNotRatedRatingsByOwner(ratedBy: number): Promise<Rating[]> {
    const functionName = this.findRatingByOwner.name;

    try {
      return this.ratingRepository.find({
        where: { ratedBy, isRated: false },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async findAllRatingsByOwner(owner: FindOperator<any>): Promise<Rating[]> {
    const functionName = this.findRatingByOwner.name;

    try {
      return this.ratingRepository.find({ where: { owner, isRated: true } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async createNewRatingRecord(createRate: any) {
    const functionName = this.createNewRatingRecord.name;

    try {
      await this.ratingRepository.save(createRate);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async updateRatingRecord(update: any) {
    const functionName = this.updateRatingRecord.name;

    try {
      return await this.ratingRepository.save(update);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
