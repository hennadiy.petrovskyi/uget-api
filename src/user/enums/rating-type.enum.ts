export enum UserDeliveryRole {
  SENDER = 'SENDER',
  TRAVELLER = 'TRAVELLER',
}
