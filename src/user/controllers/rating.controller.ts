import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  HttpException,
  HttpStatus,
  NotFoundException,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { response, Response } from 'express';

import {} from '../emitters/user-details.emitter';
import { RatingService } from '../services';
import {} from '../dtos';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { User } from 'src/common/decorators';
import { UserDeliveryRole } from '../enums';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { SocketEvents } from 'src/socket/enums/events';
import { AUTH_EVENTS, SOCKET_EVENTS } from 'src/common/events';

@ApiTags('Rating')
@Controller('rating')
export class RatingController {
  constructor(
    private service: RatingService,
    private eventEmitter: EventEmitter2,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Put()
  async rateUser(@Body('') rateUserDto: any, @User() user) {
    const ratingToUpdate = await this.service.findRatingById(rateUserDto.id);

    if (!ratingToUpdate) {
      throw new NotFoundException();
    }

    if (ratingToUpdate.ratedBy !== user.id || ratingToUpdate.isRated) {
      throw new ForbiddenException();
    }

    await this.service.updateRatingRecord({
      ...rateUserDto,
      isRated: true,
    });

    const result = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_ID,
      ratingToUpdate.owner,
    );

    if (result && result.length) {
      await this.eventEmitter.emitAsync(SOCKET_EVENTS.SEND_MESSAGE, {
        uuid: result[0].uuid,
        payload: {
          action: SocketEvents.UPDATE_PROFILE,
        },
      });
    }

    console.log(ratingToUpdate);
  }
}
