import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  NotFoundException,
  Post,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { response, Response } from 'express';

import { UserDetailsEmitter } from '../emitters/user-details.emitter';
import { UserDetailsService, RatingService } from '../services';
import {} from '../dtos';

@ApiTags('User Details')
@Controller('user-details')
export class UserController {
  constructor(
    private service: UserDetailsService,
    private ratingService: RatingService,
    private userDetailsEmitter: UserDetailsEmitter,
  ) {}

  @Post('')
  async getUserDetails(@Body('userUUID') userUUID: any) {
    const authRecord = await this.userDetailsEmitter.geAuthRecord(userUUID);

    if (!authRecord) {
      throw new NotFoundException();
    }

    const profileImage = await this.userDetailsEmitter.getUserProfileImage(
      userUUID,
    );

    const userDetails = await this.service.findUserDetails(authRecord.id);

    const ratedRatings = await this.ratingService.findAllRatingsByOwner(
      authRecord.id,
    );

    const countAverage = (arr: any[]) => {
      if (!arr.length) {
        return 0;
      }

      return (
        arr.map((el) => el.stars).reduce((prev, current) => prev + current) /
        arr.length
      );
    };

    return {
      fullName: userDetails.firstName + ' ' + userDetails.lastName,
      profileImage,
      rating: { average: countAverage(ratedRatings) },
    };

    // await this.service.buildUserDto();
    // console.log('!!!!', userUUID);
  }
}
