import { LoggerModule } from '../utils/logger';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

import { TypeOrmModule } from '@nestjs/typeorm';

import { UserDetailsService, RatingService } from './services';

import { Rating, UserDetails } from './entities';

import { RatingController, UserController } from './controllers';
import { UserDetailsListener, RatingListener } from './listeners';
import { UserDetailsEmitter } from './emitters';
import { MorganMiddleware } from 'src/common/middlewares/morgan.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([UserDetails, Rating]), LoggerModule],

  controllers: [UserController, RatingController],
  providers: [
    UserDetailsService,
    RatingService,
    UserDetailsListener,
    RatingListener,
    UserDetailsEmitter,
  ],
  exports: [UserDetailsService],
})
export class UserModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes(UserController);
  }
}
