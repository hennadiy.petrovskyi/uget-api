import {
  AUTH_EVENTS,
  FILE_EVENTS,
} from './../../common/events/events-constants';
import { USER_EVENTS } from '../../common/events/events-constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { FileType } from 'src/file/enums';

@Injectable()
export class UserDetailsEmitter {
  constructor(private eventEmitter: EventEmitter2) {}

  public async getUserAuthCredentials(owner: number) {
    return await this.eventEmitter.emitAsync(
      USER_EVENTS.GET_AUTH_CREDENTIALS,
      owner,
    );
  }

  public async geAuthRecord(owner: string) {
    const response = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_UUID,
      owner,
    );

    if (!response.length) {
      return;
    }

    return response[0];
  }

  public async getUserProfileImage(owner: string) {
    const file = await this.eventEmitter.emitAsync(
      FILE_EVENTS.GET_FILE_BY_TYPE,
      { owner, type: FileType.PROFILE_IMAGE },
    );

    if (file && file.length) {
      return file[0];
    }

    return null;
  }
}
