import { PickType } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';
import { UserDetails } from '../entities';

export class UserCreateUserDetailsDto extends PickType(UserDetails, [
  'owner',
  'firstName',
  'lastName',
]) {}

export class UserGetUserDetailsDto extends PickType(UserDetails, [
  'firstName',
  'lastName',
]) {
  static buildDto(payload: UserDetails): UserGetUserDetailsDto {
    return plainToClass(UserGetUserDetailsDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
