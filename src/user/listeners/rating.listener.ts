import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { SOCKET_EVENTS, USER_EVENTS } from 'src/common/events';

import { RatingService } from '../services';
import { SocketEvents } from 'src/socket/enums/events';

@Injectable()
export class RatingListener {
  constructor(
    private readonly ratingService: RatingService,
    private eventEmitter: EventEmitter2,
  ) {}

  @OnEvent(USER_EVENTS.ADD_NEW_UNRATED_RATE, { promisify: true })
  public async createUserDetails(newRateDto: any) {
    await this.ratingService.createNewRatingRecord(newRateDto);

    await this.eventEmitter.emitAsync(SOCKET_EVENTS.SEND_MESSAGE, {
      uuid: newRateDto.owner.uuid,
      payload: {
        action: SocketEvents.UPDATE_PROFILE,
      },
    });
  }

  @OnEvent(USER_EVENTS.SET_RATING_IS_RATED, { promisify: true })
  public async getUserDetails(updateRateDto: any) {
    const rating = await this.ratingService.findRatingByOwner(
      updateRateDto.owner,
    );

    // if (!rating || rating.isRated) {
    //   return null;
    // }

    console.log(updateRateDto);

    await this.ratingService.updateRatingRecord(updateRateDto);

    // await this.eventEmitter.emitAsync(SOCKET_EVENTS.SEND_MESSAGE, {
    //   uuid: newRateDto.owner.uuid,
    //   payload: {
    //     action: SocketEvents.UPDATE_PROFILE,
    //   },
    // });
  }
}
