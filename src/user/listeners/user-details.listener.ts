import { map } from 'rxjs';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { USER_EVENTS } from 'src/common/events';
import { UserCreateUserDetailsDto, UserGetUserDetailsDto } from '../dtos';

import { UserDetailsService, RatingService } from '../services';
import { UserDeliveryRole } from '../enums';
import { FindOperator } from 'typeorm';

@Injectable()
export class UserDetailsListener {
  constructor(
    private readonly userDetailsService: UserDetailsService,
    private readonly ratingService: RatingService,
  ) {}

  @OnEvent(USER_EVENTS.CREATE_USER_DETAILS, { promisify: true })
  public async createUserDetails(
    userCreateUserDetailsDto: UserCreateUserDetailsDto,
  ) {
    return await this.userDetailsService.createNewUserDetails(
      userCreateUserDetailsDto,
    );
  }

  @OnEvent(USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID, { promisify: true })
  public async getUserDetails(owner: FindOperator<any>) {
    const details = await this.userDetailsService.findUserDetails(owner as any);

    const ratedRatings = await this.ratingService.findAllRatingsByOwner(owner);

    const countAverage = (arr: any[]) => {
      if (!arr.length) {
        return 0;
      }

      return (
        arr.map((el) => el.stars).reduce((prev, current) => prev + current) /
        arr.length
      );
    };

    const notRated = await this.ratingService.findNotRatedRatingsByOwner(
      owner as any,
    );

    if (!details) {
      return null;
    }

    const asTraveler = ratedRatings.filter(
      (el) => el.role === UserDeliveryRole.TRAVELLER,
    );
    const asSender = ratedRatings.filter(
      (el) => el.role === UserDeliveryRole.SENDER,
    );
    return {
      ...UserGetUserDetailsDto.buildDto(details),
      rating: {
        average: countAverage(ratedRatings),
        sender: {
          value: countAverage(asSender),
          amount: asSender.length,
        },
        traveler: {
          value: countAverage(asTraveler),
          amount: asTraveler.length,
        },
        toRate: notRated.map(({ id, role }) => ({ id, role })),
      },
    };
  }

  @OnEvent(USER_EVENTS.UPDATE_USER_DETAILS, { promisify: true })
  public async updateUserDetails(updateDto: any) {
    const { owner, ...update } = updateDto;
    const details = await this.userDetailsService.findUserDetails(owner);

    await this.userDetailsService.updateUserDetails({
      id: details.id,
      ...update,
    });
  }
}
