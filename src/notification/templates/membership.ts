export const membershipTemplate = ({ link }) => {
  return /*html*/ `<html lang="en">
  <head>
    <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@500;700&display=swap"
      rel="stylesheet"
    />
    <style>
      body {
        width: 100%;
        background-color: #e5e5e5;
      }

      .logo {
        width: 120px;
        height: auto;
        margin: 0 auto;
        display: block;
        margin-bottom: 60px;
      }

      .container {
        max-width: 800px;
        margin: 0 auto;
        background-color: white;
        background-image: url("https://drive.google.com/file/d/1WQLh5fap3KwDZI_38KY2UvQRT4Awyhdg/view?usp=sharing");
        background-position: center;
        background-size: cover;
        padding: 50px 0;
      }

      .copyright {
        font-weight: 500;
        font-size: 20px;
        font-family: Noto Sans KR;
        color: #0c1221;
      }

      p,
      h1 {
        margin: 0;
      }

      .p1,
      .p2,
      .p3,
      .p4,
      .p5,
      .p6,
      title {
        font-family: Noto Sans KR;
        font-weight: normal;
        font-size: 20px;
        line-height: 32px;
        text-align: center;
        color: #0c1221;
        margin-bottom: 18px;
        text-align: left;
      }

      .title {
        font-weight: bold;
        font-size: 28px;
        line-height: 41px;
        margin-bottom: 30px;
      }

      .p4 {
        margin-bottom: 70px;
      }

      .p5 {
        margin-bottom: 0px;
      }

      .p6 {
        font-weight: bold;
        margin-bottom: 100px;
      }

      .inner-content-container {
        max-width: 512px;
        margin: 0 auto;
      }

      a {
        background-color: #5652f4;
        padding: 15px 0;
        color: white;
        font-weight: bold;
        font-size: 16px;
        line-height: 20px;
        text-align: center;
        letter-spacing: -0.24px;
        border-radius: 62px;
        border: 0;
        cursor: pointer;
        font-family: Noto Sans KR;
        display: block;
        margin: 0 auto;
        max-width: 300px;
        margin-bottom: 80px;
        text-decoration: none;
      }
    </style>
    <title>Flow Membership Confirmation</title>
  </head>
  <body style="width: 100%; background-color: #e5e5e5">
    <div
      class="container"
      style="
        max-width: 800px;
        margin: 0 auto;
        background-color: white;
        background-image: url('https://drive.google.com/file/d/1WQLh5fap3KwDZI_38KY2UvQRT4Awyhdg/view?usp=sharing');
        background-position: center;
        background-size: cover;
        padding: 50px 0;
      "
    >
      <div
        class="inner-content-container"
        style="max-width: 512px; margin: 0 auto"
      >
        <img
          class="logo"
          src="cid:logo"
          style="
            width: 120px;
            height: auto;
            margin: 0 auto;
            margin-bottom: 60px;
            display: block;
          "
        />

        <h1
          class="title"
          style="
            font-family: Noto Sans KR;
            font-style: normal;
            font-weight: bold;
            font-size: 28px;
            line-height: 41px;
            color: #0c1221;
            margin-bottom: 30px;
          "
        >
          Flow Membership Confirmation
        </h1>

        <p
          class="p1"
          style="
            font-family: Noto Sans KR;
            font-weight: normal;
            font-size: 20px;
            line-height: 32px;
            text-align: left;
            color: #0c1221;
          "
        >
          Thanks for joining our beta community! 🎉
        </p>

        <p
          class="p2"
          style="
            font-family: Noto Sans KR;
            font-weight: normal;
            font-size: 20px;
            line-height: 32px;
            text-align: left;
            color: #0c1221;
          "
        >
          Our aim is to get you into your natural ‘state of flow’. As a Founding
          Member, you will enjoy the current benefits of the app for free.
        </p>

        <p
          class="p3"
          style="
            font-family: Noto Sans KR;
            font-weight: normal;
            font-size: 20px;
            line-height: 32px;
            text-align: left;
            color: #0c1221;
          "
        >
          As we launch more premium features, Founding Members will be invited
          to receive a heavily discounted lifetime membership as a thank you for
          your feedback.
        </p>

        <p
          class="p4"
          style="
            font-family: Noto Sans KR;
            font-weight: normal;
            font-size: 20px;
            line-height: 32px;
            text-align: left;
            color: #0c1221;
          "
        >
          We’ve packed lots of exciting gems into this app. So enjoy your
          experience! Get into your ‘flow state’.
        </p>
        
        <a
        href=${link}
          style="
            background-color: #5652f4;
            padding: 15px 0;
            color: white;
            font-weight: bold;
            font-size: 16px;
            line-height: 20px;
            text-align: center;
            letter-spacing: -0.24px;
            border-radius: 62px;
            border: 0;
            cursor: pointer;
            font-family: Noto Sans KR;
            display: block;
            margin: 0 auto;
            max-width: 300px;
            margin-bottom: 100px;
            text-decoration: none;
          "
        >
          Get FLOW
        </a>

        <p
          class="p5"
          style="
            font-family: Noto Sans KR;
            font-weight: normal;
            font-size: 20px;
            line-height: 32px;
            text-align: left;
            color: #0c1221;
            margin-bottom: 0px;
          "
        >
          With love
        </p>

        <p
          class="p6"
          style="
            font-family: Noto Sans KR;
            font-weight: normal;
            font-size: 20px;
            line-height: 32px;
            text-align: left;
            color: #0c1221;
            font-weight: bold;
            margin-bottom: 80px;
          "
        >
          The Flow Team
        </p>

        <p
          class="copyright"
          style="
            font-weight: 500;
            font-size: 20px;
            font-family: Noto Sans KR;
            color: #0c1221;
            text-align: center;
          "
        >
          Copyright ${new Date().getFullYear()}. All rights reserved
        </p>
      </div>
    </div>
  </body>
</html>
`;
};
