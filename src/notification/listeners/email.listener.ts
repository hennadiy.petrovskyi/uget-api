import { EMAIL_EVENTS } from './../../common/events/events-constants';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { AUTH_EVENTS } from 'src/common/events';

import { EmailService } from '../services';
import { EmailType } from '../enums';

@Injectable()
export class EmailListener {
  constructor(private readonly emailService: EmailService) {}

  @OnEvent(EMAIL_EVENTS.SEND_RESET_PASSWORD, { promisify: true })
  public async resetPassword(resetPasswordDto: {
    email: string;
    link: string;
  }) {
    await this.emailService.sendEmail({
      ...resetPasswordDto,
      type: EmailType.RESET_PASSWORD,
      title: 'Uget: Password reset',
    });
  }

  @OnEvent(EMAIL_EVENTS.SEND_VERIFICATION, { promisify: true })
  public async verifyEmail(verifyEmailDto: { email: string; link: string }) {
    await this.emailService.sendEmail({
      ...verifyEmailDto,
      type: EmailType.VERIFY_EMAIL,
      title: 'Uget: Email verification',
    });
  }
}
