// import { FirebasePhoneAuthService } from '../services/firebase-phone-auth.service';
import { ORDER_EVENT } from '../../common/events/events-constants';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { AUTH_EVENTS } from 'src/common/events';

import { SmsService } from '../services/sms.service';
import { CacheService } from 'src/common/services/cache.service';

@Injectable()
export class SmsFirebaseListener {
  constructor(
    private readonly smsService: SmsService,
    // private readonly firebasePhoneAuthService: FirebasePhoneAuthService,
    private cacheService: CacheService,
  ) {}

  @OnEvent(AUTH_EVENTS.SEND_CONFIRMATION_CODE, { promisify: true })
  public async sendConfirmationCode(sendConfirmationCodeDto: {
    recaptcha: string;
    phoneNumber: string;
  }) {
    // const sessionId = await this.firebasePhoneAuthService.sendSmsCode(
    //   '+491775114823',
    //   sendConfirmationCodeDto.recaptcha,
    // );

    // if (!sessionId) {
    //   return;
    // }

    // await this.cacheService.set(
    //   sendConfirmationCodeDto.phoneNumber,
    //   {
    //     sessionInfo: sessionId,
    //   },
    //   300,
    // );

    const { phoneNumber } = sendConfirmationCodeDto;

    console.log(phoneNumber);

    await this.cacheService.set(
      phoneNumber,
      {
        code: '111111',
      },
      3000,
    );
  }

  @OnEvent(AUTH_EVENTS.VERIFY_CONFIRMATION_CODE, { promisify: true })
  public async verifyCode(verifyCodeDto: {
    phoneNumber: string;
    code: string;
  }) {
    const verificationDetails = (await this.cacheService.get(
      verifyCodeDto.phoneNumber,
    )) as {
      sessionInfo: string;
    };

    if (!verificationDetails) {
      return;
    }

    return verificationDetails;

    // return await this.firebasePhoneAuthService.verifySmsCode(
    //   verifyCodeDto.phoneNumber,
    //   verifyCodeDto.code,
    //   verificationDetails.sessionInfo,
    // );
  }

  @OnEvent(ORDER_EVENT.PICKUP_REQUEST, { promisify: true })
  public async pickupRequest(pickupVerificationDto: any) {
    const { recaptcha, phoneNumber, orderUUID, tripUUID } =
      pickupVerificationDto;

    console.log(pickupVerificationDto);

    // const sessionId = await this.firebasePhoneAuthService.sendSmsCode(
    //   '+491775114823',
    //   recaptcha,
    // );

    // if (!sessionId) {
    //   return;
    // }

    // await this.cacheService.set(
    //   orderUUID,
    //   {
    //     tripUUID,
    //     orderUUID,
    //     phoneNumber,
    //     sessionInfo: sessionId,
    //   },
    //   300,
    // );

    await this.cacheService.set(
      orderUUID,
      {
        tripUUID,
        orderUUID,
        phoneNumber,
        code: '111111',
      },
      3000,
    );
  }

  @OnEvent(ORDER_EVENT.PICKUP_VERIFICATION, { promisify: true })
  public async pickupVerification(pickupVerificationDto: any) {
    const { orderUUID, code } = pickupVerificationDto;

    console.log(pickupVerificationDto);

    const verificationDetails = (await this.cacheService.get(orderUUID)) as {
      code: string;
    };

    if (!verificationDetails) {
      return;
    }

    return code === verificationDetails.code;

    // return await this.firebasePhoneAuthService.verifySmsCode();

    // return await this.firebasePhoneAuthService.verifySmsCode(
    //   orderUUID,
    //   code,
    //   verificationDetails.sessionInfo,
    // );
  }

  @OnEvent(ORDER_EVENT.DELIVERY_VERIFICATION, { promisify: true })
  public async deliveryVerification(verifyCodeDto: any) {
    // return await this.smsService.verifyCode(verifyCodeDto);
  }
}
