import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { PUSH_NOTIFICATION_EVENTS } from '../../common/events/events-constants';
// import { PushNotificationService } from '../services';
import { PushNotificationsEmitter } from '../emitters';
import {} from '../dtos';
import { NotificationType } from '../enums';

@Injectable()
export class PushNotificationListener {
  constructor(
    // private readonly pushNotificationService: PushNotificationService,
    private readonly pushNotificationsEmitter: PushNotificationsEmitter,
  ) {}

  @OnEvent(PUSH_NOTIFICATION_EVENTS.SEND_CHAT_NOTIFICATION, { promisify: true })
  public async sendChatNotification(sendNotificationDto: any) {
    const { to, from, message } = sendNotificationDto;

    const fromUserDetails = await this.pushNotificationsEmitter.getUserDetails(
      from,
    );

    // await this.pushNotificationService.sendMessageToUser(to, {
    //   title: fromUserDetails.firstName + ' ' + fromUserDetails.lastName,
    //   body: message,
    // });
  }

  @OnEvent(PUSH_NOTIFICATION_EVENTS.ORDER_REQUEST, { promisify: true })
  public async sendOrderRequest(sendNotificationDto: any) {
    const { to, from } = sendNotificationDto;

    console.log(sendNotificationDto);

    const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(
      to,
    );
    const fromUserDetails =
      await this.pushNotificationsEmitter.getUserDetailsById(from);

    // await this.pushNotificationService.sendMessageToUser(
    //   sendToUser.uuid,
    //   {
    //     title: 'Delivery Request (Order)',
    //     body: `from ${fromUserDetails.firstName} ${fromUserDetails.lastName}`,
    //   },
    //   {
    //     link: 'uget://orders/my',
    //     type: NotificationType.MY_ORDERS,
    //   },
    // );
  }

  @OnEvent(PUSH_NOTIFICATION_EVENTS.TRIP_REQUEST, { promisify: true })
  public async sendTripRequest(sendNotificationDto: any) {
    const { to, from } = sendNotificationDto;

    const fromUserDetails =
      await this.pushNotificationsEmitter.getUserDetailsById(from);

    const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(
      to,
    );

    // await this.pushNotificationService.sendMessageToUser(
    //   sendToUser.uuid,
    //   {
    //     title: 'Delivery Request (Trip)',
    //     body: `from ${fromUserDetails.firstName} ${fromUserDetails.lastName}`,
    //   },
    //   {
    //     link: 'uget://trips/my',
    //     type: NotificationType.MY_TRIPS,
    //   },
    // );
  }

  @OnEvent(PUSH_NOTIFICATION_EVENTS.ORDER_REQUEST_ACCEPTED, { promisify: true })
  public async sendOrderRequestAccepted(sendNotificationDto: any) {
    const { to, from } = sendNotificationDto;

    const fromUserDetails =
      await this.pushNotificationsEmitter.getUserDetailsById(from);

    const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(
      to,
    );

    // await this.pushNotificationService.sendMessageToUser(sendToUser.uuid, {
    //   title: 'Your request was Accepted',
    //   body: `by ${fromUserDetails.firstName} ${fromUserDetails.lastName}`,
    // });
  }

  @OnEvent(PUSH_NOTIFICATION_EVENTS.TRIP_REQUEST_ACCEPTED, { promisify: true })
  public async sendTripRequestAccepted(sendNotificationDto: any) {
    const { to, from } = sendNotificationDto;

    const fromUserDetails =
      await this.pushNotificationsEmitter.getUserDetailsById(from);

    const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(
      to,
    );

    // await this.pushNotificationService.sendMessageToUser(sendToUser.uuid, {
    //   title: 'Your request was Accepted',
    //   body: `by ${fromUserDetails.firstName} ${fromUserDetails.lastName}`,
    // });
  }

  @OnEvent(PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP_VERIFICATION, {
    promisify: true,
  })
  public async sendOrderPickUpVerification(sendNotificationDto: any) {
    const { to, from, orderUUID } = sendNotificationDto;

    const fromUserDetails =
      await this.pushNotificationsEmitter.getUserDetailsById(from);

    const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(
      to,
    );

    // await this.pushNotificationService.sendMessageToUser(sendToUser.uuid, {
    //   title: `Order pick up verification`,
    //   body: `by ${fromUserDetails.firstName} ${fromUserDetails.lastName}`,
    // });
  }

  @OnEvent(PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP, { promisify: true })
  public async sendOrderPickedUp(sendNotificationDto: any) {
    const { to, by, orderUUID } = sendNotificationDto;

    console.log(sendNotificationDto);

    const fromUserDetails =
      await this.pushNotificationsEmitter.getUserDetailsById(by);

    const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(
      to,
    );

    // await this.pushNotificationService.sendMessageToUser(sendToUser.uuid, {
    //   title: `Order was picked up`,
    //   body: `by ${fromUserDetails.firstName} ${fromUserDetails.lastName}`,
    // });
  }

  @OnEvent(PUSH_NOTIFICATION_EVENTS.DELIVERY_VERIFICATION, {
    promisify: true,
  })
  public async sendOrderDeliveryVerification(sendNotificationDto: any) {
    const { to, from, orderUUID } = sendNotificationDto;
    const fromUserDetails =
      await this.pushNotificationsEmitter.getUserDetailsById(from);
    const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(
      to,
    );

    // await this.pushNotificationService.sendMessageToUser(sendToUser.uuid, {
    //   title: `Order delivery verification`,
    //   body: `by ${fromUserDetails.firstName} ${fromUserDetails.lastName}`,
    // });
  }

  @OnEvent(PUSH_NOTIFICATION_EVENTS.ORDER_DELIVERED, { promisify: true })
  public async sendOrderDelivered(sendNotificationDto: any) {
    const { to, by, orderUUID } = sendNotificationDto;

    const fromUserDetails =
      await this.pushNotificationsEmitter.getUserDetailsById(by);

    const sendToUser = await this.pushNotificationsEmitter.getAuthRecordByID(
      to,
    );

    // await this.pushNotificationService.sendMessageToUser(sendToUser.uuid, {
    //   title: `Order delivered successfully`,
    //   body: `by ${fromUserDetails.firstName} ${fromUserDetails.lastName}`,
    // });
  }
}
