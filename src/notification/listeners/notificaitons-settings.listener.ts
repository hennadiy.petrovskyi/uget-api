import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { SETTINGS, USER_EVENTS } from '../../common/events/events-constants';
import { NotificationsSettingsService } from '../services';
import { SettingsDto } from '../dtos';
import {} from '../emitters';
import {} from '../enums';
import { AuthRecord } from 'src/auth/entities';

@Injectable()
export class NotificationsSettingsListener {
  constructor(
    private readonly notificationsSettingsService: NotificationsSettingsService,
  ) {}

  @OnEvent(USER_EVENTS.CREATE_USER_DETAILS, { promisify: true })
  public async createSettings(dto: { owner: AuthRecord }) {
    return await this.notificationsSettingsService.createUserSettings({
      owner: dto.owner,
    });
  }

  @OnEvent(SETTINGS.GET_USER_NOTIFICATION_SETTINGS, { promisify: true })
  public async getOwnerSettings(id: number) {
    return SettingsDto.buildDto(
      await this.notificationsSettingsService.getUserSettings(id as any),
    );
  }
}
