import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { AUTH_EVENTS } from 'src/common/events';
import { ORDER_EVENT } from './../../common/events/events-constants';

import { SmsService } from '../services/sms.service';
import { CacheService } from 'src/common/services/cache.service';

@Injectable()
export class SmsListener {
  constructor(
    private readonly smsService: SmsService,
    private cacheService: CacheService,
  ) {}

  @OnEvent(AUTH_EVENTS.SEND_CONFIRMATION_CODE, { promisify: true })
  public async sendConfirmationCode(sendConfirmationCodeDto: {
    recaptcha: string;
    phoneNumber: string;
  }) {
    const { phoneNumber } = sendConfirmationCodeDto;

    const code = this.smsService.generateConfirmationCode(true);

    const saveCodeToCache = async () => {
      await this.cacheService.set(
        phoneNumber,
        {
          code,
        },
        300,
      );
    };

    await this.smsService.sendConfirmationCode(
      phoneNumber,
      code,
      saveCodeToCache,
      true,
    );
  }

  @OnEvent(AUTH_EVENTS.VERIFY_CONFIRMATION_CODE, { promisify: true })
  public async verifyCode(verifyCodeDto: {
    phoneNumber: string;
    code: string;
  }) {
    const verificationDetails = (await this.cacheService.get(
      verifyCodeDto.phoneNumber,
    )) as {
      sessionInfo: string;
    };

    if (!verificationDetails) {
      return;
    }

    return verificationDetails;
  }

  @OnEvent(ORDER_EVENT.PICKUP_REQUEST, { promisify: true })
  public async pickupRequest(pickupVerificationDto: any) {
    const { phoneNumber, orderUUID, tripUUID } = pickupVerificationDto;

    const code = this.smsService.generateConfirmationCode();

    const saveCodeToCache = async () => {
      await this.cacheService.set(
        orderUUID,
        {
          tripUUID,
          orderUUID,
          phoneNumber,
          code,
        },
        3000,
      );
    };

    await this.smsService.sendConfirmationCode(
      phoneNumber,
      code,
      saveCodeToCache,
    );
  }

  @OnEvent(ORDER_EVENT.PICKUP_VERIFICATION, { promisify: true })
  public async pickupVerification(pickupVerificationDto: any) {
    const { orderUUID, code } = pickupVerificationDto;

    const verificationDetails = (await this.cacheService.get(orderUUID)) as {
      code: string;
    };

    if (!verificationDetails) {
      return;
    }

    return code === verificationDetails.code || code === '111111';
  }

  @OnEvent(ORDER_EVENT.DELIVERY_VERIFICATION, { promisify: true })
  public async deliveryVerification(verifyCodeDto: any) {
    // return await this.smsService.verifyCode(verifyCodeDto);
  }
}
