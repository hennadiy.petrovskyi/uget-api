import { LoggerModule } from '../utils/logger';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MiddlewareConsumer, Module } from '@nestjs/common';
// import { FirebaseAdminModule } from '@tfarras/nestjs-firebase-admin';
// import * as firebaseAdmin from 'firebase-admin';

import { CacheRedisModule } from '../common/modules/cache.module';
import {
  SmsService,
  // FirebasePhoneAuthService,
  // PushNotificationService,
  EmailService,
} from './services';
import {
  EmailListener,
  NotificationsSettingsListener,
  PushNotificationListener,
  SmsListener,
} from './listeners';
import {
  EmailController,
  NotificationsSettingsController,
  PushNotificationController,
  RecaptchaController,
} from './controllers';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Token, UserNotificationSettings } from './entities';
import { MorganMiddleware } from 'src/common/middlewares/morgan.middleware';
import { PushNotificationsEmitter } from './emitters';
import { NotificationsSettingsService } from './services/notification-settings.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Token, UserNotificationSettings]),
    CacheRedisModule,
    LoggerModule,
    // FirebaseAdminModule.forRootAsync({
    //   useFactory: (configService: ConfigService) => {
    //     return {
    //       credential: firebaseAdmin.credential.cert({
    //         projectId: configService.get('FIREBASE_PROJECT_ID'),
    //         privateKey:
    //           '-----BEGIN PRIVATE KEY-----@MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDVw+094ki0Jhjo\nliQMvC/IrW9olRtN1/utpaYcMdtmYV8CihSZlE5bnxiUpwaLY9sQNn5vXcvdgxIT\nlouXiNEcPzaZwLmnUAwHhiiGNh7GPxq7boq8bYHKY+4aYD8IyMNdOORkySMEmVI6\njIIMg3awqiB7uKGO8iPP769laiEUB2qII4R7He2ExNfQx29v7XqU3WLFt2PqXDia\nDmPU/48Qy7q17cmH6hpxK6tC59GvABmVkKJdBdcd72fLWVHxRA3TroDrp2WfBkAk\ncw+J+50SEHXcl0R352IqAM5oQhKOtSkMS5ggiEk7zdpfiGXRgfB/V5015J2dY84c\nZ5d/Hh9/AgMBAAECggEAIuhBv49KLBDklGagMGwyqWSZAs0GjmmssaiRPZ1upINW\nDoYZ4xJy0UdjrjKwQObniPg9IVaBygRzWTuGRBKVgzVXZzgvmQDbVgc0auY/lEQN\ny3m7shV30mMLKMd6w6Z1MK3vrf2ieEfJQ8JaIkz9fs/pSgrWZw9nhbcwPzFbgXeA\nB90TFxhBz2U32oztB9S1d2octwZGew0zi2fhv6xQigUfdEIIPoD6LWmic0jAXp+B\nIoijeoBRiFoSsEEu6g0Xb1EqmbcPQW2f5MQw9n3q76QgCk4zvsqG9nneFJlDMX9y\nbjaBQTfg4QrxJNmkcnzSL2aclyXb277A75yvnFaSAQKBgQD3/I/+Gw/rCHVcC109\nFMJ02o8TOoN745n5QwJquE358PK15Ez0oa7TAdsOZipFd/wjFCh1aXUEUArI4LTs\nwoQbiMjThlt0hslKZyv6wrw+MqxcTXjbacmZvZaUnfW4AsT+gVWBgwO0Mg5MKZgc\n9jVAGgYMbUbo2N/zLNFmifY3/wKBgQDcrEX/soBUTYtyZlXHRMjSDlNg+o3cOcP0\nmNez2SPSsD3VX/gVIz+FSNrUhx+JEoPx3SwiWedR83VExnofiy3ySGUhDi9z7fkW\nyduitTCyahsmLc2nFPN/M7PvrCx/CBJTpZYtxvgqO9e2BRLXzdNlCkHTbsJH48nt\nQBzuezQYgQKBgQCiWk2DFQCU5VLAUghW5vJwQm6NLi/KujWiBQbeC4+xZNfh0Xe4\naOnl5z8c7DIcjPMbnCToyYP8sNJ+tO5+WtLWm/Su1zWk0mGSPn2CjF9lcRN60pWf\nQGDmMEMXRVsLf3StEsuCIN4Z6POHVWvq+h3eu6reE+TqqKHluJ/XO9dgzQKBgQCf\nFnd0aqyHmkvw0oP+n8EjZn6HyLoizWZO3S2Mk/t2deZuALzqCDpQDYDtfOioOjfz\nUx7xON1CQIEHbws12HHsXHLDzOU675IG8smr2HJd/Y7LDHRZgwo0ZwLOw9JkVl5X\n/WVyZSId2Hyf1bKK7/D5w9ZG5m+bwkWeC/2PlDSvgQKBgEtPdvlsFvXdoKP5BRoi\nmPmJbpS+kX4eOh0oyEEfe2J/P+/t00WaJ6ykmWE3284ZyR90/lU/5HRVHCs0OXyp\nnGH3UfCHkksTiSzw1UNIMc9Oliu2v6IE7Dynt7TdFkiwpecGBrhKS52pFe3rVXCU\nTmbdn2HAEYTSe3R7XG3yQAor@-----END PRIVATE KEY-----@'.replace(
    //             /\@/g,
    //             '\n',
    //           ),
    //         clientEmail: configService.get('FIREBASE_CLIENT_EMAIL'),
    //       }),
    //     };
    //   },
    //   inject: [ConfigService],
    // }),
  ],

  controllers: [
    RecaptchaController,
    PushNotificationController,
    NotificationsSettingsController,
    EmailController,
  ],
  providers: [
    SmsService,
    SmsListener,
    NotificationsSettingsService,
    // FirebasePhoneAuthService,
    EmailService,
    // PushNotificationService,
    PushNotificationsEmitter,
    PushNotificationListener,
    NotificationsSettingsListener,
    EmailListener,
  ],
  exports: [],
})
export class NotificationModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes(PushNotificationController);
  }
}
