import { Body, Controller, Patch, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { User } from 'src/common/decorators';

import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { SettingsDto } from '../dtos';
import { PushNotificationsEmitter } from '../emitters';
import { NotificationsSettingsService } from '../services';

@ApiTags('settings')
@Controller('settings')
export class NotificationsSettingsController {
  constructor(
    private readonly service: NotificationsSettingsService,
    private readonly emitter: PushNotificationsEmitter,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Patch()
  public async registerDeviceToken(
    @User() user,
    @Body() updateSettingsDto: any,
  ) {
    const settings = await this.service.getUserSettings(user.id);

    await this.service.updateUserSettings(settings.id, updateSettingsDto);

    return updateSettingsDto;
  }
}
