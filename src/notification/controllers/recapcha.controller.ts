import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  Sse,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { User } from 'src/common/decorators';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';

// import { FirebasePhoneAuthService } from '../services';

@ApiBearerAuth()
@ApiTags('Recaptcha')
@Controller('recaptcha')
export class RecaptchaController {
  constructor() {}

  @Post()
  async saveProviderDeliveryPrice(@Body('') dto: any) {
    console.log('dto', dto);

    // await this.service.sendSmsCode('+491775114823', dto.token);
  }

  @Post('test')
  async test(@Body('') dto: any) {
    // await this.service.verifySmsCode(dto.code, dto.phoneNumber);
  }
}
