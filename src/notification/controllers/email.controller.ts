import { Body, Controller, Get, Patch, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { User } from 'src/common/decorators';

import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { SettingsDto } from '../dtos';
import { PushNotificationsEmitter } from '../emitters';
import { EmailService } from '../services';

@ApiTags('email')
@Controller('email')
export class EmailController {
  constructor(private readonly service: EmailService) {}

  @Get()
  public async registerDeviceToken() {
    // await this.service.sendEmail();
  }
}
