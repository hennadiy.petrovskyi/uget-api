import { Request } from 'express';
import {
  Body,
  Controller,
  Delete,
  Get,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { User } from 'src/common/decorators';
import { GuestGuard } from 'src/common/guards/guest.guard';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { CreateTokenDto, RefreshDeviceTokenDto } from '../dtos';
// import { PushNotificationService } from '../services';

@ApiTags('token')
@Controller('device-token')
export class PushNotificationController {
  constructor() {} // private readonly pushNotificationService: PushNotificationService,

  @UseGuards(GuestGuard)
  @Post()
  public async registerDeviceToken(
    // @User() user,
    @Req() request,
    @Body() createTokenDto: CreateTokenDto,
  ) {
    const user = request.user || null;

    // return await this.pushNotificationService.registerDeviceToken(
    //   user,
    //   createTokenDto,
    // );
  }

  /**
   *
   * @param refreshDeviceTokenDto
   * @returns
   * Firebase can invalidate user token when user is using app
   * In this case we swill update existing token to new one
   * Else cases will register new token
   */

  @UseGuards(JwtAuthGuard)
  @Patch()
  public async refreshDeviceToken(
    @Body('refresh') refreshDeviceTokenDto: RefreshDeviceTokenDto,
  ) {
    console.log(`new refresh token : ${RefreshDeviceTokenDto}`);

    return await this.refreshDeviceToken(refreshDeviceTokenDto);
  }

  // @Get('test')
  // public async test() {
  //   await this.pushNotificationService.sendMessageToUser(
  //     '1651c943-3137-4c0f-be6e-cc22a510a0f9',
  //     { title: 'new message', body: 'awdawdawdawdawd' },
  //   );
  // }
}
