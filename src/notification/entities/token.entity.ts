import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';

import { DeviceTokenPlatformType } from '../enums';
import { Expose } from 'class-transformer';
import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from 'src/auth/entities';
import { ApiProperty } from '@nestjs/swagger';

@Entity({
  name: 'device_token',
})
// @Unique(['deviceToken'])
export class Token extends BaseEntity {
  @Expose()
  @Column('text', { nullable: true })
  @Index()
  userUUID: string;

  @Expose()
  @Column('text')
  @Index()
  deviceToken: string;

  @Expose()
  @Column('enum', { enum: DeviceTokenPlatformType })
  type: DeviceTokenPlatformType;
}
