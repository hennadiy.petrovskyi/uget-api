import { Column, Entity, Index, JoinColumn, ManyToOne, Unique } from 'typeorm';

import { Expose } from 'class-transformer';
import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from 'src/auth/entities';
import { ApiProperty } from '@nestjs/swagger';

@Entity({
  name: 'user_notifications_settings',
})
export class UserNotificationSettings extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @Expose()
  @Column('boolean', { default: true })
  @Index()
  email: boolean;

  @Expose()
  @Column('boolean', { default: true })
  @Index()
  push: boolean;
}
