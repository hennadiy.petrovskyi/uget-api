import { response } from 'express';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
// import { SNS, SES } from 'aws-sdk';
// import { PublishResponse, PublishInput } from 'aws-sdk/clients/sns';

import { SNSClient, AddPermissionCommand } from '@aws-sdk/client-sns';

import { randomBytes } from 'crypto';
import { CacheService } from 'src/common/services/cache.service';
import { AuthVerifyCodeDto } from 'src/auth/dtos';
import { LoggerService } from 'src/utils/logger';
import { ConfigService } from '@nestjs/config';

/**
 * @export
 * @class AwsSnsService
 */
@Injectable()
export class SmsService {
  private readonly sns;

  constructor(
    private cacheService: CacheService,
    private configService: ConfigService,
    private logger: LoggerService,
  ) {
    this.sns = new SNSClient({
      region: this.configService.get<string>('REGION'),
      credentials: {
        accessKeyId: this.configService.get<string>('ACCESS_KEY_ID'),
        secretAccessKey: this.configService.get<string>('SECRET_ACCESS_KEY'),
      },
    });
  }

  public generateConfirmationCode(isTest?: boolean): string {
    if (!isTest) {
      const chars = '0123456789';
      const sequenceLength = 6;
      let sequence = '';
      const rnd = randomBytes(sequenceLength);
      const d = 256 / chars.length;

      for (let i = 0; i < sequenceLength; i++) {
        sequence += chars[Math.floor(rnd[i] / d)];
      }

      return sequence;
    }

    return '111111';
  }

  async sendSMS(smsOptions) {
    return this.sns
      .publish(smsOptions)
      .promise()
      .then((info) => {
        this.logger.log(`[SMS] success: ${JSON.stringify(info)}`);
        return [
          {
            statusCode: HttpStatus.OK,
            message: 'Sms sent',
            data: info,
          },
        ];
      })
      .catch((err) => {
        this.logger.error('[SMS] failed:', err);
        throw new HttpException(
          {
            statusCode: HttpStatus.BAD_REQUEST,
            message: 'Failed to send',
            data: err,
          },
          HttpStatus.BAD_REQUEST,
        );
      });
  }

  async sendConfirmationCode(
    phoneNumber: string,
    code: string,
    onSuccess: () => void,
    isTesting?: boolean,
  ) {
    const smsParams = {
      Message: `UGET code: ${code}`,
      PhoneNumber: phoneNumber,
    };

    if (!isTesting) {
      try {
        const result = await this.sendSMS(smsParams);

        if (result.length && result[0].statusCode === 200) {
          onSuccess();
        }
      } catch (error) {
        console.log(error);
      }

      return;
    }

    onSuccess();
    console.log(smsParams);
  }

  async verifyCode(authVerifyCodeDto: AuthVerifyCodeDto) {
    return await this.cacheService.get(authVerifyCodeDto.phoneNumber);
  }
}
