import { Inject, Injectable } from '@nestjs/common';
import { UserNotificationSettings } from '../entities';
import { FindOperator, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import {} from '../enums';
import {} from '../dtos';
import { LoggerService } from 'src/utils/logger';

@Injectable()
export class NotificationsSettingsService {
  constructor(
    @InjectRepository(UserNotificationSettings)
    private userNotificationRepository: Repository<UserNotificationSettings>,
    private logger: LoggerService,
  ) {}

  public async createUserSettings(createUserSettings: any) {
    const functionName = this.createUserSettings.name;

    try {
      await this.userNotificationRepository.save(createUserSettings);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getUserSettings(owner: FindOperator<any>) {
    const functionName = this.getUserSettings.name;

    try {
      return await this.userNotificationRepository.findOne({
        where: { owner },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async updateUserSettings(id, update: any) {
    const functionName = this.getUserSettings.name;

    try {
      return await this.userNotificationRepository.update(id, update);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
