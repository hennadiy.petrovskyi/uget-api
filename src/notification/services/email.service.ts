import * as nodemailer from 'nodemailer';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';

import { LoggerService } from 'src/utils/logger';
// import { ConfigService } from '@nestjs/config';
import { EmailType } from '../enums';
import { verifyEmailTemplate } from '../templates/verify-email';
import { resetPasswordTemplate } from '../templates/reset-password';

/**
 * @export
 * @class AwsSesService
 */
@Injectable()
export class EmailService {
  constructor(private logger: LoggerService) {}

  async sendEmail(sendEmailDto: {
    email: string;
    link: string;
    type: EmailType;
    title: string;
  }) {
    const credentials = {
      name: 'ses-smtp-user.20220304-151424',
      accessKeyId: 'AKIA22H73NCMNKGCA44L',
      secretAccessKey: 'BPeOHt9gIFvXnvf1J3vo6SEcX1k7U8DmgiM+aG0IyhBK',
    };

    const params = {
      from: 'no-reply@api.uget.co',
      port: 465,
      host: 'email-smtp.eu-central-1.amazonaws.com',
    };

    const mailerClient: nodemailer.Transporter = nodemailer.createTransport({
      host: params.host,
      port: params.port,
      auth: {
        user: credentials.accessKeyId,
        pass: credentials.secretAccessKey,
      },
    });

    console.log('sendEmailDto.link', sendEmailDto.link);

    return mailerClient.sendMail({
      from: params.from,
      to: sendEmailDto.email,
      subject: sendEmailDto.title,
      text: sendEmailDto.title,
      html:
        sendEmailDto.type === EmailType.VERIFY_EMAIL
          ? verifyEmailTemplate(sendEmailDto.link)
          : resetPasswordTemplate(sendEmailDto.link),
    });
  }
}
