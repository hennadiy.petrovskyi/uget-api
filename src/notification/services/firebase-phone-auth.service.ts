// import { Injectable } from '@nestjs/common';
// import { google, identitytoolkit_v3 } from 'googleapis';

// import {} from 'src/auth/dtos';
// import { LoggerService } from 'src/utils/logger';
// import { CacheService } from 'src/common/services/cache.service';

// @Injectable()
// export class FirebasePhoneAuthService {
//   private readonly identityToolkit: identitytoolkit_v3.Identitytoolkit;

//   constructor(
//     private cacheService: CacheService,
//     private logger: LoggerService,
//   ) {
//     this.identityToolkit = google.identitytoolkit({
//       auth: 'AIzaSyDVgn5hS0P5Ifu026AkSAY_i66Z-bn3SIM',
//       version: 'v3',
//     });
//   }

//   async sendSmsCode(phoneNumber: string, recaptchaToken) {
//     try {
//       const response =
//         await this.identityToolkit.relyingparty.sendVerificationCode({
//           requestBody: {
//             phoneNumber,
//             recaptchaToken,
//           },
//         });

//       return response.data.sessionInfo;
//     } catch (error) {
//       console.log(error);
//     }
//   }

//   async verifySmsCode(key, verificationCode, sessionInfo) {
//     const resp = await this.identityToolkit.relyingparty.verifyPhoneNumber({
//       requestBody: {
//         code: verificationCode,
//         sessionInfo,
//       },
//     });

//     await this.cacheService.delete(key);

//     if (resp.status === 200) {
//       return true;
//     }
//   }
// }
