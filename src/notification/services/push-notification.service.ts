// import { Inject, Injectable } from '@nestjs/common';
// import { Token } from '../entities';
// import { Repository } from 'typeorm';
// import { InjectRepository } from '@nestjs/typeorm';

// import {
//   FIREBASE_ADMIN_INJECT,
//   FirebaseAdminSDK,
// } from '@tfarras/nestjs-firebase-admin';

// import { PushNotificationsEmitter } from '../emitters/push-notification.emitter';
// import { DeviceTokenPlatformType, NotificationType } from '../enums';
// import { CreateTokenDto, PushNotificationMessageDto } from '../dtos';

// @Injectable()
// export class PushNotificationService {
//   constructor(
//     @InjectRepository(Token)
//     private tokenRepository: Repository<Token>,
//     private pushNotificationsEmitter: PushNotificationsEmitter,
//     @Inject(FIREBASE_ADMIN_INJECT) private firebaseAdmin: FirebaseAdminSDK,
//   ) {}

//   async registerDeviceToken(userUUID: string, createTokenDto: CreateTokenDto) {
//     const functionName = this.registerDeviceToken.name;

//     try {
//       const existingTokenDetails = await this.tokenRepository.findOne({
//         where: { deviceToken: createTokenDto.deviceToken },
//       });

//       if (!existingTokenDetails) {
//         return await this.tokenRepository.save({
//           userUUID,
//           ...createTokenDto,
//         });
//       }

//       if (existingTokenDetails && !existingTokenDetails.userUUID) {
//         return await this.tokenRepository.save({
//           id: existingTokenDetails.id,
//           userUUID,
//           ...createTokenDto,
//         });
//       }

//       return;
//     } catch (error) {
//       console.log(error);
//     }
//   }

//   /**
//    * This method is used app to update device-token for push-notifications
//    * this will happen only in case if device in use and Firebase will update it by itself.
//    * Else cases - token will be registered
//    *
//    * @param oldToken
//    * @param newToken
//    * @returns
//    */
//   async refreshToken(oldToken: string, newToken: string) {
//     const functionName = this.refreshToken.name;
//     try {
//       const deviceTokenDetails = await this.tokenRepository.findOneOrFail({
//         where: { deviceToken: oldToken },
//       });

//       return await this.tokenRepository.save({
//         ...deviceTokenDetails,
//         deviceToken: newToken,
//       });
//     } catch (error) {
//       return { status: false, message: 'failed to refresh fcm token' };
//     }
//   }

//   async sendMessageToUser(
//     userUUID: string,
//     message: PushNotificationMessageDto,
//     data?: { link?: string; type?: NotificationType },
//   ) {
//     const userDevicesTokens = await this.getAllUserTokens(userUUID);

//     const user = await this.pushNotificationsEmitter.getAuthRecordByUUID(
//       userUUID,
//     );

//     console.log(user);

//     await this.firebaseAdmin.messaging().sendToDevice(
//       userDevicesTokens,
//       data
//         ? {
//             data,
//             notification: {
//               ...message,
//             },
//           }
//         : {
//             notification: {
//               ...message,
//             },
//           },
//       { contentAvailable: true, mutableContent: true, priority: 'high' },
//     );
//   }

//   async getAllUserTokens(userUUID: string): Promise<string[]> {
//     try {
//       const tokens = await this.tokenRepository.find({ where: { userUUID } });

//       return tokens.map((token) => token.deviceToken);
//     } catch (error) {
//       console.log(error);
//     }
//   }
// }
