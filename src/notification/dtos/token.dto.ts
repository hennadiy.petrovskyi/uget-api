import { Token } from '../entities';
import { PickType } from '@nestjs/swagger';

export class CreateTokenDto extends PickType(Token, ['deviceToken', 'type']) {}
