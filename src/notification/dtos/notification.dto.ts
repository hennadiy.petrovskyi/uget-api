import { Expose } from 'class-transformer';

export class SendNotificationDto {
  @Expose()
  userId: number;

  @Expose()
  message: string;
}

export class RefreshDeviceTokenDto {
  @Expose()
  oldToken: string;

  @Expose()
  newToken: string;
}

export class PushNotificationMessageDto {
  @Expose()
  title: string;

  @Expose()
  body: string;
}

export class SendPushNotificationDto {
  @Expose()
  userUUID: string;

  @Expose()
  message: PushNotificationMessageDto;

  @Expose()
  link?: string;
}
