import { UserNotificationSettings } from '../entities';
import { PickType, PartialType } from '@nestjs/swagger';
import { plainToClass } from 'class-transformer';

export class SettingsDto extends PickType(UserNotificationSettings, [
  'email',
  'push',
]) {
  static buildDto(payload: any): SettingsDto {
    return plainToClass(SettingsDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class UpdateUserSettingsDto extends PartialType(SettingsDto) {}
