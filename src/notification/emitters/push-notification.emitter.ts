import { AUTH_EVENTS } from 'src/common/events';
import { USER_EVENTS } from '../../common/events/events-constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

@Injectable()
export class PushNotificationsEmitter {
  constructor(private eventEmitter: EventEmitter2) {}

  public async getAuthRecordByUUID(uuid: string) {
    const details = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_UUID,
      uuid,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async getAuthRecordByID(id: number) {
    const details = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_ID,
      id,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async getUserDetailsById(owner: number) {
    const details = await this.eventEmitter.emitAsync(
      USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID,
      owner,
    );

    if (details && details.length) {
      return details[0];
    }

    return details;
  }

  public async getUserDetails(uuid: string) {
    const authRecord = await this.getAuthRecordByUUID(uuid);

    return this.getUserDetailsById(authRecord.id);
  }
}
