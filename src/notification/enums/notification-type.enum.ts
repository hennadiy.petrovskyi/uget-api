export enum NotificationType {
  MY_ORDERS = 'order.my',
  ORDER_RELATED_TRIPS = 'order.related.trips',
  ORDERS_SEARCH_TRIPS = 'order.search.trips',
  MY_TRIPS = 'trip.my',
  TRIP_RELATED_ORDERS = 'trip.related.orders',
  TRIP_SEARCH_ORDERS = 'trip.search.orders',
}
