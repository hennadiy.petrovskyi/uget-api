export enum EmailType {
  RESET_PASSWORD = 'password.reset',
  VERIFY_EMAIL = 'email.verify',
}
