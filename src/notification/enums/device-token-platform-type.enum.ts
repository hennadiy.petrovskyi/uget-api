export enum DeviceTokenPlatformType {
  IOS = 'APN',
  ANDROID = 'GCM',
}
