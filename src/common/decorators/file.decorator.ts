import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { ApiProperty, ApiPropertyOptions } from '@nestjs/swagger';

export const ApiFile =
  (options?: ApiPropertyOptions): PropertyDecorator =>
  (target: Record<string, unknown>, propertyKey: string | symbol) => {
    if (options?.isArray) {
      ApiProperty({
        type: 'array',
        items: {
          type: 'file',
          properties: {
            [propertyKey]: {
              type: 'string',
              format: 'binary',
            },
          },
        },
      })(target, propertyKey);
    } else {
      ApiProperty({
        type: 'file',
        properties: {
          [propertyKey]: {
            type: 'string',
            format: 'binary',
          },
        },
      })(target, propertyKey);
    }
  };

@Injectable()
export class MultipleFilesToBodyInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();
    const req = ctx.getRequest();

    console.log(req.body, req.file);
    // if (req.body && req.file?.fieldname) {
    //   // const { fieldname } = req.file;
    //   // if (!req.body[fieldname]) {
    //   //   req.body[fieldname] = req.file;
    //   // }
    // }

    // if (req.body && req.files) {
    //   if (Array.isArray(req.files)) {
    //     req.files.forEach((file: Express.Multer.File) => {
    //       const { fieldname } = file;
    //       if (!req.body[fieldname]) {
    //         req.body[fieldname] = [file];
    //       } else {
    //         req.body[fieldname].push(file);
    //       }
    //     });
    //   } else if (Array.isArray(Object.keys(req.files))) {
    //     Object.keys(req.files).forEach((key) => {
    //       req.body[key] = req.files[key];
    //     });
    //   }
    // }

    return next.handle();
  }
}

@Injectable()
export class FileToBodyInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();
    const req = ctx.getRequest();

    if (req.body && req.file?.fieldname) {
      const { fieldname } = req.file;
      if (!req.body[fieldname]) {
        req.body[fieldname] = req.file;
      }
    }

    return next.handle();
  }
}
