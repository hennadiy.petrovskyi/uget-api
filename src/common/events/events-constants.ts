export const AUTH_EVENTS = {
  SEND_CONFIRMATION_CODE: 'auth.send.confirmation-code',
  VERIFY_CONFIRMATION_CODE: 'auth.verify.confirmation-code',
  GET_USER_AUTH_SESSIONS: 'auth.get.user-sessions',
  GET_USER_BY_UUID: 'auth.get.by-uuid.user',
  GET_USER_BY_ID: 'auth.get.record.by-id',
};

export const FILE_EVENTS = {
  UPLOAD_FILE: 'file.upload',
  GET_FILE: 'file.get',
  GET_FILE_BY_TYPE: 'file.get.type',
  DELETE_FILE: 'file.delete',
  GET_FILES: 'files.get.all',
  DELETE_OWNER_FILES: 'files.delete.all',
};

export const USER_EVENTS = {
  GET_USER_DETAILS_BY_OWNER_ID: 'user.get.details.by-id',
  GET_USER_DETAILS_BY_OWNER_UUID: 'user.get.details.by-UUID',
  UPDATE_USER_DETAILS: 'user.update.details',
  CREATE_USER_DETAILS: 'user.create.details',
  GET_AUTH_CREDENTIALS: 'user.get.auth-credentials',
  ADD_NEW_UNRATED_RATE: 'user.add.rating',
  SET_RATING_IS_RATED: 'user.update.rating',
};

export const LOCATION_EVENT = {
  GET_LOCATION_DETAILS_BY_ID: 'locations.get',
  GET_COUNTRY_CODE_BY_LOCATION_ID: 'location.get.country-code',
  SAVE_LOCATION_IN_DB: 'location.save',
};

export const PHONE_NUMBER_EVENT = {
  GET_PHONE_NUMBER: 'phone.get',
  SAVE_PHONE_NUMBER: 'phone.create',
  DELETE_PHONE_NUMBER: 'phone.delete',
  UPDATE_PHONE_NUMBER: 'phone.update',
};

export const ORDER_EVENT = {
  GET_USER_ORDERS: 'order.get.all',
  ADD_OUTGOING_REQUEST: 'order.add.request.outgoing',
  ADD_INCOMING_REQUEST: 'order.add.request.incoming',
  ACCEPT_DELIVERY_BY_TRAVELER: 'order.traveler.accept.delivery',
  CANCEL_OUTGOING_REQUEST: 'order.cancel.request.outgoing',
  CANCEL_INCOMING_REQUEST: 'order.cancel.request.incoming',
  CANCEL_ACCEPTED_REQUEST: 'order.cancel.accepted',
  PICKUP_REQUEST: 'order.pickup.request',
  PICKUP_VERIFICATION: 'order.pickup.verification',
  DELIVERY_REQUEST: 'order.delivery.request',
  DELIVERY_VERIFICATION: 'order.delivery.verification',
  START_DELIVERY: 'order.delivery.start',
  FINISH_DELIVERY: 'order.delivery.finish',
  GET_DELIVERY_PRICE: 'order.get.delivery-price',
  GET_EARNINGS: 'order.get.earnings',
};

export const TRIP_EVENT = {
  GET_USER_TRIPS: 'trip.get.all',
  ADD_INCOMING_REQUEST: 'trip.add.request.incoming',
  CANCEL_OUTGOING_REQUEST: 'order.cancel.request.outgoing',
  CANCEL_INCOMING_REQUEST: 'order.cancel.request.incoming',
  CANCEL_ACCEPTED_REQUEST: 'order.cancel.accepted',
  ACCEPT_DELIVERY_BY_ORDER_OWNER: 'trip.owner.accept.delivery',
};

export const PUSH_NOTIFICATION_EVENTS = {
  SEND_CHAT_NOTIFICATION: 'push-notification.send.chat',
  TRIP_REQUEST: 'push-notification.trip.request',
  ORDER_REQUEST: 'push-notification.order.request',
  ORDER_REQUEST_ACCEPTED: 'push-notification.order.request.accepted',
  TRIP_REQUEST_ACCEPTED: 'push-notification.trip.request.accepted',
  ORDER_PICK_UP_VERIFICATION: 'push-notification.order.verify.picked-up',
  ORDER_PICK_UP: 'push-notification.order.picked-up',
  DELIVERY_VERIFICATION: 'push-notification.verify.delivery',
  ORDER_DELIVERED: 'push-notification.order.delivered',
};

export const SETTINGS = {
  GET_USER_NOTIFICATION_SETTINGS: 'settings.notification.get',
};

export const PRICING_EVENTS = {
  CALCULATE_ORIGINAL_AVERAGE_PRICE: 'CALCULATE_ORIGINAL_AVERAGE_PRICE',
};

export const CHAT_EVENTS = {
  GET_USER_CONVERSATIONS: 'GET_USER_CONVERSATIONS',
};

export const EMAIL_EVENTS = {
  SEND_VERIFICATION: 'email.send.verification',
  SEND_RESET_PASSWORD: 'email.send.reset-password',
};

export const SOCKET_EVENTS = {
  SEND_MESSAGE: 'socket.send.message',
};
