import * as redisStore from 'cache-manager-redis-store';
import { CacheModule, Module } from '@nestjs/common';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { CacheService } from '../services/cache.service';
import { LoggerModule, LoggerService } from '../../utils/logger';

@Module({
  imports: [
    CacheModule.registerAsync({
      imports: [ConfigModule, LoggerModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        store: redisStore,
        host: configService.get('REDIS_HOST'),
        port: configService.get('REDIS_PORT'),
        ttl: 300,
      }),
    }),
  ],
  providers: [CacheService, LoggerService],
  exports: [CacheService],
})
export class CacheRedisModule {}
