import { Injectable, NestMiddleware } from '@nestjs/common';
import * as morgan from 'morgan';

function stringify(
  val: any,
  depth?: number,
  replacer?: (k: string, v: any) => any,
  space?: number,
): string {
  depth = isNaN(+depth) ? 1 : depth;

  function _build(key: string, val: any, depth?: number, o?: any) {
    // (JSON.stringify() has it's own rules, which we respect here by using it for property iteration)
    let a;
    return !val || typeof val != 'object'
      ? val
      : ((a = Array.isArray(val)),
        JSON.stringify(val, function (k, v) {
          if (a || depth > 0) {
            if (replacer) v = replacer(k, v);
            if (!k) return (a = Array.isArray(v)), (val = v);
            !o && (o = a ? [] : {});
            o[k] = _build(k, v, a ? depth : depth - 1);
          }
        }),
        o || (a ? [] : {}));
  }

  return JSON.stringify(_build('', val, depth), null, space);
}

@Injectable()
export class MorganMiddleware implements NestMiddleware {
  public static IS_EXTENDED = false;
  private static format = 'tiny';
  private static extendedFormat =
    ':method :url :status :response-time ms - :res[content-length] :body';

  public static token(name: string, callback: any): morgan.Morgan {
    return morgan.token(name, callback);
  }

  public use(req: any, res: any, next: any) {
    if (MorganMiddleware.IS_EXTENDED) {
      morgan.token(
        'body',
        (req) =>
          '\n' +
          '\n' +
          'Request Body :\n' +
          stringify(req.body, 1, null, 2) +
          '\n-------------------------\n',
      );
      morgan(MorganMiddleware.extendedFormat)(req, res, next);
    } else {
      morgan(MorganMiddleware.format)(req, res, next);
    }
  }
}
