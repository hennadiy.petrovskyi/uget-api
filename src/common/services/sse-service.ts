import { Injectable } from '@nestjs/common';
import { fromEvent } from 'rxjs';
import { EventEmitter } from 'events';

@Injectable()
export class SseService {
  private readonly emitter = new EventEmitter();

  subscribe(eventId: string | number) {
    return fromEvent(this.emitter, `${eventId}`);
  }

  async emit(data) {
    this.emitter.emit('12345', { data });
  }
}
