import { CACHE_MANAGER, Inject } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { LoggerService } from '../../utils/logger';

export class CacheService {
  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private logger: LoggerService,
  ) {}

  public async set(key: string, payload: any, ttl: number) {
    try {
      await this.cacheManager.set(key, payload, { ttl });

      return await this.cacheManager.get(key);
    } catch (error) {
      this.logger.error(`Failed to set key ==> ${key}`);
      this.logger.error(error);
    }
  }

  public async get(key: string) {
    try {
      return await this.cacheManager.get(key);
    } catch (error) {
      this.logger.error(`Failed to get key ==> ${key}`);
      this.logger.error(error);
    }
  }

  public async delete(key: string) {
    try {
      return await this.cacheManager.del(key);
    } catch (error) {
      this.logger.error(`Failed to delete key ==> ${key}`);
      this.logger.error(error);
    }
  }
}
