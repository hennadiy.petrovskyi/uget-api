import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { AuthService, CryptoService } from '../../auth/services';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private moduleRef: ModuleRef) {
    super();
  }

  canActivate(context: ExecutionContext) {
    const authService = this.moduleRef.get(AuthService, { strict: false });
    const cryptoService = this.moduleRef.get(CryptoService, { strict: false });

    const request = context.switchToHttp().getRequest();
    const token = request.headers['authorization'];

    if (!token) {
      return false;
    }

    const payload = cryptoService.decodeToken(token);

    return authService.findAuthRecordByUUID(payload.sub).then((authRecord) => {
      if (!authRecord) {
        return false;
      }

      return cryptoService.findUserSessions(payload.sub).then((sessions) => {
        if (!sessions.length) {
          return false;
        }

        const session = sessions.find(
          (session) => session.accessToken === token,
        );

        if (session) {
          return super.canActivate(context);
        }
      });
    }) as Promise<boolean>;
  }

  handleRequest(err, user, info) {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }

    return user;
  }
}
