import { Module } from '@nestjs/common';

import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { EventEmitterModule } from '@nestjs/event-emitter';
import { JoiConfigValidator } from './utils/config/config.validator';

import { AuthModule } from './auth/auth.module';
import { NotificationModule } from './notification/notification.module';
import { UserModule } from './user/user.module';
import { LocationModule } from './location/location.module';
import { FileModule } from './file/file.module';
import { OrderModule } from './order/order.module';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
import { TripModule } from './trips/trip.module';
import { PriceModule } from './pricing/pricing.module';
import { ChatModule } from './chat/chat.module';
import { SocketModule } from './socket/socket.module';
import { AndroidLinksVerificationController } from './verify.controller';
import { RedirectDeepLinkController } from './redirect.controller';

@Module({
  controllers: [
    HealthController,
    AndroidLinksVerificationController,
    RedirectDeepLinkController,
  ],
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.env.${process.env.NODE_ENV}`],
      isGlobal: true,
      cache: true,
      validationSchema: JoiConfigValidator,
      validationOptions: {
        allowUnknown: true,
        abortEarly: true,
      },
    }),

    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          autoLoadEntities: true,
          host: configService.get<string>('DB_HOST'),
          port: configService.get<number>('DB_PORT'),
          database: configService.get<string>('DB_NAME'),
          username: configService.get<string>('DB_USER'),
          password: configService.get<string>('DB_PASS'),
          synchronize: configService.get<boolean>('DB_SYNC'),
          logging: configService.get<boolean>('DB_LOGGING'),
        };
      },
    }),

    EventEmitterModule.forRoot({
      wildcard: false,
      delimiter: '.',
      newListener: false,
      removeListener: false,
      verboseMemoryLeak: false,
      ignoreErrors: false,
    }),

    TerminusModule,
    SocketModule,
    AuthModule,
    UserModule,
    NotificationModule,
    LocationModule,
    FileModule,
    OrderModule,
    TripModule,
    PriceModule,
    ChatModule,
  ],

  providers: [],
})
export class AppModule {}
