import { forwardRef, MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import {
  OrderService,
  StatusService,
  LocationService,
  DetailsService,
  ThirdPartyNumberService,
  OrderHelperService,
  DeliveryPriceService,
} from './services';
import { OrderController } from './controllers';
import {
  Details,
  Order,
  Status,
  OrderLocation,
  ThirdPartyPhoneNumber,
  DeliveryPrice,
} from './entities';
import { LoggerModule } from 'src/utils/logger';
import { OrderEmitter } from './emitters';
import { MorganMiddleware } from 'src/common/middlewares/morgan.middleware';
import { OrderListener } from './listeners';
import { CacheRedisModule } from 'src/common/modules/cache.module';
import { SseService } from 'src/common/services/sse-service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Order,
      Details,
      Status,
      OrderLocation,
      ThirdPartyPhoneNumber,
      DeliveryPrice,
    ]),
    HttpModule,
    LoggerModule,
    CacheRedisModule,
  ],
  providers: [
    OrderService,
    DeliveryPriceService,
    StatusService,
    LocationService,
    DetailsService,
    OrderEmitter,
    OrderListener,
    SseService,
    ThirdPartyNumberService,
    OrderHelperService,
  ],
  controllers: [OrderController],
  exports: [],
})
export class OrderModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes(OrderController);
  }
}
