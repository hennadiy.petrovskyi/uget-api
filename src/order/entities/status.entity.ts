import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';

import { BaseEntity } from 'src/common/entities';
import { OrderStatus } from '../enums';
import { Order } from '.';

@Entity({
  name: 'order_status',
})
export class Status extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => Order)
  @JoinColumn({ name: 'orderId', referencedColumnName: 'id' })
  owner: Order;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: OrderStatus })
  status: OrderStatus;
}
