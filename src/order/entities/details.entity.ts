import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';

import { ItemSize, OrderType } from '../enums';
import { BaseEntity } from 'src/common/entities';
import { Order } from '.';

@Entity({
  name: 'order_details',
})
export class Details extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => Order)
  @JoinColumn({ name: 'orderId', referencedColumnName: 'id' })
  owner: Order;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: OrderType })
  type: OrderType;

  @ApiProperty()
  @Expose()
  @Column('text')
  name: string;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: ItemSize })
  size: ItemSize;

  @ApiProperty()
  @Expose()
  @Column('int')
  quantity: number;

  @ApiProperty()
  @Expose()
  @Column('text')
  description: string;
}
