import { Currency } from './../../pricing/enums/currency.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';

import { BaseEntity } from 'src/common/entities';
import { OrderStatus } from '../enums';
import { Order } from '.';

@Entity({
  name: 'order_delivery_price',
})
export class DeliveryPrice extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => Order)
  @JoinColumn({ name: 'orderId', referencedColumnName: 'id' })
  owner: Order;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: Currency })
  currency: Currency;

  @ApiProperty()
  @Expose()
  @Column('text')
  average: string;

  @ApiProperty()
  @Expose()
  @Column('text', { default: null, nullable: true })
  routePriceModifier: string;

  @ApiProperty()
  @Expose()
  @Column('text')
  total: string;

  @ApiProperty()
  @Expose()
  @Column('text', { default: null, nullable: true })
  discountId: string;
}
