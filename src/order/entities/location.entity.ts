import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { BaseEntity } from 'src/common/entities';

import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Order } from '.';
import { OrderLocationType } from '../enums';

@Entity({
  name: 'order_location',
})
export class OrderLocation extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => Order)
  @JoinColumn({ name: 'orderId', referencedColumnName: 'id' })
  owner: Order;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: OrderLocationType })
  type: OrderLocationType;

  @ApiProperty()
  @Expose()
  @Column('int')
  detailsId: number;

  @ApiProperty()
  @Expose()
  @Column('text')
  area: string;
}
