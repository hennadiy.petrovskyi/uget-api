import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne, Unique } from 'typeorm';
import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

import { BaseEntity } from 'src/common/entities';
import { OwnerRole, Receiver } from '../enums';
import { AuthRecord } from 'src/auth/entities';

@Entity({
  name: 'order',
})
// @Unique(['uuid'])
export class Order extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: OwnerRole, nullable: false })
  ownerRole: OwnerRole;

  @ApiProperty()
  @Expose()
  @Column('text', { nullable: true })
  uuid: string;

  @ApiProperty()
  @Expose()
  @Column('text')
  deliveryDate: string;

  @ApiProperty()
  @Expose()
  @Column('boolean', { default: false })
  isDelivered: boolean;

  @ApiProperty()
  @Expose()
  @Column('text', { nullable: true })
  tripId: string;

  @ApiProperty()
  @Expose()
  @Column('text', { array: true, default: [] })
  incomingRequests: string[];

  @ApiProperty()
  @Expose()
  @Column('text', { array: true, default: [] })
  outgoingRequests: string[];
}
