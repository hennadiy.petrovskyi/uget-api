import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

import { BaseEntity } from 'src/common/entities';
import { Order } from '.';
import {} from '../enums';

@Entity({
  name: 'third_party_number',
})
export class ThirdPartyPhoneNumber extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => Order)
  @JoinColumn({ name: 'orderId', referencedColumnName: 'id' })
  owner: Order;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  formatted: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  countryCode: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  code: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  number: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('boolean', { default: false })
  isVerified: boolean;
}
