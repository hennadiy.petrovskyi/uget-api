import { PickType, IntersectionType, OmitType } from '@nestjs/mapped-types';
import { plainToClass } from 'class-transformer';
import { DeliveryPrice } from '../entities';

export class GetOrderDeliveryPriceDto extends PickType(DeliveryPrice, [
  'total',
  'currency',
]) {
  static buildDto(payload: DeliveryPrice): GetOrderDeliveryPriceDto {
    return plainToClass(GetOrderDeliveryPriceDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
