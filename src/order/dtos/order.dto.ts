import { Currency } from './../../pricing/enums/currency.enum';
import { PickType, IntersectionType, OmitType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Expose, plainToClass, Type } from 'class-transformer';

import { DetailsCreateDto } from './details.dto';
import { Order } from '../entities';
import { OrderType, Receiver } from '../enums';
import { LocationCreateDto } from '.';
import { AuthRecord } from 'src/auth/entities';

export class OrderCreateDto extends PickType(Order, [
  'deliveryDate',
  'uuid',
  'ownerRole',
]) {
  @ApiProperty()
  @Expose()
  owner: AuthRecord;

  static buildDto(payload: PublishOrderDto): OrderCreateDto {
    return plainToClass(OrderCreateDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class PublishOrderDto extends OrderCreateDto {
  @ApiProperty()
  @Expose()
  ownerRole: any;

  @ApiProperty()
  @Expose()
  details: DetailsCreateDto;

  @ApiProperty()
  @Expose()
  locations: {
    pickup: LocationCreateDto;
    endpoint: LocationCreateDto;
  };

  @ApiProperty()
  @Expose()
  deliveryPrice: {
    value: string;
    currency: Currency;
  };

  @ApiProperty()
  @Expose()
  thirdPartyPhoneNumber: any;
}

export class GetOrderDto extends PickType(Order, [
  'id',
  'createdAt',
  'deliveryDate',
  'uuid',
  'tripId',
  'ownerRole',
  'incomingRequests',
  'outgoingRequests',
]) {
  static buildDto(payload: Order): GetOrderDto {
    return plainToClass(GetOrderDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class GetOrderForTipDto extends PickType(Order, [
  'deliveryDate',
  'uuid',
]) {
  @ApiProperty()
  @Expose()
  details: DetailsCreateDto;

  @ApiProperty()
  @Expose()
  locations: {
    pickup: LocationCreateDto;
    endpoint: LocationCreateDto;
  };

  @ApiProperty()
  @Expose()
  photos: any[];

  static buildDto(payload: any): GetOrderForTipDto {
    return plainToClass(GetOrderForTipDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
