import { Expose, plainToClass } from 'class-transformer';
import { PickType } from '@nestjs/mapped-types';
import { OrderLocation } from '../entities';

export class LocationCreateDto extends PickType(OrderLocation, [
  'area',
  'detailsId',
]) {
  @Expose()
  country: string;

  @Expose()
  city: string;
}

export class LocationResponseDto extends PickType(OrderLocation, [
  'area',
  'type',
]) {
  static buildDto(payload: any): LocationResponseDto {
    return plainToClass(LocationResponseDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
