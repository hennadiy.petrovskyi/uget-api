import { PickType } from '@nestjs/mapped-types';
import { plainToClass } from 'class-transformer';
import { Details } from '../entities';

export class DetailsCreateDto extends PickType(Details, [
  'name',
  'quantity',
  'size',
  'quantity',
  'description',
  'type',
]) {
  static buildDto(payload: Details): DetailsCreateDto {
    return plainToClass(DetailsCreateDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
