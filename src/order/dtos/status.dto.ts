import { PickType, IntersectionType, OmitType } from '@nestjs/mapped-types';
import { plainToClass } from 'class-transformer';
import { Status } from '../entities';

export class GetStatusDto extends PickType(Status, ['createdAt', 'status']) {
  static buildDto(payload: Status): GetStatusDto {
    return plainToClass(GetStatusDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
