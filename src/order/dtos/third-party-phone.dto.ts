import { PickType, IntersectionType, OmitType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Expose, plainToClass, Type } from 'class-transformer';

import {} from './details.dto';
import { ThirdPartyPhoneNumber } from '../entities';
import {} from '../enums';
import {} from '.';
import {} from 'src/auth/entities';

export class ThirdPartPhoneNumberDto extends PickType(ThirdPartyPhoneNumber, [
  'code',
  'countryCode',
  'number',
  'formatted',
  'isVerified',
]) {
  static buildDto(payload: any): ThirdPartPhoneNumberDto {
    return plainToClass(ThirdPartPhoneNumberDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
