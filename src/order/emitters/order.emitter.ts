import {
  PHONE_NUMBER_EVENT,
  PRICING_EVENTS,
  PUSH_NOTIFICATION_EVENTS,
  SOCKET_EVENTS,
  TRIP_EVENT,
  USER_EVENTS,
} from './../../common/events/events-constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import {
  FILE_EVENTS,
  LOCATION_EVENT,
  ORDER_EVENT,
  AUTH_EVENTS,
} from 'src/common/events';
import { CancelEventType } from 'src/trips/enums';
import { FileType } from 'src/file/enums';

@Injectable()
export class OrderEmitter {
  constructor(private eventEmitter: EventEmitter2) {}

  public async getOrderPhotos(owner: string) {
    const photos = await this.eventEmitter.emitAsync(
      FILE_EVENTS.GET_FILES,
      owner,
    );

    if (!photos.length) {
      return [];
    }

    return photos[0];
  }

  public async getLocationDetails(id: number) {
    const locationDetails = await this.eventEmitter.emitAsync(
      LOCATION_EVENT.GET_LOCATION_DETAILS_BY_ID,
      id,
    );

    if (locationDetails.length) {
      return locationDetails[0];
    }
  }

  public async saveLocationDetails(saveLocationDetailsDto: any) {
    const locationDetails = await this.eventEmitter.emitAsync(
      LOCATION_EVENT.SAVE_LOCATION_IN_DB,
      saveLocationDetailsDto,
    );

    if (locationDetails.length) {
      return locationDetails[0].id;
    }
  }

  public async deleteAllPhotos(id: string) {
    return await this.eventEmitter.emitAsync(
      FILE_EVENTS.DELETE_OWNER_FILES,
      id,
    );
  }

  public async addTripOutgoingRequest(orderOutgoingRequestDto: any) {
    await this.eventEmitter.emitAsync(
      TRIP_EVENT.ADD_INCOMING_REQUEST,
      orderOutgoingRequestDto,
    );
  }

  public async cancelTripDelivery(
    type: CancelEventType,
    cancelOrderOutgoingRequestDto: any,
  ) {
    const eventsTypes = {
      incoming: ORDER_EVENT.CANCEL_INCOMING_REQUEST,
      outgoing: ORDER_EVENT.CANCEL_OUTGOING_REQUEST,
      accepted: ORDER_EVENT.CANCEL_ACCEPTED_REQUEST,
    };

    await this.eventEmitter.emitAsync(
      eventsTypes[type],
      cancelOrderOutgoingRequestDto,
    );
  }

  public async orderOwnerAcceptDeliveryRequest(acceptDeliveryRequestDto: any) {
    await this.eventEmitter.emitAsync(
      TRIP_EVENT.ACCEPT_DELIVERY_BY_ORDER_OWNER,
      acceptDeliveryRequestDto,
    );
  }

  public async requestPickup(verifyPickup: any) {
    await this.eventEmitter.emitAsync(ORDER_EVENT.PICKUP_REQUEST, verifyPickup);
  }

  public async verifyPickupRequest(verifyPickupDto: any) {
    const response = await this.eventEmitter.emitAsync(
      ORDER_EVENT.PICKUP_VERIFICATION,
      verifyPickupDto,
    );

    if (!response.length) {
      return;
    }

    return response[0];
  }

  public async startDelivery(startDeliveryDto: any) {
    await this.eventEmitter.emitAsync(
      ORDER_EVENT.START_DELIVERY,
      startDeliveryDto,
    );
  }

  public async finishOrderDelivery(startDeliveryDto: any) {
    await this.eventEmitter.emitAsync(
      ORDER_EVENT.FINISH_DELIVERY,
      startDeliveryDto,
    );
  }

  public async requestOwnerUUID(id: any) {
    const result = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_ID,
      id,
    );

    if (!result.length) {
      return;
    }

    return result[0];
  }

  public async savePhoneNumberDetails(phoneNumberDto: any) {
    await this.eventEmitter.emitAsync(
      PHONE_NUMBER_EVENT.SAVE_PHONE_NUMBER,
      phoneNumberDto,
    );
  }

  public async getPhoneNumberDetails(phoneNumberDto: any) {
    const result = await this.eventEmitter.emitAsync(
      PHONE_NUMBER_EVENT.GET_PHONE_NUMBER,
      phoneNumberDto,
    );

    if (!result.length) {
      return;
    }

    return result[0];
  }

  public async getUserDetails(id: number) {
    const userDetails = await this.eventEmitter.emitAsync(
      USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID,
      id,
    );

    if (!userDetails.length) {
      return;
    }

    const { firstName, lastName, rating, profileImage } = userDetails[0];

    return { fullName: firstName + ' ' + lastName, rating, profileImage };
  }

  public async getUserProfileImage(ownerId: number) {
    const owner = await this.getAuthRecordUUID(ownerId);

    const file = await this.eventEmitter.emitAsync(
      FILE_EVENTS.GET_FILE_BY_TYPE,
      { owner: owner.uuid, type: FileType.PROFILE_IMAGE },
    );

    if (file && file.length) {
      return file[0];
    }

    return null;
  }

  async getAuthRecordUUID(id: number) {
    const result = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_ID,
      id,
    );

    if (result && result.length) {
      return result[0];
    }
  }

  async addUnratedUserRating(dto: any) {
    const result = await this.eventEmitter.emitAsync(
      USER_EVENTS.ADD_NEW_UNRATED_RATE,
      dto,
    );

    if (result && result.length) {
      return result[0];
    }
  }

  async sendIncomingRequestNotification(dto: { from: string; to: any }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.ORDER_REQUEST,
      dto,
    );
  }

  async sendAcceptedRequestNotification(dto: { from: string; to: any }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.ORDER_REQUEST_ACCEPTED,
      dto,
    );
  }

  async sendOrderVerifyPickedUpNotification(dto: {
    orderUUID: string;
    to: any;
    from: number;
  }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP_VERIFICATION,
      dto,
    );
  }
  async sendVerifyOrderDeliveryNotification(dto: {
    orderUUID: string;
    to: any;
    from: number;
  }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.DELIVERY_VERIFICATION,
      dto,
    );
  }

  public async calculateDeliveryPrice(calculatePriceDto: {
    from: string;
    to: string;
    size: any;
  }) {
    const data = await this.eventEmitter.emitAsync(
      PRICING_EVENTS.CALCULATE_ORIGINAL_AVERAGE_PRICE,
      calculatePriceDto,
    );

    if (data && data.length) {
      return data[0];
    }

    return null;
  }

  async sendSocketMessage(dto: { uuid: string; payload: any }) {
    await this.eventEmitter.emitAsync(SOCKET_EVENTS.SEND_MESSAGE, dto);
  }
}
