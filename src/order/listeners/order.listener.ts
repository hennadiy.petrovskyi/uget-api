import { Currency } from './../../pricing/enums/currency.enum';
import { DeliveryPrice } from './../entities/price.entity';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { ORDER_EVENT } from './../../common/events/events-constants';
import {
  OrderService,
  StatusService,
  OrderHelperService,
  DeliveryPriceService,
} from '../services';
import {} from '../dtos';
import { OrderEmitter } from '../emitters';
import { OrderStatus } from '../enums';
import { SocketEvents } from 'src/socket/enums/events';
import { LOADIPHLPAPI } from 'dns';
import { FindOperator } from 'typeorm';

@Injectable()
export class OrderListener {
  constructor(
    private readonly orderService: OrderService,
    private readonly statusService: StatusService,
    private readonly orderEmitter: OrderEmitter,
    private readonly helperService: OrderHelperService,
    private readonly deliveryPriceService: DeliveryPriceService,
  ) {}

  @OnEvent(ORDER_EVENT.ADD_INCOMING_REQUEST, { promisify: true })
  public async orderAddIncomingRequest(dto: any) {
    const order = await this.orderService.findOrderByUUID(dto.orderUUID);

    if (order.incomingRequests.includes(dto.tripUUID)) {
      return;
    }

    order.incomingRequests.push(dto.tripUUID);

    await this.orderService.updateOrderRequests(order.id, {
      incomingRequests: order.incomingRequests,
    });

    console.log('ORDER_EVENT.ADD_INCOMING_REQUEST');

    const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);

    await this.orderEmitter.sendSocketMessage({
      uuid: orderOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_ORDERS },
    });

    await this.orderEmitter.sendSocketMessage({
      uuid: 'trip_' + dto.trip.uuid,
      payload: { data: dto.trip.update },
    });

    // await this.orderEmitter.sendIncomingRequestNotification({
    //   to: order.owner,
    //   from: dto.from,
    // });
  }

  @OnEvent(ORDER_EVENT.CANCEL_INCOMING_REQUEST, { promisify: true })
  public async orderCancelIncomingRequestDelivery(dto: any) {
    const order = await this.orderService.findOrderByUUID(dto.orderUUID);

    if (!order.incomingRequests.includes(dto.tripUUID)) {
      return;
    }

    await this.orderService.updateOrderRequests(order.id, {
      incomingRequests: order.incomingRequests.filter(
        (tripUUID) => tripUUID !== dto.tripUUID,
      ),
    });

    console.log('ORDER_EVENT.CANCEL_INCOMING_REQUEST');

    const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);

    await this.orderEmitter.sendSocketMessage({
      uuid: orderOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_ORDERS },
    });

    await this.orderEmitter.sendSocketMessage({
      uuid: 'trip_' + dto.trip.uuid,
      payload: { data: dto.trip.data },
    });
  }

  @OnEvent(ORDER_EVENT.CANCEL_OUTGOING_REQUEST, { promisify: true })
  public async orderCancelOutgoingRequestDelivery(dto: any) {
    const order = await this.orderService.findOrderByUUID(dto.orderUUID);

    if (!order.outgoingRequests.includes(dto.tripUUID)) {
      return;
    }

    await this.orderService.updateOrderRequests(order.id, {
      outgoingRequests: order.outgoingRequests.filter(
        (tripUUID) => tripUUID !== dto.tripUUID,
      ),
    });

    console.log('ORDER_EVENT.CANCEL_OUTGOING_REQUEST');

    const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);

    await this.orderEmitter.sendSocketMessage({
      uuid: orderOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_ORDERS },
    });

    await this.orderEmitter.sendSocketMessage({
      uuid: 'trip_' + dto.trip.uuid,
      payload: { data: dto.trip.data },
    });
  }

  @OnEvent(ORDER_EVENT.ACCEPT_DELIVERY_BY_TRAVELER, { promisify: true })
  public async acceptDeliveryRequest(dto: any) {
    const order = await this.orderService.findOrderByUUID(dto.orderUUID);

    if (order.tripId === dto.tripUUID) {
      return;
    }

    await this.orderService.updateOrderRequests(order.id, {
      outgoingRequests: order.outgoingRequests.filter(
        (tripUUID) => tripUUID !== dto.tripUUID,
      ),
      tripId: dto.tripUUID,
    });

    console.log('ORDER_EVENT.ACCEPT_DELIVERY_BY_TRAVELER');

    await this.statusService.addOrderStatus({
      owner: order,
      status: OrderStatus.ACCEPTED,
    });

    const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);

    await this.orderEmitter.sendSocketMessage({
      uuid: orderOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_ORDERS },
    });

    await this.orderEmitter.sendSocketMessage({
      uuid: 'trip_' + dto.trip.uuid,
      payload: { data: dto.trip.data },
    });

    // await this.orderEmitter.sendAcceptedRequestNotification({
    //   to: order.owner,
    //   from: dto.from,
    // });
  }

  @OnEvent(ORDER_EVENT.CANCEL_ACCEPTED_REQUEST, { promisify: true })
  public async orderCancelAcceptedRequestDelivery(dto: any) {
    const order = await this.orderService.findOrderByUUID(dto.orderUUID);

    if (order.tripId !== dto.tripUUID) {
      return;
    }

    await this.orderService.updateOrderRequests(order.id, {
      tripId: null,
    });

    await this.statusService.deleteAcceptedStatus(order.id as any);

    console.log('ORDER_EVENT.CANCEL_ACCEPTED_REQUEST');

    const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);

    await this.orderEmitter.sendSocketMessage({
      uuid: orderOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_ORDERS },
    });

    await this.orderEmitter.sendSocketMessage({
      uuid: 'trip_' + dto.trip.uuid,
      payload: { data: dto.trip.data },
    });
  }

  @OnEvent(ORDER_EVENT.GET_USER_ORDERS, { promisify: true })
  public async getAllUserOrders(owner: FindOperator<any>) {
    const orders = await this.orderService.getAllUserOrders(owner);

    if (!orders) {
      return [];
    }

    return Promise.all(
      orders.map((el) => this.helperService.buildOrderDto(el)),
    );
  }

  @OnEvent(ORDER_EVENT.GET_EARNINGS, { promisify: true })
  public async getEarnings(ordersUUIDs: string[]) {
    const orders = await Promise.all(
      ordersUUIDs.map((uuid) => this.orderService.findOrderByUUID(uuid)),
    );

    const deliveryPricesDetails = await Promise.all(
      orders
        .map((order) => order.id)
        .map((orderId) =>
          this.deliveryPriceService.getOrderDeliveryPrice(orderId as any),
        ),
    );

    /**
     * TO ADD CALCULATION FOR EARNINGS
     * currently default is 50% of delivery
     */

    const deliveryPrices = deliveryPricesDetails.map((price) => price.total);

    const temporaryEarningModifier = 2;

    const earnings = deliveryPrices.length
      ? +deliveryPrices.reduce((prev, current) => prev + current) /
        temporaryEarningModifier
      : 0;

    return {
      value: !deliveryPrices.length ? 0 : earnings.toFixed(2),
      currency: Currency.EUR,
    };
  }
}
