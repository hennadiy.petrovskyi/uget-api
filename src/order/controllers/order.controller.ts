import { OrderEmitter } from './../emitters/order.emitter';
import { OwnerRole } from './../enums/owner-role.enum';
import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  forwardRef,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Res,
  Sse,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { User } from 'src/common/decorators';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';

import {
  GetOrderForTipDto,
  OrderCreateDto,
  PublishOrderDto,
  ThirdPartPhoneNumberDto,
} from '../dtos';
import { OrderLocationType, OrderStatus } from '../enums';
import {
  OrderService,
  StatusService,
  LocationService,
  DetailsService,
  ThirdPartyNumberService,
  OrderHelperService,
  DeliveryPriceService,
} from '../services';
import { CacheService } from 'src/common/services/cache.service';
import { interval, map, Observable } from 'rxjs';
import { CancelEventType } from 'src/trips/enums';

import { Request } from 'express';
import { UserDeliveryRole } from 'src/user/enums';
import { FindOperator } from 'typeorm';

@ApiBearerAuth()
@ApiTags('Order')
@Controller('order')
export class OrderController {
  constructor(
    private readonly service: OrderService,
    @Inject(forwardRef(() => OrderHelperService))
    private readonly helper: OrderHelperService,
    private readonly statusService: StatusService,
    private readonly detailsService: DetailsService,
    private readonly locationService: LocationService,
    private readonly thirdPartyNumberService: ThirdPartyNumberService,
    private readonly priceService: DeliveryPriceService,
    private cacheService: CacheService,
    private orderEmitter: OrderEmitter,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async publishOrder(@Body() publishOrderDto: PublishOrderDto, @User() user) {
    const order = await this.service.createOrder({
      ...OrderCreateDto.buildDto(publishOrderDto),
      owner: user,
    });
    const owner = order;

    const deliveryPrice = await this.orderEmitter.calculateDeliveryPrice({
      from: publishOrderDto.locations.pickup.country,
      to: publishOrderDto.locations.endpoint.country,
      size: publishOrderDto.details.size,
    });

    await this.priceService.saveOrderDeliveryPrice({
      ...deliveryPrice,
      owner,
    });

    await this.thirdPartyNumberService.savePhoneNumber({
      ...publishOrderDto.thirdPartyPhoneNumber,
      owner,
    });
    await this.statusService.addOrderStatus({
      owner,
      status: OrderStatus.PUBLISHED,
    });
    await this.detailsService.saveOrderDetails({
      owner,
      ...publishOrderDto.details,
    });
    const pickupLocationDetailsId = await this.orderEmitter.saveLocationDetails(
      {
        countryId: publishOrderDto.locations.pickup.country,
        locationId: publishOrderDto.locations.pickup.city,
      },
    );
    const endpointLocationDetailsId =
      await this.orderEmitter.saveLocationDetails({
        countryId: publishOrderDto.locations.endpoint.country,
        locationId: publishOrderDto.locations.endpoint.city,
      });
    const pickupLocationDto: any = {
      owner,
      ...publishOrderDto.locations.pickup,
      detailsId: pickupLocationDetailsId,
    };
    const endpointLocationDto: any = {
      owner,
      ...publishOrderDto.locations.endpoint,
      detailsId: endpointLocationDetailsId,
    };
    await this.locationService.saveOrderLocation(pickupLocationDto);
    await this.locationService.saveOrderLocation(endpointLocationDto);

    const userOrders = await this.service.getAllUserOrders(user.id);

    const ordersResponse = await Promise.all(
      userOrders.map((order) => this.helper.buildOrderDto(order)),
    );

    return { owner: order.uuid, orders: ordersResponse };
  }

  @UseGuards(JwtAuthGuard)
  @Get('/my')
  async getAllOrders(@User() user) {
    const orders = await this.service.getAllUserOrders(user.id);

    return Promise.all(orders.map((order) => this.helper.buildOrderDto(order)));
  }

  @UseGuards(JwtAuthGuard)
  @Post('/trip/active')
  async getTravellerActiveOrders(
    @User() user,
    @Body('tripUUID') tripUUID: string,
  ) {
    const orders = await this.service.findTravellerAcceptedOrders({
      tripId: tripUUID,
      isDelivered: false,
    });

    return Promise.all(
      orders.map((order) => this.helper.buildOrderForTripDTO(order, tripUUID)),
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/trip/delivered')
  async getTravellerTripDeliveredOrders(
    @User() user,
    @Body('tripUUID') tripUUID: string,
  ) {
    const orders = await this.service.findTravellerAcceptedOrders({
      tripId: tripUUID,
      isDelivered: true,
    });

    return Promise.all(
      orders.map((order) => this.helper.buildOrderForTripDTO(order, tripUUID)),
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/one')
  async getOne(@Body('orderUUID') orderUUID: string, @User() user) {
    const order = await this.service.findOrderByUUID(orderUUID);

    if (order.owner !== user.id) {
      return this.helper.buildOrderForTripDTO(order, '');
    }

    return this.helper.buildOrderDto(order);
  }

  @UseGuards(JwtAuthGuard)
  @Post('unpublish')
  async unpublishOrder(@Body('uuid') orderUUID: string, @User() user) {
    const order = await this.service.findOrderByUUID(orderUUID);

    await this.statusService.addOrderStatus({
      owner: order,
      status: OrderStatus.INACTIVE,
    });

    return await this.getAllOrders(user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('requests')
  async getOrderRequests(@Body('uuid') orderUUID: string, @User() user) {
    const order = await this.service.findOrderByUUID(orderUUID);

    if (!order) {
      throw new NotFoundException();
    }

    const requests = []
      .concat(order.incomingRequests)
      .concat(order.outgoingRequests);

    console.log(requests);
  }

  @UseGuards(JwtAuthGuard)
  @Post('publish/inactive')
  async publishInactiveOrder(@Body('uuid') orderUUID: string, @User() user) {
    const order = await this.service.findOrderByUUID(orderUUID);

    await this.statusService.addOrderStatus({
      owner: order,
      status: OrderStatus.PUBLISHED,
    });

    return this.getAllOrders(user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  async deleteOrder(@Body('uuid') orderUUID: string, @User() user) {
    const order = await this.service.findOrderByUUID(orderUUID);

    await this.orderEmitter.deleteAllPhotos(order.uuid);

    await this.statusService.deleteAllOrderStatuses(order.id as any);
    await this.thirdPartyNumberService.deletePhoneNumber(order.id as any);
    await this.detailsService.deleteOrderDetails(order.id as any);
    await this.locationService.deleteOrderLocation(order.id as any);
    await this.priceService.deleteOrderDeliveryPrice(order.id as any);
    await this.service.deleteOrder(order.id);

    return this.getAllOrders(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/filtered/many')
  async getAllByOrderParams(@User() user, @Query() query) {
    const orders = await this.service.getAllByParams(
      user.id,
      query.arrivalDate,
    );

    const filtered = await Promise.all(
      orders.map(async (order) => {
        const from = await this.helper.getLocationsDetails(
          order.id as any,
          OrderLocationType.PICKUP,
        );
        const to = await this.helper.getLocationsDetails(
          order.id as any,
          OrderLocationType.ENDPOINT,
        );

        if (from.city.id === query.from && to.city.id === query.to) {
          return order;
        }
      }),
    );

    return Promise.all(
      filtered
        .filter((el) => el)
        .map(
          async (el) =>
            await this.helper.buildOrderForTripDTO(el, query.tripUUID),
        ),
    );
  }

  @Get('/filtered/one')
  async getOrderUpdatedOrderForTraveler(@Query() query) {
    const order = await this.service.findOrderByUUID(query.orderUUID);

    return this.helper.buildOrderForTripDTO(order, query.tripUUID);
  }

  @UseGuards(JwtAuthGuard)
  @Put('delivery/request')
  async requestOrderDelivery(
    @Body('') requestOrderDeliveryDto: any,
    @User() user,
  ) {
    const order = await this.service.findOrderByUUID(
      requestOrderDeliveryDto.orderUUID,
    );

    if (!order) {
      throw new ForbiddenException();
    }

    order.outgoingRequests.push(requestOrderDeliveryDto.tripUUID);

    const orderUpdate = await this.service.updateOrderRequests(order.id, {
      outgoingRequests: order.outgoingRequests,
    });

    const socketMessageData = await this.helper.buildOrderForTripDTO(
      orderUpdate,
      requestOrderDeliveryDto.tripUUID,
    );

    await this.orderEmitter.addTripOutgoingRequest({
      ...requestOrderDeliveryDto,
      order: {
        uuid: requestOrderDeliveryDto.orderUUID,
        update: socketMessageData,
      },
      from: user.id,
    });

    return this.helper.buildOrderDto(orderUpdate);
  }

  @UseGuards(JwtAuthGuard)
  @Put('delivery/cancel')
  async cancelDeliveryOrderRequest(
    @Body('') requestOrderDeliveryDto: any,
    @User() user,
  ) {
    const order = await this.service.findOrderByUUID(
      requestOrderDeliveryDto.orderUUID,
    );

    if (!order) {
      throw new ForbiddenException();
    }

    function handleUpdateBody() {
      let update = {
        body: null,
        orderCancelAction: null,
      };

      const isAccepted = order.tripId === requestOrderDeliveryDto.tripUUID;
      const isIncoming = order.incomingRequests.includes(
        requestOrderDeliveryDto.tripUUID,
      );
      const isOutgoing = order.outgoingRequests.includes(
        requestOrderDeliveryDto.tripUUID,
      );

      if (isIncoming) {
        update.body = {
          incomingRequests: order.incomingRequests.filter(
            (id) => id !== requestOrderDeliveryDto.tripUUID,
          ),
        };
        update.orderCancelAction = CancelEventType.OUTGOING;
      }

      if (isOutgoing) {
        update.body = {
          outgoingRequests: order.outgoingRequests.filter(
            (id) => id !== requestOrderDeliveryDto.tripUUID,
          ),
        };
        update.orderCancelAction = CancelEventType.INCOMING;
      }

      if (isAccepted) {
        update.body = {
          tripId: null,
        };

        update.orderCancelAction = CancelEventType.ACCEPTED;
      }

      return update;
    }

    const update = handleUpdateBody();

    if (!update.body) {
      return this.helper.buildOrderDto(order);
    }

    if (update.orderCancelAction === CancelEventType.ACCEPTED) {
      await this.statusService.deleteAcceptedStatus(order.id as any);
    }

    const orderUpdate = await this.service.updateOrderRequests(
      order.id,
      update.body,
    );

    const socketMessageData = await this.helper.buildOrderForTripDTO(
      orderUpdate,
      requestOrderDeliveryDto.tripUUID,
    );

    await this.orderEmitter.cancelTripDelivery(update.orderCancelAction, {
      ...requestOrderDeliveryDto,
      order: {
        uuid: requestOrderDeliveryDto.orderUUID,
        update: socketMessageData,
      },
    });

    return this.helper.buildOrderDto(orderUpdate);
  }

  @UseGuards(JwtAuthGuard)
  @Put('delivery/accept')
  async acceptDeliveryOrderRequest(
    @Body('') requestOrderDeliveryDto: any,
    @User() user,
  ) {
    const order = await this.service.findOrderByUUID(
      requestOrderDeliveryDto.orderUUID,
    );

    if (
      !order ||
      !order.incomingRequests.includes(requestOrderDeliveryDto.tripUUID)
    ) {
      throw new ForbiddenException();
    }

    const orderUpdate = await this.service.updateOrderRequests(order.id, {
      incomingRequests: order.incomingRequests.filter(
        (tripId) => tripId !== requestOrderDeliveryDto.tripUUID,
      ),
      tripId: requestOrderDeliveryDto.tripUUID,
    });

    await this.statusService.addOrderStatus({
      owner: order,
      status: OrderStatus.ACCEPTED,
    });

    const socketMessageData = await this.helper.buildOrderForTripDTO(
      orderUpdate,
      requestOrderDeliveryDto.tripUUID,
    );

    await this.orderEmitter.orderOwnerAcceptDeliveryRequest({
      ...requestOrderDeliveryDto,
      order: {
        uuid: requestOrderDeliveryDto.orderUUID,
        update: socketMessageData,
      },
      from: user.id,
    });

    return this.helper.buildOrderDto(orderUpdate);
  }

  @UseGuards(JwtAuthGuard)
  @Post('delivery/request/pickup')
  async requestPickupVerification(
    @Body('') requestOrderDeliveryDto: any,
    @User() user,
  ) {
    const order = await this.service.findOrderByUUID(
      requestOrderDeliveryDto.orderUUID,
    );

    if (!order || order.tripId !== requestOrderDeliveryDto.tripUUID) {
      throw new ForbiddenException();
    }

    let phoneNumber;

    if (order.ownerRole === OwnerRole.SENDER) {
      phoneNumber = await this.orderEmitter.getPhoneNumberDetails(order.owner);
    }

    if (order.ownerRole === OwnerRole.RECEIVER) {
      phoneNumber = await this.thirdPartyNumberService.findPhoneNumberByOwner(
        order.id as any,
      );
    }

    await this.orderEmitter.requestPickup({
      ...requestOrderDeliveryDto,
      phoneNumber: phoneNumber.formatted,
    });

    await this.orderEmitter.sendOrderVerifyPickedUpNotification({
      from: user.id,
      to: order.owner,
      orderUUID: order.uuid,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Post('delivery/verify/pickup')
  async verifyPickup(@Body('') requestOrderDeliveryDto: any, @User() user) {
    const order = await this.service.findOrderByUUID(
      requestOrderDeliveryDto.orderUUID,
    );

    const isVerified = await this.orderEmitter.verifyPickupRequest({
      orderUUID: requestOrderDeliveryDto.orderUUID,
      code: requestOrderDeliveryDto.code,
    });

    if (!order || order.tripId !== requestOrderDeliveryDto.tripUUID) {
      throw new ForbiddenException();
    }

    if (!isVerified) {
      throw new HttpException(
        {
          field: 'sms',
          type: '404',
          message: 'Sms code is not found or expired',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    await this.statusService.addOrderStatus({
      owner: order,
      status: OrderStatus.PICKED_UP,
    });

    await this.orderEmitter.startDelivery({
      ...requestOrderDeliveryDto,
      to: order.owner,
      by: user.id,
    });

    return this.helper.buildOrderForTripDTO(
      order,
      requestOrderDeliveryDto.tripUUID,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('delivery/request/finish')
  async requestPFinishDeliveryVerification(
    @Body('') requestOrderDeliveryDto: any,
    @User() user,
  ) {
    const order = await this.service.findOrderByUUID(
      requestOrderDeliveryDto.orderUUID,
    );

    if (!order || order.tripId !== requestOrderDeliveryDto.tripUUID) {
      throw new ForbiddenException();
    }

    let phoneNumber;

    if (order.ownerRole === OwnerRole.SENDER) {
      phoneNumber = await this.orderEmitter.getPhoneNumberDetails(order.owner);
    }

    if (order.ownerRole === OwnerRole.RECEIVER) {
      phoneNumber = await this.thirdPartyNumberService.findPhoneNumberByOwner(
        order.id as any,
      );
    }

    await this.orderEmitter.requestPickup({
      ...requestOrderDeliveryDto,
      phoneNumber: phoneNumber.formatted,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Post('delivery/verify/delivery')
  async verifyDelivery(@Body('') requestOrderDeliveryDto: any, @User() user) {
    const order = await this.service.findOrderByUUID(
      requestOrderDeliveryDto.orderUUID,
    );

    const isVerified = await this.orderEmitter.verifyPickupRequest({
      orderUUID: requestOrderDeliveryDto.orderUUID,
      code: requestOrderDeliveryDto.code,
    });
    if (!order || order.tripId !== requestOrderDeliveryDto.tripUUID) {
      throw new ForbiddenException();
    }

    if (!isVerified) {
      throw new HttpException(
        {
          field: 'sms',
          type: '404',
          message: 'Sms code is not found or expired',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    await this.statusService.addOrderStatus({
      owner: order,
      status: OrderStatus.DELIVERED,
    });

    // const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);

    // await this.orderEmitter.addUnratedUserRating({
    //   owner: user,
    //   orderId: order.id,
    //   role: UserDeliveryRole.TRAVELLER,
    //   ratedBy: orderOwner.id,
    // });

    // await this.orderEmitter.addUnratedUserRating({
    //   owner: orderOwner,
    //   orderId: order.id,
    //   role: UserDeliveryRole.SENDER,
    //   ratedBy: user.id,
    // });

    await this.orderEmitter.finishOrderDelivery({
      tripUUID: requestOrderDeliveryDto.tripUUID,
      orderUUID: requestOrderDeliveryDto.orderUUID,
      to: order.owner,
      by: user.id,
    });

    await this.service.updateOrder({ id: order.id, isDelivered: true });

    return this.helper.buildOrderForTripDTO(
      order,
      requestOrderDeliveryDto.tripUUID,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('owner/details')
  async requestOwnerDetails(@Body('uuid') uuid: any, @User() user) {
    const order = await this.service.findOrderByUUID(uuid);

    const orderOwner = await this.orderEmitter.requestOwnerUUID(order.owner);

    if (!orderOwner) {
      throw new NotFoundException();
    }

    return orderOwner.uuid;
  }
}
