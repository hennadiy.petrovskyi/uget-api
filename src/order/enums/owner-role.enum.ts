export enum OwnerRole {
  SENDER = 'SENDER',
  RECEIVER = 'RECEIVER',
}
