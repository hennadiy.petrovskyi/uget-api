export enum OrderStatus {
  PUBLISHED = 'published',
  INACTIVE = 'inactive',
  ACCEPTED = 'accepted',
  DRAFT = 'draft',
  EXPIRED = 'expired',
  REQUEST_INCOMING = 'request.incoming',
  PICKED_UP = 'picked-up',
  DELIVERED = 'delivered',
}
