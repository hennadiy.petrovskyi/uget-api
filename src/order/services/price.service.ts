import { Injectable, Scope } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { DeliveryPrice } from '../entities';
import { LoggerService } from 'src/utils/logger';
import {} from '../dtos';

@Injectable()
export class DeliveryPriceService {
  constructor(
    @InjectRepository(DeliveryPrice)
    private priceRepository: Repository<DeliveryPrice>,
    private logger: LoggerService,
  ) {}

  public async saveOrderDeliveryPrice(orderDeliveryPriceDto: any) {
    const functionName = this.saveOrderDeliveryPrice.name;

    try {
      await this.priceRepository.save(orderDeliveryPriceDto);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getOrderDeliveryPrice(owner: FindOperator<any>) {
    const functionName = this.getOrderDeliveryPrice.name;

    try {
      return await this.priceRepository.findOne({ where: { owner } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async deleteOrderDeliveryPrice(owner: FindOperator<any>) {
    const functionName = this.deleteOrderDeliveryPrice.name;
    try {
      const details = await this.priceRepository.findOne({
        where: { owner },
      });

      await this.priceRepository.delete(details.id);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
