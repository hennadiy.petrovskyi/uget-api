import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { ThirdPartyPhoneNumber } from '../entities';

import { LoggerService } from '../../utils/logger';

@Injectable()
export class ThirdPartyNumberService {
  constructor(
    @InjectRepository(ThirdPartyPhoneNumber)
    private phoneNumberRepository: Repository<ThirdPartyPhoneNumber>,
    private logger: LoggerService,
  ) {}

  async findPhoneNumberByOwner(
    owner: FindOperator<any>,
  ): Promise<ThirdPartyPhoneNumber> {
    const functionName = this.findPhoneNumberByOwner.name;

    try {
      return this.phoneNumberRepository.findOne({ where: { owner } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async savePhoneNumber(savePhoneNumber: any) {
    const functionName = this.savePhoneNumber.name;

    try {
      return await this.phoneNumberRepository.save(savePhoneNumber);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async updatePhoneNumberDetails(id, update: any) {
    const functionName = this.updatePhoneNumberDetails.name;

    try {
      return await this.phoneNumberRepository.save({ id, ...update });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async deletePhoneNumber(owner: FindOperator<any>) {
    const functionName = this.deletePhoneNumber.name;
    try {
      const thirdPartyPhoneNumber = await this.phoneNumberRepository.findOne({
        where: { owner },
      });

      await this.phoneNumberRepository.delete(thirdPartyPhoneNumber.id);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
