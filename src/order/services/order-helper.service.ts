import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { FindOperator } from 'typeorm';
import {
  DetailsService,
  LocationService,
  DeliveryPriceService,
  StatusService,
  ThirdPartyNumberService,
} from '.';

import { LoggerService } from '../../utils/logger';
import {
  GetOrderDeliveryPriceDto,
  GetOrderForTipDto,
  LocationResponseDto,
  ThirdPartPhoneNumberDto,
} from '../dtos';
import { OrderEmitter } from '../emitters';
import { OrderLocationType, OrderStatus } from '../enums';

@Injectable()
export class OrderHelperService {
  constructor(
    @Inject(forwardRef(() => StatusService))
    private readonly statusService: StatusService,
    @Inject(forwardRef(() => DetailsService))
    private readonly detailsService: DetailsService,
    @Inject(forwardRef(() => LocationService))
    private readonly locationService: LocationService,
    @Inject(forwardRef(() => ThirdPartyNumberService))
    private readonly thirdPartyNumberService: ThirdPartyNumberService,
    @Inject(forwardRef(() => DeliveryPriceService))
    private readonly deliveryPriceService: DeliveryPriceService,
    private logger: LoggerService,
    private orderEmitter: OrderEmitter,
  ) {}

  async getLocationsDetails(
    tripId: FindOperator<any>,
    locationType: OrderLocationType,
  ) {
    const tripLocations = await this.locationService.getOrderLocations(
      tripId,
      locationType,
    );

    const details = await this.orderEmitter.getLocationDetails(
      tripLocations.detailsId,
    );

    return {
      ...details,
      area: tripLocations.area,
    };
  }

  async buildOrderForTripDTO(order, tripUUID) {
    const dto = await this.buildOrderDto(order);

    const statusesHistory =
      await this.statusService.findAllOrderStatusesHistory(order.id);

    const statusesParsed = statusesHistory.map((status) => status.status);

    const owner = await this.orderEmitter.getUserDetails(order.owner);
    const profileImage = await this.orderEmitter.getUserProfileImage(
      order.owner,
    );

    const priceDetails = GetOrderDeliveryPriceDto.buildDto(
      await this.deliveryPriceService.getOrderDeliveryPrice(order.id),
    );

    const priceForTraveler = +priceDetails.total / 2;

    function handleStatus() {
      if (
        statusesParsed.includes(OrderStatus.DELIVERED) &&
        order.tripId === tripUUID
      ) {
        return 'delivered';
      }

      if (
        statusesParsed.includes(OrderStatus.PICKED_UP) &&
        order.tripId === tripUUID
      ) {
        return 'progress';
      }

      if (order.tripId === tripUUID) {
        return 'accepted';
      }

      if (order.outgoingRequests.includes(tripUUID)) {
        return 'request.incoming';
      }

      if (order.incomingRequests.includes(tripUUID)) {
        return 'request.outgoing';
      }

      return 'available';
    }

    return {
      ...GetOrderForTipDto.buildDto(dto),
      owner: { ...owner, profileImage },
      status: handleStatus(),

      deliveryPrice: {
        value: priceForTraveler.toFixed(2), // TO ADD EARNINGS PERCENTAGE FROM FULL PRICE
        currency: priceDetails.currency,
      },
    };
  }

  public async buildOrderDto(order) {
    const statusHistory = await this.statusService.findAllOrderStatusesHistory(
      order.id,
    );

    const details = await this.detailsService.getOrderDetails(order.id);
    const photos = (await this.orderEmitter.getOrderPhotos(order.uuid)) || [];

    const thirdPartyPhoneNumber = ThirdPartPhoneNumberDto.buildDto(
      await this.thirdPartyNumberService.findPhoneNumberByOwner(order.id),
    );

    const priceDetails = GetOrderDeliveryPriceDto.buildDto(
      await this.deliveryPriceService.getOrderDeliveryPrice(order.id),
    );

    const pickup = await this.locationService.getOrderLocations(
      order.id,
      OrderLocationType.PICKUP,
    );

    const endpoint = await this.locationService.getOrderLocations(
      order.id,
      OrderLocationType.ENDPOINT,
    );

    const pickupDetails = await this.orderEmitter.getLocationDetails(
      pickup.detailsId,
    );
    const endpointDetails = await this.orderEmitter.getLocationDetails(
      endpoint.detailsId,
    );

    return {
      ...order,
      status: statusHistory[statusHistory.length - 1].status,
      statusHistory,
      details,
      thirdPartyPhoneNumber,
      locations: {
        pickup: {
          ...pickupDetails,
          ...LocationResponseDto.buildDto(pickup),
        },
        endpoint: {
          ...endpointDetails,
          ...LocationResponseDto.buildDto(endpoint),
        },
      },
      photos,
      deliveryPrice: {
        value: priceDetails.total,
        currency: priceDetails.currency,
      },
    };
  }
}
