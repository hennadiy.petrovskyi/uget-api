import { AuthRecord } from './../../auth/entities/auth-record.entity';
import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import {
  Between,
  FindOperator,
  LessThanOrEqual,
  MoreThanOrEqual,
  Not,
  Repository,
} from 'typeorm';

import { Order } from '../entities';
import { LoggerService } from 'src/utils/logger';
import { GetOrderDto, OrderCreateDto } from '../dtos';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    private logger: LoggerService,
    private eventEmitter: EventEmitter2,
  ) {}

  public async findOrderByOwner(
    owner: FindOperator<any>,
    id: number,
  ): Promise<GetOrderDto> {
    const functionName = this.findOrderByUUID.name;
    try {
      const order = await this.orderRepository.findOne({
        where: { owner, id },
      });

      if (!order) {
        return;
      }

      return GetOrderDto.buildDto(order);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getAllUserOrders(owner: FindOperator<any>) {
    const functionName = this.findOrderByUUID.name;
    try {
      const orders = await this.orderRepository.find({
        where: { owner },
      });

      if (!orders) {
        return;
      }

      return orders.map((order) => GetOrderDto.buildDto(order));
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async findTravellerAcceptedOrders(findTravellerAcceptedOrdersDto: {
    tripId: string;
    isDelivered: boolean;
  }) {
    const functionName = this.findTravellerAcceptedOrders.name;
    try {
      const orders = await this.orderRepository.find({
        where: findTravellerAcceptedOrdersDto,
        loadRelationIds: true,
      });

      return orders;
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async findOrderByUUID(uuid: string): Promise<Order> {
    const functionName = this.findOrderByUUID.name;
    try {
      return await this.orderRepository.findOne({
        where: { uuid },
        loadRelationIds: true,
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async findOrderById(id: FindOperator<any>): Promise<Order> {
    const functionName = this.findOrderById.name;
    try {
      return await this.orderRepository.findOne({
        where: { id },
        loadRelationIds: true,
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async updateOrder(updateOrderDto: any) {
    const functionName = this.updateOrder.name;
    try {
      return await this.orderRepository.save(updateOrderDto);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async createOrder(orderCreateDto: OrderCreateDto): Promise<Order> {
    const functionName = this.createOrder.name;

    try {
      await this.orderRepository.save(orderCreateDto);

      return await this.findOrderByUUID(orderCreateDto.uuid);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getAllByParams(owner: number, arrivalDate: string) {
    const functionName = this.getAllByParams.name;

    const modifiedArrivalDate = new Date(arrivalDate);

    try {
      const orders = await this.orderRepository.find({
        where: {
          owner: Not(owner),
          deliveryDate: Between(
            new Date(
              new Date().getFullYear(),
              new Date().getMonth(),
              new Date().getDate(),
            ).toISOString(),
            new Date(
              modifiedArrivalDate.getFullYear(),
              modifiedArrivalDate.getMonth(),
              modifiedArrivalDate.getDate() + 1,
            ).toISOString(),
          ),
        },
        loadRelationIds: true,
      });

      if (!orders) {
        return;
      }

      return orders;
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async updateOrderRequests(orderId, update): Promise<Order> {
    const functionName = this.updateOrderRequests.name;

    try {
      await this.orderRepository.update(orderId, update);

      return await this.findOrderById(orderId);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async deleteOrder(id: number): Promise<void> {
    const functionName = this.deleteOrder.name;

    try {
      await this.orderRepository.delete(id);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
