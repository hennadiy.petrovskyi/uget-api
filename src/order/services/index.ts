export { OrderService } from './order.service';
export { StatusService } from './status.service';
export { LocationService } from './location.service';
export { DetailsService } from './details.service';
export { ThirdPartyNumberService } from './third-party-number.service';
export { OrderHelperService } from './order-helper.service';
export { DeliveryPriceService } from './price.service';
