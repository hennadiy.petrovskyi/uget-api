import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { OrderLocation } from '../entities';
import { LoggerService } from 'src/utils/logger';
import {} from '../dtos';
import { OrderLocationType } from '../enums';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(OrderLocation)
    private locationsRepository: Repository<OrderLocation>,
    private logger: LoggerService,
  ) {}

  public async saveOrderLocation(orderLocationSaveDto: any) {
    const functionName = this.saveOrderLocation.name;

    try {
      await this.locationsRepository.save(orderLocationSaveDto);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getOrderLocations(owner: FindOperator<any>, type) {
    const functionName = this.getOrderLocations.name;
    try {
      return await this.locationsRepository.findOne({
        where: {
          owner,
          type,
        },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async deleteOrderLocation(owner: FindOperator<any>) {
    const locations = await this.locationsRepository.find({
      where: {
        owner,
      },
    });

    const functionName = this.deleteOrderLocation.name;
    try {
      Promise.all(
        locations.map((location) =>
          this.locationsRepository.delete(location.id),
        ),
      );
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
