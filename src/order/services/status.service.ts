import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { Status } from '../entities';
import { LoggerService } from 'src/utils/logger';
import { GetStatusDto } from '../dtos';
import { OrderStatus } from '../enums';

@Injectable()
export class StatusService {
  constructor(
    @InjectRepository(Status)
    private statusRepository: Repository<Status>,
    private logger: LoggerService,
  ) {}

  public async findStatusById(owner: FindOperator<any>) {
    const functionName = this.findStatusById.name;
    try {
      return await this.statusRepository.findOne({ where: { owner } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async findAllOrderStatusesHistory(owner: FindOperator<any>) {
    const functionName = this.findAllOrderStatusesHistory.name;
    try {
      const statuses = await this.statusRepository.find({ where: { owner } });

      return statuses.map((status) => GetStatusDto.buildDto(status));
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async addOrderStatus(statusAddNewDto: any) {
    const functionName = this.addOrderStatus.name;

    try {
      await this.statusRepository.save(statusAddNewDto);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async deleteAcceptedStatus(owner: FindOperator<any>) {
    const functionName = this.deleteAllOrderStatuses.name;

    const statusAccepted = await this.statusRepository.findOne({
      where: { owner, status: OrderStatus.ACCEPTED },
    });

    try {
      this.statusRepository.delete(statusAccepted.id);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async deleteAllOrderStatuses(owner: FindOperator<any>) {
    const functionName = this.deleteAllOrderStatuses.name;

    const statuses = await this.statusRepository.find({ where: { owner } });

    try {
      Promise.all(
        statuses.map((status) => this.statusRepository.delete(status.id)),
      );
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
