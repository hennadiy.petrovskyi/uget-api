import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { Details, Status } from '../entities';
import { LoggerService } from 'src/utils/logger';
import { DetailsCreateDto } from '../dtos';

@Injectable()
export class DetailsService {
  constructor(
    @InjectRepository(Details)
    private detailsRepository: Repository<Details>,
    private logger: LoggerService,
  ) {}

  public async saveOrderDetails(orderDetailsDto: any) {
    const functionName = this.saveOrderDetails.name;

    try {
      await this.detailsRepository.save(orderDetailsDto);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getOrderDetails(owner: FindOperator<any>) {
    const functionName = this.getOrderDetails.name;

    try {
      return DetailsCreateDto.buildDto(
        await this.detailsRepository.findOne({ where: { owner } }),
      );
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async deleteOrderDetails(owner: FindOperator<any>) {
    const functionName = this.deleteOrderDetails.name;
    try {
      const details = await this.detailsRepository.findOne({
        where: { owner },
      });

      await this.detailsRepository.delete(details.id);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
