export enum DeliveryProvider {
  ARAMEX = 'ARX',
  TIME_EXPRESS = 'TES',
  DHL = 'DHL',
}
