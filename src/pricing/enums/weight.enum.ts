export enum WeightOptions {
  A = '0.5',
  B = '1',
  C = '1.5',
  D = '2',
  E = '2.5',
  F = '3',
  G = '3.5',
  H = '4',
  I = '4.5',
  J = '5',
  K = '5.5',
  L = '6',
}
