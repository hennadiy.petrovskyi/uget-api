export enum Currency {
  USD = 'USD',
  AED = 'AED',
  EUR = 'EUR',
}
