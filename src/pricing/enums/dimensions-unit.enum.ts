export enum DimensionUnits {
  CM = 'cm',
  IN = 'in',
}
