import { Injectable } from '@nestjs/common';
import { FindOperator, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { LoggerService } from 'src/utils/logger';
import { ProviderPrice } from '../entities';

import {
  FindPricingByDestinationsAmdWeightDto,
  SaveProviderPrice,
} from '../dtos';
import { DeliveryProvider } from '../enums';

@Injectable()
export class PricingService {
  constructor(
    @InjectRepository(ProviderPrice)
    private pricingRepository: Repository<ProviderPrice>,
    private logger: LoggerService,
  ) {}

  async findPricingByDestinationsAndWeight(
    findPricingByDestinationsDto: FindPricingByDestinationsAmdWeightDto,
  ): Promise<ProviderPrice[]> {
    const functionName = this.findPricingByDestinationsAndWeight.name;

    try {
      return this.pricingRepository.find({
        where: {
          from: findPricingByDestinationsDto.from,
          to: findPricingByDestinationsDto.to,
          weight: findPricingByDestinationsDto.weight,
        },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async findPricingById(id: number): Promise<ProviderPrice> {
    const functionName = this.findPricingById.name;

    try {
      return this.pricingRepository.findOne({
        where: {
          id,
        },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async getAllPricing(): Promise<ProviderPrice[]> {
    const functionName = this.findPricingById.name;

    try {
      return this.pricingRepository.find({
        where: { provider: DeliveryProvider.TIME_EXPRESS },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async saveProviderPriceForTrip(
    saveProviderPrice: SaveProviderPrice,
  ): Promise<void> {
    const functionName = this.saveProviderPriceForTrip.name;

    try {
      await this.pricingRepository.save(saveProviderPrice);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
