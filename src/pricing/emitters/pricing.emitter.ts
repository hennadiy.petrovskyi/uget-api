import { LOCATION_EVENT } from '../../common/events/events-constants';

import { EventEmitter2 } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { FileType } from 'src/file/enums';

@Injectable()
export class PricingEmitter {
  constructor(private eventEmitter: EventEmitter2) {}

  public async getLocationDetailsById(owner: string) {
    const file = await this.eventEmitter.emitAsync(
      LOCATION_EVENT.GET_LOCATION_DETAILS_BY_ID,
      { owner, type: FileType.PROFILE_IMAGE },
    );

    if (file && file.length) {
      return file[0];
    }

    return null;
  }

  public async getCountryCodeById(id: string) {
    const file = await this.eventEmitter.emitAsync(
      LOCATION_EVENT.GET_COUNTRY_CODE_BY_LOCATION_ID,
      id,
    );

    if (file && file.length) {
      return file[0];
    }

    return null;
  }
}
