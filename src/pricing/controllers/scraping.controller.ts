import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  Sse,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { User } from 'src/common/decorators';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';

import {} from '../dtos';
import {} from '../enums';
import { ScrapingService } from '../services';

@ApiBearerAuth()
@ApiTags('Scraping')
@Controller('scraping')
export class ScrapingController {
  constructor(private readonly service: ScrapingService) {}

  @Get()
  async getAll() {}
}
