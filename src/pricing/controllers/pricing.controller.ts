import { Currency } from './../enums/currency.enum';
import { PricingEmitter } from './../emitters/pricing.emitter';
import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  Post,
  Put,
  Query,
  Res,
  Sse,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { User } from 'src/common/decorators';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';

import {} from '../dtos';
import {} from '../enums';
import { PricingService } from '../services';
import { CountryCode } from 'src/common/enums';

@ApiBearerAuth()
@ApiTags('Price')
@Controller('pricing')
export class PricingController {
  constructor(
    private readonly service: PricingService,
    private readonly pricingEmitter: PricingEmitter,
  ) {}

  @Post()
  async saveProviderDeliveryPrice(@Body('') saveProviderPriceDto: any) {
    return await this.service.saveProviderPriceForTrip(saveProviderPriceDto);
  }

  @Post('calculate')
  async calculatePrice(@Body('') saveProviderPriceDto: any) {
    const fromCountryCode = await this.pricingEmitter.getCountryCodeById(
      saveProviderPriceDto.from,
    );

    const providerPrices =
      await this.service.findPricingByDestinationsAndWeight({
        from: fromCountryCode.toUpperCase(),
        to: CountryCode.UAE,
        weight: saveProviderPriceDto.weight,
      });

    const averagePrice =
      providerPrices
        .map((el) => +el.price)
        .reduce((prev, current) => prev + current) / providerPrices.length;

    /**
     * TO ADD :
     * entity for route with percentage of discount for each
     * default is 20%
     *
     */

    const routePriceModifier = 20;
    const total = averagePrice - (averagePrice / 100) * routePriceModifier;

    return { value: total.toFixed(2), currency: Currency.EUR };
  }

  @Get()
  async getAllPricing() {
    const data = await this.service.getAllPricing();

    return data.map((el) => {
      const { id, createdAt, updatedAt, deletedAt, weight, ...rest } = el;

      return {
        ...rest,
        weight: String(weight),
      };
    });
  }
}
