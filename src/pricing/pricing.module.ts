import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import { PricingController, ScrapingController } from './controllers';
import { PricingService, ScrapingService } from './services';
import { ProviderPrice } from './entities';
import { LoggerModule } from 'src/utils/logger';
import { MorganMiddleware } from 'src/common/middlewares/morgan.middleware';
import { PricingEmitter } from './emitters';
import { PricingListener } from './listeners';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProviderPrice]),
    HttpModule,
    LoggerModule,
  ],
  controllers: [ScrapingController, PricingController],
  providers: [ScrapingService, PricingService, PricingEmitter, PricingListener],
  exports: [],
})
export class PriceModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes(PricingController);
  }
}
