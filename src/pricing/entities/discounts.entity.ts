import { Currency } from './../../pricing/enums/currency.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';

import { BaseEntity } from 'src/common/entities';
import {} from '../enums';
import {} from '.';
import { AuthRecord } from 'src/auth/entities';

@Entity({
  name: 'discounts',
})
export class Discount extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @ApiProperty()
  @Expose()
  @Column({ generated: 'uuid' })
  uuid: string;

  @ApiProperty()
  @Expose()
  @Column('int', { nullable: true })
  percentage: number;

  @ApiProperty()
  @Expose()
  @Column('int', { nullable: true })
  amount: number;

  @ApiProperty()
  @Expose()
  @Column('boolean', { default: false })
  isValid: boolean;

  @ApiProperty()
  @Expose()
  @Column('text')
  expireIn: string;
}
