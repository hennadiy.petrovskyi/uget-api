import { WeightOptions } from '../enums/weight.enum';
import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';

import { BaseEntity } from 'src/common/entities';
import {
  Currency,
  DeliveryProvider,
  DimensionUnits,
  WeightUnit,
} from '../enums';
import { CountryCode } from 'src/common/enums';

@Entity({
  name: 'provider_price',
})
export class ProviderPrice extends BaseEntity {
  @ApiProperty()
  @Expose()
  @Column('enum', { enum: DeliveryProvider })
  provider: DeliveryProvider;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: CountryCode })
  from: CountryCode;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: CountryCode, default: CountryCode.UAE })
  to: CountryCode;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: WeightUnit, default: WeightUnit.KG })
  weightUnit: WeightUnit;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: DimensionUnits, default: DimensionUnits.CM })
  dimensionUnit: DimensionUnits;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: Currency, default: Currency.USD })
  currency: Currency;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: WeightOptions })
  weight: WeightOptions;

  @ApiProperty()
  @Expose()
  @Column('int', { nullable: true })
  height: number;

  @ApiProperty()
  @Expose()
  @Column('int', { nullable: true })
  length: number;

  @ApiProperty()
  @Expose()
  @Column('int', { nullable: true })
  width: number;

  @ApiProperty()
  @Expose()
  @Column('text', { nullable: true })
  price: string;
}
