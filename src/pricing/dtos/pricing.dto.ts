import { ApiProperty } from '@nestjs/swagger';
import { Expose, plainToClass } from 'class-transformer';
import { PickType, IntersectionType, OmitType } from '@nestjs/mapped-types';
import { ProviderPrice } from '../entities';

export class FindPricingByDestinationsAmdWeightDto extends PickType(
  ProviderPrice,
  ['from', 'to', 'weight'],
) {
  static buildDto(
    payload: ProviderPrice,
  ): FindPricingByDestinationsAmdWeightDto {
    return plainToClass(FindPricingByDestinationsAmdWeightDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class SaveProviderPrice extends OmitType(ProviderPrice, [
  'createdAt',
  'deletedAt',
  'updatedAt',
  'id',
]) {
  //   static buildDto(payload: ProviderPrice): FindPricingByDestinationsDto {
  //     return plainToClass(FindPricingByDestinationsDto, payload, {
  //       excludeExtraneousValues: true,
  //     });
  //   }
}
