import { Currency } from './../enums/currency.enum';
import { PRICING_EVENTS } from './../../common/events/events-constants';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { ORDER_EVENT } from '../../common/events/events-constants';
import { PricingService } from '../services';
import {} from '../dtos';
import { PricingEmitter } from '../emitters';
import {} from '../enums';

@Injectable()
export class PricingListener {
  constructor(
    private readonly pricingService: PricingService,
    private readonly pricingEmitter: PricingEmitter,
  ) {}

  @OnEvent(PRICING_EVENTS.CALCULATE_ORIGINAL_AVERAGE_PRICE, { promisify: true })
  public async calculateAveragePrice(calculatePriceDto: {
    from: string;
    to: string;
    size: any;
  }) {
    const from = await this.pricingEmitter.getCountryCodeById(
      calculatePriceDto.from,
    );

    const to = await this.pricingEmitter.getCountryCodeById(
      calculatePriceDto.to,
    );

    const providerPrices =
      await this.pricingService.findPricingByDestinationsAndWeight({
        from,
        to,
        weight: calculatePriceDto.size,
      });

    const averagePrice =
      providerPrices
        .map((el) => +el.price)
        .reduce((prev, current) => prev + current) / providerPrices.length;

    /**
     * TO ADD :
     * entity for route with percentage of discount for each
     * default is 20%
     */

    const routePriceModifier = 20;
    const total = averagePrice - (averagePrice / 100) * routePriceModifier;

    return {
      average: averagePrice,
      currency: Currency.EUR,
      routePriceModifier,
      total: total.toFixed(2),
    };
  }
}
