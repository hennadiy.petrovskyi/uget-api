import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import { LocationService } from './services';
import { LocationController } from './controllers';
import { Location } from './entities';
import { LocationListener } from './listeners';

import { LoggerModule } from 'src/utils/logger';
import { MorganMiddleware } from 'src/common/middlewares/morgan.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([Location]), LoggerModule, HttpModule],

  controllers: [LocationController],
  providers: [LocationListener, LocationService],
  exports: [],
})
export class LocationModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes(LocationController);
  }
}
