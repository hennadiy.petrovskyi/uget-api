import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { LOCATION_EVENT } from 'src/common/events';
import { LocationService } from '../services';

@Injectable()
export class LocationListener {
  constructor(private locationService: LocationService) {}

  @OnEvent(LOCATION_EVENT.GET_LOCATION_DETAILS_BY_ID, { promisify: true })
  public async getLocationById(id: number) {
    return await this.locationService.parseLocationId(id);
  }

  @OnEvent(LOCATION_EVENT.GET_COUNTRY_CODE_BY_LOCATION_ID, { promisify: true })
  public async getCountryCode(id: string) {
    const googlePlace = await this.locationService.googlePlacesFindByPlaceId(
      id,
    );

    console.log(googlePlace[0].short_name);

    if (!googlePlace.length) {
      return;
    }

    return googlePlace[0].short_name;
  }

  @OnEvent(LOCATION_EVENT.SAVE_LOCATION_IN_DB, { promisify: true })
  public async saveLocationInDb(saveLocationDetailsDto: any) {
    const location = await this.locationService.findLocationByLocationId(
      saveLocationDetailsDto.locationId,
    );

    if (location) {
      return location;
    }

    const countryCode = await this.locationService.getCountryCode(
      saveLocationDetailsDto.countryId,
    );

    return await this.locationService.saveLocationInDb({
      countryCode,
      locationId: saveLocationDetailsDto.locationId,
    });
  }
}
