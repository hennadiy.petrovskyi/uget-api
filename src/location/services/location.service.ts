import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  Client,
  PlaceAutocompleteType,
} from '@googlemaps/google-maps-services-js';

import { LoggerService } from '../../utils/logger/app.logger';
import { Location } from '../entities';
import { FindCityDto, FindCountryDto, SaveLocationDto } from '../dtos';
import { LocationType } from '../enums';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Location)
    private locationRepository: Repository<Location>,
    private httpService: HttpService,
    private logger: LoggerService,
  ) {}

  public async googlePlacesFindByPlaceId(id: string) {
    const requestUrl = `https://maps.googleapis.com/maps/api/place/details/json?place_id=${id}&fields=address_components&key=${process.env.GOOGLE_MAPS_API_KEY}`;

    try {
      const response = await firstValueFrom(this.httpService.get(requestUrl));

      if (response.status === 200 && response.data.status === 'OK') {
        return response.data.result.address_components;
      }
    } catch (error) {
      this.logger.log(error);
    }
  }

  async getCountryCode(id: string) {
    const location = await this.googlePlacesFindByPlaceId(id);

    if (location.length) {
      return location[0].short_name.toLowerCase();
    }
  }

  async googlePlacesFindByInput(findLocationDto: any, type: LocationType) {
    const googleMaps = new Client({});

    const defaultParams: any = {
      key: process.env.GOOGLE_MAPS_API_KEY,
      ...findLocationDto,
    };

    if (findLocationDto.type === LocationType.COUNTRY) {
      defaultParams.types = PlaceAutocompleteType.regions;
    }

    console.log(defaultParams, process.env.GOOGLE_MAPS_API_KEY);

    try {
      const response = await googleMaps.placeAutocomplete({
        params: defaultParams,
        timeout: 1000, // milliseconds
      });

      console.log('googlePlacesFindByInput', defaultParams, response);

      const predictions = response.data.predictions;

      if (!predictions.length) {
        return [];
      }

      return this.filterResults(type, predictions);
    } catch (error) {
      this.logger.log('googlePlacesFindByInput' + error);
    }

    return [];
  }

  async filterResults(type, suggestions: any) {
    let results;

    results = [];

    if (type === LocationType.COUNTRY) {
      results = suggestions.filter((location) =>
        location.types.includes(LocationType.COUNTRY),
      );
    }

    if (type === LocationType.CITY) {
      results = suggestions.filter((location) =>
        location.types.includes('locality'),
      );
    }

    if (!results.length) {
      return [];
    }

    return results.map((location: any) => ({
      id: location.place_id,
      label: location.structured_formatting.main_text,
    }));
  }

  public async saveLocationInDb(saveLocationDto: SaveLocationDto) {
    try {
      await this.locationRepository.save(saveLocationDto);

      return await this.findLocationByLocationId(saveLocationDto.locationId);
    } catch (error) {
      this.logger.log(error);
    }
  }

  public async findLocationByLocationId(locationId: string): Promise<Location> {
    try {
      return this.locationRepository.findOne({ where: { locationId } });
    } catch (error) {
      this.logger.log(error);
    }
  }

  public async findLocationById(id: number): Promise<Location> {
    try {
      return this.locationRepository.findOne({ where: { id } });
    } catch (error) {
      this.logger.log(error);
    }
  }

  public async parseLocationId(id: number) {
    const location = await this.findLocationById(id);

    const locationData = await this.googlePlacesFindByPlaceId(
      location.locationId,
    );

    const country = locationData.find((details) =>
      details.types.includes('country'),
    );

    const city = locationData.find((details) =>
      details.types.includes('locality'),
    );

    if (country && city) {
      return {
        country: { id: '', label: country.long_name },
        city: {
          id: location.locationId,
          label: city.short_name,
        },
      };
    }
  }

  public async test(id: string) {
    const locationData = await this.googlePlacesFindByPlaceId(id);

    const country = locationData.find((details) =>
      details.types.includes('country'),
    );

    const city = locationData.find((details) =>
      details.types.includes('locality'),
    );

    const searchParams = {
      language: 'en',
      input: country.long_name,
      types: PlaceAutocompleteType.regions,
    };

    // const countryDetails =

    return await this.googlePlacesFindByInput(
      searchParams,
      LocationType.COUNTRY,
    );

    if (country && city) {
      return {
        country: { label: country.long_name },
        city: {
          id: '',
          label: city.short_name,
        },
      };
    }
  }
}
