import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Unique } from 'typeorm';
import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

import { BaseEntity } from 'src/common/entities';

@Entity({
  name: 'location',
})
@Unique(['locationId'])
export class Location extends BaseEntity {
  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  countryCode: string;

  @Expose()
  @ApiProperty()
  @IsString()
  @Column('text')
  locationId: string;
}
