import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { PickType } from '@nestjs/mapped-types';
import { Location } from '../entities';
import { LocationType } from '../enums';

export class SaveLocationDto extends PickType(Location, [
  'countryCode',
  'locationId',
]) {}

export class FindCountryDto {
  @Expose()
  @ApiProperty()
  language: string;

  @Expose()
  @ApiProperty()
  input: string;

  @Expose()
  @ApiProperty()
  type: LocationType;
}

export class FindCityDto {
  @Expose()
  @ApiProperty()
  language: string;

  @Expose()
  @ApiProperty()
  input: string;

  @Expose()
  @ApiProperty()
  countryId: string;

  @Expose()
  @ApiProperty()
  type: LocationType;
}
