import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  Res,
  Headers,
  UnauthorizedException,
  ForbiddenException,
  Get,
  UseGuards,
  NotFoundException,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

import { LocationService } from '../services';
import { FindCityDto, FindCountryDto } from '../dtos';
import { PlaceAutocompleteType } from '@googlemaps/google-maps-services-js';

@ApiTags('Location')
@Controller('location')
export class LocationController {
  constructor(private service: LocationService) {}

  @Post('find/country')
  async findCountry(
    @Body() findCountryDto: FindCountryDto,
    @Res() response: Response,
  ) {
    const searchParams = {
      language: findCountryDto.language,
      input: findCountryDto.input,
      types: PlaceAutocompleteType.regions,
    };

    const countriesList = await this.service.googlePlacesFindByInput(
      searchParams,
      findCountryDto.type,
    );

    return response.status(200).json(countriesList);
  }

  @Post('find/city')
  async findCity(@Body() findCityDto: FindCityDto, @Res() response: Response) {
    const countryCode = await this.service.getCountryCode(
      findCityDto.countryId,
    );

    const searchParams = {
      language: findCityDto.language,
      input: findCityDto.input,
      types: PlaceAutocompleteType.cities,
      components: [`country:${countryCode}`],
    };

    if (!countryCode) {
      delete searchParams.components;
    }

    const citiesList = await this.service.googlePlacesFindByInput(
      searchParams,
      findCityDto.type,
    );

    return response.status(200).json(citiesList);
  }

  @Get('test')
  async getById() {
    // const result = await this.service.googlePlacesFindByPlaceId(
    //   'ChIJRcbZaklDXz4RYlEphFBu5r0',
    // );
    // return await this.service.test('ChIJRcbZaklDXz4RYlEphFBu5r0');
  }
}

//[ { id: 'ChIJjw5wVMHZ0UAREED2iIQGAQA', label: 'Ukraine' } ]
//  { id: 'ChIJBUVa4U7P1EAR_kYBF9IxSXY', label: 'Kiev' },
//[ { id: 'ChIJvRKrsd9IXj4RpwoIwFYv0zM', label: 'UAE' } ]
//  { id: 'ChIJRcbZaklDXz4RYlEphFBu5r0', label: 'Dubai' }
