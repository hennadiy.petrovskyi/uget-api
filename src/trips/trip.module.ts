import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import {
  TripService,
  TripLocationService,
  TripHelperService,
} from './services';
import { TripController } from './controllers';
import { Trip, TripLocation } from './entities';
import { LoggerModule } from 'src/utils/logger';
import { TripEmitter } from './emitters';
import { MorganMiddleware } from 'src/common/middlewares/morgan.middleware';
import { TripListener } from './listeners';

@Module({
  imports: [
    TypeOrmModule.forFeature([Trip, TripLocation]),
    HttpModule,
    LoggerModule,
  ],
  controllers: [TripController],
  providers: [
    TripService,
    TripLocationService,
    TripEmitter,
    TripListener,
    TripHelperService,
  ],
  exports: [],
})
export class TripModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes(TripController);
  }
}
