import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';

import { BaseEntity } from 'src/common/entities';
import { AuthRecord } from 'src/auth/entities';

@Entity({
  name: 'trip',
})
export class Trip extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => AuthRecord)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  owner: AuthRecord;

  @ApiProperty()
  @Expose()
  @Column('text')
  uuid: string;

  @ApiProperty()
  @Expose()
  @Column('text')
  departureDate: string;

  @ApiProperty()
  @Expose()
  @Column('text')
  arrivalDate: string;

  @ApiProperty()
  @Expose()
  @Column('text')
  description: string;

  @ApiProperty()
  @Expose()
  @Column('text', { array: true, default: [] })
  incomingRequests: string[];

  @ApiProperty()
  @Expose()
  @Column('text', { array: true, default: [] })
  outgoingRequests: string[];

  @ApiProperty()
  @Expose()
  @Column('text', { array: true, default: [] })
  accepted: string[];

  @ApiProperty()
  @Expose()
  @Column('text', { array: true, default: [] })
  inProgress: string[];

  @ApiProperty()
  @Expose()
  @Column('text', { array: true, default: [] })
  delivered: string[];
}
