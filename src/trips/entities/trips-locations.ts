import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';

import { BaseEntity } from 'src/common/entities';
import { TripLocationType } from '../enums';
import { Trip } from '.';

@Entity({
  name: 'trip_location',
})
export class TripLocation extends BaseEntity {
  @Expose()
  @ApiProperty()
  @Index()
  @ManyToOne(() => Trip)
  @JoinColumn({ name: 'tripId', referencedColumnName: 'id' })
  owner: Trip;

  @ApiProperty()
  @Expose()
  @Column('enum', { enum: TripLocationType })
  type: TripLocationType;

  @ApiProperty()
  @Expose()
  @Column('int')
  detailsId: number;

  @ApiProperty()
  @Expose()
  @Column('text')
  area: string;
}
