import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindOperator, Repository } from 'typeorm';

import { TripLocation } from '../entities';
import { LoggerService } from 'src/utils/logger';
import { TripLocationType } from '../enums';
// import {} from '../dtos';

@Injectable()
export class TripLocationService {
  constructor(
    @InjectRepository(TripLocation)
    private locationsRepository: Repository<TripLocation>,
    private logger: LoggerService,
  ) {}

  public async saveOrderLocation(orderLocationSaveDto: any) {
    const functionName = this.saveOrderLocation.name;

    try {
      await this.locationsRepository.save(orderLocationSaveDto);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getTripLocations(
    owner: FindOperator<any>,
    type: TripLocationType,
  ) {
    const functionName = this.getTripLocations.name;
    try {
      return await this.locationsRepository.findOne({
        where: {
          owner,
          type,
        },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async deleteOrderLocation(owner: FindOperator<any>) {
    const locations = await this.locationsRepository.find({
      where: {
        owner,
      },
    });

    const functionName = this.deleteOrderLocation.name;
    try {
      Promise.all(
        locations.map((location) =>
          this.locationsRepository.delete(location.id),
        ),
      );
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  async deleteTripLocation(owner: FindOperator<any>) {
    const functionName = this.deleteTripLocation.name;
    try {
      const tripLocations = await this.locationsRepository.find({
        where: { owner },
      });

      Promise.all(
        tripLocations.map((el) => this.locationsRepository.delete(el.id)),
      );
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
