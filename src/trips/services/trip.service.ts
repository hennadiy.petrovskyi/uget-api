import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { LessThanOrEqual, Repository, Not, FindOperator } from 'typeorm';

import { Trip } from '../entities';
import { LoggerService } from 'src/utils/logger';

@Injectable()
export class TripService {
  constructor(
    @InjectRepository(Trip)
    private tripRepository: Repository<Trip>,
    private logger: LoggerService,
  ) {}

  public async findTripByOwner(
    owner: FindOperator<any>,
    id: number,
  ): Promise<any> {
    const functionName = this.findTripByOwner.name;
    try {
      const order = await this.tripRepository.findOne({
        where: { owner, id },
        loadRelationIds: true,
      });

      if (!order) {
        return;
      }

      return order;
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async findTripByID(id: number): Promise<Trip> {
    const functionName = this.findTripByOwner.name;
    try {
      const order = await this.tripRepository.findOne({
        where: { id },
        loadRelationIds: true,
      });

      if (!order) {
        return;
      }

      return order;
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getAllUserTrips(owner: any) {
    const functionName = this.getAllUserTrips.name;
    try {
      const orders = await this.tripRepository.find({
        where: { owner },
        loadRelationIds: true,
      });

      if (!orders) {
        return;
      }

      return orders.map((order) => order);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async updateTrip(id: number, update: any): Promise<Trip> {
    const functionName = this.updateTrip.name;
    try {
      await this.tripRepository.update(id, {
        ...update,
      });

      return this.findTripByID(id);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async getAllByOrderParams(owner: number, deliveryDate: string) {
    const functionName = this.getAllByOrderParams.name;

    try {
      const trips = await this.tripRepository.find({
        where: {
          owner: Not(owner),
          departureDate: LessThanOrEqual(new Date(deliveryDate).toISOString()),
        },
        loadRelationIds: true,
      });

      if (!trips) {
        return;
      }

      return trips.map((trip) => trip);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async findTripByUUID(uuid: string): Promise<Trip> {
    const functionName = this.findTripByUUID.name;
    try {
      return await this.tripRepository.findOne({
        where: { uuid },
        loadRelationIds: true,
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async createTrip(orderCreateDto: any): Promise<Trip> {
    const functionName = this.createTrip.name;

    try {
      await this.tripRepository.save(orderCreateDto);

      return await this.findTripByUUID(orderCreateDto.uuid);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async deleteTrip(id: number): Promise<void> {
    const functionName = this.deleteTrip.name;

    try {
      await this.tripRepository.delete(id);
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }
}
