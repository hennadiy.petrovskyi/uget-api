import { forwardRef, Inject, Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { AuthRecord } from 'src/auth/entities';

import { FindOperator, Repository } from 'typeorm';
import { TripLocationService, TripService } from '.';

import { LoggerService } from '../../utils/logger';
import { GetTripByOrderDto, PublishTripDto, TripCreateDto } from '../dtos';
import { TripEmitter } from '../emitters';
import { Trip } from '../entities';
import { TripLocationType, TripOrderStatus } from '../enums';

@Injectable()
export class TripHelperService {
  constructor(
    @Inject(forwardRef(() => TripService))
    private readonly tripService: TripService,
    @Inject(forwardRef(() => TripLocationService))
    private readonly locationService: TripLocationService,
    private tripEmitter: TripEmitter,
    private logger: LoggerService,
  ) {}

  async handleFilterTripsByParams(
    userId: number,
    filterParams: any,
  ): Promise<Trip[]> {
    const trips = await this.tripService.getAllByOrderParams(
      userId,
      filterParams.deliveryDate,
    );

    const filtered = await Promise.all(
      trips.map(async (trip) => {
        const from = await this.getLocationsDetails(
          trip.id as any,
          TripLocationType.FROM,
        );
        const to = await this.getLocationsDetails(
          trip.id as any,
          TripLocationType.TO,
        );

        if (
          from.city.id === filterParams.from &&
          to.city.id === filterParams.to
        ) {
          return trip;
        }
      }),
    );

    return Promise.all(filtered.filter((trip) => trip));
  }

  async handlePublishTrip(publishTripDto: PublishTripDto, user: AuthRecord) {
    const trip = await this.tripService.createTrip({
      ...TripCreateDto.buildDto(publishTripDto),
      owner: user,
    });
    const pickupLocationDetailsId = await this.tripEmitter.saveLocationDetails({
      countryId: publishTripDto.locations.from.country,
      locationId: publishTripDto.locations.from.city,
    });
    const endpointLocationDetailsId =
      await this.tripEmitter.saveLocationDetails({
        countryId: publishTripDto.locations.to.country,
        locationId: publishTripDto.locations.to.city,
      });
    const pickupLocationDto: any = {
      owner: trip,
      ...publishTripDto.locations.from,
      detailsId: pickupLocationDetailsId,
      type: TripLocationType.FROM,
    };

    const endpointLocationDto: any = {
      owner: trip,
      ...publishTripDto.locations.to,
      detailsId: endpointLocationDetailsId,
      type: TripLocationType.TO,
    };

    await this.locationService.saveOrderLocation(pickupLocationDto);
    await this.locationService.saveOrderLocation(endpointLocationDto);
  }

  async getLocationsDetails(
    tripId: FindOperator<any>,
    locationType: TripLocationType,
  ) {
    const tripLocations = await this.locationService.getTripLocations(
      tripId,
      locationType,
    );

    if (!tripLocations) {
      return;
    }

    const details = await this.tripEmitter.getLocationDetails(
      tripLocations.detailsId,
    );

    return {
      ...details,
      area: tripLocations.area,
    };
  }

  async buildTripForOrderDto(trip, orderUUID) {
    const from = await this.getLocationsDetails(trip.id, TripLocationType.FROM);
    const to = await this.getLocationsDetails(trip.id, TripLocationType.TO);

    const owner = await this.tripEmitter.getUserDetails(trip.owner);

    const profileImage = await this.tripEmitter.getUserProfileImage(trip.owner);

    function handleStatus() {
      if (trip.accepted.includes(orderUUID)) {
        return TripOrderStatus.ACCEPTED;
      }

      if (trip.incomingRequests.includes(orderUUID)) {
        return TripOrderStatus.REQUEST_OUTGOING;
      }

      if (trip.outgoingRequests.includes(orderUUID)) {
        return TripOrderStatus.REQUEST_INCOMING;
      }

      return TripOrderStatus.AVAILABLE;
    }

    const status = handleStatus();

    return {
      ...GetTripByOrderDto.buildDto(trip),
      from,
      to,
      status,
      owner: { ...owner, profileImage },
    };
  }

  async buildTripDto(trip) {
    const from = await this.getLocationsDetails(trip.id, TripLocationType.FROM);
    const to = await this.getLocationsDetails(trip.id, TripLocationType.TO);

    const { inProgress, accepted, delivered } = trip;
    const orders = inProgress.concat(accepted).concat(delivered);

    const earnings = await this.tripEmitter.getOrdersDeliveryEarnings(orders);

    const handleStatus = () => {
      const currentTime = new Date().toISOString().substring(0, 10);
      const departureDate = new Date(trip.departureDate)
        .toISOString()
        .substring(0, 10);

      if (currentTime >= departureDate && !trip.inProgress.length) {
        return 'past';
      }

      if (currentTime >= departureDate && trip.inProgress.length) {
        return 'active';
      }

      return 'upcoming';
    };

    return {
      ...trip,
      earnings,
      status: handleStatus(),
      locations: {
        from,
        to,
      },
    };
  }

  async handleDeleteTrip(id: FindOperator<any>) {
    await this.locationService.deleteTripLocation(id);

    await this.tripService.deleteTrip(id as any);
  }
}
