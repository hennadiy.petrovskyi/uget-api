import { ORDER_EVENT } from 'src/common/events';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { TRIP_EVENT } from './../../common/events/events-constants';
import { TripService, TripHelperService } from '../services';
import { TripEmitter } from '../emitters';
import {} from '../dtos';
import { SocketEvents } from 'src/socket/enums/events';
import { UserDeliveryRole } from 'src/user/enums';

@Injectable()
export class TripListener {
  constructor(
    private readonly tripService: TripService,
    private readonly tripEmitter: TripEmitter,
    private readonly helperService: TripHelperService,
  ) {}

  @OnEvent(TRIP_EVENT.ADD_INCOMING_REQUEST, { promisify: true })
  public async tripAddOutgoingRequest(dto: any) {
    console.log('TRIP_EVENT.ADD_INCOMING_REQUEST');

    const trip = await this.tripService.findTripByUUID(dto.tripUUID);

    if (trip.incomingRequests.includes(dto.orderUUID)) {
      return;
    }

    trip.incomingRequests.push(dto.orderUUID);

    await this.tripService.updateTrip(trip.id, {
      incomingRequests: trip.incomingRequests,
    });

    const owner = await this.tripEmitter.getAuthRecordUUID(trip.owner);

    await this.tripEmitter.sendSocketMessage({
      uuid: owner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_TRIPS },
    });

    await this.tripEmitter.sendSocketMessage({
      uuid: 'order_' + dto.order.uuid,
      payload: { data: dto.order.update },
    });

    // await this.tripEmitter.sendIncomingRequestNotification({
    //   to: trip.owner,w
    //   from: dto.from,
    // });
  }

  @OnEvent(TRIP_EVENT.CANCEL_INCOMING_REQUEST, { promisify: true })
  public async tripCancelIncomingRequest(dto: any) {
    const trip = await this.tripService.findTripByUUID(dto.tripUUID);

    if (!trip.incomingRequests.includes(dto.orderUUID)) {
      return;
    }

    await this.tripService.updateTrip(trip.id, {
      incomingRequests: trip.incomingRequests.filter(
        (orderUUID) => orderUUID !== dto.orderUUID,
      ),
    });

    console.log('TRIP_EVENT.CANCEL_INCOMING_REQUEST');

    const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);

    await this.tripEmitter.sendSocketMessage({
      uuid: tripOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_TRIPS },
    });

    await this.tripEmitter.sendSocketMessage({
      uuid: 'order_' + dto.order.uuid,
      payload: { data: dto.order.update },
    });
  }

  @OnEvent(TRIP_EVENT.CANCEL_OUTGOING_REQUEST, { promisify: true })
  public async tripCancelOutgoingRequest(dto: any) {
    const trip = await this.tripService.findTripByUUID(dto.tripUUID);

    if (!trip.outgoingRequests.includes(dto.orderUUID)) {
      return;
    }

    await this.tripService.updateTrip(trip.id, {
      outgoingRequests: trip.outgoingRequests.filter(
        (orderUUID) => orderUUID !== dto.orderUUID,
      ),
    });

    console.log('TRIP_EVENT.CANCEL_OUTGOING_REQUEST');

    const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);

    await this.tripEmitter.sendSocketMessage({
      uuid: tripOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_TRIPS },
    });

    await this.tripEmitter.sendSocketMessage({
      uuid: 'order_' + dto.order.uuid,
      payload: { data: dto.order.update },
    });
  }

  @OnEvent(TRIP_EVENT.ACCEPT_DELIVERY_BY_ORDER_OWNER, { promisify: true })
  public async setOrderAcceptedByTraveler(dto: any) {
    const trip = await this.tripService.findTripByUUID(dto.tripUUID);

    if (!trip.outgoingRequests.includes(dto.orderUUID)) {
      return;
    }

    await this.tripService.updateTrip(trip.id, {
      outgoingRequests: trip.outgoingRequests.filter(
        (orderUUID) => orderUUID !== dto.orderUUID,
      ),
      accepted: trip.accepted.concat([dto.orderUUID]),
    });

    console.log('TRIP_EVENT.ACCEPT_DELIVERY_BY_ORDER_OWNER');

    const owner = await this.tripEmitter.getAuthRecordUUID(trip.owner);

    await this.tripEmitter.sendSocketMessage({
      uuid: owner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_TRIPS },
    });

    await this.tripEmitter.sendSocketMessage({
      uuid: 'order_' + dto.order.uuid,
      payload: { data: dto.order.update },
    });

    // await this.tripEmitter.sendAcceptedRequestNotification({
    //   to: trip.owner,
    //   from: dto.from,
    // });
  }

  @OnEvent(TRIP_EVENT.CANCEL_ACCEPTED_REQUEST, { promisify: true })
  public async tripCancelAcceptedRequest(dto: any) {
    const trip = await this.tripService.findTripByUUID(dto.tripUUID);

    if (!trip.accepted.includes(dto.orderUUID)) {
      return;
    }

    await this.tripService.updateTrip(trip.id, {
      accepted: trip.accepted.filter(
        (orderUUID) => orderUUID !== dto.orderUUID,
      ),
    });

    console.log('TRIP_EVENT.CANCEL_ACCEPTED_REQUEST');

    const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);

    await this.tripEmitter.sendSocketMessage({
      uuid: tripOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_TRIPS },
    });

    await this.tripEmitter.sendSocketMessage({
      uuid: 'order_' + dto.order.uuid,
      payload: { data: dto.order.update },
    });
  }

  @OnEvent(TRIP_EVENT.GET_USER_TRIPS, { promisify: true })
  public async getAllUserTrips(owner: number) {
    const trips = await this.tripService.getAllUserTrips(owner as any);

    if (!trips) {
      return [];
    }

    return Promise.all(trips.map((el) => this.helperService.buildTripDto(el)));
  }

  @OnEvent(ORDER_EVENT.START_DELIVERY, { promisify: true })
  public async startDelivery(dto: any) {
    const trip = await this.tripService.findTripByUUID(dto.tripUUID);

    if (!trip.accepted.includes(dto.orderUUID)) {
      return;
    }

    await this.tripService.updateTrip(trip.id, {
      accepted: trip.outgoingRequests.filter(
        (orderUUID) => orderUUID !== dto.orderUUID,
      ),
      inProgress: trip.inProgress.concat([dto.orderUUID]),
    });

    const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
    const orderOwner = await this.tripEmitter.getAuthRecordUUID(dto.to);

    await this.tripEmitter.sendSocketMessage({
      uuid: tripOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_TRIPS },
    });

    console.log(orderOwner);

    await this.tripEmitter.sendSocketMessage({
      uuid: orderOwner.uuid,
      payload: { action: SocketEvents.UPDATE_MY_ORDERS },
    });

    // await this.tripEmitter.sendOrderPickedUpNotification({
    //   to: dto.to,
    //   by: dto.by,
    //   orderUUID: dto.orderUUID,
    // });
  }

  @OnEvent(ORDER_EVENT.FINISH_DELIVERY, { promisify: true })
  public async finishDelivery(dto: any) {
    const trip = await this.tripService.findTripByUUID(dto.tripUUID);

    if (!trip.inProgress.includes(dto.orderUUID)) {
      return;
    }

    await this.tripService.updateTrip(trip.id, {
      inProgress: trip.inProgress.filter(
        (orderUUID) => orderUUID !== dto.orderUUID,
      ),
      delivered: trip.delivered.concat([dto.orderUUID]),
    });

    console.log('ORDER_EVENT.FINISH_DELIVERY');

    const tripOwner = await this.tripEmitter.getAuthRecordUUID(trip.owner);
    const orderOwner = await this.tripEmitter.getAuthRecordUUID(dto.to);

    await this.tripEmitter.sendSocketMessage({
      uuid: tripOwner.uuid,
      payload: {
        action: SocketEvents.UPDATE_MY_TRIPS,
      },
    });

    await this.tripEmitter.sendSocketMessage({
      uuid: orderOwner.uuid,
      payload: {
        action: SocketEvents.UPDATE_MY_ORDERS,
      },
    });

    await this.tripEmitter.addUnratedUserRating({
      owner: tripOwner,
      orderId: dto.orderUUID,
      role: UserDeliveryRole.TRAVELLER,
      ratedBy: orderOwner.id,
    });

    await this.tripEmitter.addUnratedUserRating({
      owner: orderOwner,
      orderId: dto.orderUUID,
      role: UserDeliveryRole.SENDER,
      ratedBy: tripOwner.id,
    });

    // await this.tripEmitter.sendOrderDeliveredNotification({
    //   to: dto.to,
    //   by: dto.by,
    //   orderUUID: dto.orderUUID,
    // });
  }
}
