export enum CancelEventType {
  OUTGOING = 'outgoing',
  INCOMING = 'incoming',
  ACCEPTED = 'accepted',
}
