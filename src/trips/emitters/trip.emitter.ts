import { AuthRecord } from './../../auth/entities/auth-record.entity';
import {
  AUTH_EVENTS,
  FILE_EVENTS,
  ORDER_EVENT,
  PUSH_NOTIFICATION_EVENTS,
  SOCKET_EVENTS,
  USER_EVENTS,
} from './../../common/events/events-constants';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { LOCATION_EVENT } from 'src/common/events';
import { CancelEventType } from '../enums';
import { FileType } from 'src/file/enums';

@Injectable()
export class TripEmitter {
  constructor(private eventEmitter: EventEmitter2) {}

  public async getLocationDetails(id: number) {
    const locationDetails = await this.eventEmitter.emitAsync(
      LOCATION_EVENT.GET_LOCATION_DETAILS_BY_ID,
      id,
    );

    if (locationDetails.length) {
      return locationDetails[0];
    }
  }

  public async saveLocationDetails(saveLocationDetailsDto: any) {
    const locationDetails = await this.eventEmitter.emitAsync(
      LOCATION_EVENT.SAVE_LOCATION_IN_DB,
      saveLocationDetailsDto,
    );

    if (locationDetails.length) {
      return locationDetails[0].id;
    }
  }

  public async addOrderIncomingRequest(orderIncomingRequest: any) {
    await this.eventEmitter.emitAsync(
      ORDER_EVENT.ADD_INCOMING_REQUEST,
      orderIncomingRequest,
    );
  }

  public async acceptOrderDelivery(acceptDeliveryRequestDto: any) {
    await this.eventEmitter.emitAsync(
      ORDER_EVENT.ACCEPT_DELIVERY_BY_TRAVELER,
      acceptDeliveryRequestDto,
    );
  }

  public async cancelOrderDelivery(
    type: CancelEventType,
    cancelOrderDeliveryDto: any,
  ) {
    const eventsTypes = {
      incoming: ORDER_EVENT.CANCEL_INCOMING_REQUEST,
      outgoing: ORDER_EVENT.CANCEL_OUTGOING_REQUEST,
      accepted: ORDER_EVENT.CANCEL_ACCEPTED_REQUEST,
    };

    console.log('cancelOrderDelivery', cancelOrderDeliveryDto);

    await this.eventEmitter.emitAsync(
      eventsTypes[type],
      cancelOrderDeliveryDto,
    );
  }

  public async requestOwnerUUID(id: any) {
    const result = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_ID,
      id,
    );

    if (!result.length) {
      return;
    }

    return result[0];
  }

  public async getUserDetails(id: number) {
    const userDetails = await this.eventEmitter.emitAsync(
      USER_EVENTS.GET_USER_DETAILS_BY_OWNER_ID,
      id,
    );

    if (!userDetails.length) {
      return;
    }

    const { firstName, lastName, rating, profileImage } = userDetails[0];

    return { fullName: firstName + ' ' + lastName, rating, profileImage };
  }

  public async getUserProfileImage(ownerId: number) {
    const owner = (await this.getAuthRecordUUID(ownerId)) as AuthRecord;

    if (!owner) {
      return;
    }

    const file = await this.eventEmitter.emitAsync(
      FILE_EVENTS.GET_FILE_BY_TYPE,
      { owner: owner.uuid, type: FileType.PROFILE_IMAGE },
    );

    if (file && file.length) {
      return file[0];
    }

    return null;
  }

  async getAuthRecordUUID(id: any) {
    const result = await this.eventEmitter.emitAsync(
      AUTH_EVENTS.GET_USER_BY_ID,
      id,
    );

    if (result && result.length) {
      return result[0];
    }
  }

  async sendIncomingRequestNotification(dto: { from: string; to: any }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.TRIP_REQUEST,
      dto,
    );
  }

  async sendAcceptedRequestNotification(dto: { from: string; to: any }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.TRIP_REQUEST_ACCEPTED,
      dto,
    );
  }

  async sendOrderVerifyPickedUpNotification(dto: {
    orderUUID: string;
    to: any;
    by: number;
  }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP_VERIFICATION,
      dto,
    );
  }

  async sendOrderPickedUpNotification(dto: {
    orderUUID: string;
    to: any;
    by: number;
  }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.ORDER_PICK_UP,
      dto,
    );
  }

  async sendVerifyOrderDeliveryNotification(dto: {
    orderUUID: string;
    to: any;
    by: number;
  }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.DELIVERY_VERIFICATION,
      dto,
    );
  }

  async sendOrderDeliveredNotification(dto: {
    orderUUID: string;
    to: any;
    by: number;
  }) {
    await this.eventEmitter.emitAsync(
      PUSH_NOTIFICATION_EVENTS.ORDER_DELIVERED,
      dto,
    );
  }

  async getOrdersDeliveryEarnings(orders: string[]) {
    const response = await this.eventEmitter.emitAsync(
      ORDER_EVENT.GET_EARNINGS,
      orders,
    );

    if (!response.length) {
      return 0;
    }

    return response[0];
  }

  async sendSocketMessage(dto: { uuid: string; payload: any }) {
    await this.eventEmitter.emitAsync(SOCKET_EVENTS.SEND_MESSAGE, dto);
  }

  async addUnratedUserRating(dto: any) {
    const result = await this.eventEmitter.emitAsync(
      USER_EVENTS.ADD_NEW_UNRATED_RATE,
      dto,
    );

    if (result && result.length) {
      return result[0];
    }
  }
}
