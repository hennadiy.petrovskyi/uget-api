import { PickType, IntersectionType, OmitType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Expose, plainToClass, Type } from 'class-transformer';

import { Trip } from '../entities';
import {} from '../enums';
import { LocationCreateDto } from '.';
import { AuthRecord } from 'src/auth/entities';

export class TripCreateDto extends PickType(Trip, [
  'uuid',
  'departureDate',
  'arrivalDate',
  'description',
]) {
  @ApiProperty()
  @Expose()
  owner: AuthRecord;

  static buildDto(payload: PublishTripDto): TripCreateDto {
    return plainToClass(TripCreateDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class PublishTripDto extends PickType(Trip, [
  'uuid',
  'departureDate',
  'arrivalDate',
  'description',
]) {
  @ApiProperty()
  @Expose()
  locations: {
    from: LocationCreateDto;
    to: LocationCreateDto;
  };
}

export class GetTripByOrderDto extends PickType(Trip, [
  'uuid',
  'departureDate',
  'arrivalDate',
  'description',
]) {
  static buildDto(payload: Trip): GetTripByOrderDto {
    return plainToClass(GetTripByOrderDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class OrderDeliveryActionsRequestDto {
  @ApiProperty()
  @Expose()
  orderUUID: string;

  @ApiProperty()
  @Expose()
  tripUUID: string;

  static buildDto(payload: any): OrderDeliveryActionsRequestDto {
    return plainToClass(OrderDeliveryActionsRequestDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}
