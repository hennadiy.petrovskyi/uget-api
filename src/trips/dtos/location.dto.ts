import { Expose, plainToClass } from 'class-transformer';
import { PickType } from '@nestjs/mapped-types';
import { TripLocation } from '../entities';

export class LocationCreateDto extends PickType(TripLocation, ['area']) {
  @Expose()
  country: string;

  @Expose()
  city: string;
}
