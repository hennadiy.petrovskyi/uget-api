import { AuthRecord } from './../../auth/entities/auth-record.entity';
import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  forwardRef,
  Get,
  HttpException,
  Inject,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Res,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import {
  TripService,
  TripLocationService,
  TripHelperService,
} from '../services';
import { TripEmitter } from '../emitters/trip.emitter';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { User } from 'src/common/decorators';
import { CancelEventType, TripLocationType, TripOrderStatus } from '../enums';
import {
  PublishTripDto,
  TripCreateDto,
  GetTripByOrderDto,
  OrderDeliveryActionsRequestDto,
} from '../dtos';

import { Trip } from '../entities';

@ApiTags('Trips')
@Controller('trips')
export class TripController {
  constructor(
    private readonly service: TripService,
    @Inject(forwardRef(() => TripHelperService))
    private helperService: TripHelperService,
    private tripEmitter: TripEmitter,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Post('order')
  async getOrderRequests(
    @Body('') getOrderRequestsDto: { trips: string[]; orderUUID: string },
    @User() user,
  ) {
    const tripsForOrder = await Promise.all(
      getOrderRequestsDto.trips.map(async (id) => {
        const trip = await this.service.findTripByUUID(id);

        return await this.helperService.buildTripForOrderDto(
          trip,
          getOrderRequestsDto.orderUUID,
        );
      }),
    );

    return tripsForOrder;
  }

  @UseGuards(JwtAuthGuard)
  @Post('oneway')
  async publishTrip(@Body() publishTripDto: PublishTripDto, @User() user) {
    await this.helperService.handlePublishTrip(publishTripDto, user);

    const userTrips = await this.service.getAllUserTrips(user.id);

    return Promise.all(
      userTrips.map((trip) => this.helperService.buildTripDto(trip)),
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('roundtrip')
  async publishRoundTrip(@Body('trips') trips: any, @User() user) {
    await Promise.all(
      trips.map((trip: PublishTripDto) =>
        this.helperService.handlePublishTrip(trip, user),
      ),
    );

    const userTrips = await this.service.getAllUserTrips(user.id);

    return Promise.all(
      userTrips.map((trip) => this.helperService.buildTripDto(trip)),
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async getAllTrips(@User() user) {
    const trips = await this.service.getAllUserTrips(user.id);

    return Promise.all(
      trips.map((order) => this.helperService.buildTripDto(order)),
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get('/filtered/many')
  async getManyFilteredByOrder(@User() user, @Query() query) {
    const trips = await this.service.getAllByOrderParams(
      user.id,
      query.deliveryDate,
    );

    const filteredTrips = await Promise.all(
      trips
        .map(async (trip) => {
          const from = await this.helperService.getLocationsDetails(
            trip.id as any,
            TripLocationType.FROM,
          );
          const to = await this.helperService.getLocationsDetails(
            trip.id as any,
            TripLocationType.TO,
          );

          if (from.city.id === query.from && to.city.id === query.to) {
            return trip;
          }
        })
        .filter((trip) => trip),
    );

    return Promise.all(
      filteredTrips.map(async (trip) =>
        this.helperService.buildTripForOrderDto(trip, query.uuid),
      ),
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get('/filtered/one')
  async getOneFilteredByOrder(@User() user, @Query() query) {
    const trip = await this.service.findTripByUUID(query.tripUUID);

    return this.helperService.buildTripForOrderDto(trip, query.orderUUID);
  }

  @UseGuards(JwtAuthGuard)
  @Put('delivery/request')
  async requestOrderDelivery(
    @Body('') requestOrderDeliveryDto: any,
    @User() user,
  ) {
    const trip = await this.service.findTripByUUID(
      requestOrderDeliveryDto.tripUUID,
    );

    if (
      !trip ||
      trip.outgoingRequests.includes(requestOrderDeliveryDto.orderUUID)
    ) {
      throw new ForbiddenException();
    }
    trip.outgoingRequests.push(requestOrderDeliveryDto.orderUUID);

    const updatedTrip = await this.service.updateTrip(trip.id, {
      outgoingRequests: trip.outgoingRequests,
    });

    const socketMessageData = await this.helperService.buildTripForOrderDto(
      updatedTrip,
      requestOrderDeliveryDto.orderUUID,
    );

    await this.tripEmitter.addOrderIncomingRequest({
      ...OrderDeliveryActionsRequestDto.buildDto(requestOrderDeliveryDto),
      trip: {
        uuid: trip.uuid,
        update: socketMessageData,
      },
      from: user.id,
    });

    return this.helperService.buildTripDto(updatedTrip);
  }

  @UseGuards(JwtAuthGuard)
  @Put('delivery/cancel')
  async cancelDeliveryOrderRequest(@Body('') requestOrderDeliveryDto: any) {
    const trip = await this.service.findTripByUUID(
      requestOrderDeliveryDto.tripUUID,
    );

    if (!trip) {
      throw new ForbiddenException();
    }

    function handleUpdateBody() {
      let update = {
        body: null,
        orderCancelAction: null,
      };

      const isInAccepted = trip.accepted.find(
        (id) => id === requestOrderDeliveryDto.orderUUID,
      );

      const isInIncomings = trip.incomingRequests.find(
        (id) => id === requestOrderDeliveryDto.orderUUID,
      );

      const isInOutgoings = trip.outgoingRequests.find(
        (id) => id === requestOrderDeliveryDto.orderUUID,
      );

      if (isInIncomings) {
        update.body = {
          incomingRequests: trip.incomingRequests.filter(
            (id) => id !== requestOrderDeliveryDto.orderUUID,
          ),
        };
        update.orderCancelAction = CancelEventType.OUTGOING;
      }

      if (isInOutgoings) {
        update.body = {
          outgoingRequests: trip.outgoingRequests.filter(
            (id) => id !== requestOrderDeliveryDto.orderUUID,
          ),
        };
        update.orderCancelAction = CancelEventType.INCOMING;
      }

      if (isInAccepted) {
        update.body = {
          accepted: trip.accepted.filter(
            (id) => id !== requestOrderDeliveryDto.orderUUID,
          ),
        };

        update.orderCancelAction = CancelEventType.ACCEPTED;
      }

      return update;
    }

    const update = handleUpdateBody();

    const updatedTrip = await this.service.updateTrip(trip.id, update.body);

    const socketMessageData = await this.helperService.buildTripForOrderDto(
      updatedTrip,
      requestOrderDeliveryDto.orderUUID,
    );

    await this.tripEmitter.cancelOrderDelivery(update.orderCancelAction, {
      ...OrderDeliveryActionsRequestDto.buildDto(requestOrderDeliveryDto),
      trip: {
        uuid: OrderDeliveryActionsRequestDto.buildDto(requestOrderDeliveryDto)
          .tripUUID,
        data: socketMessageData,
      },
    });

    return this.helperService.buildTripDto(updatedTrip);
  }

  @UseGuards(JwtAuthGuard)
  @Put('delivery/accept')
  async acceptDeliveryOrderRequest(
    @Body('') requestOrderDeliveryDto: any,
    @User() user,
  ) {
    const trip = await this.service.findTripByUUID(
      requestOrderDeliveryDto.tripUUID,
    );

    if (!trip || trip.accepted.includes(requestOrderDeliveryDto.orderUUID)) {
      throw new ForbiddenException();
    }
    const updatedTrip = await this.service.updateTrip(trip.id, {
      incomingRequests: trip.incomingRequests.filter(
        (id) => id !== requestOrderDeliveryDto.orderUUID,
      ),
      accepted: trip.accepted.concat([requestOrderDeliveryDto.orderUUID]),
    });

    const socketMessageData = await this.helperService.buildTripForOrderDto(
      updatedTrip,
      requestOrderDeliveryDto.orderUUID,
    );

    await this.tripEmitter.acceptOrderDelivery({
      ...requestOrderDeliveryDto,
      trip: { uuid: requestOrderDeliveryDto.tripUUID, data: socketMessageData },
      from: user.id,
    });

    return this.helperService.buildTripDto(updatedTrip);
  }

  @UseGuards(JwtAuthGuard)
  @Post('owner/details')
  async requestOwnerDetails(@Body('uuid') uuid: any, @User() user) {
    const trip = await this.service.findTripByUUID(uuid);

    const tripOwner = await this.tripEmitter.requestOwnerUUID(trip.owner);

    if (!tripOwner) {
      throw new NotFoundException();
    }

    return tripOwner.uuid;
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  async deleteTrip(@Body('uuid') orderUUID: string, @User() user) {
    const trip = await this.service.findTripByUUID(orderUUID);

    if (trip.accepted.length) {
      throw new ForbiddenException();
    }

    await this.helperService.handleDeleteTrip(trip.id as any);

    const trips = await this.service.getAllUserTrips(user.id);

    return Promise.all(
      trips.map((order) => this.helperService.buildTripDto(order)),
    );
  }
}
