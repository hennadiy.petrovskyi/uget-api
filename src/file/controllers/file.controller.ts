import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Res,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { FileService } from '../services';

import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { FileToBodyInterceptor, User } from 'src/common/decorators';

import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { AuthRecord } from 'src/auth/entities';
import { FileType } from '../enums';

@ApiBearerAuth()
@ApiTags('file')
@Controller('file')
export class FileController {
  constructor(private readonly service: FileService) {}

  @UseGuards(JwtAuthGuard)
  @Post('single')
  @UseInterceptors(FileInterceptor('file'), FileToBodyInterceptor)
  @ApiConsumes('multipart/form-data')
  public async createFile(
    @User() user: AuthRecord,
    @UploadedFile() file: Express.Multer.File,
    @Body() createFileDto: any,
  ): Promise<any> {
    const ownerDto = { owner: user.uuid, type: createFileDto.type };

    if (createFileDto.type === FileType.PROFILE_IMAGE) {
      const existingUserProfileImage =
        await this.service.findFileByOwnerAndType(ownerDto);

      if (existingUserProfileImage) {
        await this.service.deleteFileByName(existingUserProfileImage.fileName);
      }

      await this.service.upload(ownerDto, file);
    }

    return { status: 'ok' };
  }

  @UseGuards(JwtAuthGuard)
  @Post('multiple')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('files'))
  public async saveMultipleFiles(
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Body() body: any,
  ): Promise<any> {
    console.log(body, files);

    const filesDetailsInDb = await Promise.all(
      files.map(async (file) => {
        return await this.service.upload(
          { owner: body.owner, type: body.type },
          file,
        );
      }),
    );

    const urls = await Promise.all(
      filesDetailsInDb.map(async ({ type, owner, fileName }) => {
        const url = await this.service.getFileLink({ type, owner, fileName });
        return { id: fileName, url };
      }),
    );

    console.log(urls);

    return { status: 'ok', photos: urls };
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  public async deleteFile(@Res() res, @Body('fileName') fileName: string) {
    await this.service.deleteFileByName(fileName);

    return res.json({
      fileName,
      text: `${fileName} was deleted from s3 andå db`,
    });
  }
}
