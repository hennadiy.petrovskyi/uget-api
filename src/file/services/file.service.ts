import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
// import { S3 } from 'aws-sdk';
import { S3Client } from '@aws-sdk/client-s3';
import { v4 } from 'uuid';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';

import { LoggerService } from 'src/utils/logger';
import { File } from '../entities';
import { GetFileByOwnerAndTypeDto, GetFileDto } from '../dtos';
import { FileType } from '../enums';

@Injectable()
export class FileService {
  private readonly s3;

  constructor(
    private configService: ConfigService,
    @InjectRepository(File)
    private fileRepository: Repository<File>,
    private httpService: HttpService,
    private logger: LoggerService,
  ) {
    this.s3 = new S3Client({
      credentials: {
        accessKeyId: configService.get<string>('AWS_ACCESS_KEY_ID'),
        secretAccessKey: configService.get<string>('AWS_SECRET_ACCESS_KEY'),
      },
    });
  }

  public async findOwnerAllFiles(owner: string) {
    const functionName = this.findOwnerAllFiles.name;

    try {
      return this.fileRepository.find({ where: { owner } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  private async findFileDetailsInDb(fileName: string): Promise<any> {
    const functionName = this.findFileDetailsInDb.name;

    try {
      return await this.fileRepository.findOne({ where: { fileName } });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async buildFileDto(file: File) {
    const functionName = this.buildFileDto.name;
    try {
      const url = await this.getFileLink(GetFileDto.buildDto(file));

      if (!url) {
        return;
      }

      return { id: file.fileName, url };
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async findFileByOwnerAndType(
    getFileByOwnerAndTypeDto: GetFileByOwnerAndTypeDto,
  ): Promise<any> {
    const functionName = this.findFileDetailsInDb.name;

    try {
      return this.fileRepository.findOne({
        where: {
          owner: getFileByOwnerAndTypeDto.owner,
          type: getFileByOwnerAndTypeDto.type,
        },
      });
    } catch (error) {
      this.logger.logError(functionName, error);
    }
  }

  public async saveFileDetailsInDb(fileDetailsDto: any): Promise<any> {
    const functionName = this.saveFileDetailsInDb.name;

    const { fileName, type, owner } = fileDetailsDto;

    const file = new File();

    file.fileName = fileName;
    file.type = type;
    file.owner = owner;

    try {
      await this.fileRepository.save(file);

      return await this.findFileDetailsInDb(fileDetailsDto.fileName);
    } catch (error) {
      this.logger.logError(functionName, error, fileDetailsDto);
    }
  }

  public async uploadFileToS3(
    {
      buffer,
      contentType,
      size,
    }: { buffer: Buffer; contentType: string; size: number },
    urlKey: string,
  ): Promise<string> {
    const fileName = v4();

    const params = {
      Body: buffer,
      Bucket: this.configService.get<string>('AWS_S3_BUCKET_NAME'),
      Key: `${urlKey}/${fileName}`,
      ContentType: contentType,
      ContentLength: size,
    };
    try {
      await this.s3.putObject(params).promise();
    } catch (err) {
      throw new InternalServerErrorException();
    }

    return fileName;
  }

  public async getFileLink({ type, owner, fileName }): Promise<any> {
    const params = {
      Bucket: this.configService.get<string>('AWS_S3_BUCKET_NAME'),
      Key: `${type}/${owner}/${fileName}`,
      Expires: 3600,
    };

    try {
      return await this.s3.getSignedUrl('getObject', params);
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }

  public async upload(
    { owner, type }: { owner: string; type: FileType },
    file: Express.Multer.File,
  ) {
    const { buffer, mimetype, size } = file;

    const urlKey = `${type}/${owner}`;

    const fileName = await this.uploadFileToS3(
      { buffer, contentType: mimetype, size },
      urlKey,
    );

    const params = {
      fileName,
      type,
      owner,
    };

    const fileDetails = await this.saveFileDetailsInDb(params);

    return fileDetails;
  }

  public async deleteFileByName(fileName: string) {
    const file = await this.findFileDetailsInDb(fileName);

    if (!file) {
      throw new NotFoundException();
    }

    const params = {
      Bucket: this.configService.get<string>('AWS_S3_BUCKET_NAME'),
      Key: `${file.type}/${file.owner}/${file.fileName}`,
    };

    try {
      await this.s3.deleteObject(params, async (err, data) => {
        if (err === null) {
          await this.fileRepository.delete({ fileName });
        }
      });

      return true;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  public async deleteAllOwnerFiles(owner: string) {
    const files = await this.findOwnerAllFiles(owner);

    Promise.all(files.map((file) => this.deleteFileByName(file.fileName)));
  }
}
