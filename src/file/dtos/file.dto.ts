import { ApiProperty, PickType } from '@nestjs/swagger';
import { Expose, plainToClass } from 'class-transformer';
import {} from '../enums';
import { File } from '../entities';

export class GetFileByOwnerAndTypeDto extends PickType(File, [
  'owner',
  'type',
]) {}

export class GetFileDto extends PickType(File, ['fileName', 'type', 'owner']) {
  static buildDto(payload: File): GetFileDto {
    return plainToClass(GetFileDto, payload, {
      excludeExtraneousValues: true,
    });
  }
}

export class CreateFileDto {
  @Expose()
  @ApiProperty()
  file: Express.Multer.File;

  @Expose()
  @ApiProperty()
  type: any;

  @Expose()
  @ApiProperty()
  uuid: any;
}

export class GetLinkDto {
  @Expose()
  @ApiProperty()
  id: string;

  @Expose()
  @ApiProperty()
  url: string;
}
