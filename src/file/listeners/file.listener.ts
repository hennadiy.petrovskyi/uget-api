import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';

import { FILE_EVENTS } from 'src/common/events';

import { FileService } from '../services';
import { GetFileByOwnerAndTypeDto } from '../dtos';

@Injectable()
export class FileListener {
  constructor(private readonly fileService: FileService) {}

  @OnEvent(FILE_EVENTS.GET_FILES, { promisify: true })
  public async getAllOwnerFiles(owner: string) {
    const files = await this.fileService.findOwnerAllFiles(owner);

    if (files && files.length) {
      return Promise.all(
        files.map((file) => this.fileService.buildFileDto(file)),
      );
    }
  }

  @OnEvent(FILE_EVENTS.GET_FILE_BY_TYPE, { promisify: true })
  public async getSingleFileByOwnerAndType(
    getFileByOwnerAndTypeDto: GetFileByOwnerAndTypeDto,
  ) {
    const file = await this.fileService.findFileByOwnerAndType(
      getFileByOwnerAndTypeDto,
    );

    if (file) {
      return await this.fileService.buildFileDto(file);
    }
  }

  @OnEvent(FILE_EVENTS.DELETE_OWNER_FILES, { promisify: true })
  public async deleteAllOwnerPhotos(owner: string) {
    await this.fileService.deleteAllOwnerFiles(owner);
  }
}
