import { Column, Entity, Index, Unique } from 'typeorm';

import { FileType, Provider } from '../enums';
import { Expose } from 'class-transformer';
import { BaseEntity } from 'src/common/entities';

@Entity({
  name: 'file',
})
@Unique(['fileName'])
export class File extends BaseEntity {
  @Index()
  @Expose()
  @Column('text', { unique: true, nullable: false })
  fileName: string;

  @Expose()
  @Column('enum', { enum: FileType })
  type: FileType;

  @Expose()
  @Column('text', { nullable: false })
  owner: string;

  @Expose()
  @Column('enum', { enum: Provider, default: Provider.AWS })
  provider: Provider;
}
