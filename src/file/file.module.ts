import { Module } from '@nestjs/common';
import { FileService } from './services';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import { LoggerModule } from 'src/utils/logger';
import { FileController } from './controllers';
import { File } from './entities';
import { FileListener } from './listeners';

@Module({
  imports: [TypeOrmModule.forFeature([File]), HttpModule, LoggerModule],
  providers: [FileService, FileListener],
  controllers: [FileController],
  exports: [FileService],
})
export class FileModule {
  constructor() {}
}
