import { Controller, Get, Param, Res } from '@nestjs/common';

@Controller('redirect')
export class RedirectDeepLinkController {
  constructor() {}

  @Get('resend/email/:id')
  verifyEmail(@Param('id') id: string, @Res() res) {
    const link = `uget://auth/verification/email/${id}`;

    return res.redirect(link);
  }

  @Get('reset-password/:id')
  resetPassword(@Param('id') id: string, @Res() res) {
    const link = `uget://auth/change/password/${id}`;

    return res.redirect(link);
  }
}
